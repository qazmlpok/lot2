00F73B34: Call Underling
00F73BA4: Phantom's Coldness
00F73C14: Storm of Blue Rain
00F73C84: Storm of Yellow Drive
00F73CF4: Paralyzing Fog
00F73D5C: Blue Curse
00F73E04: Yellow Curse
00F7449C: Counter-Magic
00F74970: Calm Down
00F749E0: Fly Up
00F75A04: Karmic Gnashing -Mystic-
00F75AEC: Graces of Tama no Oya no Mikoto
00F75B70: Oracle of Eternal Heaven and Earth
00F76278: Graces of Fruits of Darkness
00F766F0: Protecting Light from the Heavens
00F76870: Staring Eye from the Abyss
00F76CD8: Summon Elemental Crystals
00F76E60: Enforcer's Will
00F771EC: Flustered
00F8FAA4: Slash Dive
00F932F4: Flying Press
00FA1434: Perpetual Insanity
00FA172C: "C" Filling the World
00FA1E0C: Empty Mouth that Swallows Sinners
00FA235C: Boulder
00FA2804: World-Controlling Stone's Power
00FA2E54: Lady Eiki's Scolding
00FD3E40: Octangle Attack
00FDCDDC: True Ame-no-Murakumo's Slash
00FDEB64: All-Regeneration
00FDEDE0: Maximum Dish Charge
00FF7358: "Yamata-no-Orochi's soul is trembling!"
00FFA9B8: Overflowing Insanity: Inflicts Ailments on Self
00FFA9F4: Uncontrollable Urges: Inflicts Debuffs on Self
0105C8C0: Night of Bright Guest Stars
0105EC98: Attack
0105ECBC: Magic Attack
0105ECD4: Counter-Attack
0105ECFC: Wrath of Edokko God of Death
0105ED14: Wait
0105ED2C: Concentrate
0105ED54: Escape
0105ED80: Nut Throw
0105EDB8: Photosynthesis
0105EDD4: Poison Needle
0105EE00: Harden
0105EE24: Green Arrow
0105EE64: Storm of Wood Leaves
0105EEB8: Umbrella Spin
0105EF18: Paralyze
0105EF48: Red Arrow
0105EF70: Azure Arrow
0105EFA4: Thunder Arrow
0105EFE0: Magic Arrow
0105F00C: Light Arrow
0105F060: Dark Arrow
0105F074: Blade Arrow
0105F0D4: Spark Storm
0105F108: Flowing Hellfire
0105F184: Wild Dance of Freezing Mist
0105F24C: Storm of Purple Magic
0105F270: Storm of Light Particle
0105F2B4: Storm of Dark Flow
0105F300: Storm of Cutting Knives
0105F354: Great Roar
0105F370: Rainbow Wing
0105F39C: Horizontal Slice
0105F3C4: Row Attack
0105F3E8: Recover
0105F410: Ultravenomous Needles
0105F434: Venomous Fog
0105F450: Shadowstep
0105F464: Detonation
0105F48C: Dissolvent
0105F4C8: Sword Stance
0105F4EC: Establish Stance
0105F508: Poison
0105F524: Silent Fog
0105F578: Daze
0105F5AC: Earth Shaker
0105F5D4: Earthquake
0105F5E0: Ultraparalysis Needles
0105F608: Bite
0105F648: Swallow
0105F674: Acid Rain
0105F6BC: Leg Sweep
0105F744: Melt Shell
0105F764: Angelic Light
0105F78C: Flamethrower
0105F7B4: Blizzard
0105F7DC: Razor Wind
0105F804: Shining Arrows from the Sky
0105F844: Leaf Cutting Dance
0105F878: Magical Blast
0105F8A0: Ether Flare
0105F8BC: Red Curse
0105F908: Purple Curse
0105F930: Green Curse
0105F980: Dazing Fog
0105F990: Heavy Fog
0105F9A0: Madness Fog
0105F9B4: Calming Scent
0105F9C8: Poison Spore
0105F9F8: Paralysis Spore
0105FA18: Allure of Death
0105FA40: Scale Powder
0105FA68: Whirlwind
0105FAA0: Deranging Aroma
0105FAD0: Grass Knot
0105FB08: Slack Off
0105FB1C: Divider
0105FB48: Burrow
0105FB64: 1000 Needles
0105FBBC: Sandstorm
0105FBC4: Ancient Curse
0105FC4C: Divine Exile
0105FC74: Divine Messenger's Light
0105FCA4: Heat Gaze
0105FCC4: Rock Clash
0105FCF4: Magic Drain
0105FD1C: Magical Light
0105FD44: Piercing Light
0105FD5C: Charm
0105FD6C: Lava Flow
0105FD88: Blazing Light Shot
0105FDA4: Crimson Lotus Fang
0105FDC4: Karmic Evil Fang -Mystic-
0105FE1C: Karmic Evil Fang -Dark-
0105FE60: Karmic Gnashing -Dark-
0105FE7C: Karmic Evil Fang -Spirit-
0105FEB8: Karmic Gnashing -Spirit-
0105FED4: Karmic Thunderous Gnashing -Spirit-
0105FEE4: Void Flash Claw
0105FF10: Digest
0105FF1C: Dividing Slash
0105FF38: Gravity Manipulation
0105FF68: Black Universe
0105FF98: Half Moon Slash
0105FFC0: Blood Drain
0105FFDC: Dangerous Scent
0105FFF0: Terrifying Wave
01060020: Terror Eater
01060054: Flux of Yomotsu Hirasaka
0106007C: Devil's Crimes
010600B4: Destroy Magic
010600DC: Oracle of Sacred Mirror Dedication
01060128: Second Coming of the Divine Sword
0106013C: Yakumo's Futsu no Mitama no Tsurugi
01060160: Great Tree that Descended from the Sky
010601E0: World-Devouring Calamity
010601F4: World-Devouring Destruction
0106021C: Spirit Decomposition
01060254: Ame-no-Murakumo's Wild Dance
01060284: Tentacle Breed
01060294: Summon Shikigami
0106033C: Advent of the Divine Child of God
01060350: Godly Scarlet Gold Slash
01060368: Resummon Doll
01060384: Azure Shining Light
01060398: Earth Element's Protective Light
010603A8: Azure Shining Body
010603B8: Rasetsu Fist
01060400: Golden Advent
01060428: Summon Shikigami + Shikigami
01060438: Golden Protection
01060448: Healing Power
01060454: Strengthen Sorcery
0106046C: Scythe of Calamity
0106049C: Greatsword of Calamity
01060504: Great Catastrophe
01060514: Great Destruction
01060534: "C" Falling from the Sky
01060554: Space Compression
01060590: Time-Space Warp
010605DC: Dark Star
0106061C: Shredder
01060650: Corona
0106065C: Ice Rock
0106066C: Petal Blast
01060680: Electroshock
01060694: Gravity Accel
010606B0: Graviton Wall
010606D4: Instant Death Attack
010606EC: Boss Rush
0106070C: Double Slash
0106072C: Lance of Malice
0106075C: Pummel
010607E0: Silencing Attack
010607F0: Holy Prayer
010607FC: Solar Laser
0106083C: Defensive Barrier
01060848: Great Whirlwind
01060868: Activate Life Energy
01060874: Dark Flux
01060880: Half Moon Mirror
010608C8: Refresh
010608DC: - Manifestation of the Evil Dragon-
01060920: - Soul Transfer: DRK -
01060964: - Soul Transfer: SPI -
010609A8: Hand of Terror
010609D0: Roar Shockwave
010609F0: Destroy Armor
01060A0C: Karmic Retribution
01060A3C: Guard Stance
01060A4C: Peerless Chaotic Dance
01060A60: Were-Hakutaku's Rage
01060A74: Loyal? Gardener's Support
01060A88: Sutra - Birth of a Superhuman
01060A9C: Bishamonten Tag
01060B00: True Adminstrator - Rivalry of Good & Evil
01060B38: True Adminstrator - Compatibility of Good and Evil
01060B84: Yamata-no-Orochi's Evil Eye
01060BA8: Shadow Team (9)
01060BD8: Slacking Off
01060BF8: Pretending to Attack
01060C0C: Astronomic Observation
01060C90: Hakkero Charge
01060CC4: HP Giga Boost
01060CFC: Summon Vengeful Strength
01060D1C: Evil Dragon's Eyebeam
01060D2C: Extra-Large Entwining
01060D6C: Divine Spirit-Burning Hellfire
01060D8C: Evil Scarlet Gold Slash
01060E1C: Countdown to Ragnarok



00F73E7C: Atom Decomposition
00F73EEC: Nature's Wrath
00F73F54: Ignis Fireball
00F73FBC: Nightfire Gem
00F74024: Grand Heat Slash
00F74094: Gaea Rage
00F746E8: Summon Kunitsukami
00F74750: Summon Mother Goddess
00F747B8: Shippu Jinrai
00F74BA4: Summon Herculean Power
00F74BEC: Power of Truth
00F74C3C: Godly Wave
00F775E0: Green Acid Breath
00F77648: Ignis Explosion
00F77710: Absolute Zero Ice Crystal
00FA1074: Ura Dragon - Grand Summon
00FA47FC: The Word for World is Forest
00FA4944: Javelin Rain
00FE1078: Awakaned Dark Side of the Moon
00FE540C: Dragon God's body color returned to normal...
00FE54D8: The void eyes are resting...
00FE57C4: Dragon God's body color has changed!
00FE5CC4: Green Brute has drawn his demon sword!
00FE7208: Body is regenerating at abonrmal pace!
00FE7588: Red Hero has entered Boost Mode!
00FE773C: Shredding Entity's eyes are focused!
00FE86B8: Aria-singing Entity's eyes are focused!
00FE8704: Staring Entity's eyes are focused!
00FFAD30: Nitori exchanged her burnt barrel!
00FFBDB0: Green Brute's level has raised infinitely!
00FFD2C4: Red Hero has entered Double Boost Mode!
00FFD6B8: King's parameters have risen above the limit!
010099EC: Dark Lord of Woe reachs for his own crown!
01009B94: Dark Lord of Woe groans from his old wounds!
0100AA54: ***WINNER*** opened spellbook of "Life"!
0100B0FC: ***WINNER*** opened spellbook of "Nature"!
0100CA14: ***WINNER*** opened spellbook of "Sorcery"!
0100D978: ***WINNER*** opened spellbook of "Death"!
0100E008: ***WINNER*** opened spellbook of "Crusade"!
0100E1BC: ***WINNER*** opened spellbook of "Chaos"!
0100F22C: King begin focussing a terrifying force!
010188B8: Dark Lord of Woe reachs for a terrifying scepter!
01019D6C: Dark Lord of Woe starts chanting for a strong spell!
0101A1D0: Nitori reloads for Super Scope!
0101B054: ***WINNER*** begins charging a strong spell!
01021638: Green Brute levels up in battle and becomes stronger!
0105D7B4: Shikigami "Ran Yakumo+"
0105F154: Hellfire Breath
0105F16C: Storm of Flames
0105F1C4: Cocytus Breath
0105F1DC: Storm of Ice
0105F2A8: Call Light
0105F404: True Healing
0105F6A4: Bloody Rain
010602A8: Ura Dragon - Summon
0106040C: Summon Abyss Dragon and Tiger
010604C4: True Greatsword of Calamity
01060578: Decomposition Breath
0106059C: Teleport Evil
01060634: Magic Erase
01060A1C: Eye for an Eye
01060C24: Eternal Summon Herculean Power
01060C5C: Eternal Summon Super Power
01060DCC: Awakened Moonlight Ray
01060E3C: Shatter Attack
01060E5C: Brain Attack
01060E7C: Mana Stealing Hand
01060E9C: Hell Sphere
01060F38: Unmarred Light of the Two Trees
01060F68: Hell Fire
01061020: Meteor Strike
01061160: Day of the Dove
01061180: True Resistance
01061198: Lightning Bolt
010611F4: Ray of Sunlight
01061210: Flesh to Stone
01061220: Lightning Tempest
01061290: Slow Monster
010612B0: Haste Self
010612C8: Globe of Invulnerability
010612E0: Malediction
010612F8: Stinking Cloud
01061310: Word of Death
01061328: Mass Genocide
0106134C: Hellfire
010613C0: Purify Body
010613F4: Restoration
01061428: Cure Wounds+Poison
0106145C: Holy Orb
01061490: Starburst
010614C8: Divine Wrath
01061538: Magic Missile
01061568: Doom Bolt
01061588: Summon Chaos
01061670: Form Change - Fire
01061690: Form Change - Cold
010616C0: Form Change - Nature
010616DC: Form Change - Wind
01061760: Dragon Whirlwind
01061770: Dragon Strength
010617A0: Lightning Breath
010617E8: Strong Down Thrust
01061814: Infuse Raval Ore
01061840: Brocia Serum
0106186C: Brave Sword
010618AC: Ventus Whirlwind
010618C4: Amulet
010618DC: Ventus Tornado
010618EC: Terra Bodyslam
01061904: Illusion Mirror
0106191C: Terra Shockwave
01061934: Red Hero made a stance with his sword!
01061950: Red Hero changed to Ignis Bracelet!
01061994: Red Hero changed to Ventus Bracelet!
010619BC: Red Hero changed to Terra Bracelet!
010619EC: Mercy Attack
01061A14: Assault
01061A44: Train Slash
01061A58: Gird Stance
01061A78: One Shot Wonder
01061A98: Verbal Abuse
01061AF8: Kichiku Attack
01061BEC: Soul-searing Sunlight
01061CA4: Ice Crash
01061CBC: Form Change - Terminate
01061CEC: Calamitous Rainbow Wind
01061D74: Last Stand
01061D9C: Zero Shock
01061DC8: Freikugel
01061DFC: Summon Hero
01061E1C: Summon Elephant
01061E3C: Summon Buddy
01061E60: Summon Queen
01061E88: Ultimate Life
01061EA8: Demon Rave
01061EC8: Taunt
01061EE8: Dekunda
01061EF4: Dekaja
01061F00: Grand Curse
01061F24: Astral Burst
01061F50: Gate of Hell
01061F78: Power Draining Circle
01062048: Magic Draining Circle
010620AC: Speed Draining Circle
