00FBC694: Treasure Chest Key
00FBC6A0: A key occasionally found within the Great Tree, designed to open chests 
00FBC6E8: that require keys. Note that it disappears upon use.
00FBC708: Stone of Awakening
00FBC714: A stone of miracles that awakens new abilities within the subconscious.
00FBC760: Bring it to the Great Library to teach a character a new subclass.
00FBC7D0: Money-Growing Tree
00FBC7E0: Increases money gained after battle by 1% just by being carried.
00FBC82C: The effect will stack up to 25% increase.
00FBC860: Mr. Midnight Oil Set
00FBC880: Increases experience gained after battle by 1% just by being carried.
00FBC8D0: Items Discovery Weekly
00FBC8E8: Increases item drop rate by 1.6% just by being carried.
00FBC940: The effect will stack up to 40% increase.
00FBC974: Jewel of Greater Awakening
00FBC984: A jewel of miracles that awakens new abilities within the subconscious.
00FBC9D0: Learning subclasses will no longer consume Stones of Awakening.
00FBCA08: Tome of Reincarnation
00FBCA18: This item is used when resetting a character's skills. It will reset the
00FBCA50: parameter levels of that character, and returns all money used. If base 
00FBCAA0: value increasing items, Training Manuals, and Tomes of Insight were
00FBCAF0: used on the character, those items will be returned as well.
00FBCB1C: Training Manual
00FBCB2C: A consumable item that increases a character's skill points by 1 when used.
00FBCB74: You can use it at the Hakurei Shrine.
00FBCB9C: Life Gem
00FBCBA8: An item that increases a character's HP base value slightly when used.
00FBCBEC: Up to 10 may be used per character.
00FBCC1C: Magic Gem
00FBCC28: An item that increases a character's maximum MP when used.
00FBCC6C: Energy Gem
00FBCC78: An item that increases a character's maximum TP when used.
00FBCCBC: Fighting Gem
00FBCCC8: An item that increases a character's ATK base value slightly when used.
00FBCD0C: Protection Gem
00FBCD18: An item that increases a character's DEF base value slightly when used.
00FBCD5C: Wisdom Gem
00FBCD68: An item that increases a character's MAG base value slightly when used.
00FBCDAC: Affection Gem
00FBCDB8: An item that increases a character's MND base value slightly when used.
00FBCDFC: Swiftness Gem
00FBCE08: An item that increases a character's SPD base value slightly when used.
00FBCE4C: Hakurouken
00FBCE54: A blade that belongs to Youmu Konpaku, the half-ghost swordmaster and 
00FBCE94: gardener of Hakugyokurou. Circumstances led to Cirno stealing it.
00FBCEC0: Kogasa's Umbrella (Broken)
00FBCED8: The umbrella said to be the true form of the umbrella youkai Kogasa Tatara.
00FBCF08: Its shaft is badly damaged, so it can't really be used as an umbrella.
00FBCF58: Kogasa's Umbrella (Fixed)
00FBCF78: The umbrella's shaft has been fixed. It'll defend you from any rainstorm.
00FBCFC8: The design is obviously untouched, though, so it still gives off the mood
00FBCFF0: of an unpopular umbrella.
00FBD038: Lamprey Serpent's Meat
00FBD050: Meat obtained from defeating the Lamprey Serpent
00FBD088: FOE. It looks a lot like lamprey eel meat.
00FBD0C4: Therefore, it is lamprey eel meat. Probably.
00FBD0F0: Mysterious Scrap of Iron - 1
00FBD104: A scrap of iron Marisa picked up on a whim.
00FBD144: It looks like some sort of narrow and long iron rod.
00FBD164: Mysterious Scrap of Iron - 2
00FBD178: It looks like some sort of curved steel wire.
00FBD19C: Mysterious Scrap of Iron - 3
00FBD1B0: It looks like some sort of black wire bent into a looping shape.
00FBD1E0: Gland of Super Hemotoxin
00FBD1F4: Poisonous gland extracted from some poisonous-looking insect.
00FBD22C: Reason #1 of Wriggle's paralysis. Since even Wriggle was affected like 
00FBD258: this, we can assume that direct exposure to this is extremely
00FBD280: dangerous to both humans and Youkai.
00FBD2B8: Gland of Super Neurotoxin
00FBD2CC: Poisonous gland extract from some evil-looking insect.
00FBD304: Reason #2 of Wriggle's paralysis.
00FBD330: It is said to be stronger than even Tetrodotoxin.
00FBD374: Great Tree Insect Antidote
00FBD38C: A super-antidote that is mixed by a super skilled medic, and created by
00FBD3B8: analyzing super hemotoxin and super neurotoxin. Super-effective on super 
00FBD410: strong poison. By the way, it's poisonous to drink if you're not poisoned.
00FBD44C: Tome of Insight - Vitality
00FBD460: A strange book that enlightens its reader to a new level.
00FBD4BC: Used to teach HP Boost to a character.
00FBD4FC: Tome of Insight - Sense
00FBD510: Used to teach MP Boost to a character.
00FBD550: Tome of Insight - Energy
00FBD564: Used to teach TP Boost to a character.
00FBD5A4: Tome of Insight - Attack
00FBD5B8: Used to teach ATK Boost to a character.
00FBD5F8: Tome of Insight - Defense
00FBD60C: Used to teach DEF Boost to a character.
00FBD64C: Tome of Insight - Magic
00FBD660: Used to teach MAG Boost to a character.
00FBD6A0: Tome of Insight - Mind
00FBD6B4: Used to teach MND Boost to a character.
00FBD6F4: Tome of Insight - Speed
00FBD708: Used to teach SPD Boost to a character.
00FBD748: Tome of Insight - Evasion
00FBD75C: Used to teach EVA Boost to a character.
00FBD79C: Tome of Insight - Accuracy
00FBD7B0: Used to teach ACC Boost to a character.
00FBD7F0: Tome of Insight - Affinity
00FBD804: Used to teach Affinity Boost to a character.
00FBD844: Tome of Insight - Resist
00FBD858: Used to teach Resistance Boost to a character.
00FBD898: Seaweed of Serenity
00FBD8A8: A naturally grown seaweed from the mysterious sea level in the middle of 
00FBD8EC: the Great Tree. If boiled, it can calm one's spirit when imbibed.
00FBD930: Coral of Silence
00FBD940: A naturally grown coral from the mysterious sea level in the middle of 
00FBD984: the Great Tree. If made into a pill, it can dull one's senses when imbibed.
00FBD9CC: Patchy's Philosopher's Stone... that seemed to have gained auto-movement 
00FBDA10: somehow. They snuck out into the Great Tree without Patchy noticing,
00FBDA48: until they got beaten into submission by Reimu's party.
00FBDA74: Byakuren's Scroll #1
00FBDA88: A rainbow glyph floating around Byakuren. Called a Sorcerer's Sutra Scroll. 
00FBDADC: Scroll. Buried in the sand, but wasn't damaged since it's a scroll of air.
00FBDB1C: Byakuren's Scroll #2
00FBDB30: It has a lot of features, but automatic recollection when lost isn't one.
00FBDB7C: Byakuren's Scroll #3
00FBDB90: Left inside the hellfire, but having no substance meant it's undamaged.
00FBDBE8: Byakuren's Scroll #4
00FBDC00: Monsters were carrying it, but no one can use it other than Byakuren.
00FBDC60: If you were to take it, its best use would be as a shining flag.
00FBDCB4: Platycodon Seal
00FBDCC4: A mysterious seal obtained from defeating an enemy on 14F.
00FBDCF4: You can't enter certain areas without it.
00FBDD24: Iris Seal
00FBDD34: A mysterious seal obtained from defeating an enemy on 15F.
00FBDD64: Dianthus Seal
00FBDD74: A mysterious seal obtained from defeating an enemy on 16F.
00FBDDA4: Mikagami Seal
00FBDDB4: A mysterious seal obtained from defeating an enemy on 20F.
00FBDDE4: Ara-Mitama Seal
00FBDDF4: Magician's Proof
00FBDE08: A book that records the methods to effectively make use of mana.
00FBDE3C: Unlocks the subclass "Magician."
00FBDE74: Mana is something you can now just throw away.
00FBDE90: Herbalist's Proof
00FBDEA4: A book that records all the medicinal herbs throughout the world's history.
00FBDED8: Unlocks the subclass "Herbalist."
00FBDF10: Eventually you can use a Stand to open up your own Italian restaurant too.
00FBDF5C: Strategist's Proof
00FBDF6C: A book that records the basic knowledge of tactics and strategies.
00FBDFA8: Unlocks the subclass "Strategist."
00FBDFDC: Specially made for those who want to be strategists without working for it.
00FBE020: Gambler's Proof
00FBE034: A book that records the burning hot spirit of the gambler.
00FBE080: Unlocks the subclass "Gambler."
00FBE0B8: You may start your journey by collecting emblems from expensive cars.
00FBE104: Diva's Proof
00FBE114: A book that records all charming songs and dances.
00FBE14C: Unlocks the subclass "Diva."
00FBE180: You're going to need a lot of training.
00FBE198: Transcendent's Proof
00FBE1AC: A book that records ways of effective training and diet.
00FBE1F4: Unlocks the subclass "Transcendent."
00FBE22C: Danmaku is all about power. Everything else is all about power too.
00FBE25C: Dark Fragment
00FBE270: A fragment of the power of darkness dropped by the fakes of Gensokyo's
00FBE2C8: girls. Rinnosuke's diagnosis calls it a "piece of scarlet gold tainted by
00FBE318: darkness." Occasionally reduces TP loss after a character is defeated in
00FBE370: battle. The reduction rate is based on the amount of Dark Fragments held.
00FBE3C0: Infinity Gem
00FBE3D0: A dull-colored gem with a tinge of rainbow light peeking out, found in the
00FBE428: Infinity Corridor. A dangerous item with the power of malice leaking from
00FBE484: it, but someone seems to want them. Gather loads and trade them for items.
00FBE4B0: Weapon Recipe Set 1
00FBE4D0: A conveniently-arranged set of recipes detailing how to make all types of
00FBE52C: equipment. You can create more equipment with this. Nitori will be pleased.
00FBE564: Weapon Recipe Set 2
00FBE580: The second conveniently-arranged set of recipes detailing how to make all
00FBE5E4: types of equipment. You can create even more equipment with this.
00FBE610: Weapon Recipe Set 3
00FBE630: The third conveniently-arranged set of recipes detailing how to make all
00FBE694: types of equipment. You can create far more equipment with this.
00FBE6CC: Weapon Recipe Set 4
00FBE6E8: The fourth conveniently-arranged set of recipes detailing how to make all
00FBE750: types of equipment. You can create extremely strong equipment with this.
00FBE7AC: Life Jewel
00FBE7C0: You can use a maximum of 10 per character at the Hakurei Shrine.
00FBE818: Only characters who have already maxed on "Life Gems" can use it.
00FBE870: Magic Jewel
00FBE880: An item that increases a character's maximum MP when used.
00FBE8C0: Only characters who have already maxed on "Magic Gems" can use it.
00FBE918: Energy Jewel
00FBE928: An item that increases a character's maximum TP when used.
00FBE968: Only characters who have already maxed on "Energy Gems" can use it.
00FBE9C0: Fighting Jewel
00FBE9D0: Only characters who have already maxed on "Fighting Gems" can use it.
00FBEA28: Protection Jewel
00FBEA38: Only characters who have already maxed on "Protection Gems" can use it.
00FBEA90: Wisdom Jewel
00FBEAA0: Only characters who have already maxed on "Wisdom Gems" can use it.
00FBEAF8: Affection Jewel
00FBEB08: Only characters who have already maxed on "Affection Gems" can use it.
00FBEB60: Swiftness Jewel
00FBEB70: Only characters who have already maxed on "Swiftness Gems" can use it.
00FBEBC8: Moneybag-Growing Tree
00FBEBE0: The effect of this item stacks with "Money-Growing Trees."
00FBEC18: Mr. Dawnbreak Oil Set
00FBEC38: The effect of this item stacks with "Mr. Midnight Oil Sets."
00FBEC7C: Explorer King's Legend
00FBEC90: The effect of this item stacks with "Item Discovery Weeklys."
00FBECD0: Great Elemental Barrier
00FBECE4: The power of the Great Elemental Barrier erected around the Great Tree with
00FBED24: the help of Ame-no-Murakumo. Allows learning of the new Subclass "Ame no"
00FBED64: Murakumo's Blessing." Only 1 character can have this subclass at a time.
00FBEDA0: Spartan's Tome - Vitality
00FBEDB4: A suspicious tome addressed to those who want to optimize their training.
00FBEE00: Only characters who have maxed the "HP Boost" skill can use it.
00FBEE58: The item itself overwrites "HP Boost" with "HP Boost 2."
00FBEEB4: Spartan's Tome - Sense
00FBEEC8: Only characters who have maxed the "MP Boost" skill can use it.
00FBEF20: The item itself overwrites "MP Boost" with "MP Boost 2."
00FBEF7C: Spartan's Tome - Energy
00FBEF90: Only characters who have maxed the "TP Boost" skill can use it.
00FBEFE8: The item itself overwrites "TP Boost" with "TP Boost 2."
00FBF044: Spartan's Tome - Attack
00FBF058: Only characters who have maxed the "ATK Boost" skill can use it.
00FBF0B0: The item itself overwrites "ATK Boost" with "ATK Boost 2."
00FBF10C: Spartan's Tome - Defense
00FBF120: Only characters who have maxed the "DEF Boost" skill can use it.
00FBF178: The item itself overwrites "DEF Boost" with "DEF Boost 2."
00FBF1D4: Spartan's Tome - Magic
00FBF1E8: Only characters who have maxed the "MAG Boost" skill can use it.
00FBF240: The item itself overwrites "MAG Boost" with "MAG Boost 2."
00FBF29C: Spartan's Tome - Mind
00FBF2B0: Only characters who have maxed the "MND Boost" skill can use it.
00FBF308: The item itself overwrites "MND Boost" with "MND Boost 2."
00FBF364: Spartan's Tome - Speed
00FBF378: Only characters who have maxed the "SPD Boost" skill can use it.
00FBF3D0: The item itself overwrites "SPD Boost" with "SPD Boost 2."
00FBF42C: Veteran's Tome - Vitality
00FBF448: A very suspicious tome which only veterans of many battles can comprehend.
00FBF4A8: Only characters who have maxed the "HP Boost 2" skill can use it.
00FBF508: The item itself overwrites "HP Boost 2" with "Mega HP Boost."
00FBF56C: Veteran's Tome - Sense
00FBF588: Only characters who have maxed the "MP Boost 2" skill can use it.
00FBF5E8: The item itself overwrites "MP Boost 2" with "Mega MP Boost."
00FBF64C: Veteran's Tome - Energy
00FBF668: Only characters who have maxed the "TP Boost 2" skill can use it.
00FBF6C8: The item itself overwrites "TP Boost 2" with "Mega TP Boost."
00FBF72C: Veteran's Tome - Attack
00FBF748: Only characters who have maxed the "ATK Boost 2" skill can use it.
00FBF7A8: The item itself overwrites "ATK Boost 2" with "Mega ATK Boost."
00FBF80C: Veteran's Tome - Defense
00FBF828: Only characters who have maxed the "DEF Boost 2" skill can use it.
00FBF888: The item itself overwrites "DEF Boost 2" with "Mega DEF Boost."
00FBF8EC: Veteran's Tome - Magic
00FBF908: Only characters who have maxed the "MAG Boost 2" skill can use it.
00FBF968: The item itself overwrites "MAG Boost 2" with "Mega MAG Boost."
00FBF9CC: Veteran's Tome - Mind
00FBF9E8: Only characters who have maxed the "MND Boost 2" skill can use it.
00FBFA48: The item itself overwrites "MND Boost 2" with "Mega MND Boost."
00FBFAAC: Veteran's Tome - Speed
00FBFAC8: Only characters who have maxed the "SPD Boost 2" skill can use it.
00FBFB28: The item itself overwrites "SPD Boost 2" with "Mega SPD Boost."
00FBFB8C: Water Lily Seal
00FBFB9C: A mysterious seal obtained from defeating an enemy on B1F.
00FBFBCC: Clears the blockade on 22F.
00FBFBF0: Valley Lily Seal
00FBFC00: A mysterious seal obtained from defeating an enemy on B2F.
00FBFC30: Clears the blockade on 27F.
00FBFC54: Rhododendron Seal
00FBFC64: A mysterious seal obtained from defeating an enemy on B3F.
00FBFC94: Where can it be used...?
00FBFCBC: Bodhi Tree Seal
00FBFCCC: A mysterious seal obtained from defeating an enemy on B4F.
00FBFCFC: Pagoda Part A
00FBFD0C: An item picked up in the Great Tree that evokes a strange sense of virtue.
00FBFD48: It's obviously the upper half of Shou's pagoda.
00FBFD80: Pagoda Part B
00FBFD90: It's obviously the lower half of Shou's pagoda.
00FBFDC8: Kokoro's Mask - Joy
00FBFDE0: A mysterious mask. Feels like one of the masks which float around Kokoro.
00FBFE30: The expression on the mask's face seems to signify "Joy."
00FBFE74: Kokoro's Mask - Anger
00FBFE88: The expression on the mask's face seems to signify "Anger."
00FBFECC: Kokoro's Mask - Pathos
00FBFEE0: The expression on the mask's face seems to signify "Pathos."
00FBFF24: Kokoro's Mask - Cheer
00FBFF38: The expression on the mask's face seems to signify "Cheer."
00FBFF7C: Non-Neumann Systems 9
00FBFFA8: An incomprehensible book that explains about the future of certain complex
00FC0000: tech. Rinnosuke seems to have heard of it before.
00FC0058: Non-Neumann Systems 10
00FC0088: tech. Goes into painstaking detail about watering cans.
00FC00EC: Non-Neumann Systems 11
00FC0118: tech. Goes into quantum physics, which is so hot right now.
00FC017C: Non-Neumann Systems 12
00FC01A8: tech. Goes into pros and cons about homosexuality amongst mathematicians.
00FC020C: Miko's 7-Star Charm
00FC0220: The charm given by Futo and Miko to seal the giant hole in the
00FC0260: underground. Combines with the power of the Ame-no-Murakumo to allow 
00FC0298: the party to safely explore the mysteries within the giant hole.
00FC02E0: Elemental Circle - Wood
00FC02F8: A giant elemental circle prepared by Miko to banish the evil aura at the
00FC0358: top of the Great Tree. Made more powerful with the help of the 
00FC03A8: Ame-no-Murakumo. Controls the concept of the "Wood" element.
00FC03E0: Elemental Circle - Fire
00FC03F8: Ame-no-Murakumo. Controls the concept of the "Fire" element.
00FC0430: Elemental Circle - Earth
00FC0448: Ame-no-Murakumo. Controls the concept of the "Earth" element.
00FC0480: Elemental Circle - Metal
00FC0498: Ame-no-Murakumo. Controls the concept of the "Metal" element.
00FC04D0: Elemental Circle - Water
00FC04E8: Ame-no-Murakumo. Controls the concept of the "Water" element.
00FC0520: Blue Ballistic Talisman
00FC0530: A talisman holding incredible ballistic force.
00FC0558: Can destroy blue-colored ice which blocks your path.
00FC0584: Purple Ballistic Talisman
00FC0594: Can destroy purple-colored ice which blocks your path.
00FC05C0: Swordmaster's Proof
00FC05D4: A book that records the secret arts of all kinds of combat styles.
00FC05FC: Unlocks the subclass "Swordmaster."
00FC0634: Don't worry about the name - non-sword-wielders can learn it too.
00FC066C: Archmage's Proof
00FC0680: A book that records black magic literature from all areas and eras.
00FC06C4: Unlocks the subclass "Archmage."
00FC06FC: Dump 'em in water. If they sink, it's a human. If they float, it's a witch.
00FC0744: Appraiser's Proof
00FC0758: A book that records negotiation and judgment skills for all kinds of items.
00FC079C: Unlocks the subclass "Appraiser."
00FC07D8: You won't be terrified by sudden sensations during appraisals anymore.
00FC0828: The females of Gensokyo sure are courageous.
00FC084C: Elementalist's Proof
00FC0860: A book that records musings about the 4 elements from Empedocles and co.
00FC08A8: Unlocks the subclass "Elementalist."
00FC08E0: This game actually has 8 elements, but it's fine. Just a rounding error.
00FC092C: Ninja's Proof
00FC093C: A book that records some pretty shady stuff.
00FC0970: Unlocks the subclass "Ninja."
00FC09A8: Includes a note that going nude will give you similar AC to an M4 Sherman.
00FC0A04: Oracle's Proof
00FC0A18: A book that records the way to hear the voices of gods and natural spirits.
00FC0A64: Unlocks the subclass "Oracle."
00FC0A9C: Honestly, you can just make stuff up but sound as confident as a writer
00FC0AE8: for a "For Dummies" book and still get away with it.
00FC0B08: Dragon God's Scale
00FC0B18: A large scale gotten from the Dragon God, serving as proof of your victory.
00FC0B4C: Unlocks the subclass "Dragon God's Power."
00FC0BE0: Tree. Unlocks the subclass "*WINNER*." How many hundreds of hours did it 
00FC0CD4: Shrine Maiden's Arrow
00FC0CE4: Might be more useful in Eirin's hands rather than Reimu's.
00FC0D30: Unlocks new skills for Reimu, increases Reimu's damage dealt by 
00FC0D80: (quantity)% and reduces Reimu's damage taken by (quantity / 2)%.
00FC0DE8: The damage bonuses of the item caps at 50.
00FC0E48: Phantasmal Mushroom
00FC0E58: Seems to hold strong magical power. Too dangerous to eat.
00FC0EA0: Unlocks new skills for Marisa, increases Marisa's damage dealt by 
00FC0EF0: (quantity)% and reduces Marisa's damage taken by (quantity / 2)%.
00FC0F58: Gensokyo Rarities Catalog
00FC0F6C: An encyclopedia of all kinds of rare and strange items found in Gensokyo.
00FC0FA8: Unlocks new skills for Rinnosuke, increases Rinnosuke's damage dealt by 
00FC0FF8: (quantity)% and reduces Rinnosuke's damage taken by (quantity / 2)%.
00FC1060: Ancient History Textbook
00FC1070: A book with records of ancient times where society had barely progressed.
00FC10B8: Unlocks new skills for Keine, increases Keine's damage dealt by 
00FC1108: (quantity)% and reduces Keine's damage taken by (quantity / 2)%
00FC1170: High-Quality Tengu Shield
00FC1180: A shield made with the finest material. Not meant for lower-class Tengu.
00FC11D0: Unlocks new skills for Momiji, increases Momiji's damage dealt by 
00FC1220: (quantity)% and reduces Momiji's damage taken by (quantity / 2)%.
00FC1284: Top-Class Whetstone
00FC1294: A whetstone of the highest quality. Prevents Youmu's swords from dulling.
00FC12D0: Unlocks new skills for Youmu, increases Youmu's damage dealt by 
00FC1320: (quantity)% and reduces Youmu's damage taken by (quantity / 2)%.
00FC1388: Scaring for Dummies
00FC13AC: A book explaining the various ways to scare others.
00FC13EC: Unlocks new skills for Kogasa, increases Kogasa's damage dealt by 
00FC1438: (quantity)% and reduces Kogasa's damage taken by (quantity / 2)%.
00FC14A0: Powered Ribbon Charm
00FC14B8: Rumia's ribbon, which looks like a charm, seems to have been powered up.
00FC1500: Unlocks new skills for Rumia, increases Rumia's damage dealt by 
00FC1550: (quantity)% and reduces Rumia's damage taken by (quantity / 2)%.
00FC15BC: Absolute-0 Ice Fragment
00FC15D8: Pretty cold. Absolute zero ice is actually scary, but no one here cares.
00FC1638: Unlocks new skills for Cirno, increases Cirno's damage dealt by 
00FC1688: (quantity)% and reduces Cirno's damage taken by (quantity / 2)%.
00FC16F0: Shining Ear of Rice
00FC1700: A glowing ear of rice blessed with good harvest prayers. Effective vs gods.
00FC1740: Unlocks new skills for Minoriko, increases Minoriko's damage dealt by 
00FC1790: (quantity)% and reduces Minoriko's damage taken by (quantity / 2)%.
00FC17F8: Bronze Four-Letter Coin
00FC1808: Ancient coins that Komachi collects. Heiji Zenigata throws these around.
00FC1860: Unlocks new skills for Komachi, increases Komachi's damage dealt by 
00FC18B0: (quantity)% and reduces Komachi's damage taken by (quantity / 2)%.
00FC1918: Captivating Catnip
00FC192C: Cats are all about catnip. That's just common sense.
00FC194C: Unlocks new skills for Chen, increases Chen's damage dealt by 
00FC1998: (quantity)% and reduces Chen's damage taken by (quantity / 2)%.
00FC19FC: State-Of-The-Art Circuit
00FC1A14: Some bleeding-edge tech gadget, apparently. No one else knows what it is.
00FC1A60: Unlocks new skills for Nitori, increases Nitori's damage dealt by 
00FC1AB0: (quantity)% and reduces Nitori's damage taken by (quantity / 2)%.
00FC1B18: Symbol of Jealousy
00FC1B20: An extremely unpleasant item that fills all who sees it with jealousy.
00FC1B80: Unlocks new skills for Parsee, increases Parsee's damage dealt by 
00FC1BD0: (quantity)% and reduces Parsee's damage taken by (quantity / 2)%.
00FC1C3C: Insect Leader's Proof
00FC1C50: It may sound cool, but it's just a bag of pheromones that attract insects.
00FC1CB8: Unlocks new skills for Wriggle, increases Wriggle's damage dealt by 
00FC1D08: (quantity)% and reduces Wriggle's damage taken by (quantity / 2)%.
00FC1D70: Shining Bamboo
00FC1D80: A glowing bamboo from the princess of the moon... which may be unrelated.
00FC1DE8: Unlocks new skills for Kaguya, increases Kaguya's damage dealt by 
00FC1E38: (quantity)% and reduces Kaguya's damage taken by (quantity / 2)%.
00FC1EA0: Jug of Hourai Elixir
00FC1EB0: The jug that was discarded somewhere has somehow been unearthed again.
00FC1EE0: Unlocks new skills for Mokou, increases Mokou's damage dealt by 
00FC1F30: (quantity)% and reduces Mokou's damage taken by (quantity / 2)%.
00FC1F98: Crow Tengu's Notebook
00FC1FB0: Aya finally realized she can just get scoops from other tengu's notebooks.
00FC2014: Unlocks new skills for Aya, increases Aya's damage dealt by 
00FC2060: (quantity)% and reduces Aya's damage taken by (quantity / 2)%.
00FC20C4: Chirper's Honey
00FC20D8: Drinking it gives you a clear and beautiful voice. A certain poet's secret.
00FC2128: Unlocks new skills for Mystia, increases Mystia's damage dealt by 
00FC2180: (quantity)% and reduces Mystia's damage taken by (quantity / 2)%.
00FC21F0: Box of 100 Medicines
00FC2200: That item that caused lots of trouble. ...Or a replica of it, perhaps?
00FC2234: Unlocks new skills for Kasen, increases Kasen's damage dealt by 
00FC2280: (quantity)% and reduces Kasen's damage taken by (quantity / 2)%.
00FC22E8: Black Pendulum
00FC2300: A new pendulum with hidden powers. Looks like it can be used for dowsing.
00FC2348: Unlocks new skills for Nazrin, increases Nazrin's damage dealt by 
00FC23A0: (quantity)% and reduces Nazrin's damage taken by (quantity / 2)%.
00FC2410: Worn-Out Nagashi-bina
00FC2420: With its duty done, it flowed all over and eventually washed up here.
00FC2468: Unlocks new skills for Hina, increases Hina's damage dealt by 
00FC24B8: (quantity)% and reduces Hina's damage taken by (quantity / 2)%.
00FC251C: Fine Skull
00FC252C: A skeleton with a visibly pleasant shape. In Orin's eyes, at least.
00FC2558: Unlocks new skills for Orin, increases Orin's damage dealt by 
00FC25A8: (quantity)% and reduces Orin's damage taken by (quantity / 2)%.
00FC2610: Extra Control Rod
00FC2624: Insert it for more control. As deep as you can.
00FC2668: Unlocks new skills for Utsuho, increases Utsuho's damage dealt by 
00FC26B8: (quantity)% and reduces Utsuho's damage taken by (quantity / 2)%.
00FC271C: Mind-Reading Crystal Eye
00FC2780: Unlocks new skills for Satori, increases Satori's damage dealt by 
00FC27D0: (quantity)% and reduces Satori's damage taken by (quantity / 2)%.
00FC2838: Oni's Gold Sake Cup
00FC2850: A cup that Yuugi seems to use to drink sake. Makes your room look refined.
00FC28A4: Unlocks new skills for Yuugi, increases Yuugi's damage dealt by 
00FC28F0: (quantity)% and reduces Yuugi's damage taken by (quantity / 2)%.
00FC2958: Dragon-Embroidered Dress
00FC2970: Meiling's new clothes. She wanted them for a while, but refrained till now.
00FC29C4: Unlocks new skills for Meiling, increases Meiling's damage dealt by 
00FC2A10: (quantity)% and reduces Meiling's damage taken by (quantity / 2)%.
00FC2A78: Frictionless Ball Joint
00FC2A90: Ball joints with an astounding lack of friction. Skillfully crafted.
00FC2AE8: Unlocks new skills for Alice, increases Alice's damage dealt by 
00FC2B38: (quantity)% and reduces Alice's damage taken by (quantity / 2)%.
00FC2BA0: Otherworldly Grimoire
00FC2BB0: A renowned black magic grimoire. Lots of nasty things are written in it.
00FC2BF0: Unlocks new skills for Patchouli, increases Patchouli's damage dealt by 
00FC2C48: (quantity)% and reduces Patchouli's damage taken by (quantity / 2)%.
00FC2CB8: Hourai Elixir Ingredient
00FC2CCC: Eirin's trying to make it again. More trouble than it's worth, honestly.
00FC2D18: Unlocks new skills for Eirin, increases Eirin's damage dealt by 
00FC2D68: (quantity)% and reduces Eirin's damage taken by (quantity / 2)%.
00FC2DD0: Mind-Piercing Bullet
00FC2DE4: A bullet that damages your psyche instead of your body.
00FC2E1C: Unlocks new skills for Reisen, increases Reisen's damage dealt by 
00FC2E68: (quantity)% and reduces Reisen's damage taken by (quantity / 2)%.
00FC2ED0: Modern School Uniform
00FC2EE4: Sanae should wear it sometimes. Lotsa people would like to see her in one.
00FC2F30: Unlocks new skills for Sanae, increases Sanae's damage dealt by 
00FC2F80: (quantity)% and reduces Sanae's damage taken by (quantity / 2)%.
00FC2FE8: Silver Hagoromo
00FC2FF8: a flexible hagoromo that shines with silver. Pleasant to touch.
00FC302C: Unlocks new skills for Iku, increases Iku's damage dealt by 
00FC3078: (quantity)% and reduces Iku's damage taken by (quantity / 2)%.
00FC30E0: Ultra HQ Sake - Ghosts
00FC30FC: HQ sake made extreme by brewing it with vengeful spirit powers.
00FC3140: Unlocks new skills for Suika, increases Suika's damage dealt by 
00FC3190: (quantity)% and reduces Suika's damage taken by (quantity / 2)%.
00FC31F8: HQ Juicy Fried Tofu
00FC3214: This slop! Why do I crave it!?
00FC3244: Unlocks new skills for Ran, increases Ran's damage dealt by 
00FC3290: (quantity)% and reduces Ran's damage taken by (quantity / 2)%.
00FC32F4: Bloodlike Tomato Juice
00FC3314: Remilia sometimes settles for this. Yes, she is capable of self-restraint.
00FC3360: Unlocks new skills for Remilia, increases Remilia's damage dealt by 
00FC33B0: (quantity)% and reduces Remilia's damage taken by (quantity / 2)%.
00FC341C: Royals' Ceramics Set
00FC3430: From cute little teacups to large vases. Raises the SDM's nobility.
00FC347C: Unlocks new skills for Sakuya, increases Sakuya's damage dealt by 
00FC34C8: (quantity)% and reduces Sakuya's damage taken by (quantity / 2)%.
00FC3530: Battle Onbashira
00FC3540: A massive and extremely heavy onbashira has manifested right here.
00FC3578: Unlocks new skills for Kanako, increases Kanako's damage dealt by 
00FC35C8: (quantity)% and reduces Kanako's damage taken by (quantity / 2)%.
00FC3630: Frog Badge
00FC3640: A cute little froggy badge. Absorbs water, fire, earth and wind... maybe.
00FC36A0: Unlocks new skills for Suwako, increases Suwako's damage dealt by 
00FC36F0: (quantity)% and reduces Suwako's damage taken by (quantity / 2)%.
00FC3758: HQ Peach From Heaven
00FC3768: "Far better than the surface's yucky peaches." - Delinquent Celestial
00FC37B0: Unlocks new skills for Tenshi, increases Tenshi's damage dealt by 
00FC3800: (quantity)% and reduces Tenshi's damage taken by (quantity / 2)%.
00FC3868: Jewel of Insanity
00FC3878: A jewel that emits a mysterious spectrum-ignoring light. Causes madness.
00FC38D0: Unlocks new skills for Flan, increases Flan's damage dealt by 
00FC3920: (quantity)% and reduces Flan's damage taken by (quantity / 2)%.
00FC3988: Saigyou Akashi's Petal
00FC39A0: Yuyuko still seems to be curious about the corpse buried under the tree.
00FC39F0: Unlocks new skills for Yuyuko, increases Yuyuko's damage dealt by 
00FC3A40: (quantity)% and reduces Yuyuko's damage taken by (quantity / 2)%.
00FC3AA8: Rainbow Sunflower
00FC3AB8: A rainbow-tint flower. Said to appear once out of a million sunflowers.
00FC3AF4: Unlocks new skills for Yuuka, increases Yuuka's damage dealt by 
00FC3B40: (quantity)% and reduces Yuuka's damage taken by (quantity / 2)%.
00FC3BA8: Gap Shard
00FC3BB8: Collects lots for a TM... Wait, wrong shard. Holds a strange power anyway.
00FC3C18: Unlocks new skills for Yukari, increases Yukari's damage dealt by 
00FC3C68: (quantity)% and reduces Yukari's damage taken by (quantity / 2)%.
00FC3CCC: Newly-Made Sutra Scroll
00FC3CE0: Bykauren's new Sutra Scroll. She aims to be a more muscley monk.
00FC3D2C: Unlocks new skills for Byakuren, increases Byakuren's damage dealt by 
00FC3D78: (quantity)% and reduces Byakuren's damage taken by (quantity / 2)%.
00FC3DE0: Ebony Pagoda
00FC3DF0: Made by human hands as a symbol of their own deaths.
00FC3E14: Unlocks new skills for Eiki, increases Eiki's damage dealt by 
00FC3E60: (quantity)% and reduces Eiki's damage taken by (quantity / 2)%.
00FC3EC8: Complex Science Textbook
00FC3EE4: Renko's the only one who understands its contents, probably.
00FC3F24: Unlocks new skills for Renko, increases Renko's damage dealt by 
00FC3F70: (quantity)% and reduces Renko's damage taken by (quantity / 2)%.
00FC3FD8: Illusionary Specs
00FC3FE8: Makes barriers visible - not exactly helpful. Gives the Glasses attribute.
00FC4040: Unlocks new skills for Mari, increases Mari's damage dealt by 
00FC4090: (quantity)% and reduces Mari's damage taken by (quantity / 2)%.
00FC40F8: Forged Spear
00FC4110: Shou's not too happy about this item - she not a fan of melee combat.
00FC416C: Unlocks new skills for Shou, increases Shou's damage dealt by 
00FC41B8: (quantity)% and reduces Shou's damage taken by (quantity / 2)%.
00FC421C: Magical Racoon Leaf
00FC4238: Allows a moustached plumber to grow extra ears and a tail.
00FC4270: Unlocks new skills for Mamizou, increases Mamizou's damage dealt by 
00FC42C0: (quantity)% and reduces Mamizou's damage taken by (quantity / 2)%.
00FC432C: Sharp Star Dragon Bow
00FC433C: What Futo uses to fire those light arrows happily into the air.
00FC4380: Unlocks new skills for Futo, increases Futo's damage dealt by 
00FC43D0: (quantity)% and reduces Futo's damage taken by (quantity / 2)%.
00FC4438: Crimson Court Dress
00FC4448: An orangey kimono and headdress. A symbol of power in a bygone era.
00FC448C: Unlocks new skills for Miko, increases Miko's damage dealt by 
00FC44D8: (quantity)% and reduces Miko's damage taken by (quantity / 2)%.
00FC4540: Emotion Mask: Extra
00FC455C: You're getting more emotions! Way to go, Kokoro!
00FC4590: Unlocks new skills for Kokoro, increases Kokoro's damage dealt by 
00FC45E0: (quantity)% and reduces Kokoro's damage taken by (quantity / 2)%.
00FC4648: Bulky Brown Western Book
00FC465C: Hopefully Tokiko doesn't lose it or let it get stolen this time...
00FC46A0: Unlocks new skills for Tokiko, increases Tokiko's damage dealt by 
00FC46F0: (quantity)% and reduces Tokiko's damage taken by (quantity / 2)%.
00FC4758: Proof of Koishi's Existence
00FC476C: Koishi has more friends now. Maybe things will be different this time.
00FC47A8: Unlocks new skills for Koishi, increases Koishi's damage dealt by 
00FC47F8: (quantity)% and reduces Koishi's damage taken by (quantity / 2)%.
00FC4860: Miare Memories
00FC4878: Having more than nine would break lore? Don't sweat the small stuff.
00FC48CC: Unlocks new skills for Akyuu, increases Akyuu's damage dealt by 
00FC4918: (quantity)% and reduces Akyuu's damage taken by (quantity / 2)%.
