00F73A60: Hmm?
00F77C34: Ah. 
00FC9A48: ... 
00FCC208: ......
00FCEBE0: ...Hm?
00FD0568: Yeah... Who would erect a barrier at a place like this@anyway? 
00FD09F8: Ehh? You don't know her, Marisa? ...Reimu, do you know@her?
00FD9EDC: You should've said that even sooner!
00FD9F48: The 17th floor, huh... We headin' there, Reimu? 
00FDA0B8: I'm getting a really bad feeling from it. It must be@pretty strong.  
00FDB514: You should've said so sooner! 
00FDB6A0: What was that pause?
00FDE974: Hmph. Guess we'll have to investigate.
00FE0E04: Oh? That's strange... 
00FE368C: Mari, Mari! Are you okay!? Do you recognize me!?
00FE450C: Guess we have to... 
00FE5A28: Urgh. I guess that's all we can do... 
00FE7624: What's this? Hmm, let's see...
00FE7798: Mari? 
00FE7C84: ......Eww, what's this mask? It's ugly as sin.
00FE9290: The way is blocked because of a mysterious device. It's @too difficult to try and break through it.
00FE93B0: The tear in space on the 17th floor that I mentioned@earlier. It's right around here.
00FEC484: ...Oh?
00FEE2A8: That Mari who poked a border and got spirited away for@her troubles? 
00FEFC60: Saying we'll save her is all well and good, but where @could she be? This Great Tree's pretty darn big.
00FF0F64: The moment the mysterious jewel was touched, the jewel@and its dazzling light completely vanished. 
00FF1330: This switch might've done something. The flow of magic@on the 9th floor feels a bit different. Maybe something @changed about it? 
00FF2234: .........I shouldn't have bothered worrying.
00FF3CF8: There's a monster we never seen before... What's that?
00FF414C: The Great Tree has changed upon defeating a Hidden@Boss. 
00FF6994: But... Huh? Is there someone on the floor there?
00FF6C84: Hey, relax. It may be an ally.
00FF7A54: Uuhn....
00FF8118: We've beat 'em once before, they're no big deal now.
00FF8944: Indeed. The closest thing you could imagine is when I @use my gaps to move around. 
00FF89B8: ...We're going to have to keep walking around till our@legs are numb, aren't we. 
00FF8BA0: Ehh? ...Who's that? Let's go take a look! 
00FFA508: We've gotten really far into the Great Tree.  
00FFA710: A young woman lying on a bed of sand. Won't that make @for a great piece o' art? 
00FFA7EC: Now now, I would think anyone who collapses in a desert @would be hungry and thirsty.
00FFB000: Though if we're backin' you up, I guess we'd pull @through somehow or other. 
00FFB2D8: But that'll be boring! ...I mean, it'll make us feel@really bad. Can't we go along with you? Please? 
00FFB9D8: A seal on 21F was removed!
00FFC754: Oh? So this girl here is... 
00FFC9B0: Yeah, this entire region feels really strange. It's like@there's a barrier around it with a really huge hole in@it. 
00FFCD68: It should be okay to let 'em come, no? If you told me to@sit still at the inn too, I'd ask what the heck you're@on. And there's no way I'll actually just stay at the @inn. I'd die of boredom, for sure.
00FFD9B4: Hm. Whaddya want to do? 
00FFDEE8: Obtained Sub Equipment "Great Question's Mask!" 
00FFE460: That's simple. Exterminate, exterminate! Let's go!
00FFF944: Do be quiet. You want the treasure, don't you? Let's@go! 
00FFFE10: You sound awfully excited about this... Anyway, we@should get Mari back to the inn.
00FFFEA4: Waaaaah... I'm sorry, I'm so sorry... 
010001E8: Ahh...... Good morning, Renko...
01000340: I told you before, I don't want it... 
01002150: This is the inside of the Great Tree. What are you doing@here? 
010022B8: This switch might've done something. The flow of magic@on this 14th floor feels a bit different. Maybe @something changed about it? 
01002354: And what's this Great Tree you keep talking about? And@where are we? You mean this isn't the forest behind the @university? 
010029E8: Of course I can't! We managed to pull out a victory, but@still!
01002AD0: Ooh... Stupid Mari! I told her we should've gone back @home for the day! 
01002BB0: It's about time for the boss of all these monsters to @appear, ain't it? Y'know, like how the one that stays @in the shadows and hides their power levels are always@the strongest?
01002E5C: Please save Mari! I'll do my best to help out too!
01002F7C: Gah, how many of them are there... This is so @annoying... 
01003088: ...What was that? 
0100374C: So the ever-helpful Reimu says, as she heads off towards@the problem anyway. The end.
01003980: So regardless of the warning, what we're gonna do @doesn't change, huh?
01003A48: That's not something I can answer, I'm afraid. How about@asking the next monster?
01004F28: Ehhhhhhhhhhhhh!? No way! I can't let Mari get eaten!! 
010054C0: That's fine. It was being sneaky in a remote place in @the Great Tree, so it must've been up to no good. Come@on, let's go! 
01006218: Ahhh, right. So this is the upgraded version of that@other consecutive-battle test of strength place nearby. 
01006CA0: Yeah! 
010076F4: Yep, you two should just rest at the village for now. 
010077B8: Huh. So something's gonna come out here?
010077E8: ...Yukari's fox can do all the math we need, I believe. 
01007834: Yeah... Thank you, you've done so much... 
01007C0C: Huh?
01007C50: Isn't it obvious? It's going out of its way to give us a@reward if we prove our strength. The least we can do is @to answer it. 
01008008: Uni... versity...?
01008288: That sounds as fair a summary as any. 
0100855C: That's exactly what I mean. And I've warned you well in @advance this time.
010085A8: Ah... Wait. I'm sensing something strange on the 16th @floor again...
01008B20: I feel for that monster... Its luck ran out the moment@it encountered a shrine maiden with that kind of@attitude. 
010091BC: Well, that's one shrine maiden who never looks back...
01009458: ...Would math help? 
01009F48: I don't want to either... If wearin' this is what it@takes to be strong, I'll rather be weak...
0100A390: ..............There's no one else like that. Yes. 
0100A8A0: Ahhhhhhhhhhhhhhhhhh! Mari! It's you, Mari! What are you @doing here!? ...Wait, I should ask that of myself!
0100CA40: Hmm?
0100CA48: A "university" is the highest level academic center in@the outside world. In other words, this girl seems to @have arrived here from the outside world. 
0100E9C0: Geez, is THAT what you have to say!? Are you okay!? Are @you hurt anywhere!? Do you feel sick or anything!?
0100EA44: Tell us when we're close to it! 
0100EAF0: ...Ah... Yes, that's right. Hey, you've really done a @lot for us, and we'd feel really bad if we don't give @you anything in return. Would you let Mari and I help @you out?
0100F3D0: ...Hm? What's this sensation? 
0100F4B0: This switch might've done something. The flow of magic@on this floor feels a bit different. Maybe something@changed about it? 
0100FA90: Ahh... So basically, two girls from the outside world @got "spirited away" and landed within this Great Tree?
0100FC80: I suppose so.... Now that we've met her, it's not like@we can just say "Oh, that's nice, but I don't care one@bit." I wouldn't be able to sleep at night. 
0100FDA8: Wha? A tear in space? 
01010148: Hm? There's someone there... Though they're on the@floor.
010101AC: Practically her first words are to declare she's hungry?@She's quite something.
010101F0: ......Would it? ...Anyway, we've found her, let's go@save her. 
01010348: Uhhhhn... 
01010388: Ahh, forget this stand-up comedy act! I'm so glad I've@finally found you...! 
01010490: Sick......? No, I just feel hungry. Oh, and thirsty @too...... 
01010558: She looks a li'l weak. We should let her rest at our inn@for the time being. 
01010740: Good afternoon! Hello! *poke poke*
0101076C: .......Mm...
010107EC: I could do without that, thank you very much. 
01010830: Ehhhhhh... What we're doing is really quite dangerous,@you know. 
010108B0: I don't.
010108BC: Reimu, Reimu. It's around here. 
010108F8: Help...? What can ya even do? 
0101093C: You be quiet for a second.
010109D0: Waaaaaaaaahh! Please just save heeeeeer!
01010A20: Hmmmmm... Well, fine I guess. We're always heading back @to Gensokyo whenever it gets dangerous anyway.
01010A80: Yay! We'll be in your care for a while once again, then!@Let's resolve this incident together! 
01010D90: My, it's over already? Getting through those enemies was@easier than I was expecting... Well, whatever.
01010E68: Ah, yeah we should. Mari, our adventure starts now! 
0101102C: Uhhhhhn... I want water.... 
0101118C: .........Are these two really going to be okay? 
010115B8: This switch might've done something. The flow of magic@on the lower floor feels a bit different. Maybe @something changed about it? 
01012234: Something good, huh? That's gotta be some treasure. 
01012864: ...Marisa, you wanted treasure, right? You can have @this. 
01012AC4: A notice board stands at the separated area.
01012C3C: ......Eh? 
01013F78: Err, what does that mean? 
01013FEC: But... Treasure!
010154C8: Well, it was right in front of us. I thought you might@have gained the ability to sense gaps and space and @such as well... 
010155C0: This switch might've done something. The flow of magic@on the 14th floor feels a bit different. Maybe@something changed about it? 
01016D30: This switch might've done something. The flow of magic@on this 13th floor feels a bit different. Maybe @something changed about it? 
010175E8: So, Renko. This place is called Gensokyo. It's a@mysterious place connected to the world you came from,@but kept apart by a barrier.
01017CE0: Quite a number of the humans from the ouside that get @"spirited away" here ended up being eaten by youkai. In @this Great Tree filled to the brim with monsters, it's@probably even worse.
01019410: W-Wait a second! You're calling me strong? That's a joke@and a half! I barely passed Phys Ed!
01019608: This is pretty high up too. If ya made it this far solo,@I guess you've gotta be pretty strong, huh? Haven't @seen you around before though.
0101A7C8: .........Great Tree? Ehhhh? ...What's that? 
0101AA3C: ...Where's that? ...Is it even a place? 
0101B39C: That's another name I've never heard. 
0101BC70: Nah, it can't be that. I'm feelin' it too, and I know @nothing about barriers. 
0101BFC8: We have time, we should. Now that this Great Tree @incident's finally been settled, I'll rather fix these@abnormalities before they become annoying.
0101C050: All right, all right. I'll be sure to do that for the @next one. 
0101C160: You're both close, but no cigar. There is a distortion@indeed, but it is not a barrier. There is a tear in the @"space" around here.
0101C200: Perhaps so. I've detected a very powerful force @approaching at the same time the tear in space@appeared. 
0101C2C8: Cultural exchange is a wonderful thing, isn't it? Ah, by@the way, it'll be here in three seconds.
0101C378: Whoa whoa, the heck was that? If anythin', wasn't it@stronger than the Ame-no-Murakumo?
0101C41C: ...The next one? There's another one? 
0101C5E0: Yukari, can you feel any other of those things coming to@Gensokyo? You should be able to do that space @manipu-whatever to guess, right?
0101CA24: What pause? 
0101CA8C: Yeah, that's true... Guess there's no choice. 
0101CB58: Ah, this one can talk. I guess she's not the boss of@those monsters, then. Good, good. 
0101CC08: How would I know? Those monsters themselves are @incomprehensible to begin with. 
0101D2B0: There's a similar distortion in space in the 18th floor.@At first I was merely suspicious, but now that it's @happened twice, it's probably a similar monster up@there too.
0101D3C0: The outside world? Erm... I'm pretty sure I was just in @the mountain forest behind the university... What's @going on? 
0101D460: ...Ah, I haven't introduced myself, have I? My name is@Renko. I'm a university student majoring in Quantum @Mechanics and Unified Physics.  
0101D678: Ehh? Umm, I was deep in the mountain forest behind the@university with Mari, when we found some strange@barrier thing. We wanted to investigate it properly @today, but when Mari touched the barrier, it@suddenly... 
0101D998: She's awake.
0101D9E4: Whew... H-Huh? Who are you? Where am I? 
0101DA40: Reimu, Marisa.
0101DCF0: ...You don't mean...
0101DDA8: What the... What's going on here...?
0101DDFC: May I have your name? 
0101DE28: I'm Yukari. Nice to meet you. 
0101E1A8: This next one's probably going to be just as disgusting @as the rest, huh... 
0101E348: Oooh... Was this the last?
0101E718: Yukari! Why do you keep waiting to tell us about these@things until just before they appear!?
0101E87C: Yep yep.
0101E958: I think so. Probably. 
0101ED48: Send it back to where it came from, obviously. What, you@want to have a tea party with it? 
0101EF08: Seriously. How much longer do we have to keep @exterminating these gooey squishy monsters? 
0101EFF8: Who put this notice board up in the first place...@There's way too much things to point out about this, I@don't know where to start.
0101F1F0: What are you going to do once it comes out? 
0101F2B0: Oh, hush. Get ready, it's coming! 
0101F7D4: No thanks, I don't want something like this...
0101FA44: Geez, don't make fun of me, please. 
01020C00: Marisa, here's your treasure. Take it.
010215D4: ............Let's go back...
01022098: "This is a place to test your strength. As you advance, @enhanced bosses you have faced will attack you@consecutively. If you defeat them all, you will receive @something good."
01022A10: "This is a place to test your strength. As you advance, @all the bosses you have faced will attack you @consecutively. If you defeat them all, you will receive @something good."
01022BB4: That's what I expected from ya, Reimu. Your @well-polished skills at exterminating random passerby @youkai aren't for show! 
01022C48: They're still monsters we beat before. We dealt with@them anyway.
01022E00: Who knows what we'll get this time? The best way to find@out would be to beat up all these enemies first. Let's@go! 
01022EE0: All those enhanced enemies coming one after another was @rather tiring.
01022F90: What are you saying? It may look more grotesque than the@monsters around here, but it's definitely got good@stats. If you're picky about everything, you'll never @become a powerful magician. Though I'll never wear it @myself. 
0102318C: That was rather blunt for an explanation... Well, what@are we going to do? 
010231C0: A reward, huh... Hope it ain't that weird mask. 
010231F8: This is...! 
0102322C: ......It's the same mask as before... 
01023318: *sigh* Well, this was a real downer... Let's go back, @Marisa... 
010544B0: We still beat it, at least. What in the world was that, @though? I can't think of anyone who can do these kind @of space thingamajigs, except Yukari. 
0109D7C8: We seem to be holding different conversations here... @Where in the world did you come from? 
0109D810: Mmm, shall I explain what I suspect happened? The two of@you happened to tamper with a barrier, correct? You @don't appear to have any power to manipulate barriers,@so I'll take it that this Mari person was the one who @touched it. 
0109D900: When she touched the Greet Hakurei Barrier, something @happened which led to Gensokyo absorbing the two of you @from the outside world. Based on this, Mari's power to@control barriers probably isn't very high.
0109DA18: Wait, that doesn't matter right now! Do you know where@Mari is!? If anything happens to her, I...!!
0109DAE0: Oh, leave it. It'll be more annoying if we leave it to@become an even bigger problem. Hurry up and follow me.
0109DBA0: Whaaa? There's more? And on the 16th floor again... This@had better not be an infinite loop. 
010A0D20: Ahh, don't worry about that. I'll have some time once we@finish up a bit of business we have here. I'll send the @two of you back to the outside world then.
010A0F20: Someone who could manipulate space, you mean? Besides @myself, I don't know anyone like... 
010A10E0: The barrier that isolates Gensokyo from the outside @world is extremely strong, so it should not be a@triviality to arrive here from the outside... How did @you manage to come to Gensokyo? 
010A1198: ...Ahhhhhhhh! Where's Mari!? Where did she go off to!?
010A1278: You shoulda just explained it that way from the @beginning. But if that's the case... Not to scare @anyone, but we don't have much time to waste, y'know? 
010EC578: ...Well, whatever. This certainly isn't the first time@you've hidden something from us. More importantly, if @there's other monsters like that around, we can't leave @them to their devices.
010EC608: Hmm... I do sense the space on the 17th floor being @distorted as well. I can't be certain if it's due to a@monster like this one was, though.
010EC740: I was quite sure that the tear in space I sensed after@we defeated that gooey monster was around here... But @now that we're close, I can't feel anything at all. 
010EC7C8: What? Someone who's strong enough to make it this far,@but not known by either Reimu or Marisa...? I can't @believe it... 
010EC868: Erm, maybe... Mari always had some strange ability to @see weird things like barriers and such, but she@recently found out she could touch them too... We ended @up experimenting with her touching a barrier close to @the university... 
010EC948: You don't need to worry too much. Mari was probably @warped somewhere around Gensokyo, like you. Given that@you appeared within this Great Tree, it's highly likely @that she's been thrown somewhere here as well.
010ECA08: Now now, don't scare her so much. Let's put her up at @the inn in the village for now, then go save that Mari@girl for her, shall we? We'd be wandering around the@Great Tree anyway, regardless.
010ED1A0: It doesn't say anything about treasure. I am getting a@really strange feeling from what's up ahead, though. If @it's a test of strength, then we might as well take it. 
