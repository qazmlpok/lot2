00FC66F8: Reimu, do ya happen to know what's that?
00FC73A0: What's that?
00FD2304: Oh. Marisa, just in front of us...
00FD91D8: This is... Byakuren's sutra scroll? 
00FD9BB8: Well, I guess Yuyuko's always been that kind of @character...
00FDE5D0: ...Oh? Who is it that called me just now? 
00FDEC38: Hm? Huh...? 
00FDEFD4: Yes, loud and clear. I can't see you though.
00FDEFFC: ...Nitori, what's that hand for?
00FDFED4: I dunno, she says...
00FDFF2C: A strange sensation? Like what? 
00FE4BD8: My oh my, long time no see. It took longer than I @expected. 
00FE5F30: Ummm, where was that place again...?
00FE70D0: You say "a rare opportunity," but this is just to show@off your power as the Netherworld's master... 
00FECBA8: No, it's just that I leave all the mapping and knowing@where we're going to Yukari. It's quite the weak point@for me. 
00FED058: Who will you side with? 
00FEF878: The monster became enveloped in light and shrunk until@it became an entirely different shape altogether. 
00FF24AC: Sound? I'm only hearin' the sound of flames...
00FF3034: Geez, Lady Yuyuko... Are you satisfied now? 
00FF7D40: Heey, Yuyuko, over here! Turn around just to your left. 
00FF7D98: Stay safe, Yuyuko.
00FF84A8: Don't be so stuffy. Let's go. 
00FF9FD4: Yeaaa...... 
00FFBA98: So before anything, can we have a light brawl against @each other? 
00FFC38C: Oh, you're the workshop's kappa. Well, it's just as you @can see.
00FFC62C: It became small enough to fit into my palm. 
00FFCC64: ...No, I hear something now. It's like the sound of @someone fightin'. 
00FFE8B8: Yuyuko, weren't ya together with Yukari? What happened@to her? 
00FFF240: You'll become a black mouse. The only way you can get us@into any more trouble would be if you were to wear a@pair of red pants too.
00FFFED0: Well then, like we talked about earlier, I wanted to@join you, however...
01000078: We had to turn around in circles on the floor above...@Definitely took a lot more time than we thought.
010009A4: No, more like, something I don't understand. But the@sensation is really strong. 
01001C78: Hyaaa, your skill has certainly increased. I'm happy, @you've grown very powerful even since you came here,@Youmu.
01002B60: The temperature needs to be from %d.%d to %d.%d for the @warp to work. 
01003690: Look at that, there's more and more fire making this@place even more terrible. I really don't think it's a @good idea to go in there. 
010048D8: Yes, quite. I'm getting tired alone, so I think I'll@join you for now. Please treat me well~.
01005030: Ummm....... I dunno?
010055E8: ...However? 
01005AA4: Hehe, it works every time. Now then, let's start right@now...
01005AE0: Wait a bit. Ummmmm......
01006620: ...Well, at least it's not our wallet that's suffering. 
01007728: My my, don't be so stubborn. Now then, let's go!
0100C560: Well, didn't your papa and mama tell you that you have@to make sure you're properly rewarded when you do @something no one else can do? ...Eh, they didn't? Sure@they did, hmm?
0100D828: ......Uugh. Fine then, seems like I'll have to pay! Ah@crap, with that second-hand shop owner, they are really @too many cheapskates in here. 
0100E240: ...Got it. Here you go. 
0100E8F8: For real!? I would be really grateful if you could do @it, please... 
0100F1B4: Eh? Ah, yeah, there was somethin' like that...
0100F374: Ah, Yuyuko? 
0100F420: Yes yes, you too. I'll be waiti~ing.
01012EA8: Eh? Well, even if you say there's something inside... 
010134B8: Don't worry, I'll be ok one way or another. Now then, @I'll look for a place to stay safe on this side and @wait for you. Do try to hurry!
010137E4: Hhng, but there was a reaction... 
01013D80: ...It's like before. My dowsing rod has a strong@reaction to something inside here.
01014708: Hehehe, what weak flames. With the fire-proof clothes @that I've developed, I should be able to get whatever @your dowsing rod reacted to.
010160E8: You mean, that monster's job was to protect that seal?@That's your take on it? 
01016B48: Well, let's go back and start looking. Reimu, tell us @where that place is.
010172B8: Phew... They've quite grown, I can't be careless against@these small fries anymore...
01017490: Well, we were together until just a while ago... Did you@guys go to the 15th floor?
010176E4: Hey hey, can ya hear us?
01018068: When we came here, wasn't there a place where "we @couldn't proceed without the Platycodon Seal," right? 
010183D0: Oh my, Mousey, what's the problem?
01019204: Well, I've got a bad feeling about this.
0101971C: Huh? Reimu, can you wait a bit? 
01019750: Hm? Is something wrong, Nazrin? 
010198B4: Ah, what's that monster? It ain't like the small fries@we saw 'round here. 
01019A28: Ah, you don't need to worry. I'm not demanding anything @from you and the rest. This is private business between @me and Mousey here. 
01019BC8: Yea, we're hearing this from the other side of this @dead-end... 
01019C28: Looks like we can see a bit from this side from that gap@in the trees. Reimu, can't ya see somethin'?
01019D9C: Uugh, I get it, damnit... 
01019DD8: ...? Ah, oh my, oh my. Is it who I think it is on the @other side of the wall? Is that you and your group, @Reimu?
01019F10: ...Hm? Wait a bit Marisa, didn't you hear a sound?
0101A090: Anyway, Yuyuko, are you ok by yourself? We heard you@speaking to yourself just then, saying it was slowly@getting harder. 
0101A158: We'll welcome you for sure... But we're not even sure we@can even meet up with ya in the first place. How do we@get to where you are? 
0101A290: Well, we'll wander a bit, so can we find a way to get to@you. Are you gonna be alright 'til then?
0101A340: Thanks for the tip. 
0101A448: Good. We'll try our best on this. 
0101A538: ...I really want to know why you and all the others @always say that kind of stuff when we meet. 
0101A690: Now now, it's been so long since we've had a battle,@hasn't it? This would be a rare opportunity.
0101A6F4: Well, this exchange was done quick... 
0101A8A0: Yea, for some reason, I get a really strange sensation@from this monster...
0101AAFC: ......So that huge monster... 
0101AB50: Ummm, what's that?
0101ABBC: Uuuuh, this is......
0101ADA8: I think so. Probably. 
0104B1C0: You guys always complain about others picking fights@with each other, but you're just as belligerent as@them... 
0109D6A0: This is all that was on the floor. Now, let's return to @the village so we can talk money, shall we? 
010EC3C0: It's filled with pitfalls, I got caught in one and fell @down to this floor. I lost sight of Yukari because of @this. If you go to the 15th floor, make sure to watch @your step.  
010EC458: Mmm, so you were listening... I suppose it would be time@for me to join up with you. After being separated from@Yukari, wandering this place alone has gotten a tad bit @lonely. 
