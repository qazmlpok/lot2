00FC9A48: ... 
00FCC8E4: ......... 
00FCE714: ............
00FD03CC: Ahaha, I guess so.
00FD06C4: What, is that your intuition speaking?
00FD11CC: Ah... Ahhh... 
00FD1900: And that's when Byakuren invited you. 
00FD1B80: Cheeeeeeeeeeeeeeeeeeeen!
00FD1FE4: Is that a chink in her armor that we can exploit? 
00FD2148: It is I.
00FD215C: Yeah... 
00FD6138: Oh my, it isn't?
00FD7518: Huh? Byakuren, you know about that? 
00FD8268: Right, thanks for the info! 
00FD8318: ...Huh? This is Lady Ran's barrier. 
00FD83C0: Yay! Lady Ran, thank you! 
00FDA8C4: Is that really something to laugh about...? 
00FDB54C: (...Wait, what's going on? My body is...) 
00FDBA20: ......Did I just step on it?
00FDC3BC: Which means?
00FDC448: ......? 
00FDC6DC: Yeah, we know. We saw Eiki earlier. 
00FDC720: Well, Chen? Know anything about this? 
00FDD880: Figured... Ahh, will I never experience such bliss@again...
00FDDC50: What are you, some kinda drug lord~? Why you shrine @maiden... Gwaah...
00FDDFA8: Hmph, I was thinking of leaving you to your own devices @for a while until you're actually a match for me... 
00FDE750: Reimu, Marisa. Our apologies for interrupting the heated@moment, but we shall be joining the fray. 
00FDF2CC: So unfaaaair! 
00FDF680: I don't have the time to waste on lowly servants. I @shall be excusing myself now! 
00FDFC6C: My intuition... You could say that. The rest of you @didn't feel it? 
00FE0210: From those words, how did you manage to arrive to the @conclusion that attacking Reimu's group would be the@best course of action?
00FE0900: Yes, that's right... *sigh* Um, I know I've caused a lot@of trouble... 
00FE0A4C: There, you see! La-dy Ran, I'm become pretty strong, you@know? 
00FE17D0: Yeaaah. 
00FE252C: *sigh*... 
00FE2808: She was hard to rely on at the start, but she's become a@full-fledged exploration party member now. It'll be @rough on us if ya drag one of our members away. 
00FEA0B4: Nuh-uh. Didn't we say it just now? You get one cup per@exploration run.
00FEAF68: Yeah. Let's get searchin' for that Tenshi.
00FEC0CC: ......Hm? Is it me, or is Tenshi getting even weirder?
00FEC3DC: I guess that's true. Let's go then. 
00FED174: Lady Ran, wait! Nooooooooooo! 
00FEE408: Oh? Oh my.
00FEE570: ......? That was... 
00FEE79C: Huh, you serious? 
00FEE86C: Hm? ...Oh, there's someone standing there and lookin' @real imposing.  
00FEE900: Do we look like we arrived to assist you? If we do not, @then naturally we have come to stop you.
00FEFE20: Huh?
00FF0890: (My whole body is going numb...? Urgh, what is this!? @Does this mean my body's still not used to the@Ame-no-Murakumo's power...!?) 
00FF0C40: I can't go on without that kind of sake anymore... Ahh, @luxury truly spoils an oni... 
00FF13B0: No we don't. Do you know expensive it was?
00FF1664: How unfair... You know I can't say no to that~! 
00FF1860: Well, Lady Tenshi always had her strange points...
00FF1B6C: Hmm, I suppose your objective is to stop the celestial@holding the Ame-no-Murakumo?
00FF1C44: How's that! We're a lot better than last time.
00FF1EB0: Chen!! Are you okay!? Did those nefarious villains who@kidnapped you mistreat you in any way!? 
00FF20CC: ...Where? I ain't too familiar with barriers. 
00FF2138: Hmph, good! I'll use my power to crush all of you at@once!!
00FF21E8: ...Hmph, not bad for a bunch of commoners taking on a @celestial holding a divine sword. 
00FF23F0: Good day, everyone. I am glad we made it in time, @barely. 
00FF252C: Ahhh yes, I'm sorry... Er, so, what do you want to say? 
00FF29A0: Hm. This root desperately crying out for attention looks@familiar. 
00FF2A98: Well then, Shiki and I shall continue our pursuit of the@celestial.
00FF2B88: Those youkai have no respect though, honestly. Byakuren,@please make them stop referring to my shrine as some@gathering place for youkai. 
00FF2F1C: Yeah, it's right here. Not that I know what it's for. 
00FF3060: With all three of our groups chasing the celestial, we@are sure to find her as well. I shall take my leave,@then. 
00FF3190: The priest of Myouren Temple... Byakuren, you mean? @She's here too? 
00FF3414: We'll find out once we cut it. So allow me... 
00FF3580: Of course it isn't! Those youkai just keep coming @continuously, and because of them, human worshippers@keep their distance. Honestly...  
00FF35E4: It's Lady Ran's danmaku battle barrier. It's like the @one near the entrance.
00FF37E8: I will rip your bodies to shreds! Consider this what@little repayment you can offer for kidnapping my@precious Chen!
00FF38BC: Which means?
00FF3B60: ...Hmm, what could be going on? I sense far more divine @energy than before. 
00FF3E18: Why would I care!? You'll regret having a laughable @dream that you could defeat me! Me, the controller of @all of Heaven!  
00FF3FCC: Oooooooh.... I-It wasn't half, more like one-third... 
00FF40C0: Lady Ran, let's go together, okay? I wanna be with you@too...
00FF4330: ...Fine, let me come along with you. I'll be in your@care. 
00FF47EC: ...Huh!? W-What!? 
00FF4BB8: They did? 
00FF52FC: ...Ah, Marisa! Look there!
00FF53AC: Hm, this barrier... 
00FF53C0: Could this be a root from the plant blocking the path @near the entrance? It feels like the other roots we've@seen. 
00FF5A00: Hohohoho! Reimu and Marisa! You have done well to repel @the fearsome attack of my servant Iku and make your way @here! 
00FF5D18: ...When would that be? From what I saw, she was drunk on@her own power from the start till the end.
00FF6064: Which means?
00FF60BC: Yes, that sounds like the way to go.
00FF63B0: Whenever you use that kind of tone, it generally means@lecture time for me...
00FF6608: Well yes, that's exactly what I was aimin' for. 
00FF68DC: ...It has been a long time, Reimu and Marisa. And @Komachi as well.
00FF6964: Oi, open your eyes. It's me and the Hakurei shrine@maiden. 
00FF6AD8: Ooooh... I-it's called sampling, Chen... It's to ensure @the taste is good...
00FF6CF0: Ahhh, stooooooooop! ...I really am sorry... Chen, let's @go back to Mayohiga...  
00FF70D0: This is the sword of gods, and its only rightful owner@is me, the one to who the gods kneel! Gensokyo has@delivered this sword before me! 
00FF7FA0: Barriers are my speciality after all. Anyway, let's keep@going.
00FF84E8: Byakuren! And Eiki too! 
00FF88A0: Yes. It was an offer I could not refuse. So as spoken,@we will continue to pursue our objectives separately. @Komachi!
00FF9DD8: ...Why are you even apologizing?
00FFA8F4: Marisa, there's a barrier right ahead!
00FFA934: Your love for fried tofu sure hasn't changed... 
00FFA9E0: ......Completely found out. 
00FFAA98: You wanna talk about unfair? How about the oni who@drinks all those super-expensive sake in one swig then@runs away without repaying the favor, or even a word of @thanks? 
00FFAEA8: My, I see.
00FFB1A8: No can do. If we give it to you, you'll empty it in a @flash and run off.
00FFB914: Yeah. Wasn't she clearly panicking at the end?
00FFBCEC: Hm? ....Ahhh! 
00FFBDD8: Good grief! Lady Tenshi!
00FFC3C0: Perhaps drawing out that much power from the@Ame-no-Murakumo places a huge burden on her, so if she@tries to do it too much... Perhaps we have a bigger @chance of victory than we thought.  
00FFC564: Seriously, she's so strong, it's crazy... Owowow... 
00FFC854: Got it. Be careful! 
00FFCA34: That silhouette can only belong to... 
00FFCAEC: ....Eh... Lady Shiki? 
00FFCC54: Eeeep!? 
00FFCE30: C'mon, vouch for me here! 
00FFD17C: Are you now~? 
00FFD2A8: I dunno...
00FFD380: So we're not letting Chen go. 
00FFD5C4: Huh? K-kidnapped...?
00FFE0A0: Don't bother. She's not going to listen.
00FFE434: ......On your knees.
00FFEBDC: Yep. So let's hurry our search for Tenshi.
00FFF678: What's with this thicket? It's blocking our way.
00FFF8C0: Grrr....
00FFFB2C: Enough complaints. Let's root out all the roots we find @here. 
01000008: Oh, is that so? In that case, it'll be safest to get rid@of this barrier. It'll be a problem if Ran gets any @stronger, when she's already strong enough. 
010001B4: Urrrrrrgh.
0100021C: Hmm, that voice should belong to... 
010003B0: Yes!
01000F90: Ohohohoho, mere servants like you desire this divine@sword!? How foolish!
010013D0: I shall say this in front of Reimu and her group as @well. If you ignore my orders and attack them again, I@will not overlook it so easily. 
01001818: Hmmm... This is definitely beneficial information.
0100221C: Yes...
01002A24: I will rip your bodies to shreds! Consider this what@little repayment you can offer for kidnapping my@precious Chen!
01002C58: Once you give a dog milk with a little sugar inside,@that dog can't drink milk without sugar anymore. It's @like that.  
01002CD8: Hrm... If there's stuff we don't know hidin' in the @background, then the best way we've got to expose that@is to beat up the one who's standin' in the @foreground... That celestial. 
01002D78: Do you have any more of that last sake? I can't forget@it's taste~...  
01002F30: Ehh? Sake even better than that? How's that even@possible!? Gimme! 
01003130: How about this, then... You join our Great Tree @exploration party, and for each exploration run we do,@you get one small cup of this sake. 
01003190: In a way, yes. Due to my training as a priest, I would@notice the existence and movement of a sword possessing @such divinely power regardless of whether I wanted to @or not. 
01003218: Mmhm. I'm not sure if we're a match for her yet, but@we've definitely gotten more used to fighting since our @last encounter. We'll give her a better fight this@time. 
01003320: In other words, we're collecting on the debt you owe us.@So how about it, Suika? If you want this sake, join us. 
010033C8: Yes, that's the big problem. We really need to do @something about it... 
01003450: All right, I surrender to you all. Now hand the sake@over! 
010034E0: Furthermore, I am quite well-known amongst the youkai at@Myouren Temple by now. I have overheard from them about @the conversations at the Hakurei Shrine regarding a @certain miraculous and revered sword already. 
010035C0: Ahahaha, good job keeping it secret, eh? Guess it was @always a tall order keepin' anything from someone on@Yukari or Byakuren's level though.
01003648: I want to drink sake... 
01003674: What are you trying to say? 
01003790: That's because you aren't treating your duties@seriously. I keep hearing about how "The Hakurei Shrine @has been taken over by youkai" at the village all the @time. 
01003848: Anyway, if you're searching for the celestial and the @Ame-no-Murakumo as well, would you like to join us? 
010038F0: Ah, yeah. Let's stick together. It'd be huge to have@someone as strong as you with us. 
010039C0: Mmm... Thank you for the invitation, but I am travelling@with the Enma. Though we are acting separately at the @moment. 
01003A90: Enma? Enma... You mean, my boss? Whoa, Lady Shiki's here@too?  
01003AF8: I requested for her help, as I did not believe that this@was a matter I could handle alone. She seemed quite @disconcerted by this Great Tree as well, so she was @quite open to my petition for assistance. 
01003BA8: You have indeed grown stronger in such a short time.@Which means I'll have to use a bit more of my power!
01003C30: Going by how much you're always slackin' and how much @she's always lecturin' you, that's hardly surprising... 
01003C88: When searching for a single celestial in such a massive @tree, even when we catch up to her, travelling as one @group might make it easier for the target to escape.@For now, I believe it most effective to keep apart@while searching for her.
01003D98: (It pains me to do this against a bunch of fodder like@them, but I should retreat for safety's sake...)
01003E08: And you and Eiki shouldn't have any problems with the @enemies 'round here.
01003E60: Although there are many details that we lack... Why did @this Great Tree grow shortly after that celestial @acquired the Ame-no-Murakumo? And how she was able to @extract such power from the Ame-no-Muraumo? These all @remain mysteries. 
01003F20: ...Speaking of that, Yukari and that ghost queen said @something similar too.
01003F68: I'm the type of person who remembers those tiny details.@They said something like how a celestial shouldn't be @able to draw out that kind of power from the@Ame-no-Murakumo.
01004098: Tenshi herself explained it by saying that the@celestials possessed power close to the gods, or@something like that...  
010040F0: That is not possible. No matter how much training the @celestials undergo, drawing out the Ame-no-Murakumo's @power requires strength several orders of magnitude @above their peak. 
010041B0: Even I can tell that. Geez, so that wasn't her limit? 
01004250: There's no way that strength could be from Tenshi alone.@The only real possibility's that she's getting the@strength from the Ame-no-Murakumo somehow.
010042F8: That celestial said that the Great Tree grew because@it's her who has the sword though. If we're to believe@her, that means the tree is actually borne from the @celestial's own power...
01004388: By following that logic, that means that celestial was@chosen by the Ame-no-Murakumo to be the one who @controls the land?  
01004440: To perform such a complicated deed would require the@Ame-no-Murakumo to acknowledge someone as its true@wielder, so that that person can make full use of its @power.
010044B0: Looking at the current situation, that would be the most@fitting explanation... But that cannot happen so@suddenly. I believe that we must be missing some pieces @of the puzzle.  
010045AC: ...Hmm? There's some sort of barrier in the direction @we're heading.
010045E8: Hmm, that Tenshi... If she hasn't really mastered the @Ame-no-Murakumo, then how did she get so much power?
01004680: This boggles the mind... How can she evoke such power @from the Ame-no-Murakumo...?  
010046F8: Though I suppose this is not a situation I should keep@quiet about... And Reimu has realized half of it@anyway. 
01004760: Hmm... If that is true, then we should continue @challenging the celestial. Even if she defeats us every @time, it would not be meaningless.  
010047E0: As we said just now, there's no point in pondering@things that we don't know yet. Let's hurry on so that @we can find the answer as soon as possible. 
0100485C: Hello there. I see you have come here as well.
01004930: Who are you calling your servant? I am certainly not one@of yours. 
010049B0: Reimu, that golden-colored sword that Lady Tenshi is@holding... Would that be the Ame-no-Murakumo? 
01004A40: After the second battle ended, the celestial felt a @sudden and severe abnormality in her body, and withdrew @based on that. She was in quite a panic from that @situation.  
01004C78: And here I am, fulfilling the great responsibilities@that come with being justice while mere commoners who @don't know their place dare to criticize me! How@absolutely absurd!  
01004CF8: Hmm, yeah, she's pretty weird now. Was she always this@type of character?  
01004DD8: But if you're getting in the way of my duty to control@Heaven, then I will destroy you right now!
01004E70: This is pointless. She's way too hyped up for words to@have any effect. Is it a full moon tonight or @somethin'?  
01004F88: Relaxed enough to crack jokes, aren't you? I'll give you@plenty to think about right now!
01005060: ...Possibly. The celestial herself did not know why the @situation turned out as it did. 
010050F8: ...Really? That's weird. All of Ran's barriers up to now@have been those that only Chen can sense, right? How@come Reimu can sense this one?  
010051C8: Hmm... This is also Ran's barrier, but it might be of a @different type and usage than those that were scattered @all over the lower floors, perhaps? 
01005480: Don't worry, we can use the help. Tenshi, if you want to@come at us again, now's the time. 
01005538: Ugh... We should have taken the advice of Reimu's group @more seriously... 
01005598: Ah-ha-ha-ha-ha-ha-ha! Look at you two now! So much for@all that fanfare! 
0100566C: She's glowin' with some light... I'm getting a real bad @feeling 'bout this. 
01005778: Oh.... Oh-ho-ho-ho! Didn't I tell you? You cannot defeat@me! 
01005808: ...Who's there!?
01005820: I should be able to, somehow or other. Stand back,@please. Mm, paste a charm here and... 
01005928: Do learn from your experience and don't even try to @fight me again! Ah-ha-ha-ha-ha-ha!
01005A18: Mm, about when she beat us all up, including you and@Byakuren? Yeah, around there. 
01005B08: Up till then, she was always giving off a really@overbearing atmosphere, but it suddenly turned into one @of disturbance and worry... Or at least, that's what I@felt at that point. 
01005B80: Mm, I do not have much experience in that field, so @noticing the subtleties of a celestial's feelings is@not quite my forte... 
01005C18: Hmm... Satori, what do you think about that situation?@You were reading her mind, right? 
01005CB8: Who are you two? Are you trying to get in my way too? 
01005CE8: ...I did ask to stop using my power in such ways, did I @not?
01005DA0: *sigh*... I am in a hurry, so I shall move on ahead. Do @take care of yourselves.
01005E24: S-She is strong...
01005E70: Lady Shiki, are you okay? 
01005E90: Hmm... So the ghosts are coming to this Great Tree? Why @are they doing that?
01005F44: ......Hm? 
01006040: Ummm... I don't know about the ones on the lower floors,@but I think I know about this one.
010060D4: Ergh, damnit... 
01006154: ...Tenshi was behaving rather strangely, was she not? 
01006260: Heheheh, now that we know your weakness, your luck's ran@out, Tenshi. We're gonna hunt you down. 
01006398: That's what I'm hoping for. And besides, that @celestial's now got Yukari, Yuyuko, Byakuren and Eiki @all chasing after her.
01006430: It's become quite the major incident. Though I suppose@it would have been that the moment the Ame-no-Murakumo@was confirmed to be involved... 
010064B8: Anyway, we've got victory in our sights now. All the@more reason to keep our spirits up and go on. 
01006520: Komachi, do you recall what was the first thing I said@to you? I had said to lend your strength to Reimu and @her group.
0100666C: Ugh... Oooh...
010067A4: We shall take our leave, then. Do excuse us.
01006810: I suppose so. If this plant's the same as the one in the@floor below, finding a similar root and cutting that@should cause this plant to wither away. 
010068CC: Mmm... Guess we've found a silver lining in the dark@cloud?
01006908: And while we don't know what Ran's doing erecting @barriers all over the Great Tree, it'll be best to get@rid of these and chip away at her power. Reimu, can you @do that?  
010069B0: Yes. We decided to act separately for now, to find the@means to remove the vegetation obstructing the pathway@near the entrance of the floor. She should be somewhere @else on this floor. 
01006A58: Ahhh, I'm sorry, I'm sorry! I don't know what I'm sorry @for, but I'm sorry! 
01006AB0: Ahhh, roots, roots, the root of it all is roots. Rooting@around the place for treasures is way more exciting @than rooting around the place for roots.  
01006B18: This is one of those barriers I can't see. Which means@that this barrier that Marisa stepped on isn't a battle @support barrier, but more like one of the barriers in @the lower floors. 
01006BC0: Don't be so selfish. The upper floors are full of @danger, won't you just be a burden? 
01006CA0: Yeah! 
01006CE8: So it really is lecturing time... Oooh, I'm sorry, I'm@sorry... Wait, how did you know that in the first @place?
01006E00: Yeah, why are you here anyway? And alone at that. 
01006F30: Ahh, that's right, you were paired up with Byakuren. We @met her a little earlier. 
01007038: My, you met the priest? Then I suppose there is no need @for any further explanations. 
010070C8: So she's inside here too... I suppose she'd naturally be@bothered by this incident, but the whole matter is@blowing up into something really huge.
010071F0: By the way, Eiki. You said that this Great Tree has @become a source of trouble for you? What kinda trouble@do you mean, exactly? 
01007364: Hyah! 
010074D0: I'm sorry, I'm sorry! I'm already following them with @the utmost of obedience...
01007548: Ooh, nice and clean.
01007598: ...Huh? Marisa, look over there.
010075C0: If you're so worried about her, then you should come@along with us. No one else should be able to get into @Mayohiga anyway, so it'll be okay to leave it empty.  
010078D8: We can leave that to Eiki. Besides, resolving whatever's@happening with this Great Tree should deal with that as @well. 
010079FC: Hahhh!
01007A24: Ohhhh.
01007AF8: So we should get rid of this one for now. Reimu, if you @would.
01007B28: Mm, leave it to me. Hyah. 
01007B60: Hm, efficient as ever.
01007B90: Working with barriers is my forte after all. Let's move @on. 
01007BC8: ......? .......Ah!
01007D40: Let's check back on that blocked pathway later. @Hopefully this will allow us to pass through it.
01007EB8: ...And what's it supposed to do...? 
01008038: ......Hm? Wait, I can hear something... 
010080FC: ........eeeeen......
01008208: ...You need to eat half of the fried tofu for that...?
010082B8: Ah, La-dy Ran!? 
010082D0: ......*sigh* Oh well... I do owe you all a lot after@all, and seeing Chen grow so much in such a short time@does make me happy... 
01008464: You evildoers, how dare you! You won't get away with@this! 
010085F8: Um, we've had Chen with us all the while, so how about@asking us first?
010086A0: (......This scene looks familiar...)
0100872C: Cheeeeeeeeeeeeeeeeeeeen!
01008888: No, no, no! I don't wanna go back yet!
01008928: And you still need to make it up to us for attacking us @outta the blue like that. 
01008998: Ooh... Marisa, that's cruel. You knew if you played that@card, I wouldn't have any counter to it...
01008AB0: How nice, increasing our numbers is certainly a welcome @development. We'll be counting on you, Ran. 
01043D10: It means exactly what I said~! Ahh, I'm really out of @fuel... 
010536B8: My my, my dear opponents, you seem very well acquainted @with the ground now! I suppose I should go the extra@mile to ensure you don't think of fighting me aga...  
010A0830: Ooooooh... No helping it, I'm getting tired of climbing @this place alone anyway...
010A0878: You all just aren't on my level, that's all! Now that I @have the Ame-no-Murakumo, I control all of Gensokyo's @Heaven! I am justice! 
010A08F8: Hmm, I thought that's why she sent me out in the first@place... If she's here herself too, then she really @doesn't trust me much.
010A09A8: But that Tenshi's definitely way stronger ever since she@got her hands on that sword. Even as one whole group, @we weren't a match for that one celestial.  
010A0A50: ...La-dy Ran, while you were in dreamland nibbling at @the fried tofu while you were preparing for lunch, the@barrier got a lot weaker. 
010D9FA0: We ain't got that sake anymore... But we've got some@that's even more expensive right here. It's called@"Pure HQ Sake - Butterfly." 
010DA020: Hmm, yeah, you have a point. Yukari and Yuyuko seem to@be chasing Tenshi down as well. With the 3 of us@working separately, we should definitely be able to @find Tenshi sooner rather than later. 
010DA0D0: That cannot be... While it is true that the @Ame-no-Murakumo has the power to control the land, and@a potential evil application of it would be to cause@something like the Great Tree to grow in an instant...
010DA178: Getting even stronger is just unfair. That fight earlier@was hard enough, can't you show a little restraint? 
010DA1F8: I see, so this is the Ame-no-Murakumo that you spoke of?@It does possess tremendous power indeed... Goodness me, @when did such an object flow to Gensokyo? 
010DA290: Indeed it is. Shall we maintain the status quo? We, @Reimu's group and Yukari's group shall continue to@chase after the celestial, and exhaust her by forcing @her to constantly evoke power from the@Ame-no-Murakumo?
010DA328: Thank you. All of you should be careful as well. The@celestial is one matter, but the monsters around the@area are constantly getting stronger as well. 
010DA3A8: It's a barrier meant for use in battles. It increases @La-dy Ran's strength while it's active. If she sets up@several of these barriers around her before a danmaku @battle, she'll get really really strong.
010DA428: That is not specialized for you. You should pay @attention to what people say instead. For all the @unnecessary conversations you have with the ghosts who@you ferry across the Sanzu River, your ability to @listen to me has not improved.  
010DA4B8: ...*sigh* I shall leave that matter aside, since you are@at least properly serving under Reimu and her group @now. Furthermore, I would not come here specifically to @lecture you.
010DA548: This Great Tree has become a source of trouble for me.@Thus, when the priest of Myouren Temple requested for @my assistance, I accepted on the reasoning that it@would be an excellent chance to resolve the situation.
010DA5D0: Ahh, yes. I do not understand why, but the ghosts that@were supposed to be waiting to be ferried across the@Sanzu River have been somehow bewitched by this Great @Tree, and fled the Sanzu River to flock to it.  
010DA668: The number of ghosts who escaped were minimal at first, @and could be dealt with by sending Komachi to retrieve@them. But as time went on, the number of ghost escapees @kept multiplying to the point where it can no longer be @ignored.
010DA720: ...Why are you glancing in my direction? It is fastest@to deal with these plants by cutting their roots@instead of the plants themselves. We have already tried @the latter. 
010DA7A8: Um, well, I can explain, you see... I erected a barrier @around Mayohiga, yet Chen disappeared... I thought that @Chen wouldn't be able to cross that barrier herself, so @she must've been kidnapped by some evildoers... 
010DA870: I get it all now. Since you were under the impression @that Chen was kidnapped, you put up barriers all over @the Great Tree in order to ascertain where Chen and her @"kidnappers" were.  
