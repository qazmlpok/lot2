00F74108: Shut it.
00F74820: Why'd ya say that?
00F76B4C: Hm, what? What's wrong? 
00F77C34: Ah. 
00FB4B00: Wahahahaha! 
00FB4EF8: ...Pleased to be working with you...
00FC6D78: Is this better? 
00FC9A48: ... 
00FCB230: I have heard of it, at least. 
00FCB5FC: See?
00FCBC80: Ah, that voice is...
00FCBE98: Ah, it's you. Suwako. 
00FCC208: ......
00FCE6D0: Yes?
00FCEBE0: ...Hm?
00FD0014: Sanae came as far as this place too?
00FD0030: Mm? 
00FD04E8: Huuuuuh, no, look, hm, it's, like, to repay my debt to@you for bringing me these things? 
00FD175C: Hm? What to do... 
00FD3C48: Oh well, let's just keep on going.
00FD5A1C: ...... Hey, can we really not step on it? 
00FD5E08: Eh? How did this happen...? 
00FD60F8: Hey, Iku, I'm gonna get angry if you keep this joke up! 
00FD6678: .........Huh? 
00FD70AC: This little one just now, what did it say...
00FD71D0: Hm? 
00FD8224: Ah! Reimu, right here!
00FD8530: As one would expect from Reimu! 
00FD89DC: Now then, we should be going. 
00FD929C: Hm......
00FD99C8: Wait...what? What's going on all of a sudden? 
00FDA3DC: With! Iku Nagae!
00FDA454: Yea, that's the one.
00FDAF44: Eeeh.... Hmm, what are these? 
00FDB278: Hooot! So hooooooot! I'm burning to death~! 
00FDB570: Okuu, lower the output, quickly!
00FDB98C: ...Haa, fine. Now, let's keep going.
00FDC00C: Shut up. Just be silent for a while.
00FDC448: ......? 
00FDC4D8: Of course. Good to be working with you, Iku.
00FDC608: Whoa, that's amazing. There's a floating ball of fire.@It really looks like a small sun. 
00FDCDF4: Really. Are you satisfied now?
00FDD06C: ...Eh, Iku? 
00FDD244: Oh, a voice I don't recognize.
00FDD3BC: ......Eh? 
00FDD3E8: They've been telling us that they still haven't played@enou... No, no, that they're still doing their best to@regain their dignity as gods. 
00FDD690: (Though, right after that, Suwako saw through that and@said "That's just some random bull...") 
00FE17D0: Yeaaah. 
00FE74F0: Hng, too bad... Please bring them to me as soon as@possible. 
00FE7F2C: ...Ah, I see now. It was you. 
00FE8D94: Goddamnit, you always do that kind of stuff so@suddenly. 
00FE91EC: More like, you started the fight. 
00FE9A2C: Likewise. I will be in your care then.
00FEA05C: Even if ya ask us again, ya won't get them any@faster... 
00FEA0E0: What's wrong? 
00FEB8E8: Eh, really? Then, please make a little one! 
00FEBA10: Are you alright? Can you understand us? 
00FECDF0: Aah, they haven't only thought about the fate of the@Moriya Shrine, but their actions will help the fate of@religion for all of Gensokyo! 
00FECED0: Oh, scary.
00FED27C: Hm? There's someone over there. 
00FED548: Ah, yea. That's true... But well, the bath there is so@enjoyable, We should go in together.
00FEDBA4: Dignity as gods...? What do they mean by that, I@wonder? 
00FEDBD4: Ah, so that's how it was. 
00FEE3A8: Hiiiii~~~!
00FEE4A8: ...The Eldest Daughter trashed you? Really? Your story@suddenly stopped being believable...
00FEE83C: Haa, I'm so tired. It's 'bout time we go back to the@inn.  
00FEE9D0: I don't like the previous stratum, I wasn't able to use @my powers.  
00FEEAF4: Oi Reimu, that green shrine maiden wants to take our@items from us.
00FEEC30: Mmm, okay okay. This will be the first and the last @time. 
00FEEC6C: ...I see, if that's correct then... 
00FEEC94: What are you talking about? These are "Symbols of @Faith." You're the one who wanted them, remember? 
00FEEE30: Lady Tenshi?...That's right, Lady Tenshi! I saw her @previously, but what happened with the Eldest Daughter? 
00FEEF38: ...When you answer so obediently, it gives me a really@bad feeling...
00FEEFF0: What's going to happen to the Moriya Shrine if you come @with us? There's nobody there right now.
00FEF288: ...This flower looks kinda gloomy and kinda withering to@me. 
00FEF32C: Oh my, I'm so glad. Well then, take care of me for a@while.
00FEF3B8: But, a person that loves flowers can't be very bad, you @know. Please take care of us! 
00FEF560: It looks like this time, Lady Tenshi will be needing a@great punishment. So please allow me to join you. 
00FEF7D0: Wait Marisa. The state that Iku's in is weird.
00FEFA90: ...Eh? I, I did, to you guys? How can this be... Is it@really true?
00FEFBA8: Now it is our turn to ask. What's going on with Tenshi? 
00FEFD90: (So we can take this to be Tenshi's doing.) 
00FEFE20: Huh?
00FEFE28: Hmmm, this has turned into quite an unexpected@situation...
00FF00E8: Ugh...You really know how to hit a sore spot... 
00FF0418: We heard about it from Kanako. You're doing your best to@get back your dignity as gods and revive your shrine? 
00FF0480: Eeeh, what, really? Am I really someone so easily @forgotten?
00FF09E0: Really!? Fufufu, well then, I look forward to going @together with you.
00FF0A40: That's fine. Wow, that was surprising.
00FF0B3C: Wait, Utsuho! Your power is...
00FF0E54: ...That's how much Sanae has to deal with. For the first@time, I feel bad for that girl. 
00FF1228: I'm sorry, this is... How did I get into such a state?@Why am I on the ground, surrounded by everyone... 
00FF12E8: All of a sudden, the bush withered in front of their@eyes, creating a curved path for Reimu's group to go@through.  
00FF20B4: After that, after that I...?
00FF225C: Ah? 
00FF29D8: Gah.
00FF3698: So you're really saying she has acting weird? I'm @feeling it a bit now that you mention it, but we've got @no choice but to prepare for war first! 
00FF4990: You came out together with her, and then she pushed you @onto us to get past us. 
00FF4B0C: Oh my, we met again.
00FF575C: Oh my, we met again, yea. 
00FF57D8: You wanted 3 of them, right? Too bad for you, we didn't @get them yet. 
00FF6024: (...What is it, Satori?)
00FF6418: She woke up.
00FF6784: I said "Don't." 
00FF6C74: Sanae, catch. 
00FF6F64: I "am" thank you, yea.
00FF7AD0: I have to find those two as quick as possible to get@back home. Marisa, Reimu, did you happen to see any of@those two?
00FF7CD0: ...Hm?
00FF7E1C: What's this plant? It's blocking the path.
00FF7E48: Well, I guess that's ok.
00FF7FD8: Huh, wait now, what's going on? Weren't you in the mood @earlier to help us deal with this situation?
00FF8050: Oh, a face I recognize. 
00FF8144: ...Oh, it's Reimu's group... Good day to you all... 
00FF8520: Oh, so you don't remember anything. 
00FF9C88: "Living for centuries" is impossible for humans, you@know... 
00FF9CC8: You attacked us. It looks like you can't remember what@happened to you.
00FFAD58: Hahaha. Well, that's how things are for me, I don't want@to go back so quickly, so I can't have Sanae catch me.@I came across Sanae around here not too long ago, it's@a good thing she didn't recognize me. 
00FFB0EC: Hmph... 
00FFBDA4: For the flower's whole life, probably.
00FFC1A8: That's right. It's starting to get difficult by myself, @though. Good grief, I'm sure I heard Lady Suwako's@voice around this just a while ago too... 
00FFC26C: They're gonna get caught by Sanae and forced to return@really quickly... 
00FFCB80: That's true... But since Lady Kanako and Lady Suwako are@doing their best, I can't go back home empty-handed @without losing face...
00FFCEA0: Now that you mention that, it was really mysterious. I@really wonder what happened to them.
00FFCEE8: Besides, I'm the goddess responsible of harvest. I can@make flowers healthier, but making them wither is a bit @difficult for me. 
00FFD070: Huh, what was Kanako sayin' again? "The humans of @Gensokyo ain't really devout on the whole faith thing"@or somethin'. 
00FFD3C8: What, something like that exists? 
00FFD4F0: ......Can I come with you too?
00FFD6A4: What a worshipper ya are. 
00FFD818: Since it has come to that, it might be best for me to go@back like a good girl and not disturb those two!
00FFD868: They are rare, but I intend to return back with some of @those items...Say, Reimu, Marisa, do you have 3 of@them? 
00FFDB18: Ah we saw them not too long ago. One on a lower floor,@and one on this floor. There's no chance for them to@return to the shrine though.
00FFDB78: And things like "So we'll go resolve that incident, get @back our dignity, and get a buncha followers and faith@in the process..." Hmm, I guess that's the impression @she gave? The mood she had while saying was kinda like@that. 
00FFDD60: That's true, the baths are nice. We can really get back @into shape after being so weary from all that @exploration.
00FFDDB8: In disheartening times like these, you only have to hold@your heart in your hands and discover the beauty of @nature in this environment. For example, look at this @flower growing on this vine. Isn't it tiny and cute?  
00FFDE50: Shut up. Well, fine, if you're sure you want to come@Sanae. We always give a warm welcome whenever we get a@new member. 
00FFE000: Really now, what's with the bad environment here... It's@all damp, the footing's all slushy, and it feels like @the intense scent of flowers and the mud's odor are @mixing together.  
00FFE0D0: But Minoriko, do we really have to take care of all the @vegetation in this Great Tree?
00FFE1B0: That's it, that's what I'll do. In this Great Tree, I @heard there's a dream-like item called a "Symbol of @Faith" that makes people become devout followers just @from looking at it! 
00FFE348: That's Lady Suwako and Lady Kanako for you! Their @actions have so much thought behind them! 
00FFE498: Huh, how the hell did you see it that way...? Well, I @should keep quiet about it... 
00FFE5B8: That sounds like a good idea. I mean, it's really @difficult being around here by yourself, no?
00FFE798: Hm? ...Ah yeah, there was some mysterious item like @that. We've picked it up before.
00FFE7E8: Ah... This little one, it's simply suffering from lack@of light. Hm, originally, the plants around this place@had bad exposure to sunlight, but the ones around here@seem to have it especially bad. 
00FFE87C: Hmm, I kind of long for the peace and pleasantness of @the previous stratum, or the forest before it...
00FFE8F0: Hm, you're good, Sanae, being able to come here by@yourself. 
00FFE934: ...Eh? Well, that's out of the blue.
00FFE96C: Ahh, good for you!
00FFE9E8: Wha, how disgraceful. I'm only counting on your good@will in the end.  
00FFEA50: Haa, 3 "Symbol of Faith," right? We'll think about it if@we come across any. 
00FFEB60: That's it. Huuuh, it's goes around around this place@just there. And then, after that... 
00FFEC14: Oh, right, right. Another of Ran's gates? 
00FFEC50: Don't, that will only bring us trouble. You're always @like this Marisa, hearing that something is prohibited@just fires you up even more to do it. 
00FFECC8: As expected of you Reimu, you understand me so well.@With that said... 
00FFED40: Ah, oh, so that's what these things are? Hmm, they have @a shady appearance, and they don't seem to manipulate @human faith...
00FFEDB0: Hey you, why are you saying gloomy stuff like being @"shady" the moment you receive them?
00FFEE6C: Do you have them? The "Symbols of Faith?" 
00FFEEB0: Ah, no no, that's not it! I am thank you? Really, I'm @very truly grateful!  
00FFEF50: Uuuh, no no, I thank you! Look, I am very truly thankful@to both of you. 
00FFEFF8: You would think that, naturally. ...Ahh, no point in@complaining.
00FFF040: With the amount of people we've got now, we won't be@able to take it easy at the inn, it's gonna be so @noisy...  
00FFF0F8: You say someone, but there's only one person that I can @think of that could be in such a secluded place.
00FFF190: ...But, I'm not going in with you Marisa. You're just @going to splash water at me with no warning.
00FFF314: I "am" thank you? 
00FFF370: Whoa, aren't you being a bit too harsh on Rinnosuke? In @the end, the bad person is the one who stole it. Let's@not let the story get astray. 
00FFF424: That's right, that's right, that's better.
00FFF470: ...I mean, that story about their Moriya Shrine is the@same as the Hakurei from before. There's no one there @right now too.
00FFF500: Guh... That's fine. We show our face once in a while. 
00FFF5B0: (......That sounds nice...) 
00FFF5D8: ............Sa~ay...
00FFF618: Well, that's 'cuz of this bad man.
00FFF65C: Eh, what's that all of a sudden?
00FFF6B8: ...Wait, that's strange. How were you able to come here @in the first place? The way should've been blocked by @my flowers... 
00FFF770: I have no clue... 
00FFF788: All the plants around here just regenerate after being@destroyed... From the looks of it, there must be a deep @root somewhere else. How troublesome. 
00FFF860: Oh my, it really is. It must be fate that brought us@together. I should be able to do something, as the@goddess of harvest. 
00FFF9E0: Daylight? If you need a sun, I can make one.
00FFFAB8: Well, when you think about it, this girl's power isn't@much different from the sun itself... 
00FFFB60: Whoa. That's really incredible. If it's like this, then @this little one will become healthy in no time. Thank @you!
00FFFBB0: ......I filled this Great Tree with flowers, to make it @my territory. 
00FFFCD0: Well, with the power that I have, there's no way I could@be able to do that. But I can't be indifferent to the @suffering of those children when they are right in@front on me, no?
00FFFD48: Ah, it's true, but that wasn't much of a bother. What @I'm more concerned is how you lost your sanity with @Tenshi manipulating you.  
00FFFE00: *hmph*
00FFFFFC: Hehe. 
010000C8: This ball, how long can it last?
01000108: Wahaha. To say that you're weaker than a single youkai, @that's quite something for one of the eight million @gods. 
01000260: So, that's what happened. That miracle-workin' sword is @now bein' carried by Tenshi. Like, right now. 
010002DC: She said "ah," Marisa. How scary. 
01000374: Uh, me? What about me?
01000420: So, I was thinking of having my way with the whole bunch@of you who disrupted my plans.
01000478: ...But I've decided not to. I'll be joining you guys for@a bit.
010004D0: Don't try to wriggle your way out. Get ready! 
01000558: ...That's what I thought, however. If I'm going to@follow you, then I think I'll test your abilities a bit @first.
010005B0: ...*pant*...
01000610: Eeee~h. First, ya suddenly claim you're gonna be our@ally, then ya suddenly challenge us to battle...? 
01000670: ...Yes, thank you. I guess I'll just have to follow you @quietly now.
010006F0: Gensokyo's heroine, the master of the Great Tree@piercing the heavens! It's the beautiful, dual-wielding @swordswoman, Tenshi-chan! 
0100078C: That girl's seriously incomprehensible... 
010007B0: ......
01000810: We knew it was you the moment we heard your voice. Just @give us back the sword! 
01000958: Oh no, she was always my obedient underling! Now, Iku!@Do it!  
010009E8: My, Reimu, your instincts are as sharp as ever. Well, @whatever. Iku, I'm counting on you! 
01000B18: That's right, we can't help it. Let's postpone our@theorising, and make her experience a bit of pain!
01000B90: ...Ame-no-Murakumo? Why are you asking all of a sudden? @I know about it to a certain extent, like everyone@else... 
01000C50: Looks like you were right, Reimu. Tenshi was controllin'@her with some kinda power.
01000CF4: Yes, it's that voice! 
01000F38: Then it all went dark for a moment, and suddenly, I @heard Lady Tenshi's voice, after that...
01001000: Hm, no, sorry... I cannot recall any events that took @place after that... 
01001080: (I heard the voice of her heart, and I can tell she's @telling the truth. She's not lying, she really can't@remember.)
01001128: ...Hey, this girl really didn't give much thought into@it. No matter how I look at it, she came here just to @fool around.  
010011A0: (It seems so. By the way, I would like you to refrain @from using me as your portable mind-reader.)  
01001200: When we met before on the 4th floor, you had not met, @nor seen Lady Tenshi, correct?
0100134C: .........!
01001370: Tenshi started a fight with us and we got trashed to the@ground. 
01001484: Where am...?
010014F8: I would think it would be the opposite, with you having @gathered such a large amount of powerful comrades and @Lady Tenshi being alone, the result would be as anyone@could expect... 
01001640: Well, we got involved into a bit of a troubling @situation along the way... Iku, have you heard about@the Ame-no-Murakumo?
01001698: A divine sword, offered to the sun goddess Amaterasu by @Susanno, who took it from the tail of the @Yamato-no-Orochi. Then, Amaterasu's grandson, Ninigi, @when descending upon the earth, carried the sword along @with him. That sacred treasure? 
01001768: Fine, let's get back to Tenshi's story. For sure, if@that was the usual Tenshi, I'm sure we woulda had no@problem takin' her on, but... 
01001840: No way... Wha!? Is, is that true!? How did this sword @get into Gensokyo!? I, I had no idea about this...
010018F8: So, even if we lost that one time, there's no way we can@leave that Tenshi alone the way she is... For now,@we've still got some more to go before we can catch @her.
010019C0: True, and I too cannot leave it as it is... With how@things have become, please let me accompany you.  
01001AA8: Right now, with the help of the Ame-no-Murakumo, we've@got no idea how powerful her strength could be. She @turned the tables on us in an amazing way 'cuz of that. 
01001B60: And with the power of the Ame-no-Murakumo in her hands, @she must've manipulated you.
01001C34: After that? 
01001CF4: Nah, it's nothing.
01001E68: Yea, we've already said it, but we've only been defeated@just one time. But the next time won't go that badly. 
01001EC8: No no, first of all, it's Kanako who's been lying about @being diligent. After all, it's Kanako who came here@together with me. 
01002078: ......Ah, right, yea that's it. Yes, that's our aim.
010020C8: Hmm, we were able to go through that bush just a while@ago.
01002144: ...Eh?
01002188: No, that's wrong, that's not it. We've been really@diligent into investigating the tree to resolve the @incident, and not playing around at all. It's true, @it's true.
01002258: Honestly, Kanako actually gave some thought to why she@came here, even though it was a bit twisted. You on the @other hand... 
0100239C: How unpleasant, I'm not a bad person... I'm just the@victim of theft.  
01002444: Of course, we'll welcome you with open arms. Right, @Reimu?
01002470: Saying that we wanted to look good for humans was just a@front she put up for you guys. And that's it, that's@the truth unveiled. 
01002508: Don't ask me what goes through a god's head... I really @can't understand their reasoning. 
01002610: Being human or a god doesn't really matter. Once you've @lived for centuries, you'll become like this too. 
01002840: Hhhhng, I can't stay here any longer! Just keep Sanae @back, I want to reach the top before Kanako most of @all. Well then, this is it for me. Bye bye! 
010028D0: ...Is that so?
01002AB8: Yea, I get it a bit too.
01003064: Ah, wait, wait. Look! 
01003104: But it's not the same here... 
0104F950: She did. Look, we haven't play... I mean, investigated@enough yet. Can't you take Sanae with you, since you're @on good terms with her? You know, since you're both @humans, and both shrine maidens.
010D9900: Huuuh, well, see, how to phrase this... Lady Kanako and @Lady Suwako are still at it, so I'll come with you to @search this place so that I can help those two in the @end!
010D99A8: ...Well, if it isn't Reimu. What kind of business brings@you here? That's a very fun way to pass your time,@coming all the way to a place like this just to start a @fight with me.
010D9A30: I was sure those plants were there to be a nuisance to@us but... While we were making a fuss about how we@couldn't pass them, those flowers suddenly withered and @opened up the way.
010D9AB8: Ah, did I cause those flowers to wither, by any chance? @I don't think I could've done that. That flower was too @strong for me in the first place. You and your flowers@are way out of my league. 
010D9B68: ...Well, Yuuka's behaviour has always been erratic, @practically on the same level as Marisa. I don't really @get what your thoughts, we will welcome you if you want @to join us. 
010D9BF0: Hm, well...There is not much I can say. I noticed Lady@Tenshi on the 8th floor, I chased after her as best I @could, but in the end, she escaped... 
010D9C80: Incidentally, I don't understand what you mean by the @state that I was in... You said that Lady Tenshi was@manipulating me, but the Eldest Daughter never@possessed such a power in the first place...
010D9D18: Look at how confused Iku is now that she knows 'bout the@Ame-no-Murakumo. Ya can't keep trying to defend that@keepin' things like that under cover isn't anything @dangerous. Or do I hafta spell it out for you?
010D9DE8: With the state the Eldest Daughter is in, it doesn't@seem like I alone will be enough to scold her. Since@that is the case, if I wish to put that obstinate Lady@Tenshi back in her place, joining you is the best @course of action. 
010D9E88: You have a strong heart. I am Lady Tenshi's supervisor, @not her servant. I cannot let things stand as they are@now.  
