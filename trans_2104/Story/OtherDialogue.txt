00F74A8C: ......Who knows?
00F74F60: Here. This stone. 
00F7CBC4: Patchouli~ You here?
00F7E128: Oh, she's here. 
00F97168: Eh, really?!
00FC616C: There's nothing to do here. 
00FC8574: I could ask what the heck you two are doing here too. 
00FC897C: You're like a mind-reader, Reimu! 
00FC8BC4: Why, thank you so much for the explanations.
00FC8F6C: (Ugh, she realized it...) 
00FC8F84: What do you do here normally? 
00FC9028: We're back in the village again, huh? 
00FC906C: Well, I'm a main character. It's expected for me to do@this. 
00FC9120: A record? ...That's it? 
00FC92F8: Ah, I want to eat too...
00FC9314: No, there's not really anything to do here at the @moment. 
00FC949C: From me. It's my library, so it shouldn't be a surprise,@right?
00FC9728: Oh, is that so? Proceed.
00FC974C: What do you mean by that? 
00FC9914: Oh, that voice is...
00FC9A48: ... 
00FC9AF0: Yay! There's treasure chests, huh? Now that gets me @excited!
00FC9BC4: Reimu, stop. Hold it. 
00FC9C74: ...What kind of greeting is that? 
00FCA020: Nothing, really.
00FCA168: The donation box is empty as always too. Nothing out of @the ordinary at all.
00FCA1A8: *cough cough* This place is always super moldy every@time I come.  
00FCA208: Nuh-uh, that was all you. 
00FCA378: Hello hello, welcome one and all! How are you doing on@this fine day?  
00FCA494: Aah... now I don't feel like doing anything. I'll just@close the shop and go back. 
00FCA55C: Yeah, we're here for some reason... 
00FCA580: Interesting thing? Let's see. 
00FCA7E4: So basically, we pay money here to get stronger?
00FCA9C0: But if you're not satisfied with just that... Well, @fine. I'll give you this too. 
00FCAAF4: We'll take whatever help you can give. What can you do@for us? 
00FCAC5C: ......It's nice that we're looking around Keine's @school, but...
00FCAD94: Ahem. Shall we stop this line of conversation...? 
00FCB184: Excuse us. We're coming in, Akyuu.
00FCB218: ........................... 
00FCB288: Thank you very much! Then, I think it'll take me about@30 minutes. Feel free to kill some time while I work. 
00FCB410: Yeah, I'm right here. How may I help you? 
00FCB4A0: These are the only humans who'd do something like that, @so relax. 
00FCB4E8: As I thought, the villagers here're more restless than@usual.
00FCB590: You're obviously using Reimu and Marisa as your base for@human behavior... Normal humans are too scared to get @anywhere close to that thing. 
00FCB694: Hmm, lessee... Hmm......? 
00FCB710: But this is surprising. I've never heard of a kappa @workshop in the village before. 
00FCB870: Anyways, why are we here at the shrine? Do we have@something to do here? 
00FCB8B8: Incidents don't get much more obvious than that, so I @figured it was about time the firefighters of Gensokyo@started to act. 
00FCB94C: It'll be too tiring to respond to them, so don't. 
00FCB990: So what. I'm a main character too.
00FCB9AC: Except that there aren't a lot of humans exploring that @tree. 
00FCB9E4: So basically, if we want to look at the bestiary, we can@come here?
00FCBA54: Eh... Ehhhh...
00FCBAE0: Are these threats normal for trying to greet my @customers courteously? The Human Village sure is a@scary place...  
00FCBB50: Akyuu, do you know anything about that thing? 
00FCBB9C: *sigh* I guess we'll have to do some actual legwork to@know more about that thing. 
00FCBBF8: I knew humans loved to talk big when they're not the@ones in danger, but I didn't know youkai did the same @too.  
00FCBC98: No worries there, I knew this would happen! I've been @doing daily maintenance on my gadgets to keep them in @top shape.
00FCBCE8: My, exploration records are important. Especially in@RPGs. 
00FCBD18: Guh. Why'd you wait till now to say that? You should go @get them yourself then. 
00FCBE60: It disappears after using it once? ...How does that even@work? 
00FCBEB0: ...How nice for you...
00FCBF98: However, it seems this key disappears after you use it@once. Since it's not reusuable, please consider your@options carefully before using it.
00FCC14C: Reaaaally? Is that reaaaally the only reason? 
00FCC174: ......What are you trying to say. 
00FCC208: ......
00FCC220: Marisa, you got a paper fan?
00FCC2F8: Ah-ha. With a tree that big, I was worried that its root@or leaves might trash up the shrine. Seems like there @wasn't much to worry about though.  
00FCC400: ......Oh really. Well, fine, let's see... I'll raise@your stats for you. 
00FCC474: Yeah, I can probably fix it. But I'd need materials.
00FCC4C4: Can you fix it? 
00FCC4E0: Of course. You two are the only ones who can remain @calm... or rather, be excited about this situation at @all. Normal people aren't that strong.
00FCC5C4: ...I figured you would be coming anytime now. 
00FCC5E8: Yaaaay! Now I can finally repair the construction @machine!
00FCC618: Moneygrubber... Well, can't be helped. Here, take them. 
00FCC660: ...You really don't beat around the bush, do you. 
00FCC684: Geh, it's the annoying one... 
00FCC704: Always! Here to create equipment? 
00FCC790: If you know about it, then just help us somehow already.@Like giving us some ancient spellbooks, or strong magic @items. We'd welcome those with open arms. 
00FCC840: Well then, let's take it to Kogasa. 
00FCC878: Hah... a workshop whose only customers are the dirt-poor@shrine maiden and her companions... My store is already @looking bleak from the first day... 
00FCC8E4: ......... 
00FCC910: Oooh, that's good! Alright, go ahead. Make it a big one,@stat! 
00FCC960: Well yeah, with this kind of obvious incident going on, @there wouldn't be anyone who'd be taking a faraway trip @or anything. What about it? 
00FCC9D4: What, you look like you wanna say somethin'.
00FCCA08: Tch. But well, we have all the stuff you need. Well,@should I give them to you now?  
00FCCAF8: And we're the type of humans who solve these incidents. @The ways of the world works well if ya think about it.
00FCCB60: I don't have one, and there's none for sale either. @Guess this wrench will have to do.
00FCCBD4: Yeah. *sigh*... we sure spent a lot of effort for her @sake... 
00FCCC0C: ......Dare I ask what the 2nd and 1st ones are...?
00FCCC38: I thought that Kourin out of his store would be like a@kappa out of water... That's really well thought-out. @I'm seeing you in a new light now!
00FCCCC8: W-well, whatever you say. From the looks of the village,@there really are lodging faciltiies popping up@everywhere. 
00FCCD30: I can't tell if you're complimenting me or insulting@me... I guess I'll just treat it as a compliment this @time. 
00FCCDC0: Maybe you're the mind-reading one... Yeah, we've got it.@Here ya go. 
00FCCE0C: We've got all five of 'em here too. Take 'em. 
00FCCE38: Yes, it was no easy feat either. What about it? 
00FCCE68: Ehh? You're kidding right?
00FCCE78: We're just about dealing with it, though. 
00FCCF40: For those of them who're not used to fighting, they'll@flock to my workshop in search of all sorts of goods. @How's that? Perfect plan right? 
00FCCFBC: Welcome!
00FCD014: No. 
00FCD038: But of course. I started this shop just now. With a @fun-looking tree popping out of nowhere like that,@there must be a lot of humans exploring it. 
00FCD0F0: ...And that's where you come into play. When you talk @about making things in Gensokyo, the kappa are the@first thing to come to mind.
00FCD16C: What's with this broken umbrella? 
00FCD198: Weeeeelcooome!
00FCD1F0: Sorry to say, but we're not buying anything right now.@About this umbrella...
00FCD254: Heeey, Akyuu! You there~? 
00FCD2D0: Hey, don't worry about it. We're just the right @customers for you. So make sure you prepare the best@for us. 
00FCD328: ...Ahh, I can see where this is going. What do you need @us to get?  
00FCD3A8: Very perfect plan.
00FCD458: An Insect Exoskeleton and a Great Tree's Leaf... And@let's see now... 120 money I guess? 
00FCD558: Remember when you asked me to find a way to use all @those materials? That's the request I'm talking about.
00FCD600: Hmm, right. It seems interesting, so how about I handle @them for you? The troublesome part. 
00FCD654: And if you don't equip those weapons and armor! 
00FCD69C: I don't want to teach you two either... 
00FCD750: Eh? Er, no, we weren't thinking of anything like that @exactly.
00FCD7B0: Hmph, this is nothing for a kappa! It's child's play for@me. 
00FCD8A4: You're only preparing now? Your machines don't get dusty@or anything?  
00FCD908: There's nothing interesting going on in a school for@you... Or perhaps you want to study?  
00FCD974: Well, it can't be helped for now. We're not used to @exploring after all.
00FCDAAC: Yes, hello there. May I help you? 
00FCDAE0: I'll be waiting for the good news~. 
00FCDB00: Don't you have someone in your party way more @experienced at running an items store than me? So @what's it do, bro?
00FCDB64: What's this?
00FCDB70: It's a key to open locked treasure boxes in that tree.@Since I can't fight in there, I really have no use for@this. So I'll give it to you. 
00FCDC00: *sigh*... Is this really going to be okay...? 
00FCDC88: I don't know much. At the very least, it's not something@already well-known, like the cycle of the number of @ghosts increasing every 60 years. 
00FCDE48: You're charging us? 
00FCDE64: Well, if you'd like that umbrella repaired, be sure to@do so.  
00FCDEA8: Hmmm... At the very least, I'll help with what I'm good @at. I'll make a record of your explorations.
00FCDF08: I see, so it's the reason you abandoned the shrine and@are coming back all worn out. 
00FCDF5C: Hmm. Sounds like it'll be best if we make full use of @the advantages of having many people to work with as we @progress onwards. 
00FCDFD8: Awesome! Those unknown materials are waiting for me!@I'll have to get my gadgets out then. 
00FCE078: Characters in front of the party will be targeted more@often. Conversely, characters in the back will be @targeted less.  
00FCE0D0: (Eh, what, it's colder than I thought...!)
00FCE188: What's that?
00FCE1B8: Perfectly. Here.
00FCE1CC: Just here and here remaining... All right, culprit@apprehended.
00FCE384: Now THAT was fast.
00FCE390: We found this interesting thing. Take a look at it. 
00FCE4B0: Hmmm... I see. You guys really picked up something@interesting. This here is quite the item. 
00FCE510: Nah, same old same old. 
00FCE520: However, my service only begins once Marisa returns all @the books she "borrowed" from me. 
00FCE5A0: I don't think any of us got that last bit. So, where can@we find one of these Strong Rugged Horns? 
00FCE640: Heeeey, can I come along with you to the Great Tree?
00FCE670: Ahhh, this is so exciting! What are you gonna let me@make? C'mon Reimu, let me see your materials already! 
00FCE748: Patchouli, from what we could tell from fighting these@stones, you've been strengthening them quite a bit. 
00FCE818: Nitori Nitori~ Are you there? 
00FCE834: More members are always welcome. C'mon in!
00FCEA30: Hmmm? So you want me to find a use for these things,@huh...
00FCEAD0: Oh, here you are. I figured you would come quickly if I @waited just a little. 
00FCEB08: You have it tough. Good work. 
00FCEB18: Ah~ yeah that. Because of that, we have to aimlessly@walk a lot. I really don't want to do that. 
00FCEB90: By the way, you guys, how did you secure lodging inside @the village for us? 
00FCEBD0: Then they won't have any effect!
00FCEC38: Oh dear. Your honor, what should her punishment be? How @about getting her to explore the Great Tree with us?
00FCEC98: Thanks for waiting~.
00FCECA4: Whew, that was tiring. We'll be heading back to the inn @for a rest, then... 
00FCECD8: Done? 
00FCED20: You're not going to ask me to do it for free just @because I'm good at it, right? Right? 
00FCED84: Oh, fine. 
00FCEDC8: Tch. But well, we don't have all the stuff you need. I@guess we'd have to go and collect them. 
00FCEEA0: So you have the necessary stuff, give them to me@already. Come on, let me have them. 
00FCEF04: That's because I have a little favor to ask of you@today.  
00FCEFB0: And you think we like wandering around for no reason@either? ...Almost done! 
00FCF0D0: Heyyy, Nitori! In the Great Tree, we found something we @don't really understand, a "Material."
00FCF190: Oh? Why now?
00FCF238: Hm? Well... Even though I have the "Ability of Knowing@the Name and Utility of Tools," when I see these kind @of items, all it tells me is that there's no specific @use for them. 
00FCF314: I'm asking you because we don't know either.
00FCF3A0: I'm definitely not going to do something like that. I'll@lend you these broken swords, so do it yourself.
00FCF560: When she talks like that, she's definitely hiding @something. Watch out, or you might get burned.
00FCF638: What? How rude. I was just thinking of how to fulfill @YOUR request. 
00FCF728: No worries! With the party you have now, led by the @famous incident resolution pair, it'll be easy! 
00FCF798: I've already looked into that. It's a common drop off @the FOE on the second floor!
00FCF7E0: ...Nitori's offer does sound pretty good, though. If we @get that for her, we can make equipment from materials@in return. That's a huge plus for us. 
00FCF8D8: Ah, by the way. For some reason, it feels like the@enemies ain't targeting us equally when attacking. Am I @just imagining things?  
00FCFA18: My, you really do have all of them. 
00FCFA44: That FOE, huh... That might be tough... 
00FCFBB0: And then, if we try to make a lot of noises, would that @draw them towards us as well? Probably but... 
00FCFC28: Bah, enough out of you. You should learn how to catch on@to things fast from Reimu!  
00FCFD08: ...And it's done! 
00FCFD28: Way to go, Mari!
00FCFD44: I hereby sentence you to etc etc etc, effective @immediately. Ms. Reimu, tie her up. 
00FCFDE8: You found some new materials, then? 
00FCFE40: Anyway, we can get this Strong Rugged Horn from the FOE @on the second floor, right? We'll keep that in mind.  
00FCFF10: Good mooorning! Heeeey, have you gotten Strong Rugged @Horns yet? Well, have you?
00FD0000: Same old, huh...
00FD0070: You'll need to finish repairing it before we can start@making equipment, right? How long will that take? 
00FD0120: Now you can bring your materials here. I'll have a look @at them and tell you what you can make. 
00FD027C: Heeeey, Patchouli?
00FD02E8: Amongst our current group, the shopkeeper and I are a @little tougher than Reimu and Marisa. It might be best@to put us at the front using the Formation option, to @offer Reimu and Marisa a little more protection.
00FD03B4: Mukyuu~...... 
00FD04B4: Yes.
00FD05B0: Fufufu, that would be my work, don't you know? Now, be@sure to shower me with your thanks. 
00FD0630: The enemies sure hit hard. In battles, we should aim to @defeat them before they can hit us. 
00FD06E0: However, if we go all out like that from the beginning, @we'll run out of gas quickly... 
00FD07F0: Well, that was our first run inside that tree.
00FD08A4: Hmm. I guess all we can do is keep going at it. 
00FD08D8: Ohh, well said, Kourin. We'll be relying on you.
00FD0908: Patchouli, we found this interesting looking stone, so@we want to hear about it. 
00FD09DC: .........Mukyuu...
00FD0B28: If ya thought ya could deceive a magician like me with@your scammer prices, that's your fault for@underestimating me. 
00FD0E18: ...By the way. This might sound a little naggy, but do@take note that you have to go to the Human Village and@switch your party around to add new party members to@the exploration team. Make sure to give the Human @Village a visit later.
00FD0EF8: My party's getting bigger!
00FD0F30: ...I give up, just do whatever you want...
00FD0F84: Hyaaaah! Nooooo! I hate walking!
00FD1054: Then, we'll be relying on you, Youmu. 
00FD12F0: Ah, I see. I don't know anything about avoiding them, @but it's easy if you want to lure them out. They seem @to react weirdly to sound of swords.  
00FD13B0: I'm back, Gensokyo~. Ahh, I'm tired today; I wanna lie@down already. 
00FD1554: Ah, sure. 
00FD15C0: Sound of swords?
00FD15F0: You don't need to shout, I can hear you just fine. What @do you need?  
00FD1810: Rinnosuke said something like that too. But you see, it @looks like something that's quite akin to magic. So we@don't really know much about how it should be used. 
00FD1920: Basically, if we want to fight monsters, we just have to@get Youmu to yell while making a ruckus with her sword, @right?
00FD1A3C: And that happens even less often. 
00FD1C04: Really? Thanks Patchy, you really are too kind! 
00FD1C40: Ugh...
00FD1F00: I've recently been working on magic to make my@Philosopher Stones autonomous. It would be much easier@for me to have the stones move of their own will@instead of me having to control them all the time.
00FD2178: Hm, can't be helped. I will follow you as well from now @on. 
00FD2268: My, you're greeting us at the entrance? That doesn't@happen often. 
00FD24F4: ...I know where this is going.
00FD2560: ? In layman terms, please?
00FD25DC: Thank you. I'll be counting on you. 
00FD261C: *sigh* Honestly...
00FD26B4: Patchouli, here are the Philosopher's Stones you asked@us to get.
00FD28D0: Hrmph... I guess it can't be helped. We just need to@whack those five stones while we explore, right? Fine,@we'll do it.  
00FD2998: Thank you, you've been a great help. I'm glad I asked @you to do it. 
00FD2AE0: And besides, I've known you for ages. I know you already@have your pockets restrained by your books and magic, @there's no way you'd squirrel away all your savings on@this kind of frivolous expense. 
00FD2C54: *gulp*
00FD2C6C: ...? Marisa, what do you mean?
00FD2D58: ......My my, are you saying I shouldn't be compensated@for my time and effort? 
00FD2EE0: Err, so... Patchouli's been ripping us off? 
00FD2F60: And that settles that.
00FD3A74: Saying "up to no good" is rude. We're helping people@with this.  
00FD4358: Pretty much is. 
00FD5308: Well well, I found you at last. 
00FD5D40: That's what she said... Well, it will eventually be @helpful to humans. Recently, we've been going through @the Great Tree. 
00FD6590: We were asked to do something about that Great Tree. A@thing like that is a real bother, wouldn't you agree? 
00FD7480: ...How did it get to that? Well, we should be happy to@increase our battle potential... But are you really @okay with it? 
00FD7678: Hm, that voice... 
00FD8798: Really now, who are you calling "annoying one?" You @guys, just what in the world have you been doing these@days? 
00FD8858: You guys've been the talk of the town. The villagers@keep talking about how you're coming back looking all @worn-out every time. Based on those reports, you'll @need something a lot more convincing to prove to me @that you're doing fine... 
00FD8AD8: So, what are things like inside that Great Tree? Isn't@that dangerous? 
00FD8CF8: At this point, can it really be helped? You keep coming @back all battered and bruised. Once something more@serious happens, it would already be too late to do @anything. 
00FD8DF8: Well, we're really happy to increase our party, but can @you just please keep to your preaching to a minimum...
00FD9010: Helping people? 
00FD91B8: That will depend on your behavior.
01018368: Alright. Thanks in advance! 
0101B678: Who said I'll do it for free? 
0101BE9C: Yes indeed. So earn all you can, and keep the money @coming, if you would. 
0101C528: What do you need? 
0101C89C: Uh huh, say no more. You want us to recover those @stones, right?
0101D100: Ooh. It's a better job than I thought it would be.
0101DD58: That's the second time. 
0101DFF4: Hello, Nitori? You there? 
0101E150: I see. That's a kappa for you.
0101E75C: ...What did you just make me say? 
0101F600: Mm~ I'm tired too. I want to eat some sweet stuff.
010242C8: Good day to you. It has been a long time. 
010439B0: After a whole lot of barganing, I managed to bring an @out-of-date and broken version to the human village.@But being broken, it still needs to be repaired.
0104A9D0: ...Thank you for that frank summary of what I've just @went through... 
0104D660: By the way, I'd be happy if you can resolve this tree @incident too. That thing is blocking the sun's light, @and I don't want to catch a cold. 
0104F648: We should just come back after we exterminate monsters@within that tree and accumulate experience. Then we can @level up, and distribute bonus points obtained from @levelling up towards any parameter we want here.
0104FA48: Also, if we want to use any special items we find within@the tree on a character, here is where we'd do it...@But that's for later. For now, just think of the shrine @as the place for level ups and bonus point@distributions.
0104FD10: Well, I'm also the worst at defeating the enemies. I'll @at least be a tank... If I'm just sitting there @uselessly while you girls are shielding me, I'd have@trouble sleeping at night.
0104FE18: Anyway... It's been a long while since I was able to@take a casual walk around the village. It sure has@changed a lot everywhere. We've walked in the outskirts @for quite a bit, but there are still lots of food and @lodging facilities around here. 
0104FF00: You're not. The enemies tend to attack characters that@they have an easier time reaching.  
010500B0: From what I have heard, there are a lot of humans and @youkai that have entered the tree. If we continue to@explore the tree, perhaps we will find others who will@help your exploration. As our party gets bigger, we'd @need a place to gather everyone around. 
01050268: (Though honestly, I got all the gold from Marisa's@father and permission to use their good name to get a @large discount... But Marisa probably wouldn't stand@for this kind of thing. I'll just pretend that it's all @thanks to me...)
01050700: Hmm? Well, it's a school, so normally I teach here. @Otherwise... I thought of compiling a summary on all@the monsters inside the Great Tree. It'll be useful if@they ever get out of the tree, though I doubt that'll @happen. 
01050C38: With the amount of lodging facilities in the village, we@should be able to get a pretty good price on staying at @one for a while. If we make use of that, we should be @able to accomodate for quite a number of people.  
01050EE8: So I'll help in what little way I can. If that thing@keeps growing, the village won't be getting any @sunlight at all soon. Please do something about it@before that happens.  
010523D8: Just like Reimu and Marisa said, it appears to carry@some traces of spiritual and magical power. So we may @be able to use them to make some useful gear. 
01052F08: You're like a mind-reader, Reimu! Heard of "Strong@Rugged Horns?" I need one of those. With the proper @processing, it should be able to replace the iron core@transformer portion.
010535D8: We're thinking of exploring that tree a little from now @on, so we were wondering if you had any information to@share about it. 
01053970: I can't fault your taste in choosing an inventor for@these things, though! Oh well, there's no other @customers other than you guys anyway, so I have some@free time. I'll think about a way to put them to good @use, and I'll let you know when that happens. 
01053D48: ...Well, when this kind of abnormal situation happens,@donating to the shrine is hardly high on anyone's list@of immediate priorities.  
01054048: A "long while" from you is probably from several decades@ago... Really, you half-youkai and your lifespans...
01054728: Indeed. The cause is most likely due to my programming@of the stones to prioritize searching for areas with@high magical activity over everything else when their @magic capacity runs low.
01061598: The All-Purpose Construction Machines that the kappa at @the Youkai Mountain use should be able to make lots of@items from those materials. But convincing the kappa to @let me bring one of those machines outside of the @mountain is a pretty hard sell. 
0109EB28: ...Honestly, I'm not taking your money for nothing@either. This kind of thing requires a lot of items and@medicines. I'm just charging you for the materials. 
0109EBD8: Well, messing around with all these materials has been@way more fun that I thought. I've been getting really @interested in the materials themselves too. A lot of@these materials that you bring back can't be found in @Gensokyo. 
0109ED68: We went ahead and picked it up, but it doesn't do @anything special when we were carrying it. Does it have @a use?  
0109EDB0: So instead of always waiting for what kind of new @materials you'll bring back, I want to see what that@Great Tree's hiding myself! And I figured the fastest @way to do that would be to join you.  
0109EEA8: It'll be done in a flash. All the other repairs are @already done, all that's left to do is to turn this @horn into a shape that can fit inside the machine.
0109EF58: Ahh... I finally got to say it! "I knew this would@happen," the 3rd or so most famous line that all@inventors love to say!
0109F1B8: Anyways, it's good to have more allies, but only 4@people can be in battle at once, yeah? Is there any @reason to get more allies than that?
0109F288: Still, those enemies inside the tree... While we can't@avoid them even when we want to, when we actually want@to fight them, they don't seem to come out so easily. 
0109F328: Kourin's deciphering says that this "Stone of Awakening"@works on a person's subconscious to give them new @power... Something like that. 
0109F3D0: You're always helpin' make us stronger with magic, so @this's the least we could do. What's up?
0109F470: The magic was completed, and the stones were able to@move autonomously to quite a high degree, and...  
0109F510: These stones have had their magic strengthened to a @large degree, and even have autonomity imprinted on @them. On all five of them, in fact. ...It must have @cost a lot to do that, hm?  
010A2C98: As we recruit new members, get stronger, become luckier,@and chip away at it, our exploration area will little @by little become wider as well. 
010A2D48: Of course there is. Only 4 people may participate in@battle at a time, but you can still bring 8 others with @you. The more people you have, the more battles you can @endure, and the wider you can explore for each run@through the Great Tree. 
010A2E18: Furthermore, people resting in the back during battle @will slowly recover HP and MP. If someone's spent, you@can shift that person to the back, where they'll slowly @heal back up until they've recovered enough to fight at @the front again.  
010A2EF8: ...Though we don't quite have enough party members to do@that kind of rotation, so don't worry about it yet. @Just continue on as you are now, and give it more @thought once we're up to 8 or so party members. 
010A2FC8: Ah, sounds like when I sheathe my sword. Basically they @seem to react easily to the sound of metal. Also, @things like slashing really hard things like the outer@walls also lure them out. 
010A3068: Hmm, well, the shopkeeper is right. If you use this, you@can learn new powers from it. However, it's also true @that you need a lot of stuff to use it, so it's kind of @troublesome.
010A3110: At least let me finish, mm? The stones clearly enjoyed@their freedom too much, and ended up going into the @Great Tree. None of the five of them have returned. 
010A31B0: Erm, well... The five stones which haven't returned are @have most likely stationed themselves in an area of the @Great Tree with lots of magic around, and will attack @whoever approaches them.
010A3250: I had to wait because you wouldn't accept if I put that @out up front, obviously. I'm always helping you out @here, can't you do me this one little favor?
010A32E8: Patchouli, where did you get all the money for that @from? When we first spoke about you helping to@strengthen us, you told us that it requires lots of @items and medicines, so you'd be charging us for the@materials, right? 
010A33A0: Don't try and deceive us. Sure, you should be @compensated for the time and effort you put in. But @there's no way that that time and effort is equivalent@to how much these stones have been powered up.
010D6418: I heard from an aquaintance. You've been neglecting the @shrine to go to some random place, and you've been@coming back all tattered and torn. You're not up to no@good again, are you?  
