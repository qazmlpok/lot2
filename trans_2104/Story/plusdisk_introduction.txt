00FEBD78: But probably not that much. Yeah, just a little.
00FFCD34: My, what's wrong? It's not like you to be leery about @something like this.
00FFD4BC: Now then, shall we meet up with everyone else at the@inn?
00FFDA04: (...Yeesh, this girl...)
01000E30: Several days have passed since the Ame-no-Murakumo was@sealed. 
0100FDE8: Mm, good. I'm here today with bad news and worse news @for you. Which do you want to hear first? 
0101F8A4: Psh, why don't ya be honest with yourself? I'm sure @gonna enjoy this, for one.
0101FBE8: What's up? Is mutterin' to yourself popular now?
010271B4: Reimu? Marisa? Are you two around?
01027EA0: Whuh? 
01035A50: Whatevs. We just need to look around the Great Tree @again, right? I've been kinda bored recently, so@this'll be some good exercise.
0103A258: Huh. Did another strong monster appear? ...Ah, for the@record, the Ame-no-Murakumo hasn't changed one bit@since we left it at my shrine.  
0103AC10: Well, if y'look at the big picture, it seems to be the@usual deal. We just need to keep the villagers out for@now, then do some extermination ourselves.
0103B648: I can't imagine any of you being the culprits, so @despite how unbelievable it sounds, your theory may @well be the exact case. Let's call this the "Impostor @Incident" from now on.
0103D848: The villagers and youkai weren't perturbed either - they@had figured out that the Hakurei Shrine Maiden and@friends "eventually" dealt with it, and paid it no more @mind. Soon enough, the Great Tree had become a mere @part of Gensokyo's scenery. 
0103DAC4: You sound delighted at this turn of events, Marisa. 
0103E260: Just musing about how annoying it'd be to investigate @that Great Tree again. ...The lower floors, right?@C'mon, let's get started. 
0103E8D8: I don't wanna pick either, though.
0103F668: That tree may be a nest of monsters, but most of the@really strong ones have been defeated already...@Constant numbers of humans have been going inside in@hopes of getting some materials from monsters or for@youkai extermination training, supposedly.
0103FFB8: Yep, even Rumia'd stay obedient as long as she ain't@hungry... 
01040B1C: Couldn't they be doing it themselves and just makin' it @all up? 
01040CD8: It may be a little dangerous, but on the bright side, @most people who do try to enter are at least somewhat @capable of fighting. There haven't been any major @casualties, but the number of injured has been@increasing recently.
01040D98: They weren't too happy. They insisted they wouldn't @behave like such plebian youkai, and that if they @really did went that far, they'd have gone ahead and@eaten them. They're all still enjoying feasts at the@inn, anyway. Even Rumia's behaving herself. 
01042580: Yo, Reimu! You know how hard-headed our madam teacher @can be, yet she's throwing in a bit of fun with her @speech! You can listen to her for real without any harm @goin' your way, y'know. 
01043238: That's why I injected a bit of humor of my own. You can @go listen to her yourself.
01043AE8: ...Let's go with the good news first. 
01043BD0: Ah, she started talking.
01043E48: Anyway, I didn't believe that either of you could do@something like this either, but hearing it from your@mouths gives me more peace of mind. 
01043F40: But then, on one fine day...
01044158: I tried vouching for everyone since we all had alibis - @you were obviously with other people during the time of @the incidents, and the youkai were all relaxing at the@inn. But they wouldn't budge and insisted that they @were attacked by "Reimu and her group." 
01046EC8: ...I'll start with the bad news. There've been humans @who've gotten themselves injured in the Great Tree, and @their numbers are steadily increasing.
01047FE8: Additionally, whoever attacked them had no interest in@robbery. The injured had all of their money still in@their wallets.
01048DB0: But it isn't just one or two isolated incidents... Some @people are spreading rumors that the "youkai shrine"@and the other youkai have finally joined forces.
0104A490: Of course, I don't think either of you would do @something like that. I can't exactly call you the @pillars of upstanding behavior, but at the very @minimum, you do have some morality in you. Probably.
0104ABF0: ...I suppose this could be as fun as she makes it @sound.
0104E360: Oh, the shopkeeper. You're done talking to the youkai, I@take it? What did they have to say? 
0104FCC8: Ooh... Yeah, sorry. All right, I'll just say it right @out.
010503E8: Gensokyo welcomed the peaceful days that ensued. The@Great Tree was still present, but Reimu and her group @already conquered all of it. Besides, it would soon @disappear, and it wasn't causing any harm just by being @there meanwhile.
010507E8: Geez, they were scared stiff of the tree just a li'l@while ago. Guess they saw that we'd pretty much @finished rampaging all over it, and felt like doing @some exploration themselves?
01050BD0: According to some of the injured, they were attacked by @Reimu Hakurei and her allies. 
01050D70: You sure came out of the blue there... Anyway, to be@blunt, I have no idea what's going on there.
01051090: Gah, now that sounds like a pain... I guess that's the@"worse news?" 
01051110: Hmph... Now that it's gone that far, then I have to act.@If my donations keep dwindling even further than now... @That'd be an even bigger disaster than this whole Great @Tree business.
01051238: Yep.
0105127C: I'm here. 
010512B8: Well, inflicting such wounds on themselves just to stage@a farce like that would take some serious courage. And@besides, as I mentioned earlier, there's quite a lot of @victims. In short, the attacks are probably genuine.
01051334: You've already heard your choices.
01051378: Mmmm...... The simplest thing I can think of is a bunch @of impostors runnin' around, pretending to be us. 
01051438: Hmm... That's what we'd usually do... but...
01051490: ...Yes, probably. I have faith in you, Marisa.
01051628: So you'll be looking into it? Thank you. ...The attacks @were mostly reported around the lower floors, so it'd @be best to start from the bottom. 
010516B8: We've been prowlin' around the upper stratums so much @recently. Seeing the scenery of the lower floors again@is gonna be so nostalgic. 
01051744: Why d'you gotta put it like that? 
01051780: Yep. We'd be going all 'round in a huge party, beating@up monsters and having fun again. Ain't the thought @getting you excited too?
0105185C: Me neither. I'm as innocent as they come, obviously.
01051968: Not particularly for me.
01051A7C: ...Hehe.
01051BF0: (She just can't be honest with herself, huh...?)
