00F70658: What's this stone?
00F74890: Good afternoon. 
00F74D8C: By the way, Akyuu?
00F7CC2C: Yes, good afternoon.
00F7CF8C: I like it quite a bit.
00F7E328: Ahh, that's it... 
00F96DC0: Heyyy, Akyuu! 
00FC6134: ...Let's leave that aside for now.
00FC952C: Miko, go ahead with your explanation. 
00FC95CC: So, Tenshi? Where's your apology? 
00FC99DC: I'm startin' to get a li'l creeped out... 
00FC9F48: Ahhh, I get it now... 
00FC9FFC: Ooh... Crown princess, how did you know about that...?
00FCA60C: Reimu, Reimu. 
00FCA6C0: Ooooh... I wish I could retort, but I can't...
00FCA928: ............
00FCAAD8: Gaaaaah...
00FCAC88: Hey! There you go again, always so quick to take up @these kind of deals...
00FCAD60: As random as ever, huh... 
00FCAE74: Ehh? Nah, I don't think it's possible. ...Probably. 
00FCB0B4: W-What are you talking about? 
00FCB1F4: ...Well, I s'pose so. 
00FCB2D4: We're here to record our travels as always. 
00FCB3EC: Ahem! May I continue? 
00FCB524: What's the big deal? It's no fuss for us. 
00FCB6C0: My my, you exaggerate. In any case, I'll be counting on @you to find even more of these jewels.
00FCBC74: ...Eh?
00FCBD60: Have you heard of the phrase "When one door shuts,@another one opens?" 
00FCBDB4: There it is. You've stretched your telepathy shtick long@enough already. 
00FCC0C0: Ohh, sounds good! You sure know us well, Akyuu! We'll @take it!
00FCC104: Eh? 
00FCC10C: Akyuu reveals a gem with a pale glow from her pocket. 
00FCC238: Crown princess! Let us do our best! 
00FCC27C: Me too... I suspect it'd be in everyone's best interests@to never go against what she says...
00FCC2B0: Indeed. It is about time that I search for the gems with@my own two feet.
00FCC5A0: You've got it. A nice and speedy agreement, just as I'd @expect from ya. 
00FCC6A8: That we do. Though there isn't much of a point of @keeping more than one around. 
00FCCA68: My, the usual group, I see. You came at just the right@time. 
00FCCED8: Do you want this gem, Akyuu? I don't actually mind. It's@not like we can do much with it...
00FCD3C4: Hm? 
00FCD3DC: And one to spare as well, I see.
00FCD4F0: We didn't really know what to do with it since we don't @really need it now. We're just lugging it around at @this point. 
00FCD858: Hmm, yeah... Takin' it all together, it does make @sense.
00FCDA10: I am well aware of Marisa's usual character.@Furthermore, I would not be so uncouth as to demand @such an item for free.
00FCDA78: ...Hm, I wonder.
00FCDD08: You and your random outbursts again. Since when did you @ever mention your plans to Alice and Patchouli? 
00FCDD98: All right, then. I suppose I'll be imposing on everyone @from now on. You certainly aren't going to refuse,@right?
00FCE030: Whoa whoa whoa, hold your horses Reimu! Were you really @gonna let it go for free? 
00FCE100: I happened to overhear some interesting rumors. Is it @true that you have acquired a Jewel of Greater@Awakening?
00FCE1F8: Ahh, this? Yep, we did. 
00FCE3E8: Ohh, being directly condescending for once? Obviously we@know what that is. And just as obviously, we now what @you're gettin' at.
00FCE6D0: Yes?
00FCE714: ............
00FCE8D8: ...Fine, fine. I'm okay with this too. Receiving awesome@items is always a plus in my book.
00FCE930: These items are going to draw our even more of our@power, right? How did you get your hands on something @like that?
00FCE9A0: And how did you find out that we acquired a Jewel of@Greater Awakening?
00FCEA08: Yeah. It was a real pain. 
00FCEC04: Excellent. This would be the right time to join your@group, perhaps. 
00FCED98: Honestly, I'm not going to take responsibility for@whatever happens in there, understood?
00FCF020: I was being controlled, so it doesn't count.
00FCF1AC: What are you talking about? We confirmed there were only@20 floors...
00FCF220: Hm? What's up?
00FCF340: She wouldn't apologize even if her life depended on it. @And I thought celestials were supposed to be wonderful@people of such virtue.
00FCF404: You serious? Gah, there's more to this? 
00FCF43C: Hey, we did enhance it with magic, remember?
00FCF4C0: Yeah, yeah. Let's see, where should we go first...
00FCF6B8: I see you're the same as always, Marisa. Very well, @let's go straight to the main topic.
00FCF85C: ...The mastermind behind this incident is most likely @the Yamata-no-Orochi. 
00FCF998: True enough. All right, Futo, that's enough apologizing.@Can you explain what's going on instead?
00FCFB4C: The Ame-no-Murakumo is the reason...? What do you mean? 
00FCFD80: Huh. So this shiny moustached sword is good for @something.
00FCFEBC: Ahhh, so that's what your request is... 
00FCFF68: Ooh... This guilt I'm feeling for not fulfilling what I @set out to do... I'm really sorry...
00FD0178: And besides, in Tenshi's case, she may have been@controlled eventually, but she was pretty much@following her own whims at the start... 
00FD0244: Ah-ha... Which means the creator of them underground@impostors would be... 
00FD029C: Geez, hold up! At least think about it! 
00FD03E8: Now then, this Taoist has finally apologized, and I'm @sure she has lots more to say. Let's hear her out.
00FD0450: Mmm. That reminds me, the Ame-no-Murakumo did take@control of Tenshi previously... 
00FD0530: ......In short, the Great Tree is fairly safe now,@correct?
00FD0728: Ohh, you sound confident. Let's hear it.
00FD0828: Hm? Yeah, pretty much. We've been walkin' round the @underground that grew out from it.
00FD0A80: What the...? This idiot celestial who caused so much@bother, dealt so much pain, and didn't even apologize @once... And she thinks she's got any right to speak?
00FD0BD8: You do know how much bother you caused? How much pain @you dealt? And all you do is just bow your head? Do you @think that's enough? Really?
00FD0CB4: Which means this divine sword's objective is... 
00FD0CD8: Well, there was a really small 21st floor as well, but@there wasn't any other way upwards. You're saying it@goes on from there? 
00FD0D50: Actually, you're back to your old self now, huh? How'd@the other "you" even come about?
00FD0DF4: Ahhhhhhh! All right, hold up! 
00FD0FB8: What'd we get from it? You aren't gonna ask us to do it @for free, right?  
00FD10F8: I had no idea...
00FD1118: So she's might be a puppet, just like you were. 
00FD1148: Siiiiiiigh. And I thought Taoists were supposed to be @wonderful people of such virtue.
00FD1408: ...Marisa. Are you aware that the Great Tree is still @growing?
00FD14A8: Miko, with the way you're talking, you sound like you @know what's up. Hurry up and tell us what's going on@already.
00FD1528: All right. Hmm, where to start... 
00FD161C: Yeah, I know. Futo already told us as much. And?
00FD1670: Hmm. Might be best to check out the upper floors before @Miko gets in trouble. 
00FD18C4: Only natural for me. Those kinda places are usually @hiding treasures, after all.
00FD19D8: Man, I'm beat. Everyone's kinda tired too, yet we're@still gonna explore the Great Tree's underground@later.
00FD1A58: You hags really like being roundabout when you've @figured out something, geez. Explain yourselves in a@simple enough way for us to understand. 
00FD1B04: Ooh...
00FD1B34: Wouldn't you qualify as well? 
00FD1BA8: Yeah, yeah. Anyway, tell me what you've discovered. 
00FD1D80: Furthermore, several in your group have been@acknowledged as movers and shakers of Gensokyo, @inspiring respect and fear alike. 
00FD1E3C: ...Thus, this charm can expel vengeful spirits. 
00FD1E94: There is one more point that led me to this conclusion. 
00FD1FA8: "Should" and "probably," huh... You're not doing a very @good job of sounding trustworthy. 
00FD2078: Ah-ah, are you finished yet?
00FD20F8: There's not much I can say in response to that... but @for the record, we did try to do as much as we can. 
00FD2378: Yes, I'm fine. It seems I've caused you some trouble. I @can't believe both Futo and I let ourselves fall under@control...
00FD2418: Given that it is the crown princess, I believe she is @still fine... but I cannot say for sure...
00FD2518: ...Hey, you're... 
00FD2638: Anyway, if we follow that train of thought... This@stupid sword wanted to control Tenshi to become some@sort of Susanoo replacement?
00FD2758: Nope. 
00FD27E0: Don't bother. It hasn't said a word since I put a little@seal on it at the shrine after we beat it up. 
00FD2868: Actually, what was that sake again? The one that was@used to put the Yamata no Orochi to sleep...
00FD29D8: Whatever the case, I do know you had a hard time of it@too, Futo. I must apologize for sending you to the@underground with only a mere charm for protection...
00FD2B88: Yeah, sure. You've definitely gonna use some different@sake that won't work. Probably straight from your @gourd.
00FD2CD0: The Great Tree. 
00FD2DC0: I used a combination of my Seven Stars Sword and Taoist @charms meticiously inscribed with the five elements to@withstand the aura and make slow, but steady progress @further up the Great Tree...
00FD2E58: I don't think it would matter to begin with. It already @fell prey to such a silly method once. It surely won't@be caught napping a second time.
00FD2F88: Ahh, so that's what happened there. 
00FD2FD0: Yeah. Miko, do you have any ideas to deal with it?
00FD3090: ...But think about it. Even celestials wouldn't be able @to evoke such divinity that would make humans shudder @in fear. Your own celestial is the perfect example. 
00FD3198: Cuz we were all partying at the inn every day after @beating up that shiny sword?
00FD3248: The crown princess said that there was a powerful entity@making its home at the top of of the newly-expanded @Great Tree... 
00FD32C0: Not like we can do much about all those ghosts, @though... I think going up'd be better first, but I'll@leave the final decision up to you. 
00FD3410: Futo was obviously being controlled too.
00FD34F8: ...I don't quite get it, but I guess all we need to do@is go to the 23rd floor.
00FD35A8: Eh? 
00FD35D0: Mm, my apologies for worrying everyone. As for the@explanation... hm, where should I start...
00FD36E0: I believe I made myself clear. You have brought me so @many of the Infinity Gems that have entranced me so @from the Great Tree, after all. 
00FD37C0: That's not what I meant. It's not only the underground@and its ghosts that have appeared. The Great Tree is@expanding upwards as well.
00FD38B0: Indeed. 
00FD3928: First of all, did you realize that the Ame-no-Murakumo@is the reason why you're able to explore the@undeerground and the higher floors of the Great Tree@freely? 
00FD3A30: Crown princess...? Ah, you mean Miko. Oh yeah, where is @she anyway? 
00FD3AD0: Oh, hush. You did the exact same thing. 
00FD3B14: Yashiori-no-Sake. 
00FD3B38: ....Ehh, not really.
00FD3C68: We couldn't stand waitin' anymore. Tee-hee, finger@point, et cetera. 
00FD3C90: So, there is severe divinity in the upper floors, yet it@doesn't come from celestials. There is a draconic aura@in the upper floors, but it doesn't come from the @Dragon God. And additionally, there is evility all over @the place.
00FD3D40: That's good... I didn't expect you to be the type to get@so obsessed over anything, though.
00FD3DB8: Whew... 
00FD3DDC: Feeling okay now, Miko? 
00FD3F18: The underground looks like it goes on for a while @longer, too... I'm pretty sure I saw another hole where @Futo was standing that leads further downwards. 
00FD4018: ...Huh? 
00FD40D8: ...It can expel vengeful spirits too. 
00FD4130: Just a joke. I would never think that all of you could@really do something like that. The other villagers do @not share my view, though.
00FD4270: ......Ahh, I see now. So that's why the Ame-no-Murakumo @acted that way at the start.
00FD4440: Oh my, I'm sorry. Additionally, call me a "hag" one more@time and you'll find yourself falling through a gap.
00FD4510: Furthermore, the esteemed Dragon God would never lurk @anywhere besides the very top of the Great Tree. It is@on an entirely different level... or even an entirely @different world from us. No one is even certain if it @is still in this world to begin with. 
00FD45F8: It was an ardent task for me to even step into the upper@floors of the Great Tree, let alone explore for @extended periods. 
00FD4698: However, none of you felt any difficulty. This is @because you had one thing I lacked - the protection of@the Ame-no-Murakumo.
00FD4710: This was quite the head-scratcher for me. I could only@imagine that one of the lesser ancient dragons that @serves the Dragon God decided on an act of betrayal, or @perhaps someone concocted a way to control those@ancient dragons.
00FD4838: Oh, if it isn't Reimu and friends. Are you tired of @attacking the humans who enter the tree? Is it my turn@to be beaten up this time?
00FD48D0: An entity with divinity and evility that sends people @cowering, an aura only possessed by the dragon species, @and a fate so linked with the Ame-no-Murakumo that it @caused the sword to act against it... 
00FD4998: Now now, be patient. I know everyone is incredibly@anxious to hear the genius plan that could only have@been concocted by the most brilliant of minds...
00FD4BE8: Quite. You should know the rest already. While I was@frozen into place, my eyes wandered upon all of you.@All of a sudden, an uncontrollable impulse came over@me... demanding me to attack you. 
00FD4CA0: I see. So that evil power overcame you, and was @dispelled in an instant by the Ame-no-Murakumo. 
00FD4D60: Exactly. If you connect all the dots together, the@culprit then becomes clear. 
00FD4E6C: That's true.
00FD4EB8: As Yukari mentioned before, you defeated the@Ame-no-Murakumo and started carrying it around with @you. Ever since then, it has clearly been helping you @out when possible. In other words...
00FD4F60: The most powerful and famous dragon in Japanese @folklore, combined with divinity, evility, and an @intertwined fate with the Ame-no-Murakumo... Yes, it@ticks all the boxes for this incident's culprit.
00FD5040: I thought you would, Marisa. Let yourself be entranced@by the charms of this gem.
00FD50E4: Hey!
00FD5120: If that was your plan, you should've went with me @instead. When you think of defeating the@Yamata-no-Orochi, you think of sake. And when you think @of sake, you think of me, yeah? I can make eight vats @of sake right now.
00FD5280: I remember there were places at the first few floors of @the underground that we never looked into.
00FD5340: If the entity at the top of the Great Tree truly is the @Yamata-no-Orochi, then it would be doing the same thing @by controlling Futo in the underground, and Miko at the @upper floors. 
00FD5468: Given that the sword was born from the dragon's tail, @it's plausible for them to have the same powers.
00FD552C: That should make it obvious enough, no? 
00FD5568: ...Is it possible that your "little" seal is the reason @why the Ame-no-Murakumo can't talk anymore? 
00FD5618: True enough. Its easier to create somethin' when you@have an image in mind to work with. That's how my own @power works too.
00FD5730: ...Hmmm?
00FD5760: ...As such, this is why I believe the greater entity at @the top of the Great Tree is none other than the@Yamata-no-Orochi. 
00FD57C8: Hrm, dark fragments, huh... We've acquired a fair amount@of them since back then. Maybe we can go into those @rooms now?
00FD5898: The way they make use of their spells is different from @the way us magicians work our own magic. If we mess @with it too much, we might upset the balance of the @five elements and make it useless and stuff.
00FD5954: Oh yeah, 'bout that.
00FD5A40: ...Is that how it is? Geez, why didn't you say so from@the beginning?
00FD5A88: ...Are you out of your mind? Futo had that charm with @her and she still went crazy just by peeking into that@hole. It's way too dangerous. Rejected. 
00FD5BC8: There's no scarier word than "probably" when it comes @from the youkai extermination expert... 
00FD5C40: ...And it was something I suspect I wouldn't have been@able to pull off myself. But with everyone's help, and@the power of the divine sword, it might just go better@than expected.
00FD5E2C: "Probably" is your favorite word. 
00FD5E78: Right, Yashiori. So, can you actually make it? If you @can...
00FD5EF4: ............... 
00FD5F58: I am not speaking in absolute terms, but rather @comparatively. At the very least, it is safer than it @was before, no? 
00FD6048: Heheheh, leave that to me! I have a suitable plan all @prepared. 
00FD60EC: Hehehe... 
00FD611C: ......And what else?
00FD619C: That's not what I said. 
00FD6258: Mm! Let's work well together, everyone! 
00FD6390: ...But everything has an order. I'll explain the details@at the 23rd floor. It'd be easier to explain when we're @at the spot itself. 
00FD6848: As that Taoist said, this charm is imbued with magic to @expel vengeful spirits. ...Which is obvious enough@given that it was meant to seal up the big hole @underground.
00FD6930: In any case, let's work work well together, everyone. 
00FD69C4: ...Together with the other youkai, mmm? 
00FD6A28: Ugh...
00FD6A38: I'm glad you catch on quickly. As such, since you're@going to explore the Great Tree again, I would like you @to bring any of these gems you find to me.
00FD6B38: Isn't it beautiful? 
00FD6B70: I don't recall treating anyone differently from @others... I suppose I have become friendly enough with@youkai to make the villagers anxious, though. 
00FD6CE4: Where'd your sources pick this gem up?
00FD6DB0: Just another joke. By the way, since you're @investigating the Great Tree again, I have a little @request for you. It's about this gem... 
00FD6EF8: I have recently acquired a few of these gems from my@sources, but I certainly wouldn't mind more.
00FD6FE8: Of course not. How about a fair exchange? I'll trade@parts of my collection of rare items in proportion to @the amount of gems you bring to me. 
00FD7058: Exactly. It's just as Marisa says. Here, I'll let you @have this one.
00FD7100: But at the very least, it was definitely found in the @Great Tree. Be patient in your search. I will wait@patiently here as well. 
00FD7164: Y'know, about that 22nd floor...
00FD7178: Hey, looking for it is fine and all... but where exactly@in the Great Tree can we find these?
00FD71F0: Ahhh... 
00FD7238: Mmmm... 
00FD7258: I'm not quite sure myself... It's possible that you may @not even see one for quite some time. 
00FD72B0: It's not merely the floors above that require our @investigation. Much of the underground remains@unexplored too... Perhaps we can find some clues@there.
00FD7360: That "Water Lily Seal" that's blocking the entrance,@remember? Who would have something like that? 
00FD73F0: Your memory deserves praise, Marisa.
00FD7410: It's possible. Heck, even if my intuition's failed, we@might still find seals and stuff further underground. 
00FD7590: Sure there were. Lotsa places where we couldn't enter if@we didn't have dark scarlet gold fragments and stuff. 
00FD7624: Patchouli and Marisa want you to come over. 
00FD7798: That's possible too... Anyway, let's just keep in mind@that there are possible unexplored areas in the early @floors of the underground.
00FD7898: Wah, you scared me! You didn't have to come out, I@could've gone over to your place. 
00FD78EC: Now now, just let it go.
00FD79B8: ......So none of you figured out a way to make use of @it. Not very reliable, are you? 
00FD7A58: Oh, come on. The magic in this here charm's a really@high-level five-element concept.
00FD7B18: In that respect, you might be more informed than us @regarding this charm, Reimu. You are technically a@shrine maiden after all.
00FD7BE0: Ehh? I'm not very good at that stuff... And what do you @mean by "technically?"
00FD7CD0: In short, the charm should be more powerful than before.@Probably. 
00FD7E00: So, since we've got this powered-up charm here... Let's @use it on that big hole and explore what's inside.
00FD7EB8: Heheheh...
00FD7F10: You're the last person I wanna hear that from.
00FD7FA8: ...Heheh... 
00FD8440: Oh no you don't. Do you really expect me to have faith@in you after that conversation we just had? 
00FD8628: Ahh, so that's why... Why are magicians always so @utilitarian?
0101B9F0: Just did. Via telepathy.
0101CEA4: Now, now. We can't move on until you stop teasing.
0101D530: I get it now. So that's why you think it's the@Yamata-no-Orochi! 
0101FB60: I'm so sorryyyyyyyyyy!!!
01020020: Heheh. The ears of the Toyosatomimi aren't just for @show. 
010200B8: That's what I thought too.
01020244: Ehh, nope.
010207F8: Hmhm. I see, so that is what happened.
01020E08: Were there? 
01020FD0: Well, yes. We're magicians after all. 
010219A8: Without further ado, then.
010301D0: Anyways, we're gonna clear our name by divin' into that @Great Tree and taking out our human-attacking @impostors. That'll settle everything nicely.
01038DE0: Ahh... It's about the charm they got from Futo, right?@Those two sure like their magical items.
01040A90: Of course not. As if we'd even let you walk away after@all that's happened. You're coming with us even if we @have to drag you along. 
01042C18: We already got close to the big hole before, and nothing@special happened! This is fine. Everything will be just @fine. 
010435A0: So c'mon Reimu! Let's go exploring! Who knows what@unsalvaged treasures and undiscovered magic items are @sleepin' in that big hole?
01045378: Perhaps this is your karmic retribution for how you @normally behave? Words of the Hakurei Shrine Maiden's @favoritism towards youkai had reached my ears even@before all these incidents took place.
01046750: Anyways, what we did was to increase the charm's base @strength using magic while being careful not to upset @the balance of the five elements. 
01052088: How about trading your Jewels of Greater Awakenings for @some valuable items that I recently acquired? I have@prepared many "awakening" items as well. These@treasures will help to enhance your powers even @further.
01052668: Not like I was gonna let our jewels go for free, unlike @a certain shrine maiden. This deal is better than @gettin' things for free - Akyuu gets the jewels, and we @get items that make us stronger and let us explore@more. It's a win-win for everyone.
01060EA8: ...Oh, yes. I would not dream about claiming any gems @found as my own, of course. I will still maintain my@offer of trading gems for items, so there is no need to @worry in that regard. 
0109ECB8: Sorry Akyuu, no can do! Patchy, Alice and I are gonna do@some serious research into this jewel and turn it into@some great magical item.
0109F5C0: The legendary evil dragon who was slayed by Susanoo...@and whose tail housed the divine sword@Ame-no-Murakumo...
0109F660: You and your group most likely didn't perceive too much @about the aura due to the Ame-no-Murakumo's protection. @It wasn't of any danger to you, so you didn't care too@much about its peculiarities. 
0109F740: Indeed. It'd make sense for the Yamata-no-Orochi to @create its own pawns - impostors in the image of the@people who once stood against the Ame-no-Murakumo.@Reimu's group has likely been acknowledged by the sword @as fearsome opponents, after all. 
0109F838: It's probably come up with more countermeasures after @learning from its defeat to Susanoo.
0109F8E0: Yeah yeah, that's enough out of you. I'll see whether we@can pay a trip to the big hole at the fourth floor@underground.
010A3450: Mm, about that. We split up our forces - the crown@princess ascended the Great Tree to investigate the @mysterious presence, and I descended to the @underground. She should still be in the upper floors of @the Great Tree. 
010A3500: But it has been many days since... The crown princess @did not send anything to the underground to check the @situation, or even attempt to contact me. It is @possible that she may have already fallen to the same @clutches that eroded my sanity. 
010A35C8: The biggest obstacle I encountered was the severe aura@present in the upper floors of the Great Tree. It was a @terrifying mixture of divinity, a draconic aura, and@the evility from the vengeful spirits... I felt like my @sanity could be taken from me at any moment.
010A36B0: Those thoughts were running through my head as I reached@the 22nd floor... Suddenly, I felt like I was being @stared at by a pair of horrifyingly gigantic eyes. I@froze up - my body refused to move as I wanted it to... 
010A3768: Perhaps the Yamata-no-Orochi manifested in Gensokyo in@response to the Ame-no-Murakumo's crossing over and @transformation to a tsukumogami... or perhaps it was@the other way around. 
010A3808: Whichever came first, it's likely... no, it's certain @that both parties are trying to exert their influence @now. The Yamata-no-Orochi is sort of a parent to the@Ame-no-Murakumo, after all. 
010A38A8: Perhaps from the very beginning, the sword's objective@was not to control Heaven, but actually to defeat the @Yamata-no-Orochi? It used the celestial's body and@tried to do so itself at first, but now, it's lending @its power to your group to do the deed in its stead.
010BC9B8: Quite so. There's one more thing as well. Remember the@"scarlet gold" metal that the Ame-no-Murakumo and @impostors were created from? What if they were not@merely fragments of metal, but actually a @crystallization of the power of the Yamata-no-Orochi? 
010BCA98: I don't exactly want to... but I'd imagine it'd be@helpful to have an example of what these gems look@like. Besides, your group is the only one who can @explore all of the Great Tree. Take it as a little@advance from me.
010D6320: Heheh, perhaps. in any case, many of you portray an @image of strength, which the Yamata-no-Orochi used to @its own ends. It seems to be surprisingly smart this@time around - at least, far more than when it was @killed in its sleep after drinking too much sake. 
