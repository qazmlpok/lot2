00FC9A48: ... 
00FCC208: ......
00FCFA78: Sure. 
00FD1C24: I guess I am too... 
00FD3AB4: Tell that to Flan!
00FD5740: Ah, you're worried? 
00FD5B8C: W-What is going on...?
00FD7FB8: ...Ahh, how did it turn out this way... 
00FD8778: Ah, Reimu and Marisa! 
00FD8AB8: Something weird?
00FDC17C: Oh boy, I knew it...
00FDD048: ...That second one worries me too...
00FDD07C: Marisa, please give her a good kicking. 
00FDD1C4: It did? And what's that supposed to mean? 
00FDEC90: Ah! Wait! Stop right there! Geeeeeeez!
00FE0A3C: Ooooh...
00FE2468: We should look for Flan as soon as possible... Honestly,@Remilia, you really should learn. 
00FE253C: Geez, that was so hot, I thought I was gonna die... 
00FE2C78: I thought I was getting used to it... Is this the limit @of a celestial's power...!? 
00FE3AD0: Hmm, I see now. So in short, we're gonna fight the final@boss's second form now? 
00FE3C00: Hmm, which means Suwako met Tenshi at some point. I @wonder if she's okay... 
00FE3C4C: Ugh... You're still on about that...? 
00FE3CA8: ...*sigh* No point hiding anything now. 
00FE3F90: Ahhhhhhh! You're that celestial!
00FE4000: You cheater! You've got way more power than what Tenshi @drew out from you!
00FE4600: ...Is a picnic something we need to work hard on? 
00FE4F30: Then it's an enemy. Sure feels like one anyway! 
00FE63D0: Probably when her family and close servants all went out@on a picnic and left her behind, I'd imagine. 
00FE7CF0: Of course I am. I'm worried about Flan herself, and @worried about whatever she's going to get herself up to @here. 
00FE7FF8: Erm, I'm slowly gettin' it now I think. So basically, @it's the sword itself that's the bad guy here, not@Tenshi? 
00FEBAD8: Oh my. I am pleased to see that you have no intention of@yielding. We shall be pursuing the sword on its @ascension as well.
00FEBD20: I know it may be hard for you to accept, but there@really were circumstances involved. Can you overlook@this? 
00FEF14C: What kind of circumstances would make you randomly@attack unsuspecting people!?
00FEFEB8: Marisa, let's work hard on this picnic! 
00FF0FA4: Ugh, it's way too friggin hot 'round here.
00FF2AE0: Ugh... This should not have happened....
00FF2D38: ......So that is the situation as it is now. Do you @understand? 
00FF3758: Marisa, give her a good kicking.
00FF4844: Super serious. Take a look. 
00FF5220: Yet those who are unqualified to control Heaven have@their limits... 
00FF66BC: We don't have time to ask questions! It's coming! 
00FF7CD0: ...Hm?
00FF80A8: Oh my, my dear, dear sister. What would you be doing in @such a place like this? 
00FF8C20: If you were to ask me, I would be more annoyed at @choosing the correct path at a fork.
00FF9250: It came from pretty close by... Over here! Let's go!
00FF947C: ...Marisa, is it okay if I go with you? I won't be a@bother or anything? 
00FF9668: Which would be why, in its brutal honesty, it created @the Great Tree that reaches the heavens...
00FF9950: Oh, good. I'll be counting on you.
00FF99A8: I was waiting for that reply! Count me in! Let's get@fired up and go~! 
00FF9A08: Uuuugh... How strange... My body isn't moving the way I @want it to... 
00FFA0AC: Owww! Why'd you do that! That really was what was @happening!
00FFA200: Even if you ask... What can we do about it? 
00FFA49C: I'm here too~. Looks like we reached here at a good @time? 
00FFA528: Reimu and Marisa. Be careful on your explorations as@well. 
00FFA600: Indeed. So do let me join up and let's make that@celestial cry out for mercy together. 
00FFB278: Yep, sounds good to me! Kanako, let's team up and give@Tenshi a painful visit. 
00FFB3D4: Something's approaching us from the front.
00FFB5B4: Boooooom!! Squeeze and... Kabooooooom!
00FFB93C: Don't wannaaaaaaa! Big sister's an idiot, an idiot, an@idiooooooot!
00FFBBB0: ...*siiiiigh* Where could Flan have gone off to...
00FFBC40: Hah, I know that feeling. Didn't peg you for the@obsessive-compulsive type though. 
00FFBD3C: Argh, that was so tiring. Flan, are you going to listen @to me now?
00FFBF48: Kanako? Your clothes look all torn up. What happened to @you?
00FFC09C: Tenshi, it looks like the Ame-no-Murakumo is a toy@that's too much for you!
00FFC168: Ah well. Let's get the Ame-no-Murakumo back and call it @a day...
00FFC5D0: That voice... It is undoubtedly the one I heard from the@subterreanean depths! 
00FFC78C: When we actually go back, it'll be quite cool thanks to @the shade of the Great Tree though... 
00FFC898: Don't mess with me! Even with a body like this, I can @still crush each and every one of you!
00FFC93C: It has been a long time. It seems we have arrived in@time for the finale.
00FFCCF4: Yeah, I've got it all figured out now. So to sum it up, @I was basically saying... 
00FFCE64: Ahhh, I get it. So that's why you were hidin' here. 
00FFD058: Did she set your pants on fire too, you liar? 
00FFD0EC: Hmm, that's unfortunate. We already finished things with@her.
00FFD458: Besides, you expect me to run away after what that sword@did to me!? I'm a proper celestial, I wouldn't ever do@that! 
00FFD5DC: Yes. And the feeling's getting stronger by the second...
00FFE158: The "dual-wielding beautiful swordswoman," wasn't it? @That was so you.
00FFE250: Same here... Especially since she's pretty pissed off.
00FFE298: Ahh, I'll just keep it short and simple. Remember that@golden sword she was carryin'? She got posssessed by@it, so she wasn't really herself. 
00FFE548: The ignorant who swing a divine sword like a mere blade @of steel... The enlightened who draw out the true power @of the divine sword... Her position was no less obvious @than day and night... 
00FFE610: That is certainly no different from you. Anyway, shall@we return to the correct path?
00FFFA58: Ahh, so those danmaku traces and sword marks we found on@this floor were from you and Tenshi.  
00FFFF38: Ahh, that's how you're playing it... I don't mind @bringing her along on the exploration, but if she goes@on a rampage inside the village, there's going to be a@lot to answer for.
010005D0: Ehh? Really!? 
010008A0: Mm, what a coincidence. We have quite a history with@that celestial as well, and she definitely deserves a @good kicking. 
01000ED0: I said to get out of here! If you don't wanna listen, @this is what you're going to get! 
01001CC8: Oh? What's up?
01001D70: Hold it right there. Why're you acting as if you're one @of us?  
01002B38: Mmhmm. C'mon Flan, stand up.
010030D4: It doesn't look familiar to me. 
01004F0C: Going on the wrong path gets really annoying, huh?
010057D4: Look, just listen to me first and...
01006738: Urrrrgh! I was just minding my own business and she @attacked me out of nowhere! I get furious just thinking @about it! 
010086F8: ...The Ame-no-Murakumo... 
01008F88: Hm? Another voice...? 
010090A4: Oh Marisa, you know that... 
01009248: Oh my. Everyone's here now. 
010094B4: ...This kind of picnic looks like one you'd find in @hell... 
01009890: Ahh, about that. I was walking around, trying to find @the stairs to the next floor, when suddenly some@golden-glowing dangerous-looking celestial appeared!@She then started swinging at me with her sword! 
01009F28: Oooh... *pant*... *gasp*... 
01009FA8: I suppose that's why Tenshi's behavior seemed to get@more and more unstable as time passed.
0100A0E0: To defy the power of the age of the gods... Foolish @beings... 
0100A268: ...Ah, I can see the ending. And it's filled with @danmaku.
0100A2C8: Hmm? That voice sounds familiar.
0100A434: ...Then I want to go. Bring me along, please! 
0100AA2C: ...Eh? Did someone say something? 
0100AA40: Marisa, did you hear that?
0100AC74: "I'm... all right. I am no longer under his control!" 
0100AEC8: Mm, what a coincidence. We have quite a history with@that celestial as well, and she definitely needs to @learn a harsh lesson. 
0100B028: ...Is floating... 
0100B044: Ow... My goodness, that was rather... 
0100B23C: Particularly, how shall we deal with the upward-bound @Ame-no-Murakumo from now onwards? 
0100B39C: Mmhmm. Let's move on to a more important topic, shall @we? 
0100B3C8: You keep sayin' things like "possessed" and @"brainwashed..." But you were totally yourself on the @6th floor, weren't you? 
0100B590: That may well be true. Yuyuko, shall we be off as well? 
0100B75C: In what world is that true...?
0100B7A8: But in return, I want you to let me join your party.
0100B8F8: We should make haste as well. 
0100B920: Yes. And this time, it won't be just Sakuya, Meiling and@myself. There's Reimu, Marisa, and a whole bunch of @others we don't know who'll be coming along as well.
0100BB24: Oooh... 
0100BB50: Yes! Let's do well together!
0100BB88: It's me. My, I haven't seen you since the 10th floor. 
0100BEE0: Ah, hahaha... It is getting quite difficult for me to @progress alone now~. And it would be annoying to give @up and go back after coming this far. 
0100C30C: Gotcha. We should hurry.
0100C798: That's... Flan? It looks like she's "playing" around by @reducing these monsters to cinders... 
0100CCD8: It's important, so I had to! My my my, if it isn't my @dear sister who leaves her own little sister at the @mansion while she has fun!
0100CD50: ...I seem to remember somebody being all bitter and @claiming that they'd climb the Great Tree by herself@back at the 10th floor. 
0100D60C: Rejected! I refuse! Who would go back home and leave@something so fun behind!? 
0100E068: Honestly, I was just minding my own business and she@attacked me out of nowhere! I get furious just thinking @about it. 
0100F280: You're missing Marisa's point. Whatever your reasons may@be, excluding someone out is always going to make that@person angry. 
0100F9F0: ......Ah! That swarm of incinerated enemies at the floor@below... That must've been Flan's handiwork! Marisa,@Remilia, you knew it but didn't say so, huh?
0100FB00: Did you just call me an idiot!? ...Ahh... Never mind@that! Just calm down and listen to me!
0100FBE8: Hah, trying to sound confident after losing Tenshi's@body? All bad guys are always boast about themselves in @that style when they're in a pinch! 
0100FD30: Actually scratch that - I won't show mercy either way.@I'm in a really bad mood now. Get out.
01010008: Whatever Tenshi thinks, having Iku leave would be @painful for us. And I suppose we've already absorbed@plenty of people. 
01010114: I'm not sure either...
01010458: ...You don't need to repeat yourself. 
01010608: Guess it could be traces of Tenshi going wild? That's @about the only sword-wielder that Suwako could meet @'round here.
01011380: Oooh... Fine, since you two are asking, I'll let it go. 
01011538: Yep, sounds good to me! Suwako, let's team up and give@Tenshi a painful visit. 
01011720: That was abrupt, yet somehow expected... It's hot enough@to warrant spending on something to blunt the heat when @we go back to the village for today.
01011794: ...W-Wait, what the!? What was with that flash of @heat!?
010117D0: Can't we just let this Great Tree remain for a little @while longer? We'll deal with it when the colder@seasons come, let's leave it 'round for the summer. 
01011860: Ahahahaha! Ahahahahahahahahahahahaha! One more time!@Kaboooooooom! 
01011940: ...Hmph, I get it already. I apologize for that, okay?@So what are you even doing here?
01011A48: ...I-I wasn't trying to hide it or anything. There was@no solid proof, so I didn't mention it, that's all. 
01011B00: ...? My, if it isn't my dear sister. My my, if it isn't @my dear, heartless sister who left me behind while she@went out on a fun and exciting picnic.
01011CC8: Flan! What are you doing here!? 
01011E08: Honestly, this is why I didn't want to bring you along! @I'm sure you're well aware of it yourself!
01011EB0: I could've understood that, if you at least told me. But@you just went off with everyone else and had your fun @without even uttering a single word. That was what I@wasn't able to stomach! 
01011F78: I told you, that's not it! Stop swinging around your@Laveatein and listen to me already! 
01012038: Like I said before, we shouldn't have left you out on @our trip. I'm sorry. As a token of my apology, let's@all have a picnic together from now onwards, okay?
01012310: Hmph, I don't care! You idiot big sister! 
01012340: There's a lotta folks amongst us who joined on their own@accord anyways. At this point, one more's like a@rounding error. And besides, your strength's the real @thing. If anything, you comin' along would make our @life easier.
010124B4: ...Where're we at?
010125E8: Hmm, I thought there'd have been something this deep@in... But it looks empty. 
01012768: Just to let you know, I'll show no mercy to anyone who@interferes with my picnic.
01012814: Urgh... You all and your mouths...! 
010128B0: We'll defeat you and put an end to this incident! 
01012908: Yes! How's that, Tenshi! The third time's always the@charm!
0101294C: Ah, Flan. 
01012960: Really? How boring. I was hoping to rub it in even more.
01012A10: ......I had thought that a celestial would make better@use of me...
01012AF0: Ahh... It has been a very long time since it emerged@from the Yamata-no-Orochi's tail, and in that @timeframe, it has taken in the fierce emotions of many@humans in the midst of battle... It does fulfill all@the conditions to become one. 
01012BCC: I giiiiive! 
01012C78: ...What.
01012CA8: No, of course not.
01012CC8: These are definitely marks of Kanako's fighting style.@The danmaku traces are already suspcious enough, and@those Onbashira sticking out of the ground confirms it. 
01012D44: I already know. ...So how about it, Flan? 
01012F38: Hah, let's see if you can back up your talk!
01012FA0: Why hello there. Your body isn't moving the way you want@it to, hmm? That's music to my ears.  
01013008: That's exactly what we wanted to happen anyway. That's@why Yukari, Hijiri and us were breaking out all the @guerilla tactics on you.
010130B4: It's me! Haven't seen you for two whole floors. 
010130F0: You don't look as lively as before. How about it Tenshi,@wanna tie yourself up and save us the trouble?
01013228: The friend turns to the enemy's side... After many@battles between them, the allies find out that their@friend is being manipulated by the true mastermind... 
01013344: Ah. Looks like she's passed out.
010133C0: Ame-no-Murakumo... You have become a tsukumogami, @haven't you?
01013418: It's exactly as we said. The Ame-no-Murakumo became a @tsukumogami. It is no longer just a divine sword, as it @now has been given life. It seems to have been@manipulatng Tenshi in some fashion as well. 
01013530: An item which has manifested into a tsukumogami would @first aim to accomplish their role as an item.
01013590: If we continue thinking along such lines... Do recall @that the Ame-no-Murakumo possesses the ability to @control Heaven. As a tsukumogami, the first thing it@would do would be to undertake means to bring @everything under its control. 
01013958: ...Whose voice was that?
01013970: That was then, and this is now. It really is tough to go@solo at this point, and besides, with that whole golden @sword thing and all, your group looks a whole lot more@fun.  
010139F0: And then they get defeated by the allies of justice @after! Don't forget to close with "This can't be@happening!" 
01013B3C: Ahh, I understand it all now. 
01013B60: You dream hopelessly of victory... Come. Your arrogance,@your insolence... I shall crush them all... 
01013CEC: It's me. I finally caught up. 
01013D10: Interfere no further, maidens... Mercy will not be@delivered a second time...
01013DF8: Who would've thought the Ame-no-Murakumo would become a @tsukumogami... And such a strong one at that... 
01013E40: Our adventure begins now, huh!? Ahh, exploring the Great@Tree that appeared in the center of Gensokyo, battling@against enemies together, and clashing with the true@mastermind...! I love these kind of plots!
01013EE0: And now the brainwashing has been undone, and the @friends join hands against the true enemy! Ahh, a @wonderful climax for a story! 
01013F78: Err, what does that mean? 
01013FB0: My, you two as well?
01014058: Gah, what's with this place? There's lots of traces of@danmaku, mysterious pillars, and sword slash marks@around here.
010140E0: Owwwwww! What was that for!? I didn't say anything@wrong!
01014204: Holy crap...
0101429C: Suwako? Your clothes look all torn up. What happened to @you?
01014324: Guh... I thought defeating Tenshi was gonna be the grand@finale... 
010143C8: N-No way! I was possessed from the beginning! I couldn't@do anything!
01014480: We lost to Tenshi constantly, but we kept at it and @emerged the victors in the end. We can make sure that @high-and-mighty sword meets the same fate.
010145F0: All right. Good day to all of you. Don't be too @disheartened at the loss~.
01014690: Indeed. Now that the true culprit has been unmasked, we @must stop it promptly.
01014834: Who's this friend of yours you keep speaking of?@Certainly no one I know.
01014880: I'm not acting, I am already one of "us." What, you @don't want me around? 
01014910: Eldest Daughter, shall we leave this to Reimu and her @group? We should be returning to Heaven...
010149C0: Indeed it is... But what could these sword marks be...? 
01014A60: Fine... You can come along, Tenshi. I'll rather have@both of you along that have both of you leave.
01014AC0: I knew you'd understand, Reimu! Don't worry, you'll see @I'm pretty good at this!
01014B10: Yeah, about that! I was wandering about, doing my best@to keep going upwards, when suddenly some dangerous @celestial who was glowing gold showed up! Then she@started swinging some sword at me!
01014BA0: Ahh, yes it does. But what could these sword marks@be...?
01014BF0: I gave as best as I had, but that celestial was super @powerful! I figured I was no match so I turned tail and @ran.
01014D00: Are you giving up so quickly from a single beating? 
01014D88: I fought my hardest, but that celestial was simply too@strong for me. I decided on the better part of valor. 
01014F20: Guess it could be traces of Tenshi going wild? That's @about the only sword-wielder that Kanako could meet @'round here.
01015010: Hmm, which means Kanako met Tenshi at some point. I @wonder if she's okay... 
010150C0: Y'mean she's using the three divine treasures against @us? I guess that'd be the gaudy kinda style she'd like. 
01015170: Ahh, so those danmaku traces and sword marks we found on@this floor were from you and Tenshi.  
010151DC: Ah, you're not coming with us?
0101524C: Hmm, I think we're plenty careful already...
010152EC: Anyways, let's keep aimin' upwards. 
010153B8: Yep! So let me join up and let's make that celestial cry@out for mercy together! 
01015470: Well, whatever it was, we've beaten it. It doesn't look @like there's anything else of interest here either. @Let's keep moving.
01015538: Ahaha... I'm really sorry about that fight just now!@There were circumstances and all! Sorry!
01015638: I feel something weird from around here...
01015678: ...Are you and your delinquent ways really an example of@a "proper" celestial? 
010156A8: Ahhhh... Honestly, I do not know anything anymore...
01015958: ...Well, anyone who wants to join the exploration is@welcome. Shall we go together, then?
01015B2C: Gah, what's with this place? There's lots of traces of@danmaku and sword slash marks around here.
01015DF4: Eh? Are you serious?
01015E88: Good to hear that you two agree with me! Let's do well@from now on.
01016038: Hmmm... Fine, since you two are asking, I'll let it go. 
010160A0: But in return, I want you to let me join your party.
01016160: Ah... Is it cuz of that monster over there? What's that @supposed to be anyway?
010161EC: Ah, you're that celestial from before!
01016220: I doubt it'll let us just walk way either... Let's do @this. 
01016290: Hmm... Fighting that made me feel a bit like I was@fighting Tenshi... Her Ame-no-Murakumo gave off a @similar vibe... 
01016368: ...Well, doesn't look like there's much else here for us@either. Let's move on.
01016428: The heck was that magatama-shaped thing? And why was it @attacking us? 
010164C8: Given its shape... It may have been Tenshi pestering us.@She's got the sword with her after all... 
01016590: What kind of circumstances would lead to you randomly @attack unsuspecting people? Seriously now.
01016E90: Somethin' caught your eye?
01016ED4: ...Whoa, the heck is that?
01033108: My, can't you tell by looking? I'm having lots of fun @with a picnic.
010331E0: ...It'll really come true if you say it like that.
010334E8: ...And there Flan goes. 
010A0DC8: Mmm... We do not have sufficient information on the @sword as of yet. I believe it would still be most @effective to act separately at this juncture. 
010E1F08: That's why I went on my own picnic! After all, my dear@sister doesn't care about me enough to at even say@something before she leaves me behind! As if you care @what I'm doing or where I am! 
010E1FA0: But, but! Just look at this cremated object that used to@be one of Flan's enemies! If we brought her along, it @wouldn't have been a picnic at all! 
010E2030: Not a bad summary. We should deal with it at all haste. @As the Great Tree itself appeared due to the@Ame-no-Murakumo's power, overcoming the Ame-no-Murakumo @itself should make the Great Tree disappear at some @point.  
010E20D0: Just ignore that part then! Being possessed and used at @someone's whims makes me really mad! I don't care if@it's a divine sword or whatever, I want to beat it up!@Let's take out that golden hairy sword together!  
010E2198: That celestial seems to have camped out in a place close@by. I want to get revenge, but I can't handle her @myself. So I waited for your group to come along! 
010E2228: That celestial seems to have camped out in a place close@by. I want to get revenge, but I know I can't handle@her myself, so I was waiting for your group to come @along.
