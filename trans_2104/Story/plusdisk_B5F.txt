00FD8B58: ...Plus an extra. 
00FEB1D4: Why are YOU getting angry?
00FEB9BC: As incomprehensible as always...
00FFA33C: Hmph. Barely worth the bother.
00FFFC6C: Hmph! Didn't even need to break a sweat!
010148E0: Hmph, whatever. I'll take it out on that impostor over@there instead.
0101C078: Heheh! You know it! Of course, everyone knows it too! 
0101C7A0: All right, let's take them all out at once! 
0101CD54: A mysterious lump of blue ice blocks the path. It would @be difficult to destroy it. 
0101CE60: ...........Yeah, we sure do. Let's get going... 
0101DCA8: Wah, it's my fake now!
0101EB40: I didn't see any new gadgets being used. Guess they @really are just boring copies.
01037778: It's probably because of that... thing right there. 
0103BC50: And even if there's my fake to deal with, the real one's@right here Heheh! 
0103BD80: That is true. 
0103CD94: ...Just talking about how strong you are. 
0103D020: ...This is the matter.
0103D740: Mm? This sensation... 
0103DAA0: ......
0103DF28: Mm? ...Yikes. 
0103E044: You better prepare yourself, fakester!! 
0103E624: Okay, that's the fragments sorted.
01045220: Gah... It's pretty creepy to be staring at something@with your own face... 
01046218: The surrounding trees have quieted down... The plants @around here must've adored the impostors. I feel kinda@bad about taking them out now.
010462A0: Whew, thank goodness. With how they usually are, I can't@imagine havin' to deal with another set of them.
01046990: We agree on something for once, crow tengu. My shadow,@you shall pay for wielding such a sham imitation of the @white wolf tengu's sword and shield.
01046AB8: A true journalist is always ready to report on whatever @happens, whenever it happens. Repent on your sins in@your fragment.
0104AA5C: Yeah, I kind of know what you mean. 
0104AAA0: Whoa, Reimu! Look! There are more of them hiding@behind! 
0104AB00: The Blue Ballistic Talisman silently glows, and the lump@of blue ice fades away... 
0104AB80: A mysterious lump of blue ice blocks the path. It would @be difficult to destroy it without a Blue Ballistic @Talisman. 
0104AC60: Wah, now it's my impostor...
0104ACA8: Hmph hmph! And mine as well!
0104AD00: But there's no need for fear! All the other fakes @besides mine look like they're nothing special! 
0104AE60: Then again, they don't really speak. So at the very @least, they'd be way more quiet comparatively.
0104AED8: ...True enough. 
0104AF18: Hm!? Did you say something!?
0104AFC0: Reimu, hold it. 
0104AFEC: ? What's the matter?
0104B024: Ayaya, so my impostor finally appears. This has to be a @huge scoop! ...Right? 
0104B060: You must've already guessed that you'd see your impostor@sooner or later.
0104B0A8: Now then, my other self doesn't seem to even have a @notepad. Such unrefined crow tengu should be shown the@door at once. 
0104B12C: Yeah, you guys aren't going to talk, huh? Figured.
0104B23C: This aura...
0104B278: Hmm...? The trees around us are rustling... What's this @sensation...? 
0104B2D0: Ah? I see, so there's one of me as well.
0104B300: Gaaah. This impostor looks like she's gonna be a real @handful.
0104B340: I kinda want to exchange tech notes with my impostor, @but...
0104B3A0: Wahhh... My impostor's just an extra now? 
0104B3C8: Marisa, are you asking for a beatdown?
0104B4FC: All right, that's the fragments collected.
0104B538: Mmm, feels nice to know I won't see any impostors of@myself again. 
0104B5A8: ......You really are... 
0104B648: Truly. Well then, let's keep moving. The search for new @scoops never ends.
0104B8A8: .........Never mind. The impostors have been dealt with.@Let's move on.
