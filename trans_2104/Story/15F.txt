00FCDFA4: Hmm, it's an Iris this time?
00FCE714: ............
00FD0030: Mm? 
00FD9854: This place may be hot, but I don't think that enemy's @made of iron. 
00FD9A40: Oh, shaddup. Let's go!
00FDA55C: Let's do our best, mm?
00FE063C: Ahh, that was really tiring...
00FEF878: The monster became enveloped in light and shrunk until@it became an entirely different shape altogether. 
00FF5F58: All right, then it's time to start a fight. Strike while@the iron's hot! 
00FF8898: Mm!?
00FFF3E8: I guess this'll let us explore other areas now? Let's @take a look around. 
00FFF8D4: Yukari, whatcha doing here? ...Actually, the answer's @obvious, huh. 
01001440: Honestly, why are you so reluctant to ask others for@help? 
010015A8: Indeed, you'll hear no more selfishness from me. I will @follow your group from now on.
01002910: You can treat it as a test. If you cannot beat me,@defeating the Ame-no-Murakumo would be like a dream @within a dream. Now, prepare yourselves!
010055F8: Is that really all you have to say? This is why I told@you to be careful of the pits around. 
01005688: We don't have any problems with you coming along with @us, Yukari. At this point, it's way too dangerous to@explore alone. Let's all go together. 
01007130: Ugh... I suppose I shouldn't be stubborn at this@point... We must seize the Ame-no-Murakumo before it@becomes truly dangerous as well. I suppose I shall have @to join in. 
01007BF0: Oof, ugh... 
0100F698: Maybe.
010188E8: Huh? So if we beat it up, it might drop something?
01018930: Aww, don't sound so harsh. You should come along as @well. At this point, I'm sure even you are finding it @difficult to strike it out alone. 
01019770: Aha, I really should have been. I was really worried@about how I would fare alone then.
0101A250: Oh my, Yukari. Good day, it's been a while. 
0101A35C: When you meet someone you have not met for three days,@you should see her with a new eye. Or somethin' like@that. 
0101AE88: Hmm... There's some monster that looks stronger than the@other fodder. It's camping right in front of us...
0101AFC8: My, it's a little early for that. This is when the@pursuit of the divine sword truly begins... 
0101B288: All right, that settles that. 
0101B2A8: Oh my...? 
0101B2F0: Ooh, your prediction was on the money, Reimu. 
0101B448: Yeah yeah, we gotta beat you first, right? You all are@like open books about this kinda thing. 
0101B4C4: So you ended up joining with Reimu's group? 
0101B4F0: My, you catch on quickly. An opportunity like this@doesn't come often, so we should allow everyone the @chance to let off some steam. 
0101B594: Oh, hush. 
0101B5D4: ...After you fulfill my condition, that is. 
0101B608: I do wish you could learn to find some other ways to@have all of us let off some steam without picking @fights with all of us...
0101B750: I remember when you were having such a hard time with @that fodder monster on the 3rd floor. I would've never@imagined that I would be defeated by that very same @group...
0101B7F0: Geez, why does everyone love to fight so much...
0101B828: This Great Tree has been like our second home recently. @We're used to this kind of thing by now.
0101BB58: Do you feel satisfied enough now, Yukari? 
0101BD48: Hrmph...
0109D738: ...Hm, that enemy gives off the same scent as the @monster that dropped the Platycodon Seal. 
