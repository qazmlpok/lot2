00FC9114: Do you see it now?
00FC9394: ...So, what were you saying about a thief?
00FC9684: I think everyone is too scared to leave their house.
00FC9990: The tree had suddenly appeared in Gensokyo around three @days ago. 
00FC9B38: Oh, hello Keine. There's still some time before our @appointment.  
00FC9B70: The value of the �gAme-no-Murakumo�h that was stolen@around a week ago really stood out from the rest. 
00FC9C38: I know right? So we should eat them quickly, before the @others get here.  
00FC9D9C: No thanks, I'm fine~. The dumpling I have in my mouth @might get stuck in my throat. 
00FC9DDC: Gensokyo, Human Village. Main Street. 
00FC9E20: I got it already! Thank you very much.
00FC9E54: I should probably give an explanation for that. 
00FC9E88: It's currently nine in the morning. It's usually around @this time that the people are the most busy, and the@village gets more and more active, but... 
00FC9EFC: So in short, he's making up the numbers...
00FC9F2C: That atmosphere's fine with me. Better than the heat at @least.
00FC9F70: It is as they say. A thief broke into my shop...  
00FC9FC0: ...Lately, this place has felt really empty. Isn't this @supposed to be the main street? 
00FCA030: It would be fine if it just disappeared in a day or two.@But it is not likely something that massive will simply @disappear if we leave it alone. 
00FCA0A0: An outlandish towering tree was piercing through the@clouds, its leaves and branches growing outward in@order to create a maze of levels. The Great Tree's size @was so ridiculous that anyone who raised their head @even slightly couldn't help but see it. 
00FCA1D4: ...Yeah, something like that could be bad.
00FCA228: You aren't even bothered by it... 
00FCA258: Yep. This summer looks like it's gonna be pretty cool @this year. I won't have to go cool myself at Kourin's @place.  
00FCA328: (That's how I was able to get it off her hands for dirt @cheap...) 
00FCA3B0: ......Wait a minute, what are you talking about?
00FCA3E8: Ah, but it's refreshing. It's cool and peaceful. Peace@is good. Everybody says so. 
00FCA480: Just shut up and look.
00FCA4E8: Look at that! Nothing else could scream "incident" more @than that. It's frightening everyone. You need to do@something about it quickly. 
00FCA5A0: That's how it is. Incidentally, Reimu also mentioned@that she had a feeling that the culprit was in that @Great Tree. 
00FCA628: Of course you're not going to see "it" while your eyes@are glued to the food down there! Look up and tell me @again you cannot see it.
00FCA6F8: With her face forcefully lift up, Marisa could see an @out-of-this-world massive tree, calmly rooted in the@distance. 
00FCA7A8: And so, let's all give this our best. Onwards and @upwards!
00FCA820: Is that so? Reimu, haven't you been poking your nose@into the thieving uproar that happened at Kourin's@place recently? Are you done with that already? 
00FCA898: A big tree like that isn't something to laugh at. But @hey, when looking at something that big, you really @can't help thinking 'bout human life, right?  
00FCA948: It's clearly the thief who's at fault. I am the victim@here... And really, I'm not even suited for fighting... 
00FCAA38: In fact, a portion of the crops and flowers that were @planted in the fields in the village's outskirts have @already begun to wither away. It is not difficult to@imagine that if we leave the tree alone, things are @only going to get worse.  
00FCAB28: Ain't it good though? We get a cool and sunlight-free @summer in return. And you can take the opportunity to @mass-produce bean sprouts.
00FCABF8: The Were-Hakutaku who set up an elementary school in the@human village took anything that would threaten its @peace very seriously. 
00FCACB8: Rinnosuke Morichika, a half-human half-youkai hybrid who@runs an antique shop located at the border of the @Forest of Magic. He was the last of the four for this @meeting.  
00FCAD50: This girl...! 
00FCAD84: Ooooh...... 
00FCADF8: ...Well, I also was saying to myself that it's about@time to investigate that Great Tree. The timing is@perfect, I would say. 
00FCAEA0: Most of all, how were you able to get an artifact like@that?! It's the first time I've heard about a relic @like this being in Gensokyo! Wasn't this heirloom @enshrined and worshipped somewhere in the outside @world?! 
00FCAF48: Let me see... Kourindou, was it? A thief broke into that@antiques store? What is that and the "Ame-no-Murakumo"@about? This is all news to me...
00FCAFC8: No, I'm right on time. One should be neither too early@nor too late for any appointment. Being late is @obviously out of the question, while being early makes@the other person worry too much about whether they@should have came early too. 
00FCB0C8: I see... So while you were searching for your thief,@that great tree sprouted forth and caused an even @bigger uproar.  
00FCB248: The Great Tree grew just a few days ago, and the theft@happened a bit before that. 
00FCB328: Well, we didn't know that Rinnosuke had such a thing in @his possession. And if it is true, there's no way we@can leave such an artifact alone. 
00FCB438: "Ame-no-Murakumo," huh... ... Wait, THE @"Ame-no-Murakumo!?" 
00FCB608: ...And that's how we decided to include Kourin in our @exploration of that Great Tree. 
00FCB778: Well, whether or not that sword is THE genuine@Ame-no-Murakumo handed down from ancient times... It@was definitely made of pure scarlet gold, and it was@undoubtedly an extremely powerful sword. Of that, I am@certain.
00FCBA1C: ...Trade secrets. 
00FCBA70: After all, it's his fault for keeping quiet about the @sword, and not securing it well enough. 
00FCBB94: Ohhh~!
00FCBDF0: You don't have declare that to everyone, you beasts...@Ugh, I don't want to go...  
01019094: ......Does this village really look peaceful to you?
01019370: Yes yes, your opinions reaaally matter to us. 
01019948: Guh.
01019AD4: Oh, Keine didn't know about it after all? 
01019E88: Really? ...Well, Reimu's intuition is generally right in@these times.
01031D3C: Keine grabbed Marisa's dumpling-stuffed face and forced @her to look up. 
0103E538: Yea, since we're a bit early for the meeting time, we @can do that while waiting. The dumplings they make here @are really delicious. 
01043B08: As you may know, my shop sells items that came here from@the outside world. There are also some other small and@rare articles that are not for sale and that I keep for @my personal collection... 
01045538: I'm certainly expecting nothing from you in battle. But @there's strength in numbers when wandering in a big @tree like that, don't you think?  
0104AF28: Well, we can't leave that tree alone anyway, right? So I@figured since Keine was being annoying enough to call @this meeting as well, that it was time to investigate @that Great Tree and kill two birds with one stone.  
0104CD80: You treat your house like a storeroom, and barely give@it any ventilation... That's why it's so hot in there,@even though the forest it's in helps block the sun's@rays. 
0104DCE0: The rainy season was at its end, and the blistering heat@was flickering as the early summer season began. Yet, @the main street was bereft of residents and their @liveliness. The whole village was wrapped in a chilling @atmosphere unbefitting of the current season. 
0104ECA0: Of course not. There's no way we can leave something as @ridiculous as the Ame-no-Murakumo being stolen alone, @don't you agree? But, no matter how I looked and@searched for the criminal, they didn't leave a single @trace.  
0104EEA0: (I can't possibly say where I found it, especially not@to Marisa, since I kind of tricked her into giving it @to me... Well no, it's not really tricking Marisa,@since she didn't know it was the Ame-no-Murakumo...)  
0104F128: (In the first place, I'm the one who wants to know where@she discovered that... Once in a while, her hobby of@accumulating trash pulls up some unbelievable @treasures...) 
01050200: Oh well, it's fine, right? We can just eat dumplings@while sitting on the storefront since there's so few@people. 
01050A38: The meeting point was a dumpling shop facing the@village's main street. And so, the two girls ate@without a care in the world, as they waited for the @others. 
01050FC8: An incident~? Whaddaya mean? I can't see aaaaanything at@all. Anyway, one more dumpling please.  
01051540: It appeared suddenly in the Forest of Magic, with no@clues as to the cause. Even now, it continues to grow @slowly but surely, obscuring the daylight to Gensokyo @and creating an ever-growing curtain of darkness. 
01052130: And so, Keine raged on about how "we are going to find@the cause of this Great Tree today" and Reimu and @Marisa were forcefully dragged into it... 
01052480: No, no, wai- Wait a minute, are you sure it's the @authentic one?! This isn't the time to talk about it's@value! This is most famous and powerful sword in@Japanese lore!  
01052FA8: Me too, I'm really interested to hear about how you @found that sort of thing, Kourin. 
010532F0: After hearing that story, I started looking for the @Ame-no-Murakumo and its thief...
01078D90: Oh my, Rinnosuke. You're pretty late. The last to show@up, in fact.  
01078DC8: But up till now, the culprit remains at large.@*sigh*... 
