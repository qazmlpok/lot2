00FCE714: ............
00FDD220: Whoa, bright... I mean, what's going on!? 
00FE1C10: We've finally caught up with you! Give it up! 
00FE726C: !? Be careful, something's coming from within the @light!
00FE7890: It was certainly in a rush, though. As if it did not@have the time to deal with us.
00FF8DB0: The final battle's near, I can feel it. Let's keep going@with our best.
01006CA0: Yeah! 
010096B0: Hmm...
010099E4: Yeah. 
01011250: Ah, Reimu! Over there!
01011528: Why'd that be?
01011D2C: ? It's preparing to do what...
01011D54: Whew, at least we won.
01012274: Who knows? I certainly can't claim to even imagine what @a sword thinks. 
01019F70: Reimu, wait. It's preparing to do something. Approaching@it now will be too risky. 
0101E940: Ah! It's the sword! 
0101EA80: The Ame-no-Murakumo still managed to get away in the@confusion though. Honestly... 
0101ECE8: Well, it doesn't matter, hey? We're definitely got the@Ame-no-Murakumo in our sights now.
0101EE20: Oh, that is true. 
010A1340: Geez, it's so blinding, I can't see a thing! Did the@Ame-no-Murakumo do this!? 
010A1408: Are you serious!? Attacking us while we can't see is@just plain unfair!
010ECAD0: Now now, no need to get so agitated. The very fact that @the Ame-no-Murakumo pulled this stunt is proof that @it's starting to feel cornered. It could've just wiped@the floor with us, like on the 12th floor.
010ECB98: Since it didn't do that, it means that it can't get rid @of us so easily anymore, probably. It pulled out those@orbs either to buy itself some time, or that it felt@that it was in danger.
