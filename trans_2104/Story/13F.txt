00F741EC: Ooh, now this is cooling. 
00FD3E24: *sigh*... 
00FD70D0: Wow, good work Marisa.
00FD71D0: Hm? 
00FD8DD8: Whaaaat... Why...?
00FDD804: Yep. We found your 4 sutra scrolls for you. Take them.
00FDEFB8: It's one of Lady Byakuren's sutra scrolls!
00FE4A38: Ohhhhh!? It's hot! No, it's burning...! 
00FECF50: Mm, I do not know if I will be able to meet your@expectations... But I shall give you my best, @regardless. 
00FEDE40: Seems so. They're all stuck really firm in the ground,@but Lady Shiki herself is nowhere to be found.
00FF1778: Definitely! Someone as tough as you is always welcome.
00FF1A80: ...Wait, why were you calling for me so loudly for this @time? 
00FF60D8: Reimu, can ya force-open the barrier somehow? 
00FF6B34: Huh, this voice...
00FF719C: Ooh, so hot...
00FFB63C: Let's refer to our barrier specialist on the scene. Ms. @Reimu, please give us your expert opinion.
00FFD518: Yeah. Strange, there definitely was a reaction... 
00FFD978: Sure did. Use your power to make this area a li'l @cooler. 
00FFDAC0: Ooh, this is great. I feel all refreshed now. 
00FFE398: That may well have been the case, but to have the person@who started the whole ruckus to be proclaiming her@innocence in such a manner is a little... 
00FFE650: Yeah, we should. Let's get out of this stuffy place,@stat. 
00FFE748: C'mon, Marisa. Let's make up some ground while we still @can remember what that coolness felt like.
00FFEFA8: As shameful as it is to admit, that has become apparent.
00FFF32C: Hah, how'd you like that? Are we suitable to our dear @Enma's eyes?
01000A38: Ouch... To have attained such power in such a short @time... Your group are treating this more properly than @I had foreseen. 
010012A8: As such, may I request that you assist to find the sutra@scrolls I lost? There should be 4 of them.
01001A30: Yes. With such a clear victory on the record, I can have@no complaints. I shall be accompanying you for a short@while.
01001D08: Komachi, I will not enter the bath at the same time as@you. Do ensure that our bathing times are staggered.
01001E38: ...But there's nothing. 
01002020: Yes, it does. If you have space, may I join your party? 
01002A58: I could if I had the time... For a barrier like this, @I'd have to work on it for about a day. 
010033A8: ...There, finally. It's this. 
010038A4: Right. I think doing as she says and searching for the@scrolls would be faster.
01004A18: (That sounded like something she came up with on the@spot...)
01004BF8: ...It looks on the verge of death from the heat.
01005278: ...Allow me to say this before it becomes an issue. 
01007CB8: Huh. If you needed to make an impromptu resting place,@that means that you're having trouble exploring alone,@right?
010099E4: Yeah. 
0100A5D0: Great! We're getting a lot of strong people in now. @We'll be counting on you, Eiki. 
0100C988: Is something wrong, Nazrin? 
0100CF5C: Hm? What's with this place? 
0100D004: 4!? Geez, that's a lot. 
0100D688: She makes it sound so simple... 
0100D810: We shall begin now! 
0100D9A4: Make it cooler, huh...? Heeey, Cirno! 
0100E10C: Komachi!
0100E1AC: Ehh? Why? 
0100E6FC: (...It can't be that she just likes to fight, can @it...?) 
0100F100: ...Hmm? Hello, hello? Is there someone outside the@barrier?
01011434: These tablets are yours, right? What's this supposed to @be anyway?
010119D8: Would you be the Great Tree exploration party? ...I @suppose that is a silly question. Who else could it be? 
01011D70: So it'll be a bit like the gas range that Rinnosuke has @at his house? That thing can kind of control fire by@slowly emitting some kind of atmosphere that burns@easily. 
01012E40: No way! Why do I gotta do something like that!
01014220: Maybe. We can't do anything here without her around...@Let's move on ourselves.
01014540: We can't do much but keep goin' on this path illuminated@by flames, while we dream of the shaved ice we're gonna @have once we get back to the village... 
01014FAC: Oh my, Marisa? This must mean...
01015730: Just when I thought we had escaped the desert... It @would be dangerous to stay in this heat for too long. 
010157E0: ...Man, someone sure forgot about all the stunts she@pulled as an enemy real quickly.
01015830: My goodness Marisa, are you still going on about that?@How unrefined! Haven't you realized how cool the@situation is? The rival finally becomes an ally! Then @they join forces to take down the true enemy! 
01015A10: Ahh, I see. That is something I am more familiar with...@The heat that these flames exude are no less harsh than @a real fire, though. The journey will be even tougher @than the desert...
01015B68: What rival? And in the first place, if you didn't lay a @finger on the Ame-no-Murakumo, this whole incident@wouldn't have happened. 
01015BD0: Ugh... Weren't you listening to it, Reimu? It already @became a tsukumogami and attained its own will! Even if @I didn't do it, the same thing would've happened@anyway! 
01015CE8: Dessert!? Haha, I guess it can't be helped then! Gaze in@awe at my awesome power!
01015ED0: My goodness! Thank you very much. I was truly worried @there.
010161AC: ...These are wooden tablets, huh? That means these are@Eiki's? 
010165C8: When did our objective become so grand again? Reimu,@she's misrepresenting us. 
01016630: The magic written on these are hard to see for@non-magicians, and it just happened to flash when I was @lookin' around. We're lucky it didn't land in a fire, @else there's no way we'd find it without some exact @dowsing.
01016760: And no matter how far we go, all we see around us is@crimson flames. It's like we're in hell. Ahh, this is @tough on the ol' motivation.
01016830: There is, it's here... Oww, hot hot hot! Grr, the @ground's so scorching hot, it's hard to pick stuff up.
01016898: S-Shut up! Shut up shut up shut up! Let's just keep @moving! If we don't stop the Ame-no-Murakumo fast,@Gensokyo's in big trouble! We're the heroes who have to @protect everyone's peace and happiness! 
01016A40: Hmph! You're getting all weak in the knees over a bit of@warmth, Iku? How shameful!
01016DB8: And no matter how far we go, all we see around us is@crimson flames. It's like we're in hell. Ahh, this is @tough on the ol' motivation.
01016F28: Maybe these are the remains of one of her battles? She@might have went off ahead.
01017070: Hey yeah, that's a good analogy.  
01017108: All right, I guess that should do it! Do your best and@beat this heat, okay? 
01017218: Look. My dowsing rod just reacted to something around @here... 
01017284: Now then, down to business. Reimu, Marisa, what are you @doing?
01017358: ...Eh? Wait, there's somethin' on the ground here.
01017388: But they are still mere wooden tablets, they cannot @completely stop monsters from advancing. I gave it the@order to simply stop whatever it could from entering. 
01017468: What? I don't see anything. 
01017578: Thus, I proceeded to create the barrier. But right after@it was finished, eep! I realized the scrolls that were@necessary to remove the barrier had been lost during my @journey!
01017660: All 4 of the sutra scrolls are required to remove the @barrier. I will be counting on you, thank you.
010176C4: ...? Ahh, this is...
01017774: Heheh, I'm pretty much a pick-up artist, y'know?
01017844: Hm? There's a barrier here... 
010178A8: Mmhm, leave it to me. Let's see here... 
01017938: The monsters this high in the Great Tree are pretty @strong. Stayin' here for an entire day's gonna be a big @ask...
010179E4: It is I, Byakuren. I am on the opposite side of the @barrier.
01017A24: Byakuren, why'd you erect a barrier here? 
01017B8C: "Eep?" Was that really the only reaction you had...?
01017C74: ...And where can we find a flower in this burnin' @forest? 
01017CB4: Huh? Um... Er...  
01017D84: ...Ah! Over here! There's one here! 
01017DC8: At times like these, you should rest your eyes on a @lovely flower! It will soothe your mind and make you@feel at ease. This is how humans can forge their@minds.
01017FA0: Hm? Were you callin' me?
01017FD8: ...Ahh, the atmopshere around the flower has cooled to@the perfect temperature. Now I'll just use my power to@give this girl a bit more energy... 
010180E8: Guh... It's just a few seconds since Cirno stopped, and @I already feel like I'm losin' against the heat...
01018258: Ahhh! Don't drag me around! 
010182E0: Yo, Byakuren. 
01018438: It ain't like you to lose something so important though.
01018504: Yeah, I guess things like fighting and running a lot can@make you forgetful. 
01018540: The place that Reimu's party arrived at was adorned with@10 wooden tablets with laws engraved on them. 
01018670: ......That may be for the best. But before that, shall@we do battle? 
010186E0: Oh? Does that mean what I think it means? 
010187F8: ...Ehh? Ah, Lady Shiki? It's just a natural reflex to @whenever you call for me loudly...
01018AB8: Reimu and Marisa are used to leading these kinda@dangerous charges. This is like a daily routine for @them. 
01018BA8: Huh, I see. Make it weaker then. A whole lot weaker. I@hate the heat.
01018C18: ...These are wooden tablets, huh? That means... 
01018DE0: Eek! I'm sorry! 
01018E10: ...What are you apologizing for?
01018FD0: Yeah. It's actually pretty fun. Though the more people@we got, the louder it got too.
010190DC: Ehh? Y-Yes... 
01019188: (Maybe she likes yellin' her name for fun?) 
010191B8: ......This is non-negotiable. Anyway, please take the @necessary measures. 
010192D0: Sounds like a good opporunity then. Why don't you come@with us, boss?
01019310: Hmmm... This orb seems to be controllin' the amount of@magic 'round here.
010196B4: Hm? What's this crystal?
01078E08: Hmm.. The trees are burning, but the fire itself doesn't@seem to be spreading as you would expect. Which is@strange considering how fierce the fire is... 
0109D4B8: Tch, that ol' hag... Fine, let's keep an eye out for@'em.
0109D548: Komachi. If your immediate reflex is to apologize once@called upon, it means that you are still hiding @questionable matters within your heart from me. Be@aware of your own shortcomings. 
0109D5E0: ...By the way, I have heard that you have rented out@several rooms in an inn in the human village and made @it your base of operations, correct?
010A0E60: ...Ehhh? Why? ...Wait, I don't mean that I WANT to bathe@with you, but to be rejected up front like that is@pretty shocking...
010DFAE8: Yeah it does. It's taken root in a very harsh @environment... I wish there was a way to make the area@here cooler...
010E22C0: We tend to go back to the village as soon as we get @tired, so the heat itself shouldn't be too much of a@problem... The trees on fire all around us worry me a @bit more. 
010E2370: Ahh, that does sound good. I want to cool off as well.@C'mon, Cirno. I'll give you my dessert from tonight's @dinner if you show me your power for a bit. This should @be nothing for the strongest fairy, right?
010E3160: That's probably cuz of the trees as well. Look at this@tree's branches, there's a bit of magic oozing from it. @That bit of magic seems to be providing the fuel for@the fire. 
010E3210: Ahh, the enemies have become very strong in this area.@As I needed to rest, I created a powerful barrier that@would require the magic of the sutra scrolls to remove. 
010E32C0: Mm... I do not consider myself particularly @absent-minded. Most of the occurences happened after@continuous battles with hordes of enemies, or during my @explorations... 
010E5018: It appears that way... And as the monsters in this area @have become a major obstacle to overcome, this may well @be the limit of what I can do alone...
010E55F8: Not much, really. We just happened to find this place @with all these wooden tablets standing upright. It was@hard to ignore. 
010E5D48: Yes, they are mine. These wooden tablets repel monsters.@I used them in a formation to protect this area from@potential hostile intruders as much as possible, so @that I could rest here. 
010E6698: I wish to test your resolve to solving this problem. If @you treat this battle with the seriousness it deserves, @I can expect similar results in the battles ahead.
010EC300: I told ya earlier how's there's magic at the trees@that's fuelin' the fire, right? This orb should be able @to change the amount of magic used, which'll make the @fire stronger or weaker.
