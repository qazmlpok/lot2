00FD8CA0: Now then, where are the black fragments...
00FD8FC4: That's my question too. 
00FDD480: It was a pretty tough opponent, yeah. 
00FDF73C: Ahh yes, this is mine.
00FE0120: All right! Gotta find 'em all!
010005E4: That's a pretty big group they have there.
010015E4: ......I think it's just my imagination... 
01009A60: Since you will not be lasting much longer. Shall we @begin?
0100CEC8: Whew, we managed to emerge victorious. We've defeated @most of the impostors by now, surely... I wonder how@many there are left.
0101647C: Whenever Reimu gets like that, it's never just her@imagination.
01018278: I had thought traveling with Reimu after the crux of the@incident was over would mean I wouldn't have an @impostor... but my thinking was apparently too naive. 
01018CC8: Even I have an impostor now too? Yamata-no-Orochi, how@dishonorable of you to make a copy of a non-battle@member... 
0101B310: A familiar aura...? I don't feel anything like that at@all...
0101E7C4: With the merest of touches, the ivy blocking the path @suddenl withers away, and the path ahead opens... 
0101E96C: T-That ain't what I meant! What're you talking about!?
0101EC98: A line like that would still make me feel a little shy, @you know? 
0101EEBC: I really have, huh! Coming all this way to the bottom of@the underground really helped!
01026F8C: ......Hm. 
0102AF18: Now then, what awaits us further on? A rock or a hard @place methinks... 
0102DB10: What's the matter, Reimu? 
0102DEC8: ......All right, I've collected the black fragments.
0102E05C: What kind of aura?
0102EA00: Wah... Even an impostor of mine.
0102EE00: You were muttering similar things on the floor above. 
0102F6D8: ............!!
0102FC00: You've gotten a lot stronger, Chen. I couldn't be any @more proud. *pat pat* 
0103A014: Ain't no difference if we beat them up now! Let's go! 
0103B608: I just kinda... felt a familiar aura for a split@second... 
0103BF48: This is a surprise. Even accounting for my use of the @Coral of Silence and Seaweed of Serenity to weaken my @mind-reading, I never expected to find beings besides @Koishi who are immune to my powers. 
0103DF74: Mmm...
0103DF84: Though there are still some impostors remaining.
0103E02C: What're you on about now? 
0103E13C: And it really was a split second... Hmm, what was @that...?
0103E22C: Why in the blazes is an impostor of mine actually @reasonably strong...? 
0103E398: ...Mmm, sorry. It's probably just my imgination. Let's@keep going. 
0103E928: Fweeeeh! It's me! 
0103EBB0: Now that we've found them, there's no way we can leave@them be. Imitations, prepare yourselves!
0103F4A4: Ah-ah. You're such a bully, Rinnosuke.
010400F0: You've got a long way to go before you can tease me,@Marisa. Heheh.
01040994: Here they come! Get yourself ready for battle, Chen!
01040F6C: Having your impostor outta the way does take a load off @the ol' shoulders.
01040FBC: ...Hm? That's...
01041358: Mmph. Still, I get what everyone keeps sayin' now...
01041394: Hm, I see something over there... 
01041714: Bwah!?
01041C64: ...Is that so, Ms. Impostor?
01041F0C: This might be just my imagination, but that impostor@seems like it doesn't know how to answer your @question... 
01041F48: Uh-oh. I think I made it angry. 
010453F8: We haven't finished the job yet. We need to find all of @them just to make sure they don't get us into any @trouble.
01048968: Oh my, Marisa. You want to build a harem with multiple@copies of me? How bold. 
0104DCC0: Oohhh! You've finally appeared! 
0104DDE8: They're definitely prepared...
0104DE40: That's that settled!
0104DF18: That it does. 
0104DF48: I didn't even stick my neck into the woods here till@Reimu's group got done with most of it, but I already @have a fake of my own, eh? And there's even...
0104DFCC: All right! Now we've got our spirits up...! 
0104E00C: ...That good mood lasted a whole three seconds... 
0104E078: Oh well, this is like one of those fated encounters! We @can't let them get away!
0104E108: ...With our spirits up, we can continue our exploration @through the underground of the Great Tree teeming with@ghosts... 
0104E170: That is true, but still...
0104E270: ...*sigh* It's hard to get into the mood like this... Oh@well, not like I have any choice. Let's just do this. 
0104E308: We shoulda captured that impostor and make it take@Kourin's place in battle, haha! 
0104E440: C-C-C-Cuz he's saying weird stuff and it's embarrassing @and weird and stuff!!?? 
0104E480: Ahhhhh geeeeeeez!!
0104E4B8: There she goes. 
0104E500: Hahaha. Marisa and I have known each other long enough@for me to figure out some weak points of hers.
0104E570: ...But it's nowhere near as big as our own! Fuhaha, @they've picked the wrong opponents! 
0104E5FC: .......Mmm, hmm? Is this finally... 
0104E638: ....Mine too. 
0104E700: Fuhaha! How was the taste of my dish!?
0104E784: ...Did my best... with everyone?
0104E7DC: All right, collected. Now I don't have to worry about my@impostor anymore. 
0104E818: Ohhhh!
0104E864: Ahh! Our impostors! 
0104E8F0: ...Oh well. It's a small matter.
0104E940: Whatever the case, we're in this for the long haul. @We're exploring every single inch of this tree until@all our imitations are dealt with.
0104E9BC: Fwaaaah.
0104EA24: This sensation... So you've finally appeared. 
0104EAD8: Wahhhh! I'm there too! Wow wow wow, it really looks @exactly like me!
0104EB60: Isn't it just cuz you both have the same memories, so @you can't read it's mind? 
0104EBB8: Whew! We worked really hard!
0104ED40: I didn't think it would take so long to show up, though.@It's just my impostor. I figured we'd fight it by the @first or second underground floor.
0104ED9C: All right! Now that Lady Ran's praised me, I gotta try@even harder!
0104EDE0: Yeaaaah! Come on, impostors! Lady Ran and I are gonna do@our best! 
