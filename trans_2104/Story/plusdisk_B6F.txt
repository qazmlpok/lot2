00FDD014: Oooh... Geez, master! 
00FE3F6C: You think so? Heheh, that's kinda embarrassing... 
00FED7A0: The entity who has borrowed my apperance has finally@shown itself. I may not have been going out of my way @to look for you...
00FF3F50: Waaaaah! You damn impostors, you'll pay for this! 
00FFA68C: Er, of course not...? Ah... hahaha... 
01016C40: Hmm... I can't argue with that, but still...
010171AC: ......*sigh* As long as you do it after our @explorataion... 
0101D03C: Huh, master? What are you doing there? Exploring alone@is dangerous, you know? 
0101D274: Man, this impostor's carrying a really dangerous@scythe... That's scary. 
0101D558: It does bug me, actually. It was her scythe to begin@with. 
0101DA50: But now that you have appeared before me, you will not@escape. Your life ends here.
0101F6AC: A mysterious lump of purple ice blocks the path. It @would be difficult to destroy it. 
01028228: Ahhhh! Nothing, nothing at all! 
01039288: .........Now that the weight of our impostors have been @taken off our shoulders, perhaps we can have a nice,@long talk together when we return.
0103BE6C: ...Oh. I see. 
0103C4A8: That just means both of you are strong enough as is. You@most certainly have talent beyond studies. Have some@confidence in yourself. 
0103D0E4: (I just hope we're being helpful to the rest...)
0103D434: Whoa, don't just stop in your tracks out of the blue! @What's the matter?
0103E74C: ......Oh my.
0103E970: Despite being impostors of us, they were pretty tough.
0103EAD8: Hah. That huge scythe was just for show.
0103EB9C: Does it even affect you?
0103F208: What was that?
01046B28: C'mon, does it matter? This is kind of like a fated @encounter, so shall we just defeat our impostors@promptly? 
01046D48: To be honest... We're just normal humans. Would copies@of us be helpful at all?
0104B608: Udongein, who were you talking to?
0104B6C8: ...Uh, what? .........Ah. 
0104B6F0: .....Did you just think my impostor was me? 
0104B744: ......*sigh* You've still got a long way to go. 
0104B818: (...They weren't even at fault for that, though...) 
0104B864: Okay, the fragments are collected.
0104B950: Look over there.
0104B97C: Oh boy. 
0104B9AC: It's ours this time.
0104B9F0: I guess so... All right, normie impostors! Prepare@yourselves! 
0104BA30: Ahhh, that was tiring!
0104BAA8: Whew, that's good. There won't be any possibility of@Udongein mistaking impostors for myself again, heheh. 
0104BB58: ......Hm. 
0104BC40: Truly normal humans wouldn't be here in the first @place.
0104BCEC: I know, but still.
0104BD34: ...Let's just get started already.
0104BD98: ...Which was a copy of yours. 
0104BE30: Hey now, isn't that enough of that? 
0104BEA0: You can be a real pain sometimes... 
0104BFEC: Oh, you just noticed now? 
0104C05C: Ahhhhhhh! I'm sorry, I'm sorry! 
0104C138: ......You are carrying one as well. In fact, yours is @the original. 
01050338: The Purple Ballistic Talisman silently glows, and the @lump of purple ice fades away...
