00FD3380: ...I'll think about doing so after the Ame-no-Murakumo's@been defeated.
00FD3B60: Whoa, this sensation... The atmosphere here is@overwhelming. 
00FDB0B0: Ohh, once again as you expected, Reimu. 
00FE9A58: Yeah! 
00FEF878: The monster became enveloped in light and shrunk until@it became an entirely different shape altogether. 
00FF6EA4: My, you too, Keine? And as a tour guide, what?
00FF94B0: *sigh* You two are truly idealistic to the very end...
00FFC120: Ohhh... The power accumulated... It is fading...
00FFC318: Ehhh. Well, as long as it's over and dealt with.
00FFC724: Ehh, really? C'mon, I'm sure you wanna go.
00FFD148: Nothing happened to it. It hasn't said a word since @then. 
00FFDA60: ...That's true. I've been on this ride from the start, I@suppose I'll ride on that idealism a bit more.
00FFED0C: Sorry, I think I'll pass. I've been away from the shrine@for a while, I need to do some cleaning up around here. 
00FFF1E0: Are you implying that I am to be a mere commoner spirit?@I, the bearer of the power to control Heaven? 
00FFF57C: ...Though the same applies for the sword. 
00FFFFA0: ...That's not true. Anyway, you all should be setting @off now before the sun rises, or it'll get too hot to @travel. 
010003C0: Yeah, looks like words won't have any effect on you...@So bring it on, sword of the heavens! You may think @your existence is almighty... 
01000E5C: Pardon us, drive-by extermination coming through! 
01002DEC: Geez, Lady Yuyuko! You eat too quickly! 
01004058: Honestly, that shiny sword's a nuisance to the very @end.
010042C0: ...Eh? Marisa formerly owned that sword?
01004974: Mmm... I just felt like going on a tour around@Gensokyo. 
01004D94: My, how surprising - is it going to rain tomorrow? What @brought this on?
010052E8: Yep yep. So it might be stronger than before, ya say? @We've gotten way stronger since then too. 
01005384: Ehhh!? I was hoping to keep the number of people to a @minimum...
01005F88: We're stoppin' ya cuz there'd be a huge mess if we@don't, ain't it obvious? 'Sides, you think you can get@away with making yourself sound so grand? Ooh, it's @your life's duty, I'm so impressed. 
010060F0: But it's that idealism and their ability to overcome@whatever that's put in front of them that brought both@you and I here, mm? 
010067C0: Yet you would still lump everyone else besides you as @"mere commoners" without a second thought?
01007250: Though Byakuren said that if the Ame-no-Murakumo really @wanted to, it'll break that barrier in an instant.
010083C8: Izzat so? In that case, I guess I'll go on a trip 'round@Gensokyo too. Completely separate from yours, of@course. 
01008DB0: Or perhaps it started the incident to elicit growth from@Marisa, its former owner... Hmm, not that likely, is@it? 
01009E08: ...We've finally caught up to you.
0100BF4C: ...It's kinda boring now though. Got anything @interesting goin' on, Reimu?
0100BFB0: ...No, that's not it. 
0100C1B8: Ah-ah, there's no stopping her now. 
0100C480: How 'bout you, Reimu? Wanna come along? You've got@nothing to do anyways, right? 
0100C938: You should be careful, they may hear you. 
0100D210: Hmmmmmmm.... Well...
0100F698: Maybe.
0100F9B4: I suppose it is.
010103D8: Your life has barely started. Take some time to see,@hear and know more about this place.
010106D4: Is the Human Village going to be okay without you?
01010960: ...Hm, that enemy gives off the same scent as the @monster that dropped the other seals. 
010116A4: All right, then it's time to start a fight. First strike@wins the fight! 
01011BF8: By the way, these dumplings are great. Seconds, please? 
010120EC: Reimu, if you really want to come along, we can make@room. 
010124FC: You will struggle against me again? You believe you can @defeat me?
01015278: And us coming here to stop you is one example of that.@Do you get what we're saying? 
01015E18: The originator of the incident was the Ame-no-Murakumo, @given life as a tsukumogami. The girls resolved the @incident with their strength and brought peace back to@Gensokyo. 
010163B8: ...Kourin, what's with all the luggage? Got fed up with @your shop and tryin' your luck with street stalls now?
01017F28: It is the divine sword that controls "Heaven." At this@height, the dragon god's dwelling is right before its @eyes, and as it ascends further upward, it's power will @grow even further.
010182F0: If ya really were aimin' to control and protect @Gensokyo, you chose pretty much the worst way to go @about it. 
010188E8: Huh? So if we beat it up, it might drop something?
01018A40: Not happening. It's been sealed really, really deep in@the main building with a barrier. It's too annoying to@unseal it.
01018B28: What were you expecting when you chose such a blunt and @crude way to try and control Gensokyo? For a divine @sword, you sure are narrow-minded.
01018D30: After all, you are the Ame-no-Murakumo, the divine sword@descended from the heavens. Your power and status is@more than sufficient for it.
0101A920: The life I have been bequeathed shall be used fulfill my@duty. Who would dare to oppose me? And for what @motives?  
0101AE88: Hmm... There's some monster that looks stronger than the@other fodder. It's camping right in front of us...
0101AF48: Hmm, that sounds pretty awesome actually. Now's the best@time to tour 'round Gensokyo too, while the Great @Tree's still around and keeping things cool. Rinnosuke, @I wanna go on this Gensokyo pilgrimage too! 
0101B180: Huh? There's no point then. ...I guess it means the @sword really is just stayin' silent then? Are you sure@it really still has its own will? 
0101B288: All right, that settles that. 
0101B2A8: Oh my...? 
0101BA08: Huuuh, so that shut-in antiques dealer is actually going@outside. Yukari, your power is really helpful for @peeping on others like this.
0101D590: But it's no big deal in Gensokyo, and us commoners are@gonna show you just that! 
0101D5E0: ...I have a rough gauge of the geography of Gensokyo, @but I'm entirely unfamiliar with how to get to the@places I never visit. I don't want to get lost and be @attacked by random youkai either... 
0101D918: A "world" which disregards the bonds between people is@meaningless. The world is not as simple as you think it @is. 
0101E488: Yeah, I guess that means we can find new places now.@Let's look around a bit.
0101E528: ter you understand this world a little better, if you @can find a way to truly rule and protect Gensokyo, feel @free to try it again. 
0101E698: Oh, you're right. That won't do. We'll be wandering @around Gensokyo for the time being, then. 
0101EB98: I would like to believe that the dragon god would be@fine... But with this power we can feel even at such a@distance, it's hard to discount the worst case. 
0101EE48: Ara-Mitama? Have we seen any places blocked by this @seal? 
0101EFAC: Ohh... Ooohhhhhh... My strength... My duty... 
0101F334: My, Rinnosuke? See you away from the store and at the @shrine is certainly a rare sight. 
0101F3E0: The esteemed dragon god maintains the balance of the@five elements and the weather of Gensokyo. If the @dragon god is defeated by the Ame-no-Murakumo, who@knows what fate will befall Gensokyo... 
0101F648: Mikagami? Have we seen any places blocked by this seal? 
0101F758: That's most likely due to the Ame-no-Murakumo. It's @probably somewhere ahead, charging its power to fly to@the dragon god and do battle with it. 
0101F810: Even if you take away the charging up power bit, it @still feels even stronger than when it wiped the floor@with us on the 12th floor.
0101F940: ...After two millenia. As a tsukumogami, I have been@given a purpose. I have been given life.
0101F980: Accordin' to Yukari, it'll disappear eventually once the@Ame-no-Murakumo's settled down. But since it's so dang@big, it might take awhile.
0101FA70: Why are you even talking about that possibility? We're@here to ensure that doesn't happen! 
0101FAD0: But allow us new-age gods to teach you one thing. Gods@can only survive on the fear of humans and animals. 
0101FC28: I wish you'd call it more "optmistic" or "positive" than@"idealistic" though.
0101FD48: All right, that's enough chatter! That divine sword's @likely somewhere close. Brace yourselves and let's go!
0101FDB8: Our apologies that you could not make it in time! We@will not allow you one step closer to the esteemed@dragon god! 
0101FF88: All life come into all forms of conflict and discord@amongst themselves. Yet, all of them know the limit of@their own power, and build bonds with each other as @they live on. 
01020188: Ah, you can't accept it? Then it's our job to teach a @hatchling like you what we really mean. 
010202C0: If you think we could mean anything else, then you need @to study what we were trying to tell you more @carefully.
010203B8: Sure, you may be a divine sword and have ancestry to the@gods which you can be proud of and all that.
010204D8: Yeah. As weak as humans are, they are needed by us gods.@We establish a relationship where we maintain our @existence by their reverence, and in return, we lend@them our support. It's a society we can't live@without.
010206B0: You may save your nonsense. I shall protect all of@Gensokyo. I will spare you the time after.
01020720: Hah, it thinks it's protecting all of Gensokyo! This is @pointless. It's not going to listen to us.
01020798: Commoners, come with all your might. You will know the@extent of your strength, then spend your remaining days @of peace in Gensokyo. 
010209A0: The power to control all of Heaven... The power to@control and protect this land... It is all fading...
01020A00: After incident was resolved, the Ame-no-Murakumo was@moved from a shed in Kourindou to the Hakurei Shrine. @Reimu, Yukari and Byakuren combined their powers to @erect a secure barrier around it. 
01020C98: Yukari said that since it's been given life as a@tsukumogami, it should still have it's own will now...@But it's hard to believe given how tight-lipped it's@being.
01020D18: If incidents like that happened repeatedly, we'd all be @in trouble. Boredom is proof of peace, and that's a @good thing. 
01020E48: ...By the way, when is that Great Tree going away? I@appreciate the coolness from its shade, but still...
01020F58: Oh yeah, what happened to that shiny sword anyway? I@came here pretty much to ask about that.
010210F0: I heard it got less shiny and more rusty now too? Lemme @see it. 
010211C0: It's really exactly as I said.
010211E0: ...Like I said, the shrine needs cleaning. Do you really@want me to desert the shrine that badly?
010212F0: ......... 
01021610: The incident in Gensokyo had lasted a week. 
010216A0: Not at all. I was just thinking of closing the shop for @a little while. 
010217F0: Tsk, so stingy. 
01021B18: Booooo. 
01021B5C: Oh, Marisa? Even you are here, huh? 
01021B98: Ah, Kourin? 
01021C08: I thought you looked like you wanted to go, so I figured@I'd offer. I guess I was wrong, so forget it. 
01021EA0: Though my trip destinations may just happen to be the @same as yours, and in the same order too. It'll be pure @coincidence, though.
01021F50: *sigh*... There was no way I could stop you from coming @the moment you said you wanted to anyway... Do whatever @you want... 
01022200: You're blushing.
0102222C: Oh, hush. I just want to go on a trip, that's all.
0102226C: ...And I was asked to be a tour guide.
01022488: For that hard-headed shopkeeper to want to explore@Gensokyo to widen his views... Can that be because of @the Ame-no-Murakumo's power to allow its holder to@control all of the world? 
010226B0: Geez, you and Marisa... 
01022794: My, you didn't know? That is indeed correct.
01022810: Now, who knows what will happen next? Hehe. 
01022928: ...Whatever the case, we can slowly figure it out as we @watch over them. We have all the time in the world, @after all.
0109DC58: We will teach you that your existence is nothing@extraordinary... And that us commoners and our bonds@will leave you cursing the limits of your strength! 
0109DD10: Yeah, it's over... Ahh, this incident was really@exhausting. I could use a nice, long break now. 
010A1388: Maybe the Ame-no-Murakumo's goal in starting the@incident was to get itself passed to Reimu? She's the @person who best fits the description of someone who @controls Gensokyo right now.
010A1580: What amazing divinity... I'm sure the Ame-no-Murakumo's @still some distance away, but its severe power can be @felt even from here.
010A1620: Sage of youkai, how foolish you are. Such arrogance,@such blasphemy. I, the divine sword, stand on the @summit of Gensokyo as your protector. Such careless @remarks will not be tolerated.
010A16E0: ......And I was the one who hid the divine sword at the @start, as well....
010A1730: Wahahaha! You were totally gnashin' your teeth at that@last part there, Kourin. So what, you want to go 'round @the Gensokyo you endangered, then helped save, so that@you can fall in love with it all over again?
010A1810: You mean that since he formerly held the sword, he now@wants to become someone worthy of wielding a sword that @can control the world...? That can't be. Ran, you're@overthinking things.
010ECD10: That is all that gods are. Gods can only exist because@of the existence of others... Those very commoners you@degrade. How does that make gods much different from@humans and youkai?
010ECDD0: Gods are a bit more powerful than other species. When @you think about it, that's the only real difference @between them. 
010ECE60: *shrug* Don't ask me. Still, as long as it has no @intention to destroy the barrier from the inside, the @barrier should work well enough to prevent people from@the outside from stealing it. 
010ECF00: During the incident, we were assisted by all sorts of @humans and youkai. That opened my eyes to all the @different people living their lives in Gensokyo in ways @I was unaware of. Thinking about it made me want to do@this. 
010ECFB8: So I'll be going along as the bodyguard. Rinnosuke did@come with us at my request to resolve the incident, and @stuck with us from the very beginning up to the end.@This is the least I could do as thanks. 
010ED050: There's nothing to worry about on that front - the folks@from the Myouren Temple will be going there. I've @worked myself to the bone during this incident too, a @little time off and a tour around Gensokyo should do me @some good.
010ED0E8: Not likely at all... Marisa wasn't even aware that she@had the Ame-no-Murakumo at the start, no? Does that @count as her ever having possession of it? ...Hey,@Youmu, are there any more dumplings?
