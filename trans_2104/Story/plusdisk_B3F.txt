00F74900: Wahaha! 
00FC7220: Ahhhhhhh! 
00FD4F40: I'm bein' serious, though.
00FD67D4: Gah, here's one.
00FDA874: ...Marisa, hold up. 
00FE2564: Urgh, dangit...! Guess I wasn't a match after all..!
00FE2A0C: Awwww, geeeeeeez! 
00FE6D00: Anyway, I'll take this seal along. I'm sure there's @somewhere we can make use of it.
00FE6E28: *sigh* That was exhausting. The real Mamizou was always @going to be a tough foe to handle.
00FEB1AC: It went like "Ahhhh" and "Oooooh."
00FF0CA4: I do live in a forest, y'know.
00FF0ED8: Ooooh. Pretty unusual to see you so fired up, Alice.
00FF28D0: ........A puppet of ours? Wha?
00FF5708: Oh shaddup, I know. I'm just taunting the opponents @after we beat them. Atmosphere and all. 
00FF8D34: (Hmm, people come around here too, huh?)
00FF8E64: Whatever! I definitely heard some kind of voice!
00FFC820: Ah, crap! Someone's here! 
01000DC0: Hold it. Just to make things clear - those fakes weren't@made by you?
01001DD8: None of us know anything about that either... This is @the first we heard about Futo's rampage as well.
01001FC0: Hah, we aren't some half-assed general store that lets@people come and go as they please.
01005450: All right! My apologies, but we're taking you down! 
01009270: We have to deal with the monster tanuki before that can @happen. I'm sure the feast after we deal with Mamizou @will taste extra-delicious. 
0100AEB8: Yeaaaah.
0100B2D0: Good grief. She just says what she wants, then runs @away. 
0100B550: I do wanna give it a try if there's a next time, I@s'pose. 
0100B77C: Definitely not. I haven't heard the voice before. 
0100BAE4: ...Ah, you're right. Sorry, it was nothing. 
0100BDB4: Hey! No more talking about creepies like that.
0100C3E0: I'd rather get rid of my own impostor as soon as@possible, myself... Can't let my shrine's reputation@get even worse than it is now.
0100C850: Rhododendron Seal...
0100D7D0: They're scarlet gold fragments now, not ashes. Though @they're both black, to be fair. 
0100DC68: You're the one who should keep quiet. Move aside, I'm @going to collect them.
0100DCC8: Huh? Geez, you want this moustached sword too?
0100DF14: Hrmhrm... So that's what happened with you folks... I @see, I see... 
0100E118: To be fair, we've got one idiot here who really was @controlled by it before.
0100E4E4: We don't know many Taoists 'round these parts. ...In@fact, it's pretty much just Futo. 
01011348: Ah? ...Ahh, the glow of the flames make it rather @obvious.
01011468: This is weird... I kinda feel like shouting.
01012118: Whatever, your luck ran out the moment you ran into me. @I'll burn both of you into ashes! 
01012400: All right, you've caused us enough problems. Will you @give us a clear explanation on what you were trying to@do? I need to know just how angry I should be.
01012488: (Guess I'll go somewhere else.) 
01013818: Aye. Foremost being Futo gone crazy, and how she's@summoning this whirlpool of ghosts. 
0101A4E0: What do we do with this impostor? 
0101A9D8: My impostor's even trying to protect its own Mokou? It@deserves praise for that at least.
0101BC00: Isn't it a bit late to be squeamish about ghosts? We've @already beaten up so many of 'em. And besides, I'm not@the only one who brought ghosts up... 
0101BEE8: Huh. You enjoy flowers that much? 
0101CF68: Hoho! Hitting where it hurts, eh? It's true, it's true. @I'm a fair bit proud of this one, but it ain't all that @likely to match all you folks at once.
0101D130: There're tons of vengeful spirits and such all over the @place. Hearing some unknown voice or two wouldn't be@all that strange. 
0101DB08: Ahhh... So you did that with other people besides me. @Yeah, it's not hard to imagine at all...
0101DEFC: Uhn?
0101E03C: Whoa, the puppetmaster shows up.
0101FE28: As flames begin to erupt from Mokou, the fake Keine @stands in front of the fake Mokou, as if trying to@protect her.
01020B04: Tee hee.
01022450: Our fun game of tag ends here. Get ready for a@beatdown! 
01022678: It's no laughing matter!
010226F8: Well, to make a long story short... 
010229C4: ...You're there, aren't you? Come out, Mamizou. 
01022B44: That ain't a compliment.
01022D3C: Did you really see that? The underground of the Great @Tree was already in this state back then? 
010234E0: Sure, sure, I getcha. We'll have Eirin check you out@once we get back, okay? Don't worry, lots of us've been @affected by this gloomy place. And you've always been @surprisingly sensitive to this kinda stuff, yeah? 
01024D60: Yeesh, you're always so quick to draw your sword. 
01025150: ...At least, that's what I want to think. I kind of @understand the pessimism, honestly. Staying here long @enough would drive anyone crazy eventually. Everyone@could do with a quick return to the inn and a nice, @relaxing bath.
01025640: ...Well then. A bit late to ask now, but how 'bout@having me along in your travels?
01025888: Thanks, I owe you one. I'm confident I'll be useful to@you folks.
01026228: What happened to your so-called puppets anyway? I was @expecting dozens of them to ambush us.
010271F8: ...Huh, I'm pretty sure I heard someone's voice around@here just now... How strange... 
01028670: Oooooooh! 
0102882C: Hm? What wuzzit like? 
01028A38: ...You sure it wasn't Rumia?
01028B30: Ooh?
01028D50: So she heard a ghost? 
0102A248: Whatever the case, it seems the first order of action is@to give that idiot Futo a good slap on the head.
0102BBF0: But whatever I said about the upper floors, it was a@damn sight better than what we're going through @underground. The paths are narrow, it's confined to the @max, darkness wherever we go, and the vengeful spirits@here come in bulk. Blegh. 
0102BC90: That's true... I'm especially bad with the ghosts. The@mere sight of all the ghosts around make me feel like @I'm choking.
0102BCE0: That's what this old tanuki thought too... But these@very eyes saw Futo herself surrounded by lots of@ghosts, then summoning even more to herself.
0102BE58: Additionally, the impostors that were here before @Mamizou's recreations, and why they drop blackened@scarlet gold fragments are still unexplained. 
0102C130: I do try to lend equal importance on both aesthetics and@functionality when making my dolls. Seeing my impostor@do such a halfhearted job is rather irritating. ...Do @put in a little more effort next time, hm? If there is@a next time, anyway.
0102C620: I believe so too. Making fake copies of us shows that @she really knows how to troll us. 
0102C698: Hrmph. Going into the nitty-gritty seems like it'd@require the puppet's parts... or rather, the@surrounding spirits to be of a certain strength and @type... 
0102C860: Given all the impostors we've faced so far, they've @never tried to escape from the start anyway.
0102CAD0: Y'know, the impostor itself is pretty well made... but@the dolls don't look all that detailed. 
0102CBD0: There're a whole lotta types of rhododendrons out there.@They're mostly bright and stand out from the rest, so I @like 'em a bunch. 
0102CF18: Heheh, obviously. If you dared to leave without offering@to help, you would've become the next victim of the @Hakurei-style Kidnapping Technique anyway.
0102D100: I was prepared for this to happen, but fighting one's @self still doesn't feel right.
0102D230: Hm? Reimu, look over there. 
0102D478: Huh. Confident, ain't ya? You sure you've calculated the@real Alice's strength? And all of the rest of ours@too?
0102D518: Huh. No plans to run or even hide? You've got guts for a@bunch of impostors. 
0102D578: And there's even that whole divine sword to deal with @along with you poor, confused folks. Almost all of@Gensokyo's key people've fallen already. If I fail@here, there probably ain't gonna be another chance. 
0102D620: ...Harumph. Monster tanukis are famous for wriggling@their way out in the worst of times, y'see. 
0102D688: So this is just to buy time, y'see. Time for me to@retreat like now. 
0102D750: Isn't it obvious? We beat it up. As Reimu said before,@there's no point in talking things out with these @shadows!
0102D960: Hmph! Not even worth a mention. How's it feel to be a @bunch of ashes? 
0102DA70: My word... I had came to the surface as I was worried @about the Ame-no-Murakumo's voice... but if I had @stayed in the underground, I might have been able to@deal with all the trouble...
0102DB4C: *sigh* This bites.
0102DC50: This darn sword doesn't even have to cause the trouble@itself. Trouble just gets attracted to it.
0102DDE0: But it still means our fun and enjoyable Great Tree tour@featuring swimming with the ghosts will last a little @while longer... *sigh*
0102DF94: Okay... There!
0102DFD8: ...Another fallen leaf... 
0102E01C: ...I suppose you could call an incredible @misunderstanding. 
0102F1F0: Mmm mmm... I s'pose being able to chat this way makes it@unlikely that you folks are being controlled by the @sword...
0102F3A0: Hmmm... What could this mean? 
01030030: ...Mmm, vanquishing your own shadow leaves a weirdly@uncomfortable aftertaste. 
010307E0: Yep. However, even if we take all that as the truth, it @still leaves a few things of concern. 
01030D40: My, so you know where you went wrong, how nice. That@doesn't mean we're going to go easy on you, though. @You've caused too much trouble already. 
01031198: Anyway, going on with the story... I made my way through@while fighting off the fakes. After going further down, @I saw a Taoist that you folks should know, summoning a@staggering amount of ghosts.
01031388: Hm? That's a real solemn look you've got on your face @there, Reimu. What's up? Trying to act like a cool main @character?
01031718: Ahhh, I get it too. That's why you were so bothered by@those leaves, Reimu? So complicated.
010317B0: So them puppets made with ghosts and scarlet gold @weren't enough to stop the real folks.
01031A60: All I need to do is steal your Ame-no-Murakumo and I'll @turn the tables right 'round! 
01031B30: Enough talk! Time for you folks to see a cornered @monster tanuki bite a big chunk outta a divine sword! 
01031DC8: Ehh? You mean... we would be controlling you? What're @you even going on about?
01031E74: Who gives a damn!? C'mon, anyone who wants to be burned,@step right up!
01031EC0: ...Don't play dumb with me. Or you folks've some other@way to explain that Taoist summoning the malicious@spirits like crazy further down?
01032008: ...Hold on a second. Who's this Taoist you're talking @about? And what's this malicious spirits being@summoned? 
010320B8: You folks made them impostors by using your powers on @those scarlet gold fragments, no? 
01032280: .........Ahh... It's all becoming clearer now.
01032358: Not to me. Gimme a quick lowdown, won't ya? 
010325E8: In my case, I heard 'bout the Ame-no-Murakumo crossing@over to Gensokyo, and the celestial over there@ascending the Great Tree while under its thrall...
01032760: Wahahahahaha! 
010327AC: ...Damnit, Marisa... I'm not going to forget about@this... 
010327E0: Ghosts and malicious spirits... I see now. So that's why@no ghosts have been crossing the Sanzu River recently.@They're all wandering around the underground of the @Great Tree. But what power could've summoned them @there?
01032988: Did she go crazy like Tenshi, or was she crazy to begin @with? She ain't gonna get a letoff if it's the latter.
01032B20: Ahh... We did lose to her a few times. So that's why you@thought the Ame-no-Murakumo was controlling us... I get @it now. 
01032D40: Y'folks already know how tight-lipped them fakes are, @and given the aura and power she emitted, there's no@doubt it was Futo.  
01032FA0: It was 'bout that time that word of your defeat to the@celestial reached these ears. 
01033868: Not that I feel guilty or anything, but it doesn't feel @right to leave things be. The misunderstanding with you @folks may be resolved, but not anything else. 
010339C4: Grrr... 
01033AB8: But for you Mamizou, you're all right. We already beat@you up for catharsis, so no more hard feelings. 
01033BA8: All right. Now that we've got Mamizou along, it's time@to give the supposedly-crazy Futo a good kicking! 
010340AC: Bingo. That Taoist was calling loads and loads of them@vengeful spirits to her.
010340E4: Though we'll teach her a lesson anyway even if it's the @former. 
01034258: We'll have to wander around again... *sigh*...
01034DE8: If I kept quiet, you'd have told me to try cutting it @anyway. ...Not that it worked. This thicket can't be@cut down. 
01036818: We've seen thickets like this a few times already, and@none of them could be cut down by normal means... I @guess we'll have to find another way. 
0109E058: I know I may've complained a teensy bit when we were@explorin' all over the Great Tree...
0109E128: I ain't a big fan of this impostor too. Having to keep@track of all the dolls on both sides is gonna make my @head spin.
0109E290: ...Just one thing... Call it a parting gift, whatever @you want. At least before I become a puppet of yours, @tell me why are you...
0109E380: Yeah, really. It was a real pain. Tenshi and this stupid@moustached shiny sword were way stronger than expected. 
0109E438: That's when I found some weird stuff bloomin' in the@lower floors of the Great Tree. I took a look and found @a bleedin' large place overflowing with ghosts and@malicious spirits.
0109E610: Figured it was finally time for me to pay my dues...@Then my survival instincts kicked in and I figured out@to use my power to turn the blackened scarlet gold@fragments and ghosts here into impostors of my own. 
010A1EA0: ...I figured this was going to happen eventually. Those @strings and magic... There's no mistaking it. 
010A1F70: Y'may not be fond of the way it looks, but I've gotten@better y'know. This one should be more powerful than@the ones you fought before. 
010A2010: Alice's impostor attempts to turn into dark mist and@escape, but Reimu's barrier ensnares it and makes it@shrink slowly, but steadily.
010A20B0: A moment later, the impostor and its black mist @completely evaporated. All that remained was a dark @fragment and a leaf fluttering to the ground. 
010A2158: What's wrong, Reimu? Shouldn't you retrieve the scarlet @gold fragment quickly? Who knows what'll happen if you@leave it be.
010A2450: Or the blackened scarlet gold fragments? Or the @impostors made by combining the malicious spirits and @fragments together? You want me to believe that all @isn't that sword's doing? 
010A24F0: I'm wonderin' the same thing myself. I heard that you @folks were climbing up the Great Tree to deal with the@sword, so I figured I'd look into all the fuss@underground... then I got attacked by fakes of you@folks.
010A25D0: I was gonna tack that part on after the explanation's @done. For the record, I did make a few copies of you@folks, but that only after the underground was already@teemin' with your impostors attacking everything in @sight.
010A26D8: That Taoist part's been bugging me from the start.
010A27B8: I'm well aware that that Taoist has no reason to do that@sort of thing under normal circumstances... but then I@got to thinking, maybe the Ame-no-Murakumo possessed@her just like it did to the celestial...
010F5720: These impostors keep popping up no matter how many of @them we defeat. Will there ever be an end to this?@Dealing with them and all these vengeful spirits around @is putting a damper on everyone's positivity. 
010F57C0: They do leave blackened fragments of scarlet gold when@we defeat them... That probably means they're not easy@to make. We're retrieving them as we go along too, so @we'll starve the culprit of resources eventually. 
010F5C20: That was the plan, but it ain't all that easy to@retrieve them scarlet gold fragments. Then you folks@went and took all the ones that I made to slow you down @too. Wish I had a wee bit more time to prepare... 
010F5CE8: I know you must've, cuz that's the same thing I did @myself once I got hold of those fragments by defeating@them impostors. And the only folks who can create those @fragments are the sword itself, and you lot who fell@under its thrall! 
010F5DD8: That makes everything even more incomprehensible. Futo@wouldn't... well, she isn't exactly the greatest person @ever, and no one knows what's going through her head@sometimes... but she shouldn't have any abilities that@can allow her to summon ghosts. 
010F5EA0: Aye. I thought I had to deal with you folks under the @sword's control from above, and the Taoist gone crazy @from below. I was forcing my way through enemies to get @to the surface when your merry band came traipsing down @to the underground with the celestial and the sword.
010F5F88: Ahh, this all makes sense now. So the fakes that dropped@both a leaf and a fragment were "fake impostors"@recreated by you, and the ones that dropped just a@fragment were the "real impostors," so to speak.
010F6048: Exactly. It all made sense in my head when I thought the@Ame-no-Murakumo had you folks on a leash, but since @that's not the case, then it's still a mystery... 
