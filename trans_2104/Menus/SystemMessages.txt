00F975B4: The 7-Star Charm proection has faded, and ghosts are starting
00F97660: to gather... It'd be best to return to Gensokyo for now.
00FC6090: Move to the upper floor?
00FC60D0: Move to the lower floor?
00FC6330: Obtained %d money!
00FC6350: Obtained Main Equipment "%s!"
00FC6380: Obtained Main Equipment "%s" x%d!
00FC63AC: Obtained Sub Equipment "%s!"
00FC63D4: Obtained Sub Equipment "%s" x%d!
00FC6404: Obtained Material "%s!"
00FC6428: Obtained Material "%s" x%d!
00FC6450: Obtained Special Item "%s!"
00FC64C4: This treasure chest seems to be locked.
00FC65D4: A relay point has been activated!
00FC7434: When returning to the dungeon from Gensokyo,
00FC7478: this place can be used as a starting point!
00FC956C: Hieda no Akyuu joined the party!
00FCA788: Mononobe no Futo joined the party!
00FCBB78: Kogasa Tatara joined the party!
00FCD17C: Komachi Onozuka joined the party!
00FCDD58: Obtained Special Item "%s!"
00FCE7A0: Obtained Special Item "Kogasa's Purple Umbrella (Fixed)!"
00FCECE4: Subclass and the Stone of Awakening will be able to be used 
00FCEFE8: Kaguya Houraisan joined the party!
00FCF890: Nitori Kawashiro joined the party!
00FCF8B0: Kasen Ibara joined the party!
00FCFCB0:  "%s" was given.
00FD01E8: Subclasses. Selecting a Subclass that synergizes with a 
00FD076C: again. By resetting a character's Subclass, other characters 
00FD0DAC: Youmu Konpaku joined the party!
00FD1014: Patchouli Knowledge joined the party!
00FD1650: Hong Meiling joined the party!
00FD1724: Additionally, by performing a character reset, they will lose their 
00FD17F4: Sakuya Izayoi joined the party!
00FD1CBC: From now on, by consuming a "Stone of Awakening," it will
00FD1D00: be possible to make a character learn a Subclass.
00FD1D48: and have access to new skills. There are a few categories of 
00FD1E08: character's specialities will draw more out of their abilities.
00FD20A8: By learning a Subclass, a character will have their stats increased
00FD21A8: will be able to learn a Subclass in their place.
00FD3B88: "Symbol of Faith" given to Sanae.
00FD44D0: Toyosatomimi no Miko joined the party!
00FD50BC: Momiji Inubashiri joined the party!
00FD56C8: Cirno joined the party!
00FD5878: Koishi Komeiji joined the party!
00FD5964: Alice Margatroid joined the party!
00FD6500: Obtained Special Item "Philosopher's Stone - Wind!"
00FD6D90: Suwako Moriya joined the party!
00FD73D4: Hata no Kokoro joined the party!
00FD76F0: Obtained Special Item "Kogasa's Purple Umbrella (Broken)!"
00FD7D48: Yuyuko Saigyouji joined the party!
00FD9768: Shikieiki Yamaxanadu joined the party!
00FD9D88: "Lamprey Serpent's Meat" given to Mystia.
00FDA350: Mystia Lorelei joined the party!
00FDA618: Obtained Special Item "Philosopher's Stone - Magic!"
00FDAB70: Mamizou Futatsuiwa joined the party!
00FDB15C: "Great Tree Insect Antidote" administered to Wriggle.
00FDC1EC: You received the Special Item "%s," 
00FDDDE0: Satori Komeiji joined the party!
00FDF158: Rumia joined the party!
00FE0100: Parsee Mizuhashi joined the party!
00FE3B68: Obtained Special Item "Gland of Super Hemotoxin!"
00FE4580: Utsuho Reiuji joined the party!
00FE6AF0: Hina Kagiyama joined the party!
00FE75AC: Wriggle Nightbug joined the party!
00FE77A4: Obtained Special Item "Dianthus Seal!"
00FE93E8: in all stats every turn, and survive fatal blows 50% of the time!
00FEA888: Tokiko joined the party!
00FEB650: Obtained Special Item "%s" x2!
00FEB9E0: Obtained Special Item "Seaweed of Serenity!"
00FEC2B0: Aya Shameimaru joined the party!
00FF242C: Nazrin joined the party!
00FF3C48: Obtained Special Item "Platycodon Seal!"
00FF3EA0: Obtained Special Item "Philosopher's Stone - Earth!"
00FF4AE4: Fujiwara no Mokou joined the party!
00FF55C0: Obtained Special Item "Philosopher's Stone - Fire!"
00FF5798: Obtained Special Item "Coral of Silence!"
00FF608C: Sanae Kochiya joined the party!
00FF66E4: Iku Nagae joined the party!
00FF7CF0: Ran Yakumo joined the party!
00FF7F84: Suika Ibuki joined the party!
00FF80EC: Obtained Special Item "Philosopher's Stone - Water!"
00FF92B8: Yuugi Hoshiguma joined the party!
00FFB120: Eirin Yagokoro joined the party!
00FFB9AC: the special effect "Earth Blessing!"
00FFBB74: Reisen U Inaba joined the party!
00FFCC28: the special effect "Water Blessing!"
00FFDEE8: Obtained Sub Equipment "Great Question's Mask!" 
00FFFC98: Remilia Scarlet and
00FFFF1C: Kanako Yasaka joined the party!
01000880: Yuuka Kazami joined the party!
01001618: Tenshi Hinanawi joined the party!
010059DC: The Great Tree has changed upon conquering the Boss Rush. 
01005E34: The bosses from the Boss Rush have respawned in the Great Tree. 
010096FC: Flandre Scarlet joined the party!
0100A300: Obtained Special Item "%s" x%d!
0100DFF0: Byakuren Hijiri joined the party!
0100E15C: Obtained Special Item "Mikagami Seal!"
0100F7A0: Obtained Special Item "Iris Seal!"
010111CC: Maribel Han joined the party!
010113F8: Renko Usami joined the party!
01015AD8: The Infinity Corridor is a randomly-generated dungeon independent
01015D1C: can restart at the floor that you have left. Feel free to conquer
01015DB0: the hell of infinity and nab all of the treasures(?) within!
01016AA8: When a character with the "Wood Blessing" effect gets a turn,
01017700: they will have a 50% chance of staying in battle with 1 HP!
010177FC: We have no means to open it right now.
010178DC: Obtained Special Item "%s" x3!
01017B34: When a character with the "Fire Blessing" effect is defeated,
010184C4: When a character with the "Earth Blessing" effect gets a turn,
010189D8: When a character with the "Metal Blessing" effect gets a turn,
01019504: When a character with the "Water Blessing" effect gets a turn,
01019820: Yuyuko Saigyouji joined the party!
01019E38: 25% HP, 33% MP, be cured of all ailments, and receive a 25% buff
0101BDAC: Yukari Yakumo joined the party!
01021164: Obtained Special Item "Ara-Mitama Seal!"
01028F8C: Minoriko Aki joined the party!
0102B824: which unlocks a new subclass! 
0102BD70: Chen joined the party!
0102C75C: The Special Dungeon "Infinity Corridor" has been unlocked!
0102D210: Shou Toramaru joined the party!
0102F144: Rin Kaenbyou joined the party!
0102FDE0: they will recover 33% MP!
010333E0: the special effect "Wood Blessing!"
01035DA4: they will be cured of all debuffs and ailments!
01036658: the special effect "Fire Blessing!"
01036B74: You can escape from the Infinity Corridor at any time, and you
01039F58: all five elemental blessing effects! Frontliners will recover
0103DAFC: of the Great Tree, and can be accessed from Gensokyo directly.
0103E31C: Obtained Special Item "%s" x4!
0103EB18: In this battle, all frontline allies will receive
0103ED14: they will recover 25% HP!
0103FC54: Consume 1 "Treasure Chest Key" to unlock the chest?
01040154: they will receive a 25% buff to all stats!
01040E6C: the special effect "Metal Blessing!"
01069FA8: Only one person can learn this subclass at a time!


00FC61E4: The strength of malice here is too strong to get past right now.
00FC6220: Perhaps by reducing the number of imitations at the bottom would
00FC6254: allow passage through here.
00FC9CB8: Offering 10%% of your 7-Star Amulets will brighten the area.
00FCA140: The party received 250,000,000 experience for each member,
00FD36A0: Now that Yamata no Orochi Hollow's remnant has been absorbed,
00FD6CA8: the Remnant Stock system within Infinite Corridor is unlocked!
00FCDC50: If the party has any 7-Star Remnant on hand when exiting the corridor,
00FD17C4: a precentage of the remaining 7-Star Remnant will be
00FD7EDC: converted to Remnant Stock and saved!
0103FC14: Remnant Stock, like 7-Star Remnants,
010240A4: can be used to exchange items in between corridor floors.
00FD3744: Also, Remnant Stock are kept even when leaving the corridor,
00FD3834: and will only be used up by exchanging items.
00FCC4A0: Make good use of Remnant Stock system
00FCC57C: to benefit the progress of your travels!
010086CC: as well as 100,000,000 money!
01068B80: Right now, this character cannot use %s
