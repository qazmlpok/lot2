trans_2103\Enemies
-----------------------------------
ゴールデンマッシヴ
Massive Golden

プレグリュプシス
Pregrupsis

邪龍神の眷族
Evil Dragon God Family

はぐれメタリック毛玉
Liquid Metal Kedama

-----------------------------------
trans_2103\achievements
-----------------------------------
召喚ドアドア
Summoning Door Door

200万銭を獲得できる。 
You receive 2,000,000 money as your reward.

獲得報酬：2000銭
Reward: Money +2000

パトラッシュ
Patrache

８ビットの限界
Eight-Bit Limit

ＦＣ，ＳＦＣでよく見たパラメータの限界に到達！ 
You've reached the number that's often the cap for NES and SNES games!

サブ装備「混沌王のコトワリ」を獲得できる。
You receive the Sub Equipment "King's Reason" as your reward.

65535階到達だとヤバすぎるからね、仕方ないね 
65,535 floors would've been a bit too much.

特殊アイテム「百戦錬磨の書・敏捷」を獲得できる。
You receive the item "Veteran's Tome - Speed" as your reward.

実績条件：無限の回廊256層を攻略する
Conditions: Conquer the 256th floor of the Infinity Corridor

-----------------------------------
trans_2103\items\items
-----------------------------------
基礎ダイス値と威力修正が恐ろしいことになっており、対邪、対龍必殺兵器となっているが
astounding damage values and always crits against Evil and Dragon, but its

攻撃+1680%　回避+32
ATK +1680% EVA +32

リズミカルに死亡し全滅の憂き目に合っていた主人公の面影はどこへやら。
the hardest boss in RPG history. Pity the poor heroes constantly dying to

ただひたすらに爆進しないと入手できない。実はオーバースペック過ぎて使い勝手もイマイチ。
focusing on advancing the plot. Perhaps made a bit too strong to be useful.

余りにも厳し過ぎる条件の為、他の全てのレアアイテムの存在を忘れ
12 hours, which is strict enough to mean forgoing all other rare items and

MP&TP+8 魔力&精神&敏捷+720%
MP/TP+8 MAG/MND/SPD +720%

栓抜きっぽいが格好いいという絶妙なデザイン。
The usual update cycle means it'll be power-creeped trash within 8 months. 

エクスカリバー�U
Excalibur II

極めればほぼ全属性への完全耐性と各種最強級特技、魔法をボコボコ覚える壊れ職。
everything and learns the strongest magic and skills. Utterly broken.

今日も眠った相手に苦戦したりＭＰ切れでハメられたりしている事だろう。
him. In fact, they're probably asleep and out of MP against him right now.

現存する全平行次元と対になる存在「反物質世界」からエネルギーを抽出すると共に、
A fundoshi with power accrued from "Antimatter Worlds" from all parallel

「天命の石版」と角笛によって力を与えられており、世界で数人しか所持していない
it allows control of fate itself. The few holders of this ultra-rare item

ラスボスにぶつけるとラスボスすら殺してしまう大古龍の力は防具になっても凄かった。 
HP/ATK/MND +330% DEF +660% FIR/CLD/WND/NTR +100

アラガン・バトルアクス
Allagan Battleaxe

問答無用で次々と砕いていく、恐るべき妖怪メダル砕きがたまーに使っていた斧。
Medals, and even occasionally, shatter the terrifying Youkai Medals.

来世に生まれる究極の剣。ラスダンに１２時間以内に辿りつくと入手できるが、
An ultimate sword from the future. Get it by reaching the final dungeon in

Ｍっ気の強い狂人達がいつの間にか信奉していた謎のモンスターの像。
The mysterious statue of a monster who somehow accrued an insane cult of

余りの重さに使いこなせるものが限られている。やっぱり記念装備。
massive weight makes it hard to use. Yep, another bragging rights reward.

便利ワードと化した「古代アラグ帝国」のテクノロジーによって作られた至高の斧。
A supreme axe forged using technology from the ancient Allagan Empire,

魔王魔人の持つ、全攻撃を無効化する「無敵結界」を打ち破る事の出来る魔剣。元エロ盗賊。 
A demonic sword with a potty mouth that can nullify the Invincibility Field 

何故ふんどし型なのかは不明。女性型ガイコツよりはマシかもしれない。
it's in a fundoshi shape is unknown. At least it's not a female skeleton.

ルッカを慰めながらこれを渡す場面は個人的名シーン。
by passing this over is, in my opinion, one of the best scenes ever.

皆の憧れはぐれメタルに転職できるようになるアイテム。
An item that allows access to everyone's most desired class, the Liquid

神竜に毛が生えた程度で大して強くもないのが泣ける。
but it's still disappointing that he wasn't much harder than Divinegon.

攻撃+780%　HP&防御+500%　回避+24　全属性耐性+64
ATK +780% HP/DEF +500% EVA +24 All Affinities +64

激レア武器だったり霊災を防ぐ鍵だったりする。ついでに笑顔が素敵になるかもしれない。
will defend against all calamities. May make your smiles more wonderful.

色々と忙しい変遷を遂げているご様子。
defined as a magnet at some point.

例の負けイベント戦にて、ＯＲヴェルトールを無理やり倒す事で入手可能な防具。 
Armor gotten by defeating Alpha Weltall somehow in a hopeless boss fight.

不滅の学聖ボタンへの力の注入を止め、遂に実力を解き放った斬真 豪と共に
Gou Zanma gives this button to you as he unleashes his full power, having 

信頼の定食宣言により８か月後にゴミと化す事が確約されている。
who's been the go-to plot point lately. Looks like a cool, weird corkscrew.

HP&精神+999%　回避+32　冷&風&然耐性+144
HP/MND +999% EVA +32 CLD/WND/NTR +144

-----------------------------------
trans_2103\items\special_items
-----------------------------------
これの初入手に何百時間かかった事だろうか。
take to get this for the first time? It must feel so terrible when a 

まっすぐにこちらを見据えて来る、透き通った瞳の作り物。
A crafted eye that sees right through all within its gaze.

大樹上層にいた、よく分からないオッサンを殴り飛ばし続けてたら手に入った称号。
Gotten by constantly beating up some weird old fart at the top of the Great

何十時間かコツコツ育てたキャラが終層で死んで完全消滅した時の虚無感は結構凄い。
lovingly-raised character dies at endgame and your save file gets erased. 

-----------------------------------
trans_2103\Menus\menus
-----------------------------------
二周目以降は、全ての仲間が加入した状態でゲームを開始する事が可能です。
For second plays, you may start with all characters unlocked.

東方の迷宮２ プラスディスク  ver1.103
Labyrinth of Touhou 2 Plus Disk ver1.103

オーバーヒート Lv%d
Overheat Lv%d

所持している七星の護りの２割を捧げる事で、周囲を照らす事ができるようだ。
Offering 20%% of your 7-Star Amulets will brighten the area.

-----------------------------------
trans_2103\Menus\floornames
trans_2103\Menus\ClassNames
trans_2103\Menus\SystemMessages
trans_2103\Skills\characterSkills
-----------------------------------
スキル取得者が前衛にいる状態で敵が攻撃を外した場合、その敵の全能力を微減少させる。
When the user is at the front and an enemy misses an attack, that enemy

沈黙効果が更に強力になる。
attacking will have an even stronger effect.

スキル取得者の回避が10上昇する。
Increases the user's EVA by 10.

「攻撃の起点」が付与されている敵に行動順が回った時、「攻撃の起点」効果は消滅する。 
active. The effect wears off when the afflicted enemy gets a turn.

攻撃時、（ 八卦炉チャージｶｳﾝﾄ*3 ）％の与ダメージボーナスを得る。
When the user attacks, a (counter*3)% damage bonus is applied.

敵の防御性能を完全無視してダメージ計算を行うようになる。
completely ignoring the enemy's defenses.

前衛に神子がいる場合のみ、八十平瓮カウントの増加速度が２倍になる
When Miko is at the front, [Sake Cups] counter rises at double the rate.

この全能力微減少効果は敵の耐性に関わらず確実に成功する。
will be inflicted with an unresistable and minor debuff to all stats.

ｶｳﾝﾄの最大数は33。八卦炉チャージｶｳﾝﾄは戦闘中に減少する事はない。
The counter maxes at 33. The counter never goes down during battle.

このカウントは最大５までスタックする事が出来る。
(counter*15)% bonus to damage dealt. The counter maxes at 5.

「月の民」スキルを既に取得している場合、攻撃時に25%の確率で
When [People of the Moon] is learnt, attacks will have a 25% chance of

そのキャラの魔力、精神を微上昇させる。
will have their MAG and MND increased slightly.

戦闘中、（ ｶｳﾝﾄ*4 ）％の敏捷ボーナスと（ ｶｳﾝﾄ*15 ）％の与ダメージボーナスを得る。
attack. In battle, the buff provides a (counter*4)% SPD bonus and a

-----------------------------------
trans_2103\Skills\characterSpells.txt
-----------------------------------
ブースト効果を付与する補助スキル。
increases damage dealt for their next action.

-----------------------------------
trans_2103\Skills\CharacterAttacks.txt
trans_2103\Skills\enemyAttacks
trans_2103\Skills\subclassSkills
trans_2103\Skills\subclassSpells
-----------------------------------
基本消費MP12　　敵全体：炎+冷+風+然属性　　複合総合攻撃・命中+50
Cost 12 MP  All Enemies: FIR+CLD+WND+NTR  Composite Attack - ACC +50

-----------------------------------
trans_2103\Story\Introduction.txt
trans_2103\Story\1F.txt
trans_2103\Story\2F.txt
trans_2103\Story\3F.txt
trans_2103\Story\4F.txt
trans_2103\Story\5F.txt
trans_2103\Story\6F.txt
trans_2103\Story\7F.txt
trans_2103\Story\8F.txt
trans_2103\Story\9F.txt
trans_2103\Story\10F.txt
trans_2103\Story\11F.txt
trans_2103\Story\12F.txt
-----------------------------------
うーーん、これ……。@この弾幕痕の方は、諏訪子の弾幕じゃないか？@諏訪子の遣うヘビ型スペカが這ったような跡もあるし……。
Hmm... These danmaku traces looks like Suwako's. It's @similar to her Snake-type spellcards she uses...

-----------------------------------
trans_2103\Story\13F.txt
trans_2103\Story\14F.txt
trans_2103\Story\15F.txt
trans_2103\Story\16F.txt
trans_2103\Story\18F.txt
trans_2103\Story\19F.txt
trans_2103\Story\20F.txt
trans_2103\Story\21F.txt
trans_2103\Story\Postgame.txt
trans_2103\Story\OtherDialogue.txt
trans_2103\Story\plusdisk_1F.txt
trans_2103\Story\plusdisk_21F.txt
trans_2103\Story\plusdisk_22F.txt
trans_2103\Story\plusdisk_23F.txt
-----------------------------------
はは、それは後で本人に行ってあげるといいね。@特に重要な４枚のお面を手放したんだ。@恐らくそう遠くへは行けてはおるまいよ。
Hahah, we can tell her so once we find her. With her@four most important masks with us, she can't be very@far away. 

-----------------------------------
trans_2103\Story\plusdisk_24F.txt
trans_2103\Story\plusdisk_25F.txt
trans_2103\Story\plusdisk_26F.txt
-----------------------------------
霊烏路空よ、@地獄の輪禍よ！！
Scorching and troublesome divine flame, Utsuho Reiuji!! 

火焔猫燐よ、@核熱の悩む神の火よ！！
Hell's traffic accident, Rin Kaenbyou!! 

-----------------------------------
trans_2103\Story\plusdisk_B10F.txt
trans_2103\Story\plusdisk_B1F.txt
trans_2103\Story\plusdisk_B2F.txt
trans_2103\Story\plusdisk_B3F.txt
trans_2103\Story\plusdisk_B4F.txt
trans_2103\Story\plusdisk_B5F.txt
trans_2103\Story\plusdisk_B6F.txt
trans_2103\Story\plusdisk_B7F.txt
trans_2103\Story\plusdisk_B8F.txt
trans_2103\Story\plusdisk_B9F.txt
trans_2103\Story\plusdisk_ending.txt
trans_2103\Story\plusdisk_gensokyo_dialogue.txt
trans_2103\Story\plusdisk_introduction.txt
trans_2103\Story\hints
-----------------------------------
無限の回廊で階段を見つけ、次層に行くと
Upon choosing to go to the next floor, you will always

新たに「七星の残滓」カウントとしてストックされます。
used are changed into "7-Star Remnants." The preparation

-----------------------------------
trans_2103\Story\Rocks
trans_2103\Story\missing_stuff
