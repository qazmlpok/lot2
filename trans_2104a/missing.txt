基本消費MP12　　敵全体：無属性　　魔力攻撃・命中+30
Cost 12 MP  All Enemies: VOI  Magic Attack - ACC +30

基本消費MP16　　敵全体：冥属性　　複合直接攻撃・命中-20
Cost 16 MP  All Enemies: DRK  Composite Direct Attack - ACC -20

基本消費MP8　　敵単体：霊属性　　複合魔力攻撃・命中+128
Cost 8 MP  Single Enemy: SPI  Composite Magic Attack - ACC +128

基本消費MP15　　敵全体：炎+冷+風+然属性　　複合総合攻撃・命中+50
Cost 15 MP  All Enemies: FIR+CLD+WND+NTR  Composite Attack - ACC +50

基本消費MP4　　敵単体：然属性　　直接攻撃
Cost 4 MP　　Single Enemy: NTR　　Direct Attack

基本消費MP3　　敵単体：魔属性　　魔力攻撃・命中+15
Cost 3 MP　　Single Enemy: MYS　　Magic Attack - ACC +15

基本消費MP7　　敵一列：風属性　　直接攻撃・命中-15
Cost 7 MP  Enemy Row: WND  Direct Attack - ACC -15

基本消費MP13　　敵全体：霊属性　　魔力攻撃
Cost 13 MP  All Enemies: SPI  Magic Attack

基本消費MP4　　敵一列：物属性　　直接攻撃・命中+12
Cost 4 MP　　Enemy Row: PHY　　Direct Attack - ACC +12

基本消費MP3　　敵単体：風属性　　直接攻撃・命中+20
Cost 3 MP　　Single Enemy: WND　　Direct Attack - ACC +20

基本消費MP4　　敵単体：炎属性　　直接攻撃・命中-12
Cost 4 MP　　Single Enemy: FIR　　Direct Attack - ACC -12

強力な全体攻撃だが、消費MPと行動遅延に難あり。
A powerful AOE attack, but with high MP consumption and delay.

th DTH is reduced by a further (SLv*30)%.

（ SLv*10 ）％の確率でTPを12消費してHPを全回復させる。
(SLv*10)% chance of consuming 12 TP and recovering all HP.

静寂の中の歌声バフカウントが６まで上昇する。
inflicted with SIL, the skill will provide the buffs of 6 SIL afflictions.

トルアクス
Allagan Battleaxe

問答無用で次々と砕いていく、恐るべき妖怪メダル砕きがたまーに使っていた斧。
Medals, and even occasionally, shatter the terrifying Youkai Medals.

来世に生まれる究極の剣。ラスダンに１２時間以内に辿りつくと入手できるが、
An ultimate sword from the future. Get it by reaching the final dungeon in

Ｍっ気の強い狂人達がいつの間にか信奉していた謎のモンスターの像。
The mysterious statue of a monster who somehow accrued an insane cult of

余りの重さに使いこなせるものが限られている。やっぱり記念装備。
massive weight makes it hard to use. Yep, another bragging rights reward.

便利ワードと化した「古代アラグ帝国」のテクノロジーによって作られた至高の斧。
A supreme axe forged using technology from the ancient Allagan Empire,

魔王魔人の持つ、全攻撃を無効化する「無敵結界」を打ち破る事の出来る魔剣。元エロ盗賊。 
A demonic sword with a potty mouth that can nullify the Invincibility Field 

何故ふんどし型なのかは不明。女性型ガイコツよりはマシかもしれない。
it's in a fundoshi shape is unknown. At least it's not a female skeleton.

ルッカを慰めながらこれを渡す場面は個人的名シーン。
by passing this over is, in my opinion, one of the best scenes ever.

皆の憧れはぐれメタルに転職できるようになるアイテム。
An item that allows access to everyone's most desired class, the Liquid

神竜に毛が生えた程度で大して強くもないのが泣ける。
but it's still disappointing that he wasn't much harder than Divinegon.

攻撃+780%　HP&防御+500%　回避+24　全属性耐性+64
ATK +780% HP/DEF +500% EVA +24 All Affinities +64

激レア武器だったり霊災を防ぐ鍵だったりする。ついでに笑顔が素敵になるかもしれない。
will defend against all calamities. May make your smiles more wonderful.

色々と忙しい変遷を遂げているご様子。
defined as a magnet at some point.

例の負けイベント戦にて、ＯＲヴェルトールを無理やり倒す事で入手可能な防具。 
Armor gotten by defeating Alpha Weltall somehow in a hopeless boss fight.

不滅の学聖ボタンへの力の注入を止め、遂に実力を解き放った斬真 豪と共に
Gou Zanma gives this button to you as he unleashes his full power, having 

信頼の定食宣言により８か月後にゴミと化す事が確約されている。
who's been the go-to plot point lately. Looks like a cool, weird corkscrew.

HP&精神+999%　回避+32　冷&風&然耐性+144
HP/MND +999% EVA +32 CLD/WND/NTR +144

