00F77C34: Ah. 
00FB47E0: Dear me, looks like you've been found.
00FC9A48: ... 
00FCA9A8: Oh, it's gone a bit grey. 
00FCC208: ......
00FCEA88: ...*sigh* I suppose cooking the meals is one of my daily@duties... 
00FCED84: Oh, fine. 
00FCFE24: How's that? Serves ya right!
00FD1B14: I'm startin' to get used to seeing them around now. 
00FD1BE4: Don't crush what hope I still haveeee!
00FD2410: Yee?
00FD3038: Oh, so you remember me? 
00FD3360: Thank you for the typically neutral explanation, ma'am. 
00FD35B0: Yes, it's time for us to leave as well. See you again.
00FD3784: Let's have a look.
00FD38C4: Huh? What problem? You mean the whole dignity part? 
00FD3A10: Seriously. Mind. Reading. Stop. Okay? 
00FD3BFC: Oh, if it isn't the shrine maiden and the human @magician! Hello!
00FD3E24: *sigh*... 
00FD3E94: All right, let's finish this up quickly.
00FD3EE0: Oh dear, we've been found out? Looks like we can't@return for a while then.
00FD4414: Whoa, you're falling for her sales pitch now? 
00FD44EC: Ohhh, it definitely looks weaker now. 
00FD55F8: Oh, here's why. 
00FD661C: The terrain's all different again.
00FD6E6C: Uuuuhhn... Heeeelp meeee....
00FD71D0: Hm? 
00FD7A1C: Ah, Reimu, Marisa! Help meeeee! 
00FD7BB0: Ah, right. Don't rat on me to Sanae, okay?
00FD7DC4: A little extra wouldn't hurt either.
00FD8550: You see, that's the problem.
00FD8754: Ooh? Reimu, someone's there.
00FD8F88: Gotcha, here we go! 
00FD90A8: Oh well, whatever way it goes, it looks like it won't @affect us badly. Let's just keep going. 
00FD911C: We should drag Meiling out now. 
00FD9800: Hah, I like your thinking, mister! Let's meet up back at@the inn and we can talk shop! 
00FD9C40: So Reimu, will you allow me to join so I can keep an eye@on the princess?
00FDA0E8: Erm, so I'll be joining up with you. Don't work me too@hard, okay? 
00FDA218: We've finally found you!
00FDA648: Eh!? But.... Erm, okay, understood. Are you really sure @you'll be fine, master? 
00FDABB8: Yes, yes. *sigh* Just because circumstances are @convenient... 
00FDAC60: Roger that! Be careful on your way too, master! 
00FDAE88: Tenshi... Ahh, the celestial. I suppose that means she's@carrying the Ame-no-Murakumo. 
00FDB154: Yeah. 
00FDBF10: My, you have good eyes. I suppose that's an items @merchant for you. 
00FDC448: ......? 
00FDC748: 'Sides, that gap's too small for anyone to fit inside.@you sure you weren't seeing things? 
00FDCAF8: Bah, like I care. If she wants revenge, all she'll get@is a good pasting when she comes for it.
00FDDD24: This place's weird... Let's just keep movin'. 
00FE1F44: Hm? 
00FE50C0: Prepare yourself, because we will.
00FE51A4: Heheh, our years of wisdom isn't just for show. 
00FE5604: Thank you for the warning, Reimu. 
00FE5A20: Ooh.
00FE5CF0: Stand by at the village for further instructions, if you@will. 
00FE5F8C: You KNOW that wasn't me.
00FE6BAC: Mmhm. 
00FE764C: Err, I get what you're trying to do, but... 
00FE7878: Gah, Eirin and Reisen...
00FE7DD4: My, are you underestimating me? I'll be fine for a while@yet.
00FE895C: Ehh... That actually DOES sound troublesome...
00FE9BE8: Hah, however many you've got, bring 'em all on! 
00FE9FFC: *sigh*... We were too late. I can already imagine what@happened. 
00FEA090: ...Ahh, here's why. 
00FEA28C: Meeeeeiling? You alive? 
00FEA6E4: How many years do you think I have served under the @princess? 
00FEA728: The vines look a lot more sluggish too. I think we can@do this.
00FEABF0: Whew, we're finally out of the sea! Ahh, this kind of @place suits me way better.
00FEAE98: Shall we cut it like Meiling said, then?
00FEAEC8: No worries, all additional party members are welcome. 
00FEAEF4: ...And if I don't?
00FEAFB8: ...Eh? How'd you know that? 
00FEB47C: I'm not too sure that place could really be called a@sea... What was it, anyway? 
00FEB5F0: Gotcha! I may not have been able to strut my stuff in @the sea, but you'll see the best of me here!
00FEB8B8: Let me take a look at it. ...Hmm, hrm...
00FEB934: My, is that so? Mmhmm, Youuumuuu! 
00FEBA80: Let's see, I think that voice came from behind this @curtain of vines...?
00FEBAB8: Yes she was. Satori~, c'mere~.
00FEBB30: Just remember that when you "burn 'em all up," you don't@burn us as well...
00FEBC48: Ah, a Philosopher's Stone...
00FEBC88: Umm... You messed up and got caught by that giant @insectivorous plant.
00FEBDE8: If you knew already, there wasn't any need for you to @actually say it! Waaaaaaah! 
00FEC0F8: I knew the Hakurei shrine maiden would understand. In @that case, I'll be heading upwards a little. See you~.
00FEC204: The stone looks to have settled down. 
00FEC33C: Hopefully they're all sticking out of the ground far@enough to actually do so... 
00FEC378: We haven't met you since Kaguya joined us, you know. She@didn't come along easily either.
00FEC490: Ah... Things happened, and she ended up going further up@the Great Tree. 
00FEC854: But at least we've found the princess, and that's what@matters most. 
00FEC8BC: Nope, none at all. Just didn't expect you to be so@understanding...
00FEC9B8: And the other two figured it'll be too troublesome to @save you, so they left you behind. Sounds like@something they'd do, anyway.
00FECA28: Good. Reisen, while you're with them, follow all their@instructions. I'll be off then, see you later.
00FECB18: This is good timing, we needed to talk to you anyway. @Tenshi's aiming for you.
00FECC80: To be frank though, if she really brought the @Ame-no-Murakumo here, then that makes things much more@troublesome. If only it was still stored away...
00FECEF8: Ahh... Stuff happened, and she went further up the Great@Tree. 
00FED098: Hiya~. My my, you've gotten much stronger since we last @saw you.
00FED248: Ah, Reimu and Marisa! Did you cut some of the roots?
00FED848: Ehh, she's pretty stingy, so don't expect much. She gets@mad just from me takin' a few books from her without@saying anything.
00FED8AC: Ooh, it's hard to walk 'round here... All the roots and @ivy here makes it hard to get a good foothold.
00FED908: And that's that. We're collecting them at a good pace.
00FED954: ...There seems to be a lot of fallen trees around here. 
00FEDA2C: Most of their trunks have deep scars as well. What@could've happened here? 
00FEDA68: Looking at the amount of roots below... There's probably@three more roots left. Please hurry up and cut the rest @of them tooooo! 
00FEDACC: .....Ahhh, fine, whatever...
00FEDD98: Nothing to be done about it. I will continue to search@for the princess. Reisen, you will join up with Reimu's @group.
00FEE054: Then it's gotta be a root from that insectivorous plant @that got Meiling. It pretty much feels the same,@anyway. 
00FEE0A0: No it's not! It's all gooey and slimy inside, and @tentacles keep touching me everywhere...
00FEE120: Looking at the amount of roots below... There's probably@two more roots left. Please hurry up and cut the rest @of them tooooo! 
00FEE6A0: I cannot help it, unfortunately. I do it@subconsciously. 
00FEE6E0: Looks like attacking its roots will be effective. How @many have you cut?
00FEE7C4: I don't know about the puppeteer, but Marisa was never@going to be so mature.
00FEE80C: Hmm? This root's flaying about like it wants some @attention.
00FEE968: Fool, you should know by know after that whole mess at@Floor 4. Dolls aren't gonna slow us down. We'll finish@this in 40 seconds. 
00FEEB40: Anyway Reimu, sorry for pushing Reisen onto you, but do @take care of her. I'll be looking for the princess@alone meanwhile.
00FEEBF0: You threw the first punch, and lost. What right do ya @have to complain? 
00FEF1C0: There you go, Yuyuko. It was the Ame-no-Murakumo after@all. My prediction was correct. 
00FEF208: The celestial may be getting stronger, but don't forget @that the monsters around here are getting stronger too. @Don't let your guard down. See you around~. 
00FEFE20: Huh?
00FEFFB8: This is quite an interesting topic... Whatever the case,@we should recover the stone posthaste.
00FF05D8: Yeesh, a bit late to be complainin' 'bout this now, but @Patchouli sure pushed something real annoying on us.
00FF0AC8: No worries, she's still twitching. She's always going on@about her sturdiness and all. 
00FF0CE8: My, that voice... 
00FF10D8: Ah, Reimu and Marisa! Look at this plant now! 
00FF16A4: Uuuuuhnn....
00FF1AE0: ...That looks extremely comfortable.
00FF1B00: Now that you mention it, you're right... I could not@hear the voice of their mind. I must have been seeing @things... Right...? 
00FF1BC4: Perhaps. It does look like a habitat for poisonous@insects.
00FF1DC8: Eirin! I'm saying this before you can protest! I'm not@going back to Eientei yet!
00FF2098: ...Er, well...
00FF2100: Okay, okay... We'll help you out, but what can we do@exactly...? 
00FF21C4: ............
00FF28C0: ...Mm, I see. 
00FF29F4: Not like it matters. All we need to do is burn 'em all@up! 
00FF2D94: Ahhhhhh!
00FF2E60: Eeeep.... 
00FF2EE4: Waaaaaah! Help find the roots of this insectivorous @plaaaaant!
00FF33A8: No one asked you to search for us. Besides, I'm not the @one who brought Kaguya here. She's the one who wanted @to come.
00FF3A90: Huh? What was that gross moaning all about? ...Don't@tell me you were tryin' to sound sexy?
00FF3B44: Yes, stealing people's things does usually make them@mad...
00FF3C74: Meiling, we've cut all four of the roots you requested. 
00FF431C: I've found yoooooou!
00FF4374: Umm, there seems to be more popping up. A lot more. 
00FF4580: Ahh, this isn't ivy.
00FF4750: My, Reimu and co. So we meet again. 
00FF4970: Let's hope it goes as well as your confidence @suggests... 
00FF4C14: Hey Reimu, did you see Alice's doll just now? 
00FF4CD8: ...If we feel like it!
00FF4F68: Ehh? No, I didn't see anything. 
00FF5528: Three of 'em. 
00FF5530: Hm, Reimu. Lookit that thick ivy. Looks familiar? 
00FF5650: Mmm... I feel bad for that rabbit. That should've been@my duty...
00FF5844: Whew! One insectivorous plant down for the count. 
00FF647C: That's pretty rich coming from you. 
00FF64D0: Looking at the amount of roots below... There's probably@one more root left. Please hurry up and cut the rest of @them tooooo!
00FF6920: Can't argue with that...
00FF6B98: It could pass for a forest, but I suppose a more@suitable description given the atmosphere would be that @of a subtropical jungle.
00FF6BE0: Can ya stop holding conversations with our minds@already? Wasn't that weirdo medicine supposed to have @suppressed your power anyway? 
00FF7B98: ...Ah, so you did not see it? How strange, was I seeing @things? 
00FF7BE8: *sigh*... That immortal human ran off upwards, right? @I'll keep searching for her. Master, do take care of@the princess. 
00FF8430: It's fine, but be careful. She's much stronger thanks to@the Ame-no-Murakumo.
00FF8558: (...From the way she's speaking, she knows that I was @holding the Ame-no-Murakumo? I don't remember telling @anyone about it though...)
00FF8DE0: Ehh, is that how it goes? 
00FF8EE4: Sounds simple to me, and simple is good. Go for it, @Utsuho. 
00FF9098: Yes yes, that can happen when we actually take a break@from the Great Tree! Exploration comes first! 
00FF9160: A shrine where the gods don't wanna go back to it cuz @the shrine maiden's angry at them? Yeah, now that @sounds like a really good one to worship at.
00FF91F8: Reimu, Marisa? Did you see something just now? It was @like a tiny person wearing cute-looking clothes...
00FF92DC: My, what do we have here now? 
00FF92F8: Huh, so we all weren't seein' things then. Guess you@haven't learned from experience, huh Alice? 
00FF93A8: Ahh, if it isn't Kaguya's servants. 
00FF93E4: What kinda place is this anyway?
00FF941C: Think there'll be lotsa bugs? 
00FF9450: Ah, you're right. And they look familiar too... 
00FF9528: "Dear me," really? Is that all you have to say? @Honestly... You could've let us know that you found the @princess. 
00FF95B8: It only weakens my power to read minds. What is deep in @your mind is harder to read, but the surface and@short-term memories are still clear as day. 
00FF9634: ......? 
00FF96C8: There's a whole lotta vines and grass and other things@obstructing the view here, it wouldn't be surprising. 
00FF9740: Ah, yeah. This is Patchy's Philosopher's Stone. The @surrounding plants are growing at an absurd rate too... @Looks like it's enhancin' the plants around here. 
00FF97D8: Okay. It's from here to here, then stretching to... Here@and...
00FF9840: Oh come on... 
00FF9884: ...How nice.
00FF9898: A stone that aids the growth of plants... Perhaps the @library's magician could lend a bit of it to@subterranean people. Plants don't grow well there.
00FF9930: Ah! Reimu!
00FF997C: Hm? ...Ahh, another of Ran's mysterious gates, right? 
00FF9A40: I think you need to worry about not going into a coffin @first... Surely that plant has digestive acids and all. 
00FF9B60: Ahh, I was wondering why you didn't look THAT desperate.@Wait, how long have you been stuck there...?
00FF9BE0: Huh, yeah. Weren't you with Remilia and Sakuya? What@happened to them? 
00FF9D34: Chen, tell us where it is. We'll avoid it for now.
00FF9E20: Ah, wait! This plant captures whatever prey approaches@it with extremely fast vines and then digests them@right after, apparently.
00FF9E90: And cutting or burning those vines just makes them@regrow almost immediately. That's why Lady Remilia and@Sakuya gave up and left... Anyway, it might be@dangerous to get close now. 
00FF9F68: Oh, so we can't help you. Oh well, guess you're stuck @there swimmin' in digestive acids forever then. 
00FFA000: Please feel like it noooooow! 
00FFA038: Waaaaaait! Let me explain fully! Look at the roots of @this plant! Those 4 super-thick ones at that are going@into the ground!
00FFA0E0: So we were going to settle this whole mess in one shot, @and retrieve back more followers and faith from that. 
00FFA164: Help me out of here before I can't get married@anymoreeee! 
00FFA1A8: ...Well, we'll remember it I guess. All we need to do is@tear off those 4 roots, right?
00FFA264: Two of 'em. 
00FFA298: How could we forget, with all ths fuss you caused with@Utsuho and all. What kind of trouble are you lookin' to @incite now? 
00FFA360: Sanae's really mad at you. She already knows you and@Suwako both entered the Great Tree as soon as it@sprouted. 
00FFA3F8: Yes, that part. Oh, not that I'm undignified, not in the@least. It's more that all the humans of Gensokyo are@not particularly devout on the whole faith thing. 
00FFA4D4: Mmmhmm. Exploration party members +1. 
00FFA560: By the way, where is the immortal human?
00FFA588: It was definitely there. Oh yeah, Satori and Hina were@talkin' about seeing a tiny person around here too, now @that I think about it.
00FFA6B4: As dignified as a puddle of mud.
00FFA750: I already knew you'd say that. Even if I dragged you@back now, you'd just keep running back here until @you've had your fill of fun.
00FFA848: Whether we rat on her or not, she already knows Kanako's@here... It's just a matter of time before she finds @her.
00FFAA1C: Who's... Ack! 
00FFAA38: That's simply relying on their strength: numbers. Let's @see you finish this in 40 seconds!
00FFAB08: Goodness, we've been looking everywhere for you.
00FFAB50: Guess that's true.
00FFAB68: There's nothing weird about those hobbies. What's wrong @with people having their own interests? Though@admittedly, I'm not particularly interested in dolls@myself. 
00FFABD0: A doll... For a doll, it felt awfully realistic at the@time I saw it... The surface is filled with interesting @powers, it seems. 
00FFACD0: Ahh, Mokou? 
00FFAE1C: Ah-ah...
00FFAE50: No point worrying about it now. Anyway Reimu, good to be@working with you. 
00FFAEE8: We were looking everywhere for you! Geez, why did you @have to bring the princess here? We wouldn't have to@come if you didn't. 
00FFAF44: I'm pretty sure it did. Look at the roots of this @plant.
00FFAFE4: My, is there a problem? 
00FFB054: Yeah, makes sense...
00FFB07C: Don't worry about that. Take care of yourself too.
00FFB0B0: I will! See you again!
00FFB0D8: Yeah, same here.
00FFB150: That doesn't really matter right now. Where's the @princess? 
00FFB254: Oh my, good day to you, everyone. 
00FFB338: Don't let our guard down? Tch, not like we ever could @against these monsters. 
00FFB530: Tsk~, So I owe you a red bean soup with rice dumplings. @I'll get Youmu to make it once we go back.
00FFB5AC: No. 
00FFB6B8: ...*sigh* Fine! I just need to join the lot of you, @right? Honestly, how did wanting to test the abilities@of my dolls against monsters end up like this...
00FFB758: (Urgh, I really can't let my guard down with this youkai@around...)
00FFB810: Shouldn't we start looking for the celestial and the@Ame-no-Murakumo?
00FFB898: Mm, facing her shall be our next objective. Though since@she's already looking for us, I doubt we could avoid@her even if we wanted to. 
00FFB990: O-Oh my... Is that so?
00FFBA08: My, is that so? A celestial shouldn't be able to draw @out that much power from the Ame-no-Murakumo... 
00FFBAE8: Yes, she shouldn't. What could have happened, I @wonder... Anyway, we'll be careful. 
00FFBD78: Pretty rich comin' from a pair way stronger than us...
00FFBE38: Now now, don't be so bitter. We're getting strong enough@to cross swords with monsters that those two can't let@their guard down against. 
00FFBED0: Hah, at this rate, we'll catch up to them soon enough.@Better watch your backs, you two! 
00FFBF80: It might be my imagination, but the growth of the @vegetation in this area seems to be fiercer than@normal... 
00FFBFF4: If you target groups with weird hobbies, you could@probably charge a lot for it too. 
00FFC0F0: Hmm? Now that you mention it... 
00FFC150: ...What would this be?
00FFC47C: We'll finish this in a flash. 
00FFC4D8: We just have to bring this back now. Let's go.
00FFC5A4: Ah, Marisa. There was someone hiding in that gap between@the trees just now... 
00FFC658: Yeah, we cut something that looked like one. Did it @help? 
00FFC698: Ahh, as I thought, this amount of dolls wasn't enough to@win against you all... It was a good try though.
00FFC7E0: One of 'em. 
00FFC7E8: My, how may I help? 
00FFC890: How's things now? 
00FFCA68: So we just have to get Meiling out of there now. All@right, wait right there for a little bit, Meiling.
00FFCB20: ...Oh right, while we were beating up the plant, most of@Meiling was inside it...
00FFCC98: I guess we can just bring her back with us and add her@to our exploration party then. Good for you, Meiling! 
00FFCF78: You two are really tiresome... Can't you just apologize @and make up?
00FFD03C: Youmu, you're up! 
00FFD138: Yes, yes... 
00FFD1F8: The face is quite adorable as well. If it could move@autonomously, I'm sure many people would want one.
00FFD2EC: It's Alice's doll.
00FFD320: Naturally. You won't get away with destroying my Remote @Dolls Types 1 and 2.
00FFD35C: ....Er, what? 
00FFD404: No way. 
00FFD410: It's pretty helpful for times like these though. I don't@even hafta bother trying to describe the doll.
00FFD550: ...But thinking about it, your idea can work. There's @quite a lot of doll parts around after that fight...
00FFD618: ...Hm, was what I saw earlier a doll? It did indeed look@like a tiny person, and was wearing the same outfit as@the one Marisa is thinking of.
00FFD780: So you don't care about what happens to your precious @little dolls? Feel free then. 
00FFD8E8: This looks like a plant that gives nourishment to other @roots. It may be supporting the big root on the 8th @floor. If we cut it down, we might have an easier time@moving around on the 8th floor. 
00FFDA28: ...Oh?
00FFDAF8: ...Hmm? What was that...? 
00FFDC24: What's up, Hina?
00FFDC78: ......? There ain't anyone there. 
00FFDCA0: Hmmm, was I? That's strange...
00FFDCE0: If all of us have seen dolls though, that means they're @aimin' for us, right? That Alice's surely out for @revenge.
00FFDEDC: Hm...?
00FFDF18: I know. Honestly, I don't even really need to cut this@down, you all can just blow it to pieces on your own... 
00FFDFA0: Chill a bit, eh? Just get it done and over with and we@can move on.
01032BE8: .....Waaaaaaah! Can't you guess!? 
010A0700: That does sound good. If we could bring back something@like that, I'm sure it would sell well at my store. We@should give it a try once we take a break from the@exploration.
010A0790: As they stepped through the mantle of vines, they were@greeted by the sight of a massive insectivorous plant,@and Meiling's head popping out of it. 
010D9150: Who knows? Whatever case, that sea-like area made me@feel like I was floating. It was really good for@sleeping. If I could bottle it and bring it back@somehow, I'd do so right now. 
010D91D0: It looks kinda like the forest in the first floor, but@the atmosphere's a bit different... It's all damp and @humid. Feels a lot worse. 
010D9260: But you weren't able to read the mind of whoever it is@you saw, right? You must've just been mistaken, then. 
010D92E0: Ahh, I'm okay on that front. The acid it produces seems @to be only effective on insects without shells. It@shouldn't work on humans, I think - and moreso for a@tough youkai like me. 
010D9380: These roots probably extend really far. If you can find @where these roots are sticking out in the ground and@cut them off, you should be able to stop the@regeneration of the vines and save me!
010D9428: This isn't a bad deal for you either, Reimu. If humans@start believing in the gods again, your shrine will get @more worshippers too. 
010D94B8: Now now, let's let them do as they please. As long as @this incident is resolved, it doesn't really matter who @resolves it. Kanako's probably perfectly capable of @handling the enemies around here too. 
010D9560: Even someone as distant from magic as me can tell how @much mystic power it's emitting. But can magic really @make humans or monsters go crazy enough to turn this@place into something like this? 
010D95E0: I ain't liking that glance you gave me when you said@that. But anyway, magic shouldn't have that property by @default. I guess the monsters 'round here were just @kinda weak to it? 
010D9670: By the way, this doll is very well made. It is very @exquisitely detailed, and the western clothes are @splendid as well. Especially here, these double-layered @frills with a line of lace embroidered below them.
010D9720: H-Hey, hold it! Stop that line of thought right now! I@worked quite a bit to make those dolls! Give the parts@back so I can repair them!
010D97C8: If you reaaaaaaaally want them back, then you'll have to@repay us somehow, hmm? Like joining our exploration,@for example.
010D9860: I can believe it. Alice has that kind of personality to @begin with, and that last taunt you left off with is@definitely something she won't take lying down. Thus, @it'll be all your fault, Marisa.
