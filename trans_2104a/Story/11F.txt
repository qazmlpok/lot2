00FC6678: ....Hrmph.
00FCDD40: You won't get awaaaaaay!
00FCE714: ............
00FDBBCC: Tch... She's fast...! 
00FDE2CC: You've really had a tough time of it, huh?
00FE0834: It is? What's it doing buried here? 
00FE0D98: Whenever you hafta ask, it probably isn't. Did your @dowsing rod react to somethin'? 
00FE0EB0: The golden glow has finally arrived. We are not quite a @match for that... 
00FE3C78: Yep, it is. 
00FE4CE0: I was just thinkin' that I hadn't heard or seen anything@of her in ages. What did you do to her? 
00FE5BA8: Both of them seem quite used to battle... I suppose I @should expect no less from a head priest and the@Enma... 
00FED504: We've got a whole bunch of people now, and that's a good@thing.
00FEE738: Ugh...!! Don't get overconfident, you commoner! 
00FF0740: ......Hmm? Marisa, do you hear something? 
00FF5560: Yes yes, my apologies. Now that everyone has been found,@you shall be joining up with Reimu's party as well. 
00FF55F0: Milady, this is why I advised you to bring her along@with us on our picnic...
00FF7DB8: I don't think the power she attained has anything to do @with it... Surely no one changes that much from power?
00FF7EB8: Agreed. Let's keep on movin'. 
00FF7F14: I couldn't help it! What did you expect me to do!?
00FF8064: Where, y'ask? Definitely not above, cuz she's right @here. 
00FF8174: Princess!? I see, so you were with Marisa's group.
00FF8288: *sob*... *sniff*... Waaaaaahh... Moooookou, immortal@humaaaan.... Where are yooooou....
00FF83E0: I don't really get what I'm supposed to have. 
00FF86C8: Grooooss. Urgh, just when I thought I could finally @unwind... 
00FF890C: Anyway, everyone. I will be in your care for a while. @Pleased to work with you. 
00FF8A5C: Hm, it's possible. Whatever the case, best to retrieve@it first. 
00FF8DF4: What, cat got your tongue too Sakuya? ....Ah, wait, ya@mean... 
00FF904C: ......? Remilia, what are you going on about? 
00FF9A98: The place that Reimu's group arrived at was littered@with the bodies of horribly charred monsters. 
00FFB44C: If you only pay attention to Byakuren, you're going to@be in a world of hurt!
00FFB4D8: Why you little... All of you impertinent fools...!! 
00FFB7B8: Anyway, let's not get left behind by them. We've got our@own path to walk. 
00FFBBA0: Whoa, what's this?
00FFCFC8: ...What's bugging me more now is Tenshi's behavior. Her @power's still there, but her personality's all outta@whack now.
00FFD6E0: And Reimu. And Mokou. 
00FFF978: My, nothing of course. Heheh... Anwyay, since the @princess is with your group, perhaps I should accompany @you as well?
010007E0: Hmm... Yeah, there's something... 
01000D68: That's fine with me. What about you, Kaguya?
01002974: Sorry Reimu, it was nothing. Let's hurry onwards. 
01005190: Ahh, the flame human! So you were with Reimu's group all@this while... 
010053CC: So, would it be acceptable if I were to be in your@care? 
01005D80: Ahh, it wasn't a big deal.
01005F0C: Umm, sort of... And maybe not in a way... 
0100618C: O-Oh, be quiet. What are you trying to say anyway?
010062D4: (Did my dowsing rod just react strongly for a moment@there...?)
010065D4: We could check out the direction it reacted in for a@bit, couldn't we? 
010066D8: ...Hm? What's up? 
01006FE8: I suppose that is true... 
0100732C: ...Ah, this is the sound of battle. 
0100764C: ......? 
01007968: You are open! 
01007978: Haha, don't be so sore. ...I do agree though. If power@changes you that much, then it's something I don't want @either. 
01007E7C: Byakuren, we retreat now! 
01008CB8: Oooh... I can't do anything to monsters anymore, it took@all I had to run and hide my way here...
01008F18: That would've been terrible. If we did bring her along, @we wouldn't be having a picnic... We'd be having a@splatterfest in the middle of the blazing fires of@hell. 
01009730: What kind of overpowered attack could've burned all @these monsters at once...? Hmm, maybe it's the same @person who made that crater at the floor below. 
01009EA8: Hmm, but it really was just for an instant, so I'm not@even sure which direction it was pointing at... I think @it was in this direction, but...
0100A9A8: Byakuren's group did say they were going to keep chasing@and challenging Tenshi periodically. Perhaps she@dropped that during one of their skirmishes?
0100C878: Yes, but just for a moment... There's no reaction at all@right now.
0100D068: ...It's nothing. This conversation is over. I saw @nothing.
0100DE70: I was ordered to find her, but I haven't seen hide nor@hair of her... And I'm not strong enough to climb any @further...
0100E448: Huh, is that okay with you Eirin? I would've thought@you'd instantly drag Kaguya back to Eientei with you... 
0100E528: W-We did nothing. It's none of your business anyway.
0100E620: Allow me to summarize. The princess insists on remaining@here. Thus, if I drag her back to Eientei by force, she @will definitely come up with some ploy to escape and@come back here again. You can count on it.
0100EAB0: Hm? ...Something's buried in the ground there, ain't@it? 
0100ED14: ...Hm? Reimu, lookit that figure over there.
0100EDA0: Where? ...My, that's a familiar face... 
0100EDD8: Ah, I see... Reimu, Marisa, is it okay if I join up @too?
0100EE58: Thank you very much. It's a pleasure to be working with @you~. 
0100EEC0: Hmmm, this is a conundrum... To have climbed this far @and not seen a sight of the princess... 
0100EF20: ...Perhaps I should climb a bit higher? Honestly, where @could the princess have gone off to...
0100EF8C: O-Oh... Well, you sound troubled... 
0100EFB4: Ah, Marisa! 
0100EFF8: I see you all have been taking good care of her. Thank@you very much.
0100F0B0: The 10th floor? My... So I had completely overtook the@princess in my zeal to climb upwards to find her. 
0100F1F0: I'd think it's pretty hard to search for someone while@runnin' and hidin'... 
0100F258: Ehh? We're not going back to Eientei? 
0100F334: Sure, come as you are.
0100F460: That would be for the best, if I could... Princess, do@you have any intention to return to Eientei yet?
0100F53C: By the way, where did you find the princess?
0100F588: No, I am absolutely not going back yet. Why do I have to@stay away from something so amusing?
0100F66C: She was on the 10th floor, and as black and burnt as@charcoal. 
0100F76C: And there you have it, Reimu. 
0100F920: ......Hm? 
0100FA78: Something wrong, Naz? 
0100FBBC: ......Hmm, was it my imagination...?
0100FCF8: ......? Ah, this is one of Lady Byakuren's sutra@scrolls.
0100FEE8: What the... Yikes, this is terrible.
0100FF20: One, two, three... There's easily a few dozens of 'em.
0100FF54: Really, who the heck could've done this?
0100FF90: ......Ah. 
01010090: I would think it would be best to be honest about this@situation...
010100D4: ......Ahh... Milady...? 
01010690: !?
01010B48: ...Oh, Reimu! A bunch of people are fighting past that@valley! 
01010BE0: Hehe, the magic within my sutra scrolls can enhance any @human to this standard easily! Have at you! 
01010CC0: ...Ahh, and off they go. Hope those two manage to get @away. 
01010DF8: The priests I know aren't the type to melee on the front@lines though... 
01010EA0: You're so behind the times, Kourin. Priests can do real @good work by dumping everything in STR. 
01010F38: I'm sure those two should be able to get away easily. @And they've helped to exhaust Tenshi too. 
01011060: Mm. When we bumped into her on the 6th floor, she still @acted like the normal Tenshi... 
010110C8: At that floor, the celestial did indeed seem more like@the playful type... The bloodthirsty celestial she is @now almost seems like a different person in comparison. 
01011288: Hail to the Three Treasures!
010112C8: I like myself as I am right now, thank you very much. @Besides, it's not like you'd thought any better of me @when I actually did have it, no?
010116E8: ...Is that so?
010118DC: ...Care to test that theory once we get the @Ame-no-Murakumo back? 
010DFA40: I would love to, but the princess will not consider @it... She would run off again if we drag her back by@force, so the safest way would be to stay close to her. 
010E1DB0: I have certainly learned that much from observing the @princess at Eientei for all this time. Since the@princess insists on staying, the most effective course@of action would be to allow it while staying by her @side. Do you not agree? 
010E1E70: Your displeasure means little as long as Reimu has given@her permission, I'm afraid. I'm already making several@concessions, so do try to compromise as well, princess. 
