00F74264: That's a lot of ghosts. 
00FD4A14: As for you. This is our second meeting. Do you have @anything to say for yourself? 
00FD98F4: Indeed. 
00FDA52C: Mmm... I'm getting some... weird sensation... 
00FE1234: ...It's still weird, though...
00FE2110: Ah, of course...
00FE22F4: Yeeep.
00FE277C: ......Hm? 
00FE3D30: ...A mask?
00FE54F8: And if this really is scarlet gold... then a certain@sword starts coming to mind.
00FE8D28: Time to brace ourselves for the worst...
00FE9FDC: Heheh, perfect. 
00FED994: How 'bout you try putting it on your next doll? 
00FF01B0: Given how you're acting, I suppose the way the fake @Kasen disappeared into the blackness jogged some memory @inside you? 
00FF2918: Mmm. ...Well, I'm keepin' it anyway.
010017E4: *sigh*... We may have solved one big problem a little @while ago, but that's just given way to another.
01001C00: ............Hmm?
01002EFC: Ahh, so Marisa's like that to everyone... 
01003034: ...Huh, that weird sensation just went away. How@strange...
01005240: Goodness! Why are you making jokes at a time like @this!?
010056F0: The non-noisy Kasen just turned into black mist...
01006EF4: What? Don't be so coy, Rinnosuke. Tell us what it is. 
01006F98: Of course not. ...We should get the story from the@hermit's mouth. 
010073EC: Oh my. If the master of Hakugyokurou feels that way @too...
01007488: You're going to try to turn into mist and flee again, @huh!? 
01007768: This'd be one helluva prank to pull. 'Sides, if I were@gonna do one, I'd do it at the village or the shrine. @Definitely not here of all places.
01007AB8: Take a look at this mega-thick book. It's really built@to last.
01007DA0: Kasen, when did you get ahead of us? Don't wander off by@yourself. We still don't know this place well - who @knows what might happen.
01007F60: Well, whatever. It may have been pretty strong, but @we've already beaten it once before. Now to beat it up@one more time, then make sure it doesn't get away...
01008748: Err, sorry, but I still don't get the point. So what's@that fake Kasen supposed to be? 
01008FA0: It may have become blackened and lost its sheen... but@I'm pretty sure this is scarlet gold. 
01009040: I suppose I have no reason to refuse. ...You'd steal and@read it even if I say no, anyway. 
0100AAF0: ...The impostors just...
0100B178: Mmm... Feels less like we defeated it, and more like it @got away. 
0100BDE0: We did this, of course. Remember what Yuyuko and our@Enma said before? That these impostors felt exactly @like ghosts when they turned to mist and ran? 
0100E560: Mmmm... Let's see here. 
0100E814: ......Hmm?
01010B98: What's going on?
01012D78: When the impostor disappeared into the black mist, it @gave off the aura of a ghost... despite not having one@at all when we were in battle.
01014138: Ahh geez, they're both a pain.
01014CA8: Hello? Can you say something already? 
01014EB8: Exactly! It's not very difficult to do, either. I'm sure@you can do it yourself with a bit of guidance.
01015214: Tch, how boring.
01015CB8: *gasp, gasp*... All right, impostor! Give it up!
01015FD8: ...And disappeared. 
01016528: I'm pretty sure I've heard of this term before... Ugh,@what was it again...? 
01016C1C: What's going on? Why are there two of you?
01016CD8: Oooooh... 
01016D00: Yes yes, exactly. 
010171DC: Reimu, I'll get angry if you keep saying that.
01017738: Ah, it's the quiet version of Kasen.
01017874: ...Something just fell out from them. 
01017BC0: That would be nice... Oh, Reimu, by the way...
01017C18: I'm alive right now, you know.
01017E44: Whew, they're down... 
01017EF8: Ahhh... I do recall something like that.
0101839C: What's the matter, Marisa?
01019B58: Was it just me, or were you having more fun than usual@while fighting my impostor? 
0101A850: You really have an eye for discarded items, don't you...@Is that a magical grimoire? 
0101AB88: Let's see here... 
0101C7D8: Man, we sure aren't short of explanations. Nice to have @a bunch of specialists around. I doubt there's anything @that can scare us with what we've got.
0101C8D0: No, it doesn't feel like one. Let's see here... 
0101D7A0: Don't think you can deceive me so easily. We'll need to @have a nice, long talk once we're back at the inn.
0101E2D8: Well then, Yuyuko and Eiki? Let us proceed to @exterminate evil for the safety of the village and the@harmony of Gensokyo.
0101FCD8: (They'll drag me back if I say a word. Better run away@first.) 
01020888: Ehh...? ...What's the meaning of this!? Reimu, Marisa!@Is this one of your pranks!?
01021020: Good grief, not everyone has as much free time on their @hands as you geriatrics. I shouldn't be leaving the @shrine neglected for much longer. I'd rather we clean @this up fast and get back to our normal lives.
01021270: C'mon, geriatrics. Don't just nod at each other with@those knowing faces. Give us an explanation, and make @it simple.
01021A58: Most likely, after being defeated and losing their@power, they revert to their base form. They then turn @to mist and flee in order to gather enough power to @regain their human form again.
01021D70: Anyway, I'm sure there's a use for this seal. Let's take@it with us. 
01022190: The Great Tree that refuses to fade away, the sudden@appearance of an underground stratum, the masses of @vengeful spirits, impostors assuming our forms... 
01022298: Mmm, the surrounding trees are kind of weird too... From@their appearance, you'd think they'd be dead, but @they're very much alive. In fact, they're brimming with @vitality... 
0102233C: ...Huh? 
01022378: Hrmph, say something already... This is creepy. 
010227D0: Figured it was gonna end up like this.
010228A8: Asking it won't get you any answers. Besides, they all@look like they're raring to go. 
01022A78: My, you were pretty strong despite fighting alone... And@you don't talk peoples' ears off either... Can we do a@Kasen trade?
010230D8: You're always angry with me for one reason or another @anyway. 
01023260: What's going on?
010233A8: ...I'm pretty sure I heard you shout "you noisy @preacher!" while you were launching your attacks. 
01023630: ......Hmm, it looks like a black piece of metal... It's @about the size of a fingertip...
01023910: Well, I'm not sure what it all means either. The@impostor was definitely a ghost at the moment it ran@away, but if it didn't feel like one at all when in @battle... 
010239D8: ...I more or less imagined this happening back when we@saw the hermit's impostor. The villagers did say that @they saw "impostors" after all. 
01023D18: My my, our esteemed judge thinks so too? ...Then I@suppose it can only be that, heheh. 
01023DE0: Mmm. Disregarding what the impostors are made of, we@figured out that they were essentially ghosts at their@core. 
010240E0: The area through the pit is a forest, as expected.@However, the colors of the trees are far different than @the ones at the first stratum. Ominous purple leaves@adorn the lifeless-seeming tree trunks. And @furthermore...
01024198: What's with all of them, actually? There are more of@them here than even Hakugyokurou or Higan. And besides, @these so-called ghosts feel more like...
01024370: Ahhh... I should've pushed that trade harder... 
01024488: Hmm. Perhaps it's this atmosphere of malice that's@influenced them? It's certainly a far cry from the@divine power that was present all through the top of@the tree. 
010246A8: Don't let your guard down! We're going to explore around@here and make sure our theories are correct!
01024838: For all that you run your mouth off lecturin' us, you @aren't much for patience yourself, huh? Though I@totally get you.
010248F0: I don't understand that mentality at all. I'm a "better @safe than sorry" type myself. 
01024B3C: ...Hm? What's this thing here?
01024BA0: Just to be 100%% sure... Kasen, this isn't one of your @new spells, right? And you don't have some twin sister@we don't know about?
01024C78: Suddenly, the other Kasen assumes a combative stance. @She still shows no signs of a diplomatic response.
01024CE0: Actions over words, I see... I was planning on punishing@you anyway, so this suits me just fine! 
01024E10: Yuyuko's power causes the three clouds of coagulated@darkness to slowly fade away. Eventually, all that's@left is three small dark fragments that drop to the @floor.
01024FB0: As the three impostors turned into mist and began to@disappear, the mist suddenly stopped fading away, and @started coagulating instead.
01025058: My, that's too bad. I was hoping we could unveil the@nature of the incident in pretty much our first step@into this stratum.
01025108: ......? So what? Does that mean the impostor was Kasen's@ghost?
01025248: I see. And when they were just ghosts, you used your@power on them.
010252A0: I already had a vague inkling when this whole impostor@mess suddenly started at the underground of this Great@Tree... and this revelation is only making me more@suspicious. 
01025338: Ooh, nice. It's not often we get to see a youkai sage @look so shocked. Anyway yeah, this definitely seems to@be scarlet gold.
010253F8: Mmm, finally a chance to use my powers instead of my@physical exertions. What do we have here... 
010254D8: Now now, no sighing. We'll get to have fun exploring@with everyone again. Isn't that a nice perk?
01025594: Whoa, wait! ...Well, looks like she's up for a fight. 
0102562C: ...Mmm? ....Ahh!
010256F0: Ahhhhhhhh!
01025738: Not in the slightest. I was just being myself. I suppose@my sheer passion to exterminate youkai and resolve@incidents may seem like that to you.
010257B0: It just isn't coming back to me... Oh well, I'll just @take it first.
010259A0: ...I'm not well versed in these kind of lovers' @quarrels, I'm afraid. You can have your little@conversation together.
01025B40: ......? Excuse me, what's this all about? 
01025CA8: Trying to impersonate me, though... I didn't know they@had so much disregard for their lives, heh heh... 
01025D50: Oh, my my my... 
01025D78: With respect to your courage, perhaps I'll try to crush @you to pieces as painlessly as I can. 
01025E44: .....Ahhhhhh! Reimu, look over there! 
01025E78: So it's not just Ibaraki and Suika who I get to fight? I@get to fight myself too? Hanging out with the shrine@maiden sure was the right choice! 
01025F20: Apparently not. You certainly don't seem like a normal@monster or youkai. I suppose you'd be most like a @shikigami without direct orders...? 
0102608C: Huh? What now?
010260E8: You've already escaped from us once. Do you think we'd@be so foolish to try again without a plan, hmm? 
010261C0: My my. This is quite the spectacle we have here.
01026304: ...!! 
01026340: Ahh geez, why is everyone so fired up for this? 
01026620: Unidentified items like these would be best handled by@our antiques dealer. How about it?
01026854: My my, trying that again? Hehe... Do you think I'd allow@that? 
01026A88: Figured. *sigh*.... This whole incident is taking a turn@for the worse...
01026AF8: Time for a bit of trivia I learnt from Kourin! Water@Lilies are those wheir their leaves are cut. A water@lily that has no cuts and has a stamen that forms a @ring is called a water lotus, apparently. 
01026B70: ...All right. Let's exorcise the ghostly portions of@these three entities. 
01026CA8: .........Ehh? 
01026CF0: So this is a Water Lily Seal? 
01026D70: Thanks for that piece of information that matters @exactly zilch...
01026E70: Hmm... "Non-Neumann Systems..." Issue 9.
01026F30: My, commerce traders certainly seem to pick up all sorts@of knowledge. I have no clue what this is about myself. 
0102702C: Lemme read it later!
01027100: Mmhm... 
01028330: What is the matter, Reimu?
0102838C: (Ack... It's Reimu and my big sis.) 
01028538: Geez, you're the strange one. 
010335B0: I'm not really into masks either. And besides, I'm not@familiar with eastern puppetry... 
01033C50: Looks like one. It's giving off a mysterious vibe, but@it doesn't feel like magic... 
01033CF8: It's definitely giving off a weird aura. I think I've @seen it somewhere before... These aren't really my@style, though.
01033DE8: My, I would've thought a packrat like you would love@things like these.
01034130: ...? There's something on the ground. 
010A18C8: Ghosts with concentrated malice. They're more like@vengeful spirits than ghosts at this point. 
010A1960: Hmhm. A grape-colored forest in the underground @overflowing with vengeful spirits... Put this together@with the humans' claims that we attacked them, and... @Heh heh...
010A1A10: It's a real shock to find a whole bunch of them so close@to the surface. Vengeful spirits can be pretty annoying @when they're not dealt with, or if there's nowhere to @burn them up. 
010A1AA8: It didn't exactly jog some memory... and don't call me a@geriatric, please.
010A1B50: ...Fine, come at us with as many as you want, as many @times as you want. We'll take you down each and every @time! 
010A1BF8: What does that mean? Ghosts are posing as us using@blackened scarlet gold as their power source...?
010ED2B8: They've gone feral, essentially. These are just plants@and trees, so they're no bother even when berserk...@but a few missteps and we might have the entire @underground out to get us.
010ED368: You're already stealing my likeness then showing up in@front of me! Don't think you can get away with playing@dumb! Show us your true form, then tell us your name@and objectives! 
010ED3F0: Right now, all we know is that it's something like a@ghost, but not exactly... and that it assumed the @hermit's form and attacked us. Did my random musings@give you the wrong idea? My apologies if so.
010F5678: It does bring many possible theories to mind... but we@cannot confirm anything as of yet. I hardly like to @suggest this, but perhaps we need to explore a a little @further first.
