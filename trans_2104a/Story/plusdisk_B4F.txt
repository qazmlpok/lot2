00FC7320: Heheheh.
00FC8108: Yes!
00FD5B58: Is that Gautama Buddha's bodhi tree?
00FD5BAC: Ahh, so that's how it was.
00FD7824: ...Heheh... 
00FD798C: Yeah. We've cut down ivy like these before. 
00FD9FC0: A-Ah... I see...
00FDA788: Marisa! At this rate, I'm not going to go crazy! I'm@going to get angry! 
00FDA8A4: Ohhh. Well spotted, Reimu.
00FDDA70: Wahhhhhhhhhh! I knew it!
00FDDAA8: Pretty sure anyone who comes up with ideas like that@ain't ever gonna achieve enlightenment... 
00FDE1F0: You finally show your mug, you damn Taoist...!
00FDE800: Quite possibly. Let's keep this mask safe for her for @the time being. 
00FDFDFC: Oh, Nitori? Yep, this is it.
00FE1F0C: ...Oh, there's something on the floor. Lessee...
00FE2060: Ah, you picked it up for me, I see. 
00FE24B8: ............Ohhh......
00FE29E8: Right!? C'mon Reimu, strike while the iron's hot! 
00FE3DAC: Hmph, they're not listening...
00FEBEC0: Wahhhh! Y-You've become a psychic!? 
00FF1F10: Uwaaaah! Stooooop! Don't say another wooooord!!!
00FF27A8: (...So, it was him after all...)
00FF48E8: I have no idea. The light just came all of a sudden,@then the next thing I knew, Futo was on the ground... 
00FF963C: Hrmph! Fair enough, I did blunder against you once...!
00FFCAC4: Nazriiiin!
00FFE6E0: Groan...... 
01000C98: Oh, hush. Anyway, I'll take this seal along. There's@probably somewhere we can make use of it. 
01000D08: ...Heal, you say? Heheheh, that's not all. Have a taste @of my spirit-enhanced power!
01000E1C: No, let me say it.
010036F0: Marisa, you dragged everyone here to begin with... It's @only fair for you to be the first to take the plunge. 
01005BE8: Hey, none of that now! We're in trouble if the spell@doesn't work. 
01007A34: Ooh, isn't that the famous Bishamonten's Pagoda?
01008358: *cough cough*... Ooh, you all are strong indeed...! 
0100A45C: Additionally... I recommend both of you back off a bit. 
0100A67C: Don't make such a big fuss. I'm just going based off@past experiences. 
0100ABD0: I was tryin' to tell you that I'm hungry with my mind.@Did my telepathy get through? 
0100ADC4: Will you stop with that already? I can't make out where @that strange voice is coming from.
0100AE6C: Hahaha... We've got your pagoda pieces right here. Take @a look. 
0100B070: Hello and good day to you. This is the specialist you @requested for.
0100B1EC: Y-Yes...! I, Shou Toramaru, will do my utmost best! 
0100B338: *sigh* When is my impostor going to show up...? The more@my reputation goes down, the less devotees I'll get at@the shrine... 
0100B994: Whoops, that's true. Ahem, let's try this again...
0100BE90: Er, it was the one you beat up so badly that she went to@my store to vent... 
0100BF7C: ...Who cares. Let's just beat them up already.
0100C2C8: Now now, your attention should be focused elsewhere,@hm? 
0100C444: ...Huh? You sound like this is your first time fighting @them. 
0100C5D0: Mmmhmm, that just about does it. That's three more@fragments for us. 
0100C67C: ...What if we choose not to fight?
0100C714: Hmmm. To be honest, my goal's already been achieved,@more or less. 
0100C958: Gwaaaah... Fine, fine... Geez...
0100CB0C: Feel like surrendering yet? I might just forgive you if @you cry for mercy now!
0100CF70: This is no joke. If you don't want to fight, you can@always flee with your tail between your legs. 
0100D120: ......Wait, we've got more important things to settle!@Where's Futo!?
0100D1C4: Weird. I could've sworn that's how it went... 
0100D29C: There lies a gigantic hole, easily tens of yards in @diameter. 
0100D64C: We could try... but I'm not confident it'd work.
0100DA00: This hole had finally gotten big enough to send vengeful@spirits all the way to Gensokyo...
0100DB88: This hole is stupidly big. I think the whole Hakurei@Shrine can fit inside it. It's going to take something@incredible to close it up...
0100E734: Oooh... I-I'm sure everyone'll forget about that soon @enough! 
0100EB78: She's made it this far while being attacked by vengeful @spirits and impostors. I think we can forgive a little@excitement at finally finding the real versions.
0100F16C: My, I'm sensing something.
0100F848: Ha ha ha... Let's see if it'll go the way you think!
0100F880: I've seen them often enough, and heard about them from@everyone... but I never realized how strong these @impostors are.
0100FE48: Guess we should do the same this time. It might open the@way somewhere.
01010288: She was runnin' all over the place, cryin' about how her@pagoda wasn't there anymore. I figured the way she was, @even if she was possessed, she wouldn't be a bother @anyway... Just thinking about how flustered she was @gives me the giggles again, heheh...
01010F88: All that's left... is to wait for enough ghosts to@spawn, and for "it" to manifest in Gensokyo...
010114C8: Now let me show you a bit more pain to keep you from@struggling! 
010142D4: No way! Why should I be the first? You can be the guinea@pig!
01014E20: Don't worry, you definitely can do it. Believe in what@you can do, Marisa. 
01014FE4: ....Mmph... 
01015584: *sigh* You're really are so excitable, aren't you...
01017798: I could warn you-know-who to be more careful till the @end of time and she'd still do this kind of thing @anyway... 
01017AA0: Hey! I'm trying to act cool here. Try not to make out my@impostor to be some unneeded extra, okay? 
010181E0: ....Ahh, I remember now! That book-reading youkai that@kicked my store's door into pieces had a book with a@similar title!
010186F8: ......Yeah, nothing's coming to mind. 
0101888C: Mm, what's this? ...Ahh, oh yes.
01019244: All right, Tenshi. Take the charm and and dive on @inside. 
010195A0: Maybe I'll search for them and give them a read when we @head back...
0101A3AC: Talk about a 180. 
0101A470: You think Reimu remembers every breath that she's taken?@Likewise, there's no way she'd remember every single@youkai she's beaten up. 
0101A718: It's not an unreasonble question for her to ask.@Although the charm itself seems to be working by its@dazzling glow, the hole still continues to spit out @inumerable numbers of ghosts. 
0101ACD4: .....Hm.
0101ACE8: Oh, hush. You're one to talk anyway. We all know you're @going to gouge the crap out of Shou later.
0101AD28: Hmph, it's all good. We'll give you a good thrashing, @then once you're back to your senses, you're gonna@apologize to us with all you've got. Can't wait for it! 
0101ADD0: Ohhhhh, cool! This is always a thrill! What're we gonna @find this time? 
0101B1F8: Lady Yuyuko, c'moooon!
0101B560: Whoa, now THIS is something.
0101BB20: Hm? Marisa, did you say something?
0101C308: We've got this tied-up Futo to deal with too. Maybe we@oughta go back to the village and see what she has to @say first.
0101C5AC: Or my powers of telepathy have finally manifested.
0101C6B8: I have to say, I am rather surprised to see you here. @You were at the Myouren Temple not long ago. Did you@hear about our impostors, perhaps?
0101C72C: ...She sure is excitable. 
0101C954: Hmm...? Whoa!?
0101C99C: Shou, let us give it our all together.
0101D208: Hopefully Futo has some leads that we can follow. @Anyway, we can proceed, or we can go back to the@village. It's all up to you, Reimu. 
0101DBF8: Good timing, actually. How about fixing up this pagoda? @You'll be able to play around and mess with it all you@want in return. 
0101DF5C: Mm, I'm sensing something.
0101DF70: There you are, you spectre! You attempt to assume the @form of Lady Yuyuko? How absurd! I shall deign to rust@my two blades on you! 
0101E088: You already said that.
0101E1EC: This exceeds my expectations... 
0101E274: Gaaaah... I feel like crying now... 
0101E800: ......
0101E8A8: Yikes, now this is something... 
0101E8C8: Mmm... At least now we know where all these vengeful@spirits are coming from. I can't think of any other @place, certainly. 
0101E9A8: Ha ha ha ha! I've got some unwelcome guests, I see! 
0101EAE4: She's using the vengeful spirits to heal herself!?
0101EB24: What just... Did all the vengeful spirits around... 
0101F07C: Ahh, that was so bright...
0101F160: You'd have to go out of the universe to solve something @like this, honestly. At the very least, nothing comes @to mind immediately. As the priest says, this is no @ordinary hole.
0101FA08: (I will slaughter him with my own blade...!)
0101FE90: Mmph! 
0101FED0: Wait, something's fishy. Mamizou, you were claiming that@we were all under control, so you put those puppets @against us... Why didn't you make them antagonistic to@Shou as well? 
01021350: Now now, we've got other things to discuss. ...What was @that flash of light just now? 
01021570: You've sure gotten good at kidnapping people. 
01021750: If you can defeat me, I shall grant you the wonderous @reward of investigating the "Hole to Infinity" to your@hearts content, and letting all of you realize that any @efforts to close it up would be futile. 
01021800: Well y'know, it could be maybe a little dangerous @inside, yeah? So I figured our lovely and tough saint @Tenshi and her celestial peach-enhanced body would be @the best choice to go in first. 
01021CD8: Izzat so? Then you should be able to close it up. 
01021FC8: Oh yeah! Can't make the treasures wait too long! Let's@jump in, Reimu! We gotta grab them before someone else@does! 
010232A0: .........Huh? 
01023DBC: ......
010242F0: Do not worry. Our group has more people that you can@dream of. It would be no problem to find a specialist @who can handle the repairs. 
01024520: In short, we don't know enough to solve this efficiently@right now. We've already come this far. It'd probably @be faster to find the culprit and take out the problem@at its source.
010245EC: ...Are you really going inside this thing, Marisa? That @sounds tough. I'll pass on it myself. 
01025828: I do recall buying all the issues after 13 from Reimu,@though... 
01025908: The hole's still spittin' out the vengeful spirits as @always, huh? Always disgusting no matter how many times @I look at it. 
01025968: .....ahh... 
01025C70: Youmu, Youmu! You're up!
010262D4: I am NOT doing it!
010264DC: So it wasn't you. Then what was that...?
01026578: At least we know that it works. I suppose we should be@okay to go inside the big hole now? Treasures are @waiting for you, Marisa.
01026910: Well then, Marisa. Here's the charm.
01026938: Honestly, it's a real downer to have spent all that time@investigating the huge outpouring of all these ghosts,@then finding out that we can't do anything about it for @now.
010272A8: Non-Neumann Systems. I'm getting used to seeing this@around the place. 
01027380: ...In fact, I think I've heard of this title before...
01027428: It is you, isn't it!? The real Nazrin!? My Nazrin!? 
010274D8: ...Who? 
010274FC: ...Doesn't ring a bell... 
010275E8: Waaaaah! Reimu, you've become a psychic too!? 
010278E0: But you know what? Twiddling my thumbs waiting here is@just no fun. Since you look up for it, how about a@little brawl? 
01027B38: If my eyes weren't playing tricks on me, then I believe @that flash blew all the vengeful spirits away... The@heck was that light...? 
01028688: ...I was expecting myself to be mad, but I'm actually @more relieved that it's still in a salvageable enough @state to be repaired. 
01028CC0: Of course it'd end up in pieces if you lost it in a @place teeming with ferocious monsters like this. If @anything, you should feel lucky that it was sliced@cleanly into two. 
01028D70: Ooh, Hijiri...! 
01029138: I'm getting used to sniffing these out... By the way, @Mamizou? These aren't leftovers fakes that you created, @right?
01029470: But me having telepathy would be good for you. My mind's@voice'll distract you from all these strange noises @that're driving you crazy!
01029534: Bodhi Tree Seal...
01029BA8: (Well, it's going to be my master who owes the favor, @and knowing her, she'll be the only one who'll suffer @from it... so it's none of my business...)
01029D60: You were at fault for losing your pagoda, and you have@acquired a debt due to the required repairs. Let us @continue travel hand-in-hand to repay your debts - and@to maintain your kindness.
0102A468: Don't worry. Reimu isn't going crazy, and Marisa's@powers of telepathy haven't manifested. I can attest to @that. 
0102AB10: I've already taken a quick look at it, in fact. The @important parts are essentially unscathed. All I need @to do is reinforce the damaged parts, which I could do@in my sleep, and it'll be as good as new. 
0102AC30: ...Yes, I did. Those impostors showed up and started@besmirching your reputation even while you were @standing in defiance of the Great Tree... I could not @allow that to go on any further!
0102AD58: Yeah, I definitely heard something. An enemy, perhaps?@Or an auditory illusion?
0102ADA4: A Hijiri impostor... You attacked the human villagers @using her form, didn't you!?
0102AF98: No, you were even worse than this.
0102B194: Marisa, can you hold up for a bit?
0102B37C: T-Thank you so much!
0102B418: ...Er, wait... Hijiri went along with all of you to the @Great Tree, didn't she? Which means...
0102B564: Unruly impostors, prepare yourselves! The holy light of @the pagoda shall beam upon you and bring you to ruin! 
0102B5E4: But then again, that's what I'm here for. 
0102B620: Mm? What's up? ...Wait, whenever you get this way, it's @usually...
0102B660: Yup, exactly. My dowsing rod's reacting to something. 
0102B6A0: You really are kind-hearted. I do get the impression@that your decision-making skills and prudence don't @quite match up to your kindness, but regardless, I am @proud of you. 
0102B758: That is rather strange... I did see the impostors often,@but they would just show up and disappear without doing @anything. 
0102B7E8: Hrrrrm... Looks like it's over here.
0102B858: ...You couldn't have lost your pagoda while escaping the@clutches of our impostors, right? Of course you @wouldn't! Haha, how silly of me!
0102B9D4: How mature of you. A half-full glass and stuff. 
0102BA00: Sounds good, I've always wanted to take a look at this. @All right, done deal! 
0102BAD8: Ohh, aren't you in the mood.
0102BB88: *sigh* As long as you can make it like it was before... @Honestly, that master of mine. I have to give her a @piece of my mind the next time we meet... 
0102BDD8: ......wahh... 
0102BE18: Hahaha, golly me. If it isn't my master. What are you @doing here? 
0102BEE8: Ahh, sorry for butting in at such an emotional moment...@but this whole mess was caused by two of your temple's@youkai, no? Doesn't this indicate some negligence in@your management of the temple and its disciples,@Byakuren? 
0102BF80: Ohh, so the monster tanuki's with you? Did you come to@lose to me again? 
0102C038: (How did that phrase go again... There's no such thing@as a free lunch, was it? ...Especially when it comes to @the kappa.) 
0102C088: ...Wait. Kokoro's technically a tsukumogami, yeah? She@shouldn't be the type to go throwing her masks around @everywhere like this. 
0102C2C8: Thank goodness... Um, may I impose on you to perform the@repairs?
0102C3A0: Sure thing. You Myouren Temple folks have been good to@me anyway, and it'd be good to have you owing me a@favor.
0102C414: What did you just...!?
0102C5A8: ...Not a word, as usual.
0102C814: ....Wait, pieces!?
0102C9A0: That was pretty mean of you, y'know. Not many people@would voice their complaints after seeing such a@touching scene like that. 
0102CA30: Marisa takes the charm and holds it in front of her as@she inches her way into the hole. 
0102CB90: Look, Marisa! We're here at the big hole! Are you @satisfied now?
0102CD58: Whoa, what the...? This is no joke... 
0102CDF8: It feels like a ghost... yet somehow different from all @the other ghosts floating around... 
0102CE78: ................... 
0102CEB4: Woohoo! Thanks a bunch, Reimu.
0102CEF4: (......Nope, still none of my business.)
0102CFB0: As responsive as ever. Shall we get thie started as @usual, then?
0102D2F4: Oh, sure it's working. Don't worry. It's totally being@super-effective right now.
0102D320: You'd think so, yeah... Of course, we do have a @tsukumogami who gets her umbrella stolen then wails and @whines about it. It's hard for us to emphatize. 
0102D7F8: I'm just glad to have my impostor dealt with... I think @I'll be able to sleep a little more soundly tonight.
0102D998: Heheheh. It might feel painful at first, but don't@worry. Just leave everything to the vengeful spirits@from this hole, and you'll soon become one of us. 
0102DA54: Whoa whoa whoa, what's going on!? 
0102DB70: ....Ohhh? 
0102DB80: Well, y'know... 
0102DBE0: It doesn't matter if I win or lose here. This hole's@already wide enough. Nothing can stop it now, probably. 
0102DE48: Ahhhh, that'd be my puppets. I did see you through their@eyes fairly often, now that I think 'bout it. 
0102DEF8: Your voice is still shaking.
0102E128: Blegh. There's a Youmu impostor too.
0102E190: Considering this impostor has taken my form, I kind of@want it to resist the spell I taught to you.
0102E210: Urrrgh! Looks like we have to brace ourselves! Marisa,@we're fighting her one more time, okay!?
0102E2B0: ...Uh, didn't you come to the underground to defeat the @impostors? There've been several of Mamizou's creations @as well as the real impostors on our way to this fourth @undergrounnd floor... How did you manage to get all the @way down here without ever fighting one?
0102E4A8: Welllllll, you'd understand if you saw how Shou was @behavin' at that time... Kahaha...
0102E5C0: Anyway, enough about me. It's not really good for @tsukumogami to be away from their artifacts. We oughta@return these to her as soon as we can.
0102E650: We've thrown everything we have at her and she's already@fully recovered! What can we even do about this!? 
0102E728: Would anyone else really try to go inside this thing...?@Actually, are there even treasures there to begin with? 
0102E800: But there is one main difference between the two holes -@this hole is far, far bigger, and seems to be emitting@an endless amount of vengeful spirits.
0102E8D8: The light from a little while ago had temporarily @cleared the immediate area of ghosts, but the hole has@continued to gush with them, eventually making the@situation the same as before. 
0102E980: There are so many ghosts and vengeful spirits gushing @out... Even I have not seen such a staggering amount in @one place before. 
0102EB20: (...Uh, we did see impostors pretty much right when we@entered the underground, didn't we?)
0102EBEC: ......... 
0102EC08: Mamizou moves her gaze as she speaks. Reimu's group @follows her line of sight towards the the gigantic@presence that no one can ignore... and it seems to be @getting even bigger.
0102ECA0: Ah, my apologies, master. It's nothing. C'mon everyone, @that's enough bullying. Let's move on.
0102ED10: Another mask... Feels kinda similar to the I picked up@before, though the design's different.
0102EE40: ...That's... quite a hole, isn't it...? 
0102EEC0: Indeed, it is a hole. One no different in shape from the@hole that led Reimu's group to the underground. 
0102EF54: ...On the other hand, you're really good at spoiling the@mood, aren't you... 
0102EFE8: Ahhhhhhh! You're right! Now I know why I felt like I'd@seen it before! Yeah, she had tons of masks like@these.
0102F0A8: Eh? Yes it is.
0102F198: ...What do you mean?
0102F1DC: ...Ah...
0102F2C8: So this is what that Taoist was protecting. If only I @stopped her back then...
0102F3D8: But then you all had to stick your noses in. I suppose@the vengeful spirits and impostors from this hole @weren't enough to stop the real thing.
0102F458: With how she's behaving and how Tenshi previously was, I@can't blame Mamizou for thinking that the rest of us@were the same.
0102F4DC: Oooh, even Nazrin's in on it now... 
0102F518: Ehhh? Are you saying this is how I was like? Blegh, how @terrible. 
0102F5A0: Be careful, folks! That Taoist was pretty strong to @begin with, but the sheer amount of ghosts 'round here@are making her even more powerful!
0102F638: But you're still trying to play smug in front of all@these people itching for a fight? Are you that@confident, or just an idiot!? 
0102F704: Ooh, ooooooh... 
0102F810: Who was that!?
0102F8F8: Mmm. I do believe she is very much under control. 
0102FA10: Hmm, well... I'd attack you with my dishes and ghosts @the moment you turn your back. Probably.
0102FA80: Good grief. That sounds pretty much the same as all the @other bored people here who aren't even being @controlled, honestly. 
0102FB40: Futo assumes a praying pose, and suddenly, a massive@surge of ghosts emerge from the large hole and surround @her.
0102FDA4: Surrender? How silly! 
0102FE10: Gah, is she...!?
0102FE40: Another round!? You have to be kidding me!
0102FE70: Though I'd still attack you with my dishes and ghosts as@soon as your turn your back!
0102FEF0: She's damn strong...! 
0102FF30: ......!!!!
0102FF50: Marisa, are you playing dumb? That's obviously Kokoro's @mask. 
0102FFD8: I ain't the slightest bit okay with it, but it's not@like we've got any other choice! Ahh, geeeeez!
01030100: They all disappeared!?
0103013C: What just happened? What was that light...? 
010302E8: !? This light is...!? 
01030330: Ehhh!? What's going on!? This isn't what the@Yamata-no-Orochi said would happen! 
010303B0: Reimu quickly ties up the fainted Futo like it is second@nature to her.
010304D8: And if my eyes weren't playing tricks on me, I'm pretty @sure the light came from this sword. Out you go - @anything to say?
0103067C: It's comine from the Ame-no-Murakumo...!
010306A0: ...Well, where the light came from ain't important. @We've got bigger things to deal with. 
010307C0: Ooh... Seriously, what was that...? 
010308D0: Hmm... I do not think it feasible to physically close a @hole of this size up... Furthermore, it is clear that @this is no ordinary hole. 
01030998: What say you, Yukari? Puppet Futo claimed it was@unsealable, but d'ya think that's true? Any @outta-the-box way to deal with this?
01030A90: To be more exact, it's more like a warp gate linked to a@different world, though I'm not sure of the exact @fundamentals. It's something similar to my gaps.
01030B78: I didn't say they're the same - just similar. I have no @idea what forces or principles causes the link between@the two worlds. There's nothing I can do about this.
01030F58: I can sense a considerable amount of malice and evil@coming from the vengeful spirits within this hole. We'd @best not try to go inside without proper preparations.
01031080: Ehhh...?
010310C4: ...She's completely lost consciousness... 
01031174: ......All right, done!
01031508: I'm pretty curious myself. Not about the treasures, but @about what exactly is inside this hole. 
010315F0: Hrm. Now that I can take a good look, it's actually @funny how stupidly big it is. 
010316F0: ...What exactly are we supposed to do with this?
01033B28: A mysterious light envelops the group. All of a sudden, @the ghosts and vengeful spirits scatter away from them, @and refuse to even get anywhere close.
010342D4: Hm, this is...
01034578: That really is how it is, huh? I guess this is gonna@fall into the usual pattern of "I accidentally dropped@these items while dealing with monsters" again. 
010356E0: You call THAT effective!? And you expect me to dive into@this hole with this piece of paper as my only defense!? @You gotta be kidding me!
01035E58: Don't talk as if you ain't going in.
01036104: ....Hey, everyone... Is this charm even working?
010364F0: Yeah... 
01039228: Ah, for the record, I'm not going to be the first to go @in either. Don't worry, if anything happens, I'll make@sure no one tries to retrieve your remains. Wouldn't@want any additional casualties. 
0103CE40: She's right. It was you who wanted to explore the hole@in the first place, Marisa. 
0103CF70: Marisa sticks her hand holding the charm towards the@rest of the group as she whimpers in a quivering@voice.
0103D068: Hey, see that ghost there? It looks like it's trying to @avoid the charm. So it probably is effective. 
0103D148: This takes a helluva lot more than self-belief! Gaaah, I@don't wanna do this anymore...
0103D19C: ......If no one else wants to speak up, I'll say it...
0103D2A0: Aren't you the one who kicked up a big fuss to come here@to begin with?
0103D320: ......
0103D364: ............
0103D3A0: ..................! 
0103D3E8: ...Ehh? What is this...?
0103D4E8: ...Yeah, see? It's just as I told you. Any charm that I @tuned would definitely work just fine.
0103D570: Ahh, fine. Crap, I was scared outta my mind. Geez, you@stupid charm... If you're gonna work, at least work @faster... 
0103D7E8: Fine, let's get going. We have no idea what lies ahead, @so get yourself prepared and psyche yourself up for @this. 
0103DA50: Yeaaaaah! 
0109E180: (...Which means while my master was trying to play it @cool, she actually pretty much lost the pagoda from the @very beginning...)
0109EA80: As Marisa said, the big hole is no different from @before. The flow of ghosts emerging from within seems @endless.
010A1D58: Oooh... You're right. It's all my fault. I can't let@Hijiri see how disgraceful I was... I have to at least@repair it before she figures out what happened...!
010A1E00: My. Maybe I'll gain enlightenment if I put it under my@table cushion.
010A2218: They aren't. You folks already beat all of my ones. @These are the "true" fakes. 
010A22E0: What about using barriers? Like, the ones you and Reimu @can make? Can't you just make one as a temporarily lid@over this hole? 
010A2398: We'd have to get that many experts to create the barrier@and keep it going, then another group to protect 'em... @I don't think it's too realistic. 
010F5868: My puppets were used for scouts and to slow down Reimu's@group, so they'd have no reason to deal with you. Guess @that means you were lucky enough not to meet any of the @"real" fakes. 
010F5928: Reimu and Marisa stop in their tracks with a grimace. @Even their usually boisterous group of humans and @youkai either let out yelps of surprise, or are simply@stunned into silence at the sight before them.
010F5A00: If we take the size of this hole and the strength of the@vengeful spirits it emits into consideration... I @believe it would take a very long time for the best of@Gensokyo's experts to erect a servicable barrier to @cover it... 
010F5AB0: And besides, according to Futo, this hole will only get @bigger. The speed of its expansion may outpace the time @we need to erect a barrier of the required size.
010F5B68: *sigh* Whatta pain. Anyway, I kinda feel we should be @looking into this hole and seeing how it works... but @there's also another hole leading further down the@tree, huh? This's a conundrum.
