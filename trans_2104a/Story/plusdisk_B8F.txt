00FEA850: A nice and clean slice. That was pretty awesome, Youmu! 
00FEA9C4: Oh yeah, we're supposed to beat them up. I forgot.
00FFAD1C: Ehh? You serious? 
00FFFDD4: Haaaaaaaaaah! Bring it on, impostors! 
00FFFE78: ......Heads up. That's my impostor. 
01007698: Oh, my impostor's here as well. I'm a little relieved,@actually - it took so long to find, I almost thought@that we overlooked it.
01009880: Victory!
01012584: ...It's nothing. Probably just my imagination. Let's@go. 
01014658: This is what society calls a "flag," I believe... 
0101B14C: Oh well, I'll take over this time. The shinigami's@scythe should deal with this no problem.
0101C53C: ...See, it really was a product of your own mind. 
0101CA2C: Ah, your impostor looks kinda cool. She seems to have it@together at least.
0101DB74: Blegh. Hey, impostor! This is your fault for giving @others the wrong impression!
0101DD6C: ......Milady, have you heard of the "mirror test?"
0101E0A0: Ehh, really? Aren't I cool and reliable too?
0101E298: W-Wait! Why are the two of you playing around with your @impostors!? 
0101E3E4: D-Dahaha! ...That was just a practice swing. This one's @the real thing! 
0101E414: Ahh... Honestly, these children have no sense of danger @at all... 
01020684: Youmu, ivy. 
01026C20: ...Hurry up.
01027BB4: ...She doesn't look like she's in the best of moods.
01028EE8: ......Hmph! 
0102B07C: .........Huh? It's still there. 
0102B15C: .....E-Ehhh...? It's not working. 
0102B9A4: You've got a pretty sharp tongue, Sakuya. 
0102BB30: (...Does this mean... my skills are actually...?) 
0102C974: Mirror... test? What's that?
0102E5B0: What's the matter, Reimu? 
01034158: Eh? T-That's a sudden request... which is nothing out of@the ordinary, admittedly. All right, I'll let you give@it a try. Here's the sword - be careful.
01039DA8: It's just you.
0103A0E0: Ahh, that hopeless feeling of trying to protect everyone@and making a stand, but being controlled by a greater @power instead... What a tragic heroine I am! And@totally the main character too! 
0103B74C: All right! Let's go, impostors! 
0103CC40: I know everyone's already said as much, but having@someone with your exact appearance around feels...@somehow unsettling. 
0103D254: W-When did she... 
0103D758: A mysterious object blocks the path. It does not seem @possible to break it by force. It seems to be related @somehow to the other objects in the area... 
0103DA1C: ......Hopefully that's the case...
0103DB4C: Eh? Why?
0103DDA8: ......Hyah! 
0103DE60: Erk.
0103DE70: And letting you be would lead to sleepless nights for @me. Prepare yourselves! 
0103DFFC: Dual-wielding beautiful swordswoman!
0103E108: E-Eheheh... Nah, it was nothing. Anyway, let's keep @going!
0103E514: Hey, doesn't this impostor look very grim?
0103F910: Ahhhhhhh! 
0103F9B8: And isn't it glaring daggers at me? What, it wants a@piece of me?
01040B64: My, whatever could you mean? Both ways, we have more@exploration to do, so do get moving.
01040B9C: Ahh, you do look kinda cool at that angle.
010411BC: Eh? I already cut it. 
01046C08: Hey you, hand that sword over! The dual-wielding@beautiful swordswoman will return from the ashes once @more! 
01048208: ...H-hmph, I guess there's no other choice! I'll just @have to cut it down!
010486D0: Though I guess you're okay with it, Rin? You're a kasha @youkai and all. 
0104C480: Hm, Reimu.
0104C4AC: The usual ivy.
0104C4E8: Youmu! Youmu, you're up!
0104C5B4: Youmu! Yoooooumuuuuu! 
0104C5EC: ............... 
0104C7C8: ......Hyah! 
0104C860: .....Reimu, Marisa. Is it just me, or am I no more than @a grasscutter in your eyes? 
0104C8D8: C'mon, shinigami. 
0104C910: Err, surely you know why? She's trained herself in the@way of the sword only to be called upon whenever@there's grass to be cut... That'd put anyone in a bad @mood. 
0104CA40: You've got to be kidding me. Is this ivy really that@tough?
0104CA78: I had no idea... Youmu's been slicing through it like @it's just another blade of grass... 
0104CADC: Ooooh.
0104CBC4: Yep yep. I see you in a new light now!
0104CBF0: It's heeeeeeereeee!!
0104CC38: My impostor just popped out of nowhere! 
0104CC70: ......Heheh. With all the work that the two of them push@onto her, I'm glad I could give her something to smile@about sometimes.
0104CCD0: That impostor's even carrying my Sword of Hisou.
0104CE20: N-Nope, you're wrong. I was under control from the start@till the end. 
0104CE80: (Trying to divert attention, I see...)
0104CF18: There...! 
0104CF64: T-That Sword of Hisou...! It disappeared along with the @impostor! 
0104CFA0: .........So that silliness at the lower floors of the @tree truly came from your mind, I see.
0104CFF8: You really were going for that dual-wielding-whatever @thing?
0104D070: Oh my. Our impostors have finally appeared. 
0104D0AC: Hmm...
0104D13C: (...I'm pretty sure she has the same look that you@always have, actually...) 
0104D17C: True enough! If something annoys you, just crush it to@pieces! 
0104D1C4: ......!!
0104D238: Oh, pay it no mind. Let's hurry back and have some@tomato juice. 
0104D290: Wow, there really are so many ghosts at the @undergruond.
0104D338: ......Whoa!?
0104D3C0: ...In any case, they look like they are up for a battle.@Let's waste no time and defeat them quickly.
0104D438: Wah, and there's my impostor too! 
0104D480: It certainly was for the best to let us handle our own@impostors - we know ourselves the best, after all. It @wasn't pleasant to face such a spitting image, though.
0104D4F8: Yeesh, my impostor really had the angriest look on its@face. What was its problem, really? Well, at least it's @nothing to worry about anymore. 
0104D564: .....*smugface* 
0104D598: Yes, let's. I'm sure it'll taste even better after@beating up something I disliked.
0104D630: ........Doesn't quite work for me, sorry. 
0104D6EC: ............!!
0104D7C8: *smug*
0104D818: Don't look over here! You'll ruin it! C'mon, look that@way and try again.
0104D8B4: ...Honestly, these children...
0104D8E0: You seem to be able to operate the mysterious object@correctly now. The path ahead opens.
0104D95C: Geez, stop calling for me the instant you see ivy @already.
0104D9B0: Yoooumuuu.
0104D9EC: Wait! Let me try a swing first. 
0104DA1C: Mmhmm. Thank you! 
0104DAEC: ......Ehhhh!? 
0104DB20: ...Whoa, she's right. The ivy's sliced clean in half! 
0104DB60: My surname's not ivy! Fine, fine... Stand back while I@cut it down.
0104DBDC: W-What just happened to my place in this group...?
0104DC48: Ah, Lady Yuyuko. What's the matter? You should stand@back a little - I'm about to cut down some ivy. 
0104DD88: Mysterious ivy blocks the way. It would be difficult to @cut through it. 
