00F77C34: Ah. 
00FB4100: Heeelp meee...... 
00FC9A48: ... 
00FCF004: What, then you're just holding back?
00FCF1E0: I wanna smack her...
00FCF2F8: Ah, gotcha. It's a Philosopher's Stone. 
00FCFDB8: ...Is it just me, or did that laugh we just heard seem@like it came from someone really dumb?
00FD0F0C: ...It's getting to the point where I'm seriously@considering allowing it.
00FD1068: Ah, I see, that's what ya want? That ain't a problem if @it's just that. 
00FD11AC: Reimu, did you hear something just now? 
00FD15A0: Oh, that's Patchouli's Philosopher's Stone. 
00FD3430: ...And she's on the floor.
00FD34B8: Are you pulling a Malroth?
00FD34C8: Ah, how did you guys know about that? 
00FD358C: Well then, please treat us well!
00FD71D0: Hm? 
00FD8BB0: Wahahaha! Alright, time to head for the "Heavens!" Go @for it, Tenshi!!
00FD936C: You brute~. *whimper* I want to drink sake~~. 
00FD9790: Even if you say "What can we do about that thing," we @don't have any other choice but to climb up and chase @after it, do we.
00FDA1BC: Humm, what can we do about that thing...
00FDA9DC: Wahahahahahaha! 
00FDA9F8: Hm. There's a bit of wind blowing around here.
00FDAB24: ...Was that you, Marisa?
00FDB154: Yeah. 
00FDC448: ......? 
00FE17D0: Yeaaah. 
00FE3A54: ...... Well, you folks certainly don't get@discouraged...
00FE450C: Guess we have to... 
00FE4940: Hrng... 
00FE4C6C: Hey. Stop being idiots and let's bring it back. 
00FE5000: And, what about you Okuu? Did you found the Coral of@Silence?
00FE58C4: Hmm... Sorry, I really don't know...
00FE59F8: It's pretty big though. It's hard to carry. 
00FE62D0: Yeesh, it's stupid heavy. Can we cut it in half to make @it easier to bring back?
00FE6FE4: Oi, there's some girl muttering something farcical@here... 
00FE7404: I want a piece too. Cut it into three pieces. One for @me, one for you, and one for Patchy.
00FE7D48: Ah. I heard that. Hm? That voice just now, could it be@Utsuho? 
00FE7DFC: And there's an aquatic monster beside her. It's the same@situation as Orin.
00FE7E6C: Ah, well, I'll go with you~. Just like Orin.
00FE8224: She's an oni, and you only have the strength of a human,@I'm pretty sure she won't feel a thing even if you hit@her.
00FE87B4: ...Iku? That messenger of the Dragon Palace is coming?@Here? 
00FE89CC: Alright, we've got a Philosopher's Stone. 
00FE89E4: With it's color and the situation around here, can we @assume that this thing's attribute is wind? 
00FE8E5C: I really hate people who only do as they please! In the @end, you're just a simple thief, aren't you!
00FE9350: Well, celestials have never been pushovers, but they@shouldn't be as powerful as that. 
00FE963C: The sword's voice? Stop saying stupid things and give it@back already. 
00FE96C0: Haaa, I have no choice it seems. Huuuuuum...
00FE9B9C: ......If that's the case, then that means... That was @the Ame-no-Murakumo's power?
00FEA4C4: *whimper* No more sake~. I want some "HQ Sake - De~er." 
00FEA758: "You guys really did me in" isn't what you should be@saying... You're the one who ran head first into us so@suddenly, then fired a Spell Card right off the bat.
00FEA8B8: Yea. For the time being, let's not get discouraged from @chasing after this. Tenshi can laugh now, but we'll @make sure she won't be laughing at the end. 
00FEAA78: Ehhh, Suika's gourd is actually pretty darn heavy, so I @don't think that's possible... And she absolutely won't @let go of her gourd.
00FEAADC: Looks a li'l too aggressive to me to bring it home and@hang it up someplace. Here it comes!
00FEB6C0: Hey, isn't that the Ame-no-Murakumo!? 
00FEC068: What, you want a piece of me? I'm a celestial that holds@the power of Heaven, wielding a divine sword once @presented to the gods. Do you understand what that@means?
00FED4E8: *Phew* I'm alive, I'm alive.
00FED530: Huh, so that's it.
00FED640: Hm? Ah, is it a gate that Ran made that you don't really@understand? 
00FEDCB8: Another one... What is the world is that girl trying to @pull, placing gates that we can't understand all over @the Great Tree? 
00FEEE18: Uwaah...... 
00FEEF70: ...Is that girl stupid or what? 
00FEFB00: The brains of fairies and celestials work in the same @way...
00FEFE5C: My, you have quite the mouth on you, don't you? @Hehehe... 
00FEFEAC: Ah, Orin. 
00FF0064: Heya! It's because of me~.
00FF02E4: This sword chose me! I will hear no objections! 
00FF0778: Good grief, that girl is strong.
00FF1490: Can I cut it in smaller pieces? 
00FF1BAC: ......Let's leave her alone.
00FF2004: Well, I'm hearing a witch running her mouth off next to @me. 
00FF2058: Ow ow ow... You guys really did me in, as expected! 
00FF2468: That's fine, it's the feeling it gives that matters!
00FF29D8: Gah.
00FF31D0: What's this all 'bout anyway? You always keep goin' on@and on about your firepower. What happened to that@firepower you're so proud of against that?
00FF3520: My my, let's go together then. It took quite a while, @but we'll finally be able to come to Satori's aid in@just a bit. 
00FF3A20: Well, I'm beat. That girl was really powerful. Was she@always so strong? 
00FF4660: Fufufu, I still have much more to give! Did you think @you would have enough strength to go against my new @power, befitting of the heroine that I am?
00FF4F88: Ahh, if Lady Satori and Orin were here, I could focus @entirely on letting loose with my firepower... But it's @kinda tough fighting alone here.
00FF5420: With the amount of money we've been spending, it's damn @well time you get motivated and repay the favor, ain't@that right, Suika!? 
00FF5908: Well, the surroundings look ok. If we don't understand@them, the best we can do is not to step on them. Chen,@make sure you tell us about their location so we can@avoid them. 
00FF5B88: Really really, it's the real thing with a capital "R."@Really now, why was such a half-hearted man like you be @hoarding that divine sword? So wasteful.
00FF628C: ......... 
00FF6310: This sword has been talking to me every night. "I want@you to wield me, and conquer the whole world!" It @boomed those words to me all the time! Of course, it's@only natural for me to get into a heroine-like@situation!
00FF64B8: It came from that way.
00FF653C: Heeeelp meeee.
00FF6630: Heeeelp meeee. Mmmmm... 
00FF6660: Okuu, you came to find an ingredient for Satori's @medicine, is that right? Did you find it there? 
00FF6700: I was in the same situation as you, the young misses@here helped me out by beating down a monster. Really, @non-dead humans can be really helpful at times. 
00FF67B8: No, no, help meeee. 
00FF67F0: So Orin, did you find it too? Huuuh, what was it again, @the name of that meal-like thing. 
00FF6878: The Seaweed of Serenity thing? If that's what you're@talking about then... Ta-dah! It's right here!
00FF69EC: Somehow. Heave-ho.
00FF6A00: *whimper* No more sake. 
00FF6A60: So with that, you have all the ingredients for the@medicine? We should go back to Satori to give it to her @then. 
00FF6B80: Ah! Reimu, it's here again! 
00FF6CB4: Fuel~~~. I want fuel, with the name "HQ Sake - De~er."
00FF6DB0: ...Ah, it's that. This is the source of the wind. 
00FF6DD0: The Heavens are calling for me! The Heavens are mine! @Mine! With that said, I will be making my way to the@top of this Great Tree. 
00FF6E40: Her entrance gives off the same vibe as another girl we @met earlier, doesn't it...
00FF6ED0: Could that bothersome looking girl be the one that Iku@was talking about catching...?
00FF6F98: Eww. It leaked that quickly... The dual wielding@beautiful swordswoman is already in a big pinch!
00FF7028: You've been making a racket for a while... Dual @wielding, really? Since when did you start carrying two @swords? You normally only had one, isn't that right?
00FF71C4: HQ Sake - De~er.
00FF7200: Eh, really now? So it's the real Ame-no-Murakumo? Why @are you carrying it, Tenshi?
00FF7278: Stupid things? How rude. The celestial heroine had just @attained the Ame-no-Murakumo, the divine sword that @makes the Heavens quiver with fear! And right at that @moment, a Great Tree that pierces the sky appears! Just @thinking about it gets me excited!
00FF7380: And since it was such a waste, I took it from the @warehouse at your place. Ehehe, you should be grateful! 
00FF7430: So you really stole the Ame-no-Murakumo from Rinnosuke's@place. Really now, is this the new fad for celestials @nowadays? 
00FF7500: Anyway, so long! If you want to get back the@Ame-no-Murakumo, the least you can do is to get @stronger to match up to me! Bye bye!
00FF75F8: Aah, no, noo... An easy victory for me against you would@be truly dull... Besides, right now, I am fairly busy,@I have to climb this tree.
00FF7688: "An easy victory for me" was it? Whatever your reasons@and goals may be, you think we're gonna let one of our@own goals get away so easily, hmm?
00FF77B8: Why do you always point your finger at me when things @like that happen? 
00FF77F4: Here comes the dual wielding beautiful swordswoman! 
00FF7810: Right now, you guys are aiming to go even higher and@beat up even more monsters, right? If so, can I mix in@with your group? I won't be a burden. 
00FF78C8: No you see, I heard that all the strong guys inside this@Great Tree were all getting destroyed, so it was@already too late for me to go against them. 
00FF7940: Nope, not at all. And it ain't gonna matter in a minute,@after we've settled this situation. 
00FF7988: Ah, that's right. While I'm at it, I should go mess @around with that gap youkai and ghost pair. Those two @should make for a bit more enjoyable of a battle. 
00FF7A80: Who cares about your situation! One of our goals was to @retrieve the Ame-no-Murakumo. 
00FF7B44: She's already here. She's been looking everywhere for @you.
00FF7B80: That's the Sword of Hisou in her right hand and...
00FF7CA0: The sword on her left hand is... Eh, what?
00FF7EE4: ...... Alright then, I'll play with you for a bit!
00FF7F68: Guh...... 
00FF81E0: You keep demanding for even more expensive sake, are you@even aware of how much it actually costs? You don't @even know how much the last one was worth.
00FF8398: Wow, it's illuminating the surroundings with a beautiful@blue light. Should we bring it back?
00FF85D8: Hey, here's your bloody "HQ Sake - Deer!" It was even @more expensive than the last one! 
00FF8620: The Boar one was 3000, and this one is even worse,@12000! Really, you're like a bad car when it comes to @fuel consumption! 
00FF87E0: ...She drank the entire sake in the time you were all up@and mightily ranting at her.
00FF887C: One more down.
00FF8988: Do that and Patchy'll have your hide... 
00FF8A4C: ......Haa.
00FF8A98: So, I was looking for some strong opponents to pick a @fight with, when all of a sudden, the shrine maiden and @magician just happened to pop up. I was so shocked, I @fired off my favorite Knockout in Three Steps.
00FF8B30: ... Ah, yea sure, ya really are the kind of person who@loves to be in the middle of an incident like that... 
00FF8BD0: That's right, it's been a while since I had so much fun @fighting with all my strength. And so, can I ask for a@favor?
00FF8C80: There's some girl muttering something farcial again...
00FF8CB0: If that's what you want, then that's fine. We got even@with you just now with that brawl anyway. 
00FF8E48: ...... Eh? Suika? 
00FF8E90: ...... Next time we meet her, please let me throw a @Spell Card at her face. 
00FF9288: Sure thing. Leave it to me! 
0103206C: It doesn't have those kind of abilities at all. 
01032A20: This girl makes me angry... 
01043150: You jerk, you went and drank that expensive sake we @bought like it was nothing! I know, Reimu, let's just @drag her along with us as she is. 
010A05E0: Ah, that's true. How curious, I thought there wasn't any@wind around here. 
010A0670: Alright, I've not got anything against you, but I'm @dealing with you quickly and bringing you back with me! 
010D8EE8: You've got good eyes, bro! This sword is hiding an@amazing power! It's a golden sword that tears through @space at every swing and that can manipulate and@overtake gods at will! Its name is Ame-no-Murakumo!!
010D8F78: Hah, the shopkeeper may have some power to know its name@and how it's used, but he still knew nothing. Instead @of using your power, did you try sitting down and @actually try listening to its words, shopkeeper?
010D9008: You've always gone on and on about your onanistic @rubbish so much that I've become numb to it, but now@that you actually have something pretty amazing to back @up your talk, I suppose it's not something we can leave @alone.
010D9098: Agreed. It's the first I've seen of both the sword and@the celestial. Yet even I can feel intensely divine @aura the Ame-no-Murakumo is emitting, and the mental@instability of the celestial who's wielding it. This is @something we have to deal with. 
