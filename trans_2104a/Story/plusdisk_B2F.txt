00F73AC4: Oooh... 
00FD44B4: Why, you! That's my line! 
00FDA330: Hmm, totally ignored. 
00FE1A58: What's up, Marisa?
00FE2EDC: Why're you standing there like an idiot?
00FE34A4: Good grief... She's got a lot to answer for once we find@her.
00FF8FDC: Okay! Prepare yourself, my impostor!
00FF90E4: I was just wondering why there isn't any leaf.
00FFCA0C: Oh, hush. I'm just making sure. 
01000158: Why're you starin' at that leaf so intently, Reimu? @Finally awakened your collector's spirit? 
010004F8: Now to lay the barrier Yuyuko taught me that prevents it@from escaping... then retrieve the scarlet gold @core... 
010034B0: That's so mean! You all get just as loud when you get @angry at me for making a fuss anyway! 
01003814: It looks similar to the other book we picked up @recently. Let's see...
0100457C: Hm, there's no leaf. Which means... 
01004E40: This wasn't one of the monster tanuki's impostors.
01006364: Whoa, Reimu! That's me! I've got my own doppelganger@now!
01006E44: It would be far too strange otherwise, yeah...
0100785C: Oooh... Farewell, doppelganger Kogasa. I won't forget @you ever. 
01007C14: Yeah. We're seeing them in sequence on the ground. That @must mean someone's been losing them. 
010091E0: Hrrmph! Didn't work after all. Still, with the numbers@you have, I ain't gonna think about sticking my neck@unprepared into battle. 
01009410: Why are you so happy about this? Aren't you normally@supposed to feel disgusted? 
01009500: No, idiot. I found this leaf with the dark fragment...
010096C0: Geez, more of them? Hey, Hina and Parsee! You're around,@right? So these are impostors?
01009B5C: You can't tell they're impostors without confirming @first?
0100AB14: My master's pagoda... And isn't it broken?
0100AB54: Mm? Did you say something?
0100B5F0: Time for more Kourin-taught trivia! There're a whole@lotta species of valley lilies, but the most dangerous@ones have poison that's water-soluble. Gotta be careful @when raising 'em in jars and stuff. 
0100EBFC: What's that golden moustached sword up to nowadays@anyway? 
0100F050: Furthermore, a quiet Kogasa is especially weird. It is@giving me the creeps. 
0100FDD4: Ahh, that thing? It's not up to much. 
010114B4: Did someone call for me?
01011914: ...A leaf?
010137B8: Ahhhhhhh, I get it. 
01016E38: Non-von Neumann Systems again. Issue 10 this time...
01019998: a Valley Lily Seal, huh...
01019B20: Uh, you don't have to take it out now.
01019CB8: ...Eeep... This got pretty big. 
01019EE0: Hmm...
0101A008: My, there's someone over there. That sihoulette looks @familiar... 
0101A044: All right then. Treasure get! 
0101A2F4: ...Hm? There's something else with the core...
0101A62C: ....Mmm?
0101A654: ...Y'know, this's definitely... 
0101AA8C: You can find 'em all over the place.
0101AAC0: Sure have it tough, don't ya? 
0101AF18: A leaf, you say?
0101BE30: ......Let's move on.
0101CDDC: My, that actually does sound like something a witch @would know. 
0101D868: In fact, isn't this the equipment bag we all use to put @items we found in the Great Tree? What kind of name did @you give to it? 
010200D0: That doesn't answer my question at all... Anyway, if the@impostors are made of fragments of blackened scalet @gold, then maybe the sword is involved somehow. 
01020C20: (Though our antiques dealer here isn't that much@different either, honestly...)
010214A0: Reimu, something just popped into my mind when I saw the@blackened scarlet gold... 
01021B28: What's the matter, Marisa?
01022538: She's a pretty famous elder youkai. It's a bit worrying @how she can create impostors that can give us such a@tough fight, though.
01023054: I didn't exactly mean that it's physically related... 
010238F4: And that settles that.
010259F8: That tanuki can make such strong impostors so quickly...@How much power does she have? Jealous... So jealous...
01027128: Given that it's Marisa, I doubt the owner will be seeing@it back anytime soon... 
01027190: ......Ah. 
010271DC: Look over there. There's another book on the ground.
0102732C: The one we picked up earlier was the 9th, right?
0102735C: Yeah. I'll grab this for now. 
010273F0: I already feel sorry for our poor unknown book owner. 
010274A8: No. Let's move on.
01027740: Ohh! If your dowsing rod thinks it's treasure too, then @it really must be.
01027C30: Marisa, wait. Based on what she said, I'm pretty sure @it's the real Mamizou, but... 
01027C88: Oh right, she's got the ability to transform things. If @that's the case, that makes a lot of things fall into @place. She doesn't seem to have any intention of hiding @it either...
01028740: Ain't many fragments left... Definitely not enough to @make the amount of wooden dolls needed to match up with @all of these folks. They ain't easy to make either... 
01028B5C: Ahhhh... Master really is SUCH a... 
01028BC0: My, can I take that as a confession of guilt? Honestly, @I thought the Ame-no-Murakumo was sticking its nose @where it shouldn't again, but this whole situation just @got a lot less serious. 
010290A8: Of course we'd get angry. Then we'd beat you up until @you apologize. Then beat you up again after you start @crying. 
010291B8: After the impostor Kogasa was exorcised along with the@fog, a single leaf falls to the ground along with the @blackened fragment. 
010293D0: I doubt my prattlin' would do much, but I'll say this @much. Get in my way any more than this and you're in@for a whole world of hurt.
01029698: So with nothing else we could do, we added it to the@Scarlet Devil Mansion's Rare Item Collection. See?
01029910: But I don't think it looks chipped or broken in any @place...
010299C0: Well, whatver. Thinking too much won't get anything @done. Let's keep moving.
01029A40: ...Ahh, I suppose I should at least try to engage it. @Ms. Faker, what are you all exactly? Do you have@anything to say?
01029AAC: Oh, hush. It's fine that way. 
01029B40: I figured. Though now that we've found it, I think we @should just shut up and smash it already. 
01029CA0: Geez, all of you shaddup already! Our guest here's@waiting so patiently for us, y'see? Let's get started @already.
01029DF0: Honestly, you folks came with a much bigger group that@I'd figured.
01029E30: Hmhmhm, it's just as I read. Guess this is the "fall" it@referred to. Should assume that it's not just that@sword and these folks, but a whole bunch that've been @taken for a ride... 
01029F30: Goodness me, what a failure. I didn't figure an impostor@ghost could be dealt with so easily.
01029F88: Mamizou, you're using scarlet gold as a core to turn the@ghosts around here into powerful impostors, huh?
0102A088: It'd be a real pain if we let her make any more @impostors. Let's chase her down and get this all@settled quick.
0102A108: Don't you worry, I'll be smart and retreat for now. @Hrmph, just when things are getting even worse... 
0102A198: True enough. So for now, we keep chasin' that obvious @mid-boss while investigating about the underground and@all these ghosts. 
0102A230: Waaaah! You're a devil! 
0102A2C8: Harrumph. I've shown you all my tricks already. Who else@d'you think could pull off a spell like that with just@the ingredients from around here? 
0102A3A8: That's not what I meant. When we beat up the fake @Kogasa, she dropped a leaf along with the blackened @scarlet gold fragment. The leaf was probably necessary@for Mamizou to create her fakes.
0102A4E8: Waaaah! A real devil popped up!?
0102A550: She really doesn't stop...
0102A570: I keep telling her not to repeat the same mistake over@and over again... And as a wondeful bonus, she's even @managed to break it cleanly into two pieces...
0102A618: But if this pagoda part's here, that means Shou must be @somewhere around. 
0102A708: But at least we now know Mamizou's making all these @impostors. Though we don't know her motive yet. 
0102A7AC: Mmph, just when I finally perfected the spell...
0102A7DC: ...Oh, who's there? 
0102A810: Oh hey, it's Mamizou. 
0102A840: The whole appearance of the Great Tree's underground and@the rampant amount of ghosts is most likely unrelated @to that monster tanuki. 
0102A990: The Ame-no-Murakumo...
0102AA60: Somehow, I get the feeling that your methods aren't @limited to impostors alone... 
0102AAB8: As if you have a power like that. ...My rod is really @reacting, though. I wonder... 
0102AC10: Yes yes, we're here.
0102ACF8: ...Can't find any here... Well, whatever. We already@have the dark fragment. Don't worry about it and keep @those legs moving.
0102AD94: ...!
0102ADE0: Mmph. I wanted a much bigger army on my side, but @defeating them impostors and getting those blackened@scarlet gold was no walk in the park, I tell you... 
0102AEB8: Well, whether they're your impostors or not, what we do @doesn't change. We just beat them up as soon as we see@them. 
0102AF6C: Mmmm...?
0102AFD0: Hmm, that is strange. I do recall asking Shou to look @after the temple while Nazrin and I were engaged with @this incident.
0102B270: Ugh... Don't make it sound like you're clean yourself,@Keine. You're one of us, so you're just as complicit. 
0102B300: I just saw something give off a shimmer. My dowsing @broom's readings are off the scale. There's treasure@round these parts.
0102B4A0: Ahh, right. Pagodas are supposed to look like buildings,@yeah? This looks like nothing more than a roof, though. 
0102C490: Does it really matter...? Anyway, let's take this seal@with us. I'm sure there's a place it can be used. 
0102D3A8: Hey, I ain't a witch. Call me a magician. 
0109DFA0: At least we finally have an obvious culprit to chase@down. Having your target dangling in front of your nose @makes anyone more motivated too.
010A1CA0: We already know there's no point talking, so let's clean@this up quick!
010ED4A0: We brought it back to the shrine, and put up a little @seal to keep it from wandering around. It hasn't moved@or spoken since.
