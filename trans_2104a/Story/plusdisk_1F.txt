0103D6D0: But there is one thing everyone can see - that the pit@leads to another floor, that looks like part of a @forest. 
0103EB58: ...It's a pit.
01041750: Were you now? I prefer it simple myself. Less annoying@that way. 
01044090: Erm, I would like to request for a more relaxing@adventure, if I may.
01048030: There lies a gigantic pit that was not there during @their previous excursions. It is big enough to fit an @entire barn through it, and the stagnant unnatural aura @and poor lighting means that no one can see the bottom. 
0104BBB8: That would be nice, wouldn't it? Well, nothing gained is@ventured or however that saying goes. Let's get going.
01051B00: Who would've thought that such a massive pit would be @right near the entrance? It certainly wasn't there@before - it must've happened recently, like the @Impostor Incident. Hard to believe the two are@unrelated.
01051B78: But ain't this way too obvious? I was hoping for an @adventure full of tricks and traps, slowly buildin' up@to an awesome climax! 
01051C18: And besides, can't you feel it? The aura emnating from@that pit is of an entirely different level than this@floor we're at now. The enemies there might be even @stronger than the ones on the upper floors. 
01051CC8: Looks like we're getting that adventure full of enough@tricks and traps to last you a lifetime. Good for you,@Marisa. 
01051E9C: ...That it is.
01051ED4: ...Hmm. I know the lower floors were supposed to be @suspicious, but...
