00F76D3C: Symbol of Abundance and Harvest
01056260: Flying Mysterious Shrine Maiden
0105627C: Ordinary Black Magician
01056290: Unmoving Used Goods Seller
010562A4: Rainbow-Colored Puppeteer
010562B8: Petty Patrol Tengu
010562CC: Jealousy Beneath the Earth's Crust
010562E0: A Bug of Light Wriggling in the Dark
010562F4: Youkai of the Dusk
01056304: One-Armed, Horned Hermit
01056318: Half-Human Half-Phantom Gardener
0105632C: The Cheery Forgotten Umbrella
0105633C: Small Fairy of the Ice
01056350: Girl of Hisouten
01056364: Little Dowser General
01056380: Shikigami of the Shikigami of the Youkai
01056390: Dropped and Low-Plateaued Shinigami
010563A4: Half Beast of Knowledge and History
010563B8: Traditional Reporter of Fantasy
010563CC: Aquatic Engineer
010563DC: Sinner of Eternity and the Instantaneous
010563F0: Human Form of Hourai
01056400: Nagashi-bina of the Hidden God
01056410: Girl Even the Vengeful Spirits Fear
01056428: Hell's Traffic Accident
01056438: Scorching, Troublesome Divine Flame
0105644C: Rumored Unnatural Phenomenon
01056460: Night Sparrow Youkai
01056470: Chinese Girl
0105647C: Modern Living God
01056490: Beautiful Scarlet Cloth
010564A0: Brain of the Moon
010564AC: Lunatic Lunar Rabbit
010564BC: Vampire of the Heavy Fog
010564CC: Perfect and Elegant Servant
010564E0: Independent and Inflexible God
010564F4: Substantially Titular God
01056508: Scheming Nine-Tailed Fox
01056518: Tiny Pandemonium
0105652C: Diabolic Wave
0105653C: Ghost Girl in the Netherworld Tower
01056554: Youkai that Lurks Within the Boundary
01056568: Stargazing Student
01056578: Apprentice Barrier Magician?
01056594: Supreme Judge of Hell
010565A8: The Sealed Great Magician
010565C4: Flower Master of the Four Seasons
010565E0: Disciple of Bishamonten
010565F4: Futatsuiwa of Sado
01056604: Shikaisen from Ancient Japan
01056618: Almighty Taoist who Controls the Cosmos
01056630: Expressive Poker Face
01056650: Imaginary Personality Holder
01056668: Ninth Generation Savant
