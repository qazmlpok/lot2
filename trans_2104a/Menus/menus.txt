00F704F0: Expend MP to cast a spell.
00F70518: Swap one character with another.
00F7053C: Focus your mind to recover a little MP.
00F70568: Run away from battle. Will always succeed, but will cost a lot of TP.
00F705C4: Cannot run from event battles!
00F705F8: Run away from battle. May fail, but further attempts increase success rate.
00F7066C: Choose a target.
00F7068C: Choose a spell card.
00F706A8: Choose the spell card's target.
00F706BC: Choose characters to swap.
00F706EC: ATK: %d
00F706F8: DEF: %d
00F70704: MAG: %d
00F70710: MND: %d
00F7071C: SPD: %d
00F70728: MaxHP: %d
00F70738: HP: %d
00F70748: View ally/enemy information.
00F70764: Reset party status to before battle and re-fight.
00F7079C: End battle and return to Gensokyo.
00F707C0: Lv%d
00F707C8: PSN %d
00F707D4: PAR %d
00F707E0: HVY %d
00F707EC: TRR %d
00F707F8: SIL %d
00F70804: ATK +%d%%
00F70814: DEF +%d%%
00F70824: MAG +%d%%
00F70834: MND +%d%%
00F70844: SPD +%d%%
00F70854: ACC +%d%%
00F70864: EVA +%d%%
00F70874: ATK %d%%
00F70884: DEF %d%%
00F70894: MAG %d%%
00F708A4: MND %d%%
00F708B4: SPD %d%%
00F708C4: ACC %d%%
00F708D4: EVA %d%%
00F708E4: F. Spirit Lv%d
00F708F4: Overheat %d
00F7090C: Boost %d%%
00F7091C: PStone FIR L%d
00F70934: PStone CLD L%d
00F7094C: PStone WND L%d
00F70964: PStone NTR L%d
00F7097C: PStone MYS L%d
00F70994: PStone SPI L%d
00F709AC: PStone DRK L%d
00F709C4: PStone PHY L%d
00F709DC: PStone VOI L%d
00F709F4: ALL +%d%% (EX)
00F70A0C: ATK +%d%% (EX)
00F70A24: DEF +%d%% (EX)
00F70A3C: MAG +%d%% (EX)
00F70A54: MND +%d%% (EX)
00F70A6C: SPD +%d%% (EX)
00F70A84: Moral Inversion
00F70A90: T. Keystone %d
00F70AA0: M. Circuit %d%%
00F70AB0: B. Wrath %d
00F70AC8: %d Sake Cup(s)
00F70AD8: T. Admin (G)
00F70AEC: T. Admin (E)
00F70B00: A.H. Attack Lv%d
00F70B14: S. Genetics Lv%d
00F70B2C: Invincible
00F70B34: Element: FIR
00F70B48: Element: CLD
00F70B5C: Element: WND
00F70B70: Element: NTR
00F70B84: Element: MYS
00F70B98: Element: DRK
00F70BAC: Element: SPI
00F70BC0: Element: PHY
00F70BD4: Element: VOI
00F70BE8: Reading
00F70BF0: Mask: Joy
00F70C00: Mask: Anger
00F70C10: Mask: Pathos
00F70C20: Mask: Cheer
00F70C30: Vanguard Tiger
00F70C3C: Rear Dragon
00F70C48: Invincibility %d
00F70C58: Immunity %d
00F70C68: Destruct. Bow
00F70C74: Destruct. Fruit
00F70C84: Destruct. Flame
00F70C90: Level +%d
00F70CA0: F.S. Blink Lv%d
00F70CB4: H. Charge Lv%d
00F70CC8: H. Accel Lv%d
00F70CDC: F. Proof Lv%d
00F70CF0: J.t. Ripper Lv%d
00F70D08: Onbashira Lv%d
00F70D14: Hibernation %d
00F70D3C: Assault Point
00F70D4C: M.T. Wisdom %d
00F70D60: E. Dream Lv%d
00F70D70: FIR/MYS+ %d%%
00F70D88: CLD/SPI+ %d%%
00F70DA0: WND/DRK+ %d%%
00F70DB8: NTR/PHY+ %d%%
00F70DD0: E. Memory FIR
00F70DE0: E. Memory CLD
00F70DF0: E. Memory WND
00F70E00: E. Memory NTR
00F70E10: E. Memory MYS
00F70E20: E. Memory SPI
00F70E30: E. Memory DRK
00F70E40: E. Memory PHY
00F70E50: H.A. Sword %d
00F70E64: H.A. Mirror %d
00F70E78: Sword Spirit
00F70E88: China Qigong
00F70E9C: %d Request(s)
00F70EAC: MAlice C. (M)
00F70EC0: MAlice C. (A)
00F70ED4: Far Distances
00F70EDC: Two-Way Curse
00F70EF0: Jealousy Lv%d
00F70F14: K. Queen Lv%d
00F70F28: Team (9) Lv%d
00F70F40: Adversity
00F70F48: Dexterity
00F70F50: Desperation
00F70F60: Bhava-agra Girl
00F70F70: Y. Clan Lv%d
00F70F80: Flexibility
00F70F88: With Mokou
00F70F98: Eientei Lv%d
00F70FB0: With Keine
00F70FC0: SDM Lv%d
00F70FD8: Curse Reversal
00F70FE0: E.S. Palace Lv%d
00F70FF8: L. Fortress Lv%d
00F71008: S. Silence Lv%d
00F71020: Gatekeeper Nap
00F7103C: Moriya Lv%d
00F7105C: Hisouten Guard
00F7107C: Sky Creation
00F71098: Earth Creation
00F710B4: Nimble
00F710BC: Fog Labyrinth
00F710C8: Sealing Club
00F710D8: Star Reading
00F710F0: Moon Reading
00F71108: Abuse Victim
00F71118: F.Start(A) Lv%d
00F71138: F.Start(D) Lv%d
00F71158: Mask Creator
00F71170: M. Alliance Lv%d
00F71188: Mist/Serv Lv%d
00F711A4: Enma/S-gami Lv%d
00F711B8: Lunar Power
00F711D8: I. Shooting Lv%d
00F711F4: Tunnel Vision
00F71208: Mt.Ooe Deva %d
00F71220: Ash Rekindling
00F7122C: Phenomena Lv%d
00F7123C: P. Sprinkle Lv%d
00F71254: Share Vision-
00F71268: Share Vision
00F71278: Share Vision+
00F7128C: Prince Lesson-
00F712A0: Prince Lesson
00F712B0: A. Blood %d
00F712C0: Firefly Swarm
00F712D0: Going it Alone
00F712E8: Healthy Spirit
00F712F8: Wood Blessing
00F7130C: Fire Blessing
00F71320: Earth Blessing
00F71334: Metal Blessing
00F71348: Water Blessing
00F7135C: Holy Glory
00F71370: A. Blood %d
00F71388: PSN
00F71390: PSN Removed
00F7139C: PAR
00F713A4: PAR Removed
00F713B0: HVY
00F713B8: HVY Removed
00F713C4: SHK
00F713CC: SHK Removed
00F713D8: TRR
00F713E0: TRR Removed
00F713EC: SIL
00F713F4: SIL Removed
00F71400: DTH
00F71408: DTH Removed
00F71414: ATK UP
00F71420: ATK DOWN
00F7142C: DEF UP
00F71438: DEF DOWN
00F71444: MAG UP
00F71450: MAG DOWN
00F7145C: MND UP
00F71468: MND DOWN
00F71474: SPD UP
00F71480: SPD DOWN
00F7148C: ACC UP
00F71498: ACC DOWN
00F714A4: EVA UP
00F714B0: EVA DOWN
00F716B4: Reimu Hakurei
00F716C0: Reimu
00F71734: Marisa Kirisame
00F71744: Marisa
00F717BC: Rinnosuke Morichika
00F717CC: Rinnosuke
00F71844: Keine Kamishirasawa
00F71854: Keine
00F718C8: Momiji Inubashiri
00F718D4: Momiji
00F71948: Youmu Konpaku
00F71954: Youmu
00F719C8: Kogasa Tatara
00F719D8: Kogasa
00F71A50: Rumia
00F71AC8: Cirno
00F71B3C: Minoriko Aki
00F71B48: Minoriko
00F71BC4: Komachi Onozuka
00F71BD4: Komachi
00F71C50: Chen
00F71CC0: Nitori Kawashiro
00F71CD0: Nitori
00F71D48: Parsee Mizuhashi
00F71D58: Parsee
00F71DD4: Wriggle Nightbug
00F71DEC: Wriggle
00F71E68: Kaguya Houraisan
00F71E78: Kaguya
00F71EF0: Fujiwara no Mokou
00F71EFC: Mokou
00F71F70: Aya Shameimaru
00F71F7C: Aya
00F71FEC: Mystia Lorelei
00F72008: Mystia
00F72088: Kasen Ibaraki
00F72094: Kasen
00F72108: Nazrin
00F72188: Hina Kagiyama
00F72194: Hina
00F72204: Rin Kaenbyou
00F72210: Rin
00F72280: Utsuho Reiuji
00F7228C: Utsuho
00F72300: Satori Komeiji
00F72310: Satori
00F72388: Yuugi Hoshiguma
00F72394: Yuugi
00F72408: Hong Meiling
00F72414: Meiling
00F7248C: Alice Margatroid
00F724A8: Alice
00F7251C: Patchouli Knowledge
00F72538: Patchouli
00F725C0: Eirin Yagokoro
00F725CC: Eirin
00F72640: Reisen U Inaba
00F7265C: Reisen
00F726D4: Sanae Kochiya
00F726E4: Sanae
00F72758: Iku Nagae
00F72764: Iku
00F727D8: Suika Ibuki
00F727E4: Suika
00F72858: Ran Yakumo
00F72864: Ran
00F728D4: Remilia Scarlet
00F728F0: Remilia
00F72970: Sakuya Izayoi
00F72980: Sakuya
00F729F8: Kanako Yasaka
00F72A08: Kanako
00F72A80: Suwako Moriya
00F72A90: Suwako
00F72B08: Tenshi Hinanawi
00F72B18: Tenshi
00F72B8C: Flandre Scarlet
00F72BAC: Flandre
00F72C30: Yuyuko Saigyouji
00F72C40: Yuyuko
00F72CB8: Yuuka Kazami
00F72CC4: Yuuka
00F72D38: Yukari Yakumo
00F72D44: Yukari
00F72DB8: Byakuren Hijiri
00F72DC4: Byakuren
00F72E3C: Eiki Shiki, Yamaxanadu
00F72E58: Eiki
00F72ECC: Renko Usami
00F72EDC: Renko
00F72F50: Maribel Han
00F72F68: Mari
00F72FE4: Shou Toramaru
00F72FF0: Shou
00F73068: Mamizou Futatsuiwa
00F7307C: Mamizou
00F730FC: Mononobe no Futo
00F73108: Futo
00F7317C: Toyosatomimi no Miko
00F7318C: Miko
00F73200: Hata no Kokoro
00F7320C: Kokoro
00F73284: Tokiko
00F732FC: Koishi Komeiji
00F7330C: Koishi
00F73380: Hieda no Akyuu
00F7338C: Akyuu
00F97608: In Hard Mode, if you have a character with a Lv higher than the 
00F976AC: Temp %d.%d °C
00F976D0: %d Floor(s) Bonus: ATK & DEF +%d%%
00F976F4: %d Floor(s) Bonus: MAG & MND +%d%%
00F97710: %d Floor(s) Bonus: SPD +%d%%
00F97734: %d Floor(s) Bonus: Experience Points +%d%%
00F97758: %d Floor(s) Bonus: Money +%d%%
00F97780: %d Floor(s) Bonus: Enemy Item Drop Rate +%d%%
00F977A8: %d Floor(s) Bonus: Recover %d MP After Battle
00F977DC: %d Floor(s) Bonus: %d%% Chance of 0 TP Consumption After Battle
00FC60AC: Looks like some force can send you upwards here...
00FC610C: There's a large, bottomless-looking pit here...
00FC614C: Dive inside?
00FC6184: Bonus for retrieving all treasures on this floor!
00FC61B4: Received 1 "Infinity Gem!"
00FC6474: You can feel something warm flowing through you...
00FC64FC: The bonus seems to be a %d%% increase to ATK and DEF.
00FC6580: The defeated black shadow disintegrates.
00FC65B0: The warm light disappears without a trace.
00FC7454: Offer 7-Star Amulets
00FC8AAC: x
00FC8AB0: Use an Infinity Key
00FC8B80: Touching this light seems like it'd give a bonus for %d Floor(s).
00FC8BAC: Obtained "Infinity Key!"
00FC8BFC: The bonus seems to be a %d%% increase to SPD.
00FC8C2C: Touch the light?
00FC8C44: What luck! The seal is apparently unlockable without
00FC8C68: The bonus seems to be a %d%% increase in EXP gained.
00FC8CAC: The bonus seems to be a %d%% increase in money gained.
00FC8CEC: The bonus seems to be a %d%% increase in item drop rate.
00FC8D2C: The bonus seems to be a %d MP recovery after battle.
00FC8D6C: The bonus seems to be a %d%% chance losing no TP after battle.
00FC8DA8: Touching this light seems like it'd recover %d%% MP for all allies.
00FC8DE8: Touching this light seems like it'd recover %d%% TP for all allies.
00FC8E20: The bonus seems to be a %d%% reduction in the encounter rate.
00FC8E58: Touching this light seems like it'd bring back all the allies
00FC8E9C: who have returned to Gensokyo with %d%% TP regained.
00FC8EE0: Touching this light seems like it'd let you obtain an Infinity Jewel.
00FC8F1C: You can obtain 7-Star Remnants by offering %d 7-Star Amulets here,
00FC8FBC: The warm light envelops Reimu's party.
00FC8FEC: There's something creeping around here... 
00FC9014: 7-Star Remnants:
00FC9044: Something's at the feet of the disintegrated shadow...
00FC913C: Obtained %d 7-Star Remnant(s)!
00FC917C: There seems to be some item sealed here...
00FC9220: Exchange 7-Star Remnants for rare items
00FC9250: Exchanging %d 7-Star Remnants will give you %s.
00FC927C: Are you sure?
00FC9298: The bonus seems to be a %d%% increase to MAG and MND.
00FC92C4: Touching this light seems like it'd let you obtain an Explorer King's Legend.
00FC9334: Obtained %d 7-Star Amulet(s)!
00FC9350: Offering 20 7-Star Amulets may break the seal,
00FC9410: Offer %d 7-Star Amulets in exchange for 7-Star Remnants?
00FC9444: but you don't have enough 7-Star Amulets for the exchange...
00FC9470: Offer %d 7-Star Amulets to brighten the surroundings?
00FC94C4: but it seems you can brute-force the item out without
00FC9514: Brighten the surroundings?
00FC9548: The map area around you become brighter!
00FC95BC: Leave it alone
00FC95EC: Do you want to start the game in Hard Mode?
00FC9618: any offerings, though the item's rarity may be affected.
00FC9654: 7-Star Amulets: 
00FC9674: Infinity Keys: 
00FC96D0: Brute-force it open
00FC96F4: Offering 160 7-Star Amulets may break the seal,
00FC976C: any offerings required.
00FC97AC: Take item with joy
00FC97E4: There seems to be a good item sealed here...
00FC9824: Offering 40 7-Star Amulets may break the seal,
00FC9864: There seems to be a quality item sealed here...
00FC98A4: Offering 80 7-Star Amulets may break the seal,
00FC98D8: There seems to be a high-quality item sealed here...
00FC9938: Offering 320 7-Star Amulets may break the seal,
00FC9978: There's something on the floor... 
00FC9BE8: You can obtain 7-Star Remnants by offering 7-Star Amulets here.
00FC9D38: You have no 7-Star Amulets, but looks like it'll work anyway.
00FCA2D8: Last boss has been beaten in Hard Mode. Restrictions are removed.
00FCAB88: be raised above their Lv * 1.2, and your party's highest level
00FCADCC: Additionally, once you've defeated the last boss,
00FCB068: cannot be higher than that of a boss you are fighting.
00FCBED8: Since ver1.140, you can now start a game on Hard Mode difficulty.
00FCBF50: In Hard Mode, at the Magic Library, characters' parameters can't
00FCC050: the restrictions for the difficulties will be removed.
00FFEF2C: %d Floor(s) Bonus: Encounter Rate Increase -%d%%
010027DC: Restored the magical orbs on 19F to their original state.
01015C68: Challenge Lv, You won't be able to start the boss battle!
01017B04: The shadow disappears like an illusion... 
01018C94: Do you want to start a game with all characters?
010196E8: The temperature on this floor has been reinitialized.
0103C75C: There seems to be a top-tier item sealed here...
0103FE74: Touching this light seems like it'd let you obtain a Moneybag-Growing Tree.
010403F8: Touching this light seems like it'd let you obtain a Mr. Dawnbreak Oil Set.
010553C8: %d:%.2d:%.2d
010553DC: Average Level: %d
010553F8: Highest Level: %d
01055414: Hard Mode
01055424: View character parameters.
01055450: Change character equipment.
01055474: View equipments, materials and special items in your possession.
010554AC: Escape the dungeon and return to Gensokyo.
010554D8: Change the current party formation.
010554F8: Expend skill points on characters to learn skills.
01055534: Rest within the dungeon, expending TP to recover MP.
01055574: Change game settings.
01055614: Choose a character's status to view.
01055640: Character main status information.
01055678: Character skill information.
0105569C: Battle Points
010556AC: LvUP Bonuses Stock
010556CC: Skill Points Stock
010556E8: Training Manual Used
010556FC: Character miscellaneous information. 
0105571C: Choose a character to change equipment.
0105573C: Choose an equipment slot.
01055764: - Remove equipment -
0105577C: You cannot change equipment within the dungeon!
010557C0: Choose a category to view.
010557EC: Choose an item to view its detailed information.
01055814: Stop the dungeon exploration and return to Gensokyo.
0105584C: Are you sure?
01055860: You cannot escape the dungeon while in Gensokyo!
01055898: You cannot escape the dungeon while in the Preparation Room!
010558D4: Choose the first character to switch.
01055900: Choose the second character to switch.
0105592C: Choose the character who wants to learn a skill.
01055950: Choose a skill to learn.
01055974: Current Skill Pts: %d
01055990: You cannot learn skills while in the dungeon!
010559D8: Characters whose MP is not full will expend TP to recover MP.
01055A20: You cannot rest in Gensokyo!
01055A4C: Adjust the volume of the BGM.
01055A6C: Adjust the volume of the sound effects.
01055A90: Controls shading and brightness. Actions will process faster when turned OFF.
01055AF0: Changes the strength of the shadings, when the Visual Effects are ON.
01055B34: Changes the strength of the brightness, when the Visual Effects are ON.
01055B74: Adjust the speed at which the text appears in dialogues and events.
01055BB0: Set special effects in dungeons. Actions will process faster when turned OFF.
01055C20: Return to the title screen.
01055C40: BGM Volume
01055C54: SE Volume
01055C60: Visual Effects
01055C74: Shading
01055C90: Brightness
01055CAC: Text Speed
01055CC4: Dungeon Effects
01055CE0: Back to Title
01055CF4: Choose an option to change.
01055D1C: End the current game and return to the title screen?
01055D50: Unsaved game progress will be discarded.
01056250: -No Class-
0105667C: Guardian
01056684: Monk
0105668C: Warrior
01056694: Sorcerer
0105669C: Healer
010566A4: Enchanter
010566AC: Hexer
010566B4: Toxicologist
010566BC: Magician
010566C8: Herbalist
010566D4: Strategist
010566DC: Gambler
010566E4: Diva
010566EC: Transcendent
010566F8: Swordmaster
01056700: Archmage
0105670C: Appraiser
01056714: Elementalist
0105671C: Ninja
01056724: Oracle
01057278: -Unequip-
0105728C: No Subclass
010572A0: x%d
010572BC: Page
01067260: Loading from slot #%d.
01067288: Set special effects in dungeons. Actions will process faster when turned OFF.
010672E8: Text Speed
01068330: Change character levels, give     
01068370: level up bonuses and use special items.     
01068398: Increase character     
010683B4: parameters with money.     
010683E4:  Change party members.     
01068418: Trade equips or materials.     
01068444: View your current achievements      
01068460:     and a record of enemies fought.     
0106848C:   Enter the Great Tree.    
010684B0:  Save and load data.    
010684DC:                    Challenge the underground's cavernous hole.
01068504: In recognition of your achievement, the following is awarded:
01068544: %s
01068558: %s
01068568: %s
0106857C: %s
01068590: Party's money +%d
010685AC: In recognition of your Plus achievement, the following is awarded:
010685F0: Level Characters
01068604: Level All Characters
01068620: Level Up Bonuses
01068634: Use Special Items
01068648: Delevel Characters
01068664: Level Unification
01068680: Return
01068688: Use up experience to individually level up characters.
010686B8: Automatically level up all characters as much as possible.
01068710: Distribute level up bonuses to increase stats without modifying level.
0106875C: Use special items designed for use on characters.
01068790: Remove levels from a character. Experience used will be returned.
010687DC: Select the level for all current party members to be at.
01068828: Return to Gensokyo
01068838: Choose a character to level up.
01068860: Choose a character to level down.
01068888: Choose the level for that character to reach.
010688B8: Choose the level for that character to reach.
010688E4: Level Down successful!
01068908: Learned skills and assigned level up bonuses will remain, but
01068950: but level up bonus and skill point stocks will be decreased.
01068998: Your stocks may show up as negative numbers as a result.
010689E8: MAX HP UP
010689F8: - Finished -
01068A14: Distribute bonuses.
01068A58: There are no characters who can level up right now...
01068A98: Choose the character whose bonuses you wish to distribute.
01068AD0: Choose the special item you want to use.
01068AF4: Choose the character to use this item on.
01068B1C: This item has reached its usage limit!
01068B44: This character can no longer use %s!
01068BBC: This character has already learned this skill!
01068BFC: This character cannot use %s at present!
01068C30: This character cannot use %s at present!
01068C68: Specified Level
01068C78: Select the level for the party to conform to.
01068CA8: Unified the level of all party members!
01068CE0: Watch out, a character will not reach level %d
01068D30: if they don't have enough EXP points.
01068D44: Character Status Up
01068D60: Skill Reset
01068D74: Use money to increase a character's parameters.
01068DAC: Reset the skills acquired by a character.
01068DD8: Learn Subclass
01068DEC: Use a Stone of Awakening to allow a character to take on a subclass.
01068E24: Choose the character whose parameters you wish to enhance.
01068E50: HP Up
01068E5C: ATK Up
01068E68: DEF Up
01068E74: MAG Up
01068E80: MND Up
01068E8C: SPD Up
01068E98: FIR Up
01068EA8: CLD Up
01068EB8: WND Up
01068EC8: NTR Up
01068ED8: MYS Up
01068EE8: SPI Up
01068EF8: DRK Up
01068F08: PHY Up
01068F18: Return
01068F2C: Choose the character you wish to reset.
01068F54: -No Subclass-
01068F68: Confirm the change in parameters from resetting the character.
01068FA8: Performing a character reset for %s.
01068FD0: Final confirmation for character reset.
01069000: You currently possess %d Tomes of Reincarnation.
0106902C: Do you wish to use a Tome of Reincarnation on %s?
01069068: Choose a character to take on a subclass.
01069094: This subclass excels at taking damage and shielding the party. It possesses
010690D4: skills that provoke foes into attacking this character,
0106910C: as well as reducing the amount of damage taken.
01069148: This subclass excels at making use of one's body. It possesses
0106917C: skills that enhance regeneration and speed.
010691C0: This subclass excels at physically attacking foes. It possesses
010691F8: skills that increase ATK and physical damage dealt.
01069254: This subclass excels at magically attacking foes. It possesses
01069288: skills that increase MAG and magical damage dealt.
010692E4: This subclass excels at revitalizing the party. It possesses
01069310: skills that heal allies and increase healing skill effects.
01069360: This subclass excels at strengthening the party. It possesses 
01069390: skills that buff allies and increase buffing skill effects.
010693E8: This subclass excels at debuffs. It possesses
01069420: skills that debuff foes and reduce debuff effects on allies.
01069488: This subclass excels at status ailments. It possesses
010694C0: skills that inflict ailments on foes and reduce ailment effects on allies.
01069528: This subclass excels at MP management and recovery. It possesses
01069558: skills that regenerates MP and transfers MP to allies.
0106959C: This subclass excels at miscellaneous enhancements. It possesses
010695C8: skills that reduce ailment effects on allies, increase strength of buffs 
0106962C: on allies, and transform debuffs on allies into buffs.
01069674: This subclass excels at passively boosting the party. It possesses
010696B8: skills that cause buffs to wear off slower for allies,
01069700: reduce damage taken, and increase damage dealt.
0106975C: This subclass excels at taking risks to increase power. It possesses
0106979C: skills that increase MP cost and power of spell cards,
010697DC: and increase damage dealt as well as damage taken.
01069828: This subclass excels at aiding the party outside of battle. It possesses
01069864: skills that reduce encounter rate in dungeons,
010698A0: and increase experience obtained from battles.
010698D4: This subclass excels at increasing one's own abilities. It possesses
01069918: skills that increase all parameters, as well as increasing damage dealt
01069944: and reducing damage taken for one's self.
01069990: This subclass excels at attack-focused skills. It does
010699D0: not possess any support skills, but its varied elemental attacks
010699FC: allows for adaptability to different situations.
01069A3C: This subclass excels at magic-focused skills. It does
01069A80: This subclass excels at manipulating EXP, money and drop rates. It is
01069AD0: not suited for combat, but will support the party behind the curtains.
01069B14: This subclass excels at support for specific elements. It can increase
01069B50: an element's damage, or reduce damage taken for an element. 
01069BA8: This subclass excels against status-afflicted enemies. It possesses
01069BF0: skills that increse damage against afflicted enemies, or reduce damage
01069C48: taken by afflicted enemies.
01069C60: This subclass excels at fighting enemies with buffs. It possesses
01069CA0: skills that absorb enemy buffs, or halves their effects.
01069CF4: A powerful subclass attainable by the Ame-no-Murakumo's blessing.
01069D38: Only one character can learn this subclass at a time.
01069D80: A subclass that grants a small taste of the power of the Dragon God.
01069DD8: A subclass that grants the power of a fated, undying challenger.
01069E34: Choose a subclass to take on.
01069E54: Skills that can be learned from %s
01069E70: It possesses skills that provoke the enemies, and reduce damage taken.
01069EE0: allows for adaptability to different situations.
01069F24: In order for the character to take on a subclass,
01069F50: at least one Stone of Awakening is required!
01069F74: Final confirmation of subclass.
01069FF4: Use 1 Stone to let %s learn subclass "%s."
0106A03C: Change Members
0106A04C: Unequip
0106A064: Favorites
0106A078: Register Favorite
0106A088: Change party members.
0106A0A8: Remove equipments from characters.
0106A0C8: Load a favorite party formation.
0106A0F4: Register current party formation to favorites.
0106A120: Unequip All Characters
0106A138: Unequip Non-Party Characters
0106A160: Equipment on all characters will be removed.
0106A194: Equipment on characters not in current party will be removed.
0106A1D8: Confirming equipment removal.
0106A1F4: Choose a character to remove from the party.
0106A224: Choose a character to place in the party.
0106A258: Current Party
0106A270: Slot #%d Party
0106A294: Choose the party formation to load.
0106A2C0: The current party will be replaced with party formation #%d.
0106A314: Confirming formation load.
0106A330: Choose a slot to register current party formation to.
0106A368: Slot #%d will be replaced with the current party formation.
0106A3BC: The current party formation will be registered to slot #%d.
0106A3EC: Confirming party registration.
0106A404: Choose member to join party
0106A430: Use up one "Akyuu-sealed persuasion manual" to enlist this character in party.
0106A488: Buy Equipment
0106A498: Sell Items
0106A4A8: Create Equipment
0106A4B8: Enhance Equipment
0106A4C8: Purchase equipment.
0106A4DC: Sell currently owned items.
0106A4F0: Use materials to create new equipment.
0106A52C: Use materials to refine equipment.
0106A560: Main Equipment
0106A570: Sub Equipment
0106A57C: Choose category of equipment to buy.
0106A5A4: Materials
0106A5B4: Choose type of item to sell.
0106A5DC: Choose category of equipment to create.
0106A604: Create equipment.
0106A614: Refine equipment.
0106A624: Choose equipment to buy.
0106A644: Choose item to sell.
0106A66C: Expend materials to create equipment.
0106A69C: The materials will be used to create "%s."
0106A6D4: The materials will be used to create "%s."
0106A708: Bestiary
0106A718: Achievements
0106A728: Plus Achievements
0106A73C: View data on enemies encountered so far.
0106A768: Browse the achievements earned so far.
0106A794: Browse the Plus Disk achievements earned so far.
0106A7FC: View achievement details.
0106A828: Floor 1
0106A838: Floor 2
0106A848: Floor 2 Center
0106A860: Floor 3
0106A870: Floor 3 Center
0106A888: Floor 4
0106A898: Floor 4 Northeast
0106A8B0: Floor 5
0106A8C0: Floor 5 South
0106A8D4: Floor 6
0106A8E4: Floor 6 Center
0106A8FC: Floor 7
0106A90C: Floor 7 Southeast
0106A924: Floor 8
0106A934: Floor 8 South
0106A948: Floor 9
0106A958: Floor 10
0106A968: Floor 10 Southwest
0106A980: Floor 11
0106A990: Floor 12
0106A9A0: Floor 12 West
0106A9B8: Floor 12 East
0106A9D0: Floor 12 Depths
0106A9E8: Floor 13
0106A9F8: Floor 14 Center East
0106AA14: Floor 14 East
0106AA2C: Floor 14 Center West
0106AA48: Floor 15
0106AA58: Floor 15 Center
0106AA70: Floor 15 Northeast
0106AA88: Floor 16
0106AA98: Floor 16 Northwest
0106AAB0: Floor 16 Center
0106AAC8: Floor 17
0106AAD8: Floor 18
0106AAE8: Floor 18 North
0106AB00: Floor 19
0106AB10: Floor 19 South
0106AB28: Floor 20 Center West
0106AB44: Floor 20 Center East
0106AB60: Floor 20 Depths
0106AB78: Floor 11 Extra
0106AB94: Floor 11 Extra Center
0106ABB8: Floor 10 Extra
0106ABD0: Floor 10 Extra Center
0106ABF0: Floor 9 Extra
0106AC08: Floor 9 Extra Depths
0106AC28: Floor 8 Extra
0106AC40: Floor 13 Extra
0106AC5C: Floor 14 Extra
0106AC78: Floor 15 Extra
0106AC94: Floor 15 Extra Depths
0106ACB8: Floor 16 Extra
0106ACD4: Floor 21
0106ACE8: Floor 22
0106ACFC: Floor 23
0106AD10: Floor 25
0106AD24: Floor 26
0106AD38: Floor 27
0106AD4C: Floor 28
0106AD60: Floor 29
0106AD74: Floor 29 Depths
0106AD8C: Floor 30
0106AD9C: Underground Floor 1
0106ADB0: Underground Floor 2 Switches
0106ADD4: Underground Floor 2 Center
0106ADF0: Underground Floor 2 Southeast
0106AE0C: Underground Floor 3
0106AE20: Underground Floor 4
0106AE34: Underground Floor 5
0106AE48: Underground Floor 6
0106AE5C: Underground Floor 6 Depths
0106AE78: Underground Floor 7
0106AE8C: Underground Floor 8
0106AEA0: Underground Floor 8 Southeast
0106AEBC: Underground Floor 9
0106AED0: Underground Floor 9 2nd Half
0106AEEC: Underground Floor 10
0106AF00: Underground Floor 10 Depths
0106AF1C: Underground Floor 11
0106AF34: Set out on an adventure.
0106AF48: Save Data
0106AF58: Load Data
0106AF68: Trade Infinity Gems
0106AF7C: Trade Awakenings
0106AF90: Save your current game.
0106AFB0: Load other save data.
0106AFD0: Exchange your held Infinity Gems for rare items.
0106B008: Exchange your held Jewels of Greater Awakenings for Awakening items.
0106B040: Saving your data to slot #%d.
0106B060: Save completed!
0106B07C: Infinity Gems Held:
0106B094: %d Infinity Gems will be traded for %s.
0106B0CC: Jewels Held:
0106B0E4: 1 Jewel of Greater Awakening will be traded for %s.
0106B118: Floor %d
0106B138: Proceed through the great hole in the Great Tree's underground fourth floor.
0106B164: Withdraw and return to Gensokyo.
