00F96E48: 1F　The Root of Gensokyo's Great Tree
00F96E74: 2F　The Green Hall that Knows Those Who Hold Power
00F96E9C: 3F　The Emerald Forest that Swallowed Foolish Pilgrims
00F96ECC: 4F　The Silent Coral Reefs that Shines Sapphire
00F96EF4: 5F　The Cerulean Ocean that Drowns Everything
00F96F20: 6F　The Azure Forest that Sank to the Bottom of Silence
00F96F4C: 7F　The Deep Green Forest that heard the Pulse of Nature
00F96F78: 8F　The Evergreen Tropics Filled with Illuding Shadows
00F96FA0: 9F　The Jade Jungles that Brim with Life
00F96FD8: 10F　The Maize Grasslands where All Lifeforms Struggle to Survive
00F9700C: 11F　The Sunglow Deserts where Thirsty Travellers Sigh to the Sky
00F97038: 12F　The Golden Plains where One Meets the Sky
00F97064: 13F　The Scarlet Flaming Trees that Spread Throughout the Land
00F97094: 14F　The Ruby Blazing Woods that Oppose Everything
00F970C0: 15F　The Amber Battlefield where the Lights of Lives Collide
00F970F4: 16F　The Aging Dark Forest Decayed in a Cage of Thoughts
00F97120: 17F　The Violet Hidden Road where Sinners Wander for Eternity
00F97144: 18F　The Amethyst Tatami where the Judges Cleanse Away Blood
00F97180: 20F　The Great Tree that Pierces the Heavens
00F971A0: 21F　The Locked Realm where One Breaks the Chains of God
00F971C8: 22F　The Altar of the Sage with the Three Divine Treasures
00F971F0: 23F　The Golden Skyway Tormented by the Four Gods
00F97210: 24F　The Moonstoned Road to Heaven with a Spectacular View
00F97238: 25F　The Chin of the World-Devouring Dragon
00F97254: 26F　Labyrinth of Touhou - Signalling the Great Tree's Finale
00F97294: 27F　The Great Tree Shines its Last
00F972C0: 28F　The Fateful Battlefield Against the Great Existence
00F972E8: 29F　The Scarlet Gold Warground of Champions
00F97314: 30F　The Gate Where Saints Oversee Death After Death
00F97348: B1F　The Malicious Womb as it Watches You on Your New Journey
00F97378: B2F　The Black Hole where Little Girls Born from Despair Wander
00F973A0: B3F　The Obisidian Passage Stared at by Eyes of Disappointment
00F973C0: B4F　The Hall of Spirits Crawling Out from the Depths of Hell
00F973EC: B5F　The Blue Cave Where Time Stands Still
00F97410: B6F　The Maze of Trees with Frozen Souls
00F97430: B7F　The Azure Prison Where No Light Can Shine
00F97458: B8F　The Corridor of Sentient Flesh Walls
00F97480: B9F　The Tainted Altar which Abhors Pilgrims
00F974A8: B10F　The Sanctuary of Fresh Blood and Whispers of the Dead
00F974CC: B11F　The Domains Where Disgusting Distortions Delve in Depravity
00F974F4: Infinity Corridor　Floor %d
00F97514: Infinity Corridor　Floor %d Preparation Room
00F97534: %s's TP became 0!
00F97558: %s has left the current exploration party!
00F97578: There is no one in the front anymore!
00F9759C: Returning to Gensokyo now!
00FE01C4: 19F　The Sky's Cradle where Saints Sleep
