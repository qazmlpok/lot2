00F74E64: Magic Transfer
00F74EB4: Oracle's Talisman
00F779A4: Wandering Sin
00F77BAC: Super-Ego
00F77D24: Penetrator
00F77DC0: Hex -Crimson Curse-
00F77E20: Herb of Awakening
00F7805C: Elekiter Dragon Palace
00F781A4: Rhythmic Dance
00F7831C: Start of Heavenly Demise
00F78CD4: Youkai Yakuza Kick
00F797A0: Curse of Vlad Tepes
00F79944: Soul Sculpture
00F79B44: Froggy Braves the Elements
00F79B78: Violent Motherland
00F7ABB8: Radiant Treasure Gun
00F7AC9C: Tradition of Just Rewards
00F7AF08: Four Humors Possession
00F7B010: Musketeer d'Artagnan
00F7B034: Non-Neumann Systems
00F7BF54: Sword of Light
00F7C0E4: Enchant: Lazurite
00FC7CF0: Magical Tempest
00FC8098: Wand of *Destruction*
01059ED4: Yin-Yang Orb
01059FA4: Fantasy Seal
0105A104: Exorcising Border
0105A1A4: Great Hakurei Barrier
0105A234: Magic Missile
0105A248: Asteroid Belt
0105A27C: Master Spark
0105A324: Concentration
0105A38C: First Aid
0105A3CC: Battle Command
0105A3F4: Ancient History -Old History-
0105A4AC: New History -Next History-
0105A54C: Three Treasures - Sword
0105A5C0: Three Treasures - Mirror
0105A61C: Rabies Bite
0105A6AC: Expellee's Canaan
0105A714: Present Life Slash
0105A7C8: Slash of Eternity
0105A80C: God's Slash of Karma Wind
0105A880: Slash Clearing the 6 Senses
0105A954: Karakasa Surprising Flash
0105A9EC: A Rainy Night's Ghost Story
0105AAC0: Drizzling Large Raindrops
0105AB34: Moonlight Ray
0105ABDC: Dark Side of the Moon
0105AC18: Demarcation
0105AD24: Icicle Fall
0105AE18: Diamond Blizzard
0105AE50: Perfect Freeze
0105AEA0: White Album
0105AED0: Autumn Sky
0105AF48: Warm Color Harvest
0105AF84: Sweet Potato Room
0105AFBC: Owotoshi Harvester
0105B00C: Short Life Expectancy
0105B09C: Ferriage in the Deep Fog
0105B110: Narrow Confines of Avici
0105B16C: Scythe that Chooses the Dead
0105B1B0: Flight of Idaten
0105B1F4: Phoenix Spread Wings
0105B21C: Kimontonkou
0105B244: Kappa's Illusion Waterfall
0105B274: Exteeeending Aaaaarm
0105B2EC: Super Scope 3D
0105B380: Portable Versatile Machine
0105B3B0: Large Box and Small Box
0105B430: Midnight Anathema Ritual
0105B45C: Grudge Returning
0105B4B4: Jealousy of Kind and Lovely
0105B504: Comet on Earth
0105B548: Firefly Phenomenon
0105B584: Nightbug Tornado
0105B5D4: Dragon's Neck Jewel
0105B6A0: Buddha's Stone Bowl
0105B6F4: Swallow's Cowrie Shell
0105B758: Bullet Branch of Hourai
0105B7AC: Fire Bird -Flying Phoenix-
0105B80C: Tsuki no Iwakasa's Curse
0105B858: Fujiyama Volcano
0105B8CC: Wind God's Fan
0105B924: Peerless Wind God
0105B964: Sarutahiko's Guidance
0105B990: Divine Grandson's Advent
0105B9F4: Ill-Starred Dive
0105BA28: Poisonous Moth's Dark Dance
0105BA70: Midnight Chorus Master
0105BADC: Mysterious Song
0105BB88: Higekiri's Cursed Arm
0105BC14: Echo of the Nine Forest Gods
0105BCBC: Diving Waltz of the Raijuu
0105BD18: Breath of the Hermit
0105BD44: Gold Rush
0105BDA4: Rare Metal Detector
0105BE0C: Nazrin Pendulum
0105BE74: Misfortune God's Biorhythm
0105BEAC: Pain Flow
0105BED8: Old Lady Ohgane's Fire
0105BF1C: Cat's Walk
0105BF64: Vengeful Cannibal Spirit
0105BFA4: Former Hell's Needle Hill
0105C000: Blazing Wheel
0105C05C: Giga Flare
0105C088: Intense Nuclear Reaction
0105C0B8: Hell's Tokamak
0105C118: Brain Fingerprint
0105C154: Supernatural Phenomenon
0105C194: Knockout In Three Steps
0105C1EC: Irremovable Shackles
0105C234: Brilliant Light Gem
0105C23C: Mountain Breaker
0105C260: Colorful Rain
0105C284: Healer
0105C28C: Artful Sacrifice
0105C2E0: Little Legion
0105C310: Hanged Hourai Dolls
0105C388: Trip Wire
0105C3E8: Royal Flare
0105C448: Princess Undine
0105C4B0: Djinn Gust
0105C4F8: Satellite Himawari
0105C55C: Silent Selene
0105C5A4: Mercury Sea
0105C5FC: Omoikane's Device
0105C67C: Hourai Elixir
0105C6A4: Astronomical Entombing
0105C6D0: Lunatic Red Eyes
0105C76C: Mind Starmine
0105C7D4: Discarder
0105C834: Gas-Woven Orb
0105C878: Grand Patriot's Elixir
0105C8EC: Moses's Miracle
0105C920: Yasaka's Divine Wind
0105C930: Miracle Fruit
0105C970: Light Dragon's Sigh
0105C9B0: Thundercloud Stickleback
0105CA08: Whiskers of the Dragon God
0105CA48: Throwing Mt. Togakushi
0105CAEC: Throwing Atlas
0105CB30: Gathering and Dissipating
0105CB70: Missing Power
0105CBB8: Art of Segaki Binding
0105CC00: Fox-Tanuki Youkai Laser
0105CC60: Princess Tenko
0105CCC0: Soaring En no Ozuno
0105CCF4: Banquet of General Gods
0105CD20: Eighty Million Holy Boards
0105CD58: Spear the Gungnir
0105CDF0: Misdirection
0105CE28: Killing Doll
0105CE88: Lunar Clock
0105CFD8: Private Square
0105D00C: Mad Dance on Medoteko
0105D040: Misayama Hunting Ritual
0105D0C8: Beautiful Spring like Suiga
0105D0FC: Virtue of Wind God
0105D144: Moreya's Iron Ring
0105D188: Long-Arm and Long-Leg
0105D21C: Mishaguji-sama
0105D27C: World Creation Press
0105D344: Sword of Hisou
0105D38C: State of Enlightenment
0105D3C0: Starbow Break
0105D438: Forbidden Fruit
0105D4A0: Laveatein
0105D4E8: Ghostly Dream's Butterfly
0105D544: Deadly Swallowtail Lance
0105D588: Ghastly Dream
0105D5BC: Saigyouji Flawless Nirvana
0105D61C: Flower Shot
0105D650: Gensokyo's Reflowering
0105D690: Beauty of Nature
0105D6C4: Mesh of Light and Darkness
0105D738: Hyperactive Flying Object
0105D7B4: Yukari's Spiriting Away
0105D7E4: IN Quadruple Barrier
0105D814: Skanda's Legs
0105D868: Master of the Great Trichiliocosm
0105D8B0: Magic Milky Way
0105D8F4: Star Sword Apologetics
0105D940: Sutra - Duplicating Chant
0105D974: Last Judgement
0105D9C4: Trial of the Ten Kings
0105DA30: Galaxy Stop
0105DA70: Charge
0105DACC: Liberated Abilities
0105DB28: Chaotic Quadruple Barrier
0105DB5C: Overflowing Unnatural Power
0105DB70: Mari's DIY Novice Border
0105DB8C: Hungry Tiger
0105DC08: Dazzling Gold
0105DC58: Aura of Justice
0105DC9C: Frolicking Animals Scrolls
0105DD0C: Futatsuiwa Clan's Curse
0105DD58: 10 Transformations Danmaku
0105DDD4: Full Moon Pompokolin
0105DE20: Mononobe's Eighty Sake Cups
0105DE98: Catastrophic Gate Opening
0105DECC: Miwa Plate Storm
0105DF40: Halo of the Guse Kannon
0105DF74: Wishful Soul of Desire
0105DF88: Taiyi True Fire
0105DFE0: Invigorated Kagura Lion
0105E02C: Worrisome Man of Qi
0105E08C: The Count of Monte Cristo
0105E100: The Embers of Love
0105E19C: Bedside Ancestors
0105E1E4: Selfless Love
0105E1FC: Wise Protective Art
0105E218: Wise Defensive Art
0105E224: Precise Diagnosis
0105E244: Summon Tiger and Dragon
0105E254: Bad Lady Scramble
0105E27C: Miare's Extensive Knowledge
0105E29C: Shield Bash
0105E2E8: Shield Defense
0105E31C: Puncturing Thrust
0105E354: Iron Mountain Charge
0105E35C: Severing Flash
0105E388: Explosive Flame Sword
0105E3C4: Aspiration Surge
0105E408: Prayer of Recovery
0105E420: Prayer of Good Health
0105E44C: Art of the Battlemage
0105E478: An Ounce of Prevention
0105E4DC: Hex -Dark Curse-
0105E510: Hex -White Curse-
0105E540: Poisonous Incense
0105E570: Paralyzing Incense
0105E590: Numbing Incense
0105E598: Deathly Incense
0105E5DC: Magic Circuit
0105E604: Placebo Effect
0105E610: Incense Treatment
0105E654: Dance of the Cochlea
0105E664: Moon Shadow Flash
0105E6FC: Samidare Slash
0105E7B8: Swordmaster's Stance
0105E7D0: Southern Cross
0105E884: Execution
0105E900: Archmage's Chant
0105E918: Enchant: Spinel
0105E948: Enchant: Pyrope
0105E964: Enchant: Fluorite
0105E984: Assassination Sword
0105E9CC: Ame-no-Murakumo Slash
0105E9F4: World-Shaking Rule
0105EA30: Dragon God's Sigh
0105EBC8: Vorpal Blade
0105EC5C: HP-Restoring Medicine
